﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Login.Request;
using Taaleem.Models.Login.Response;

namespace Taaleem.DataServices
{
    public class LoginDataService : ILoginDataService
    {
        private readonly IAppSettings _appSettings;
        private readonly INetworkHelper _networkHelper;
        private readonly Func<LoginDataService> _requestFactory;
        private CookieContainer _sharedCookieContainer;
        private const string BaseUrl = "http://rfe-awd1-tst.edu.gov.qa:7070/api/User";
                                       
        public String UserName
        {
            get
            {
                // return _appSettings.EmployeeCredential.UserName;
                return "SRV_Taaleem_TService";
            }
        }

        public String Password
        {
            get
            {
                // return _appSettings.EmployeeCredential.Password;
                return "Ta3leem@PR321";
            }
        }

        public string RequestUrl { get; set; }
        public bool DisposeHandler = true;
        public HttpMessageHandler HttpMessageHandler { get; set; }
        public bool NoCahce { get; set; }
        public async Task<RequestResponse<TR>> PostAsync<TR>(StringContent content, CancellationToken? token = null)
        {

            var response = new RequestResponse<TR>();
            HttpClient httpClient = null;
            try
            {
                if (!_networkHelper.HasInternetAccess())
                {
                    response.ResponseStatus = ResponseStatus.NoInternet;
                    return response;
                }

                httpClient = HttpMessageHandler != null ?
                    new HttpClient(HttpMessageHandler, DisposeHandler) :
                    new HttpClient(new HttpClientHandler(), DisposeHandler);
                if (NoCahce)
                    httpClient.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue() { NoCache = true };

               
                var authData = string.Format("{0}:{1}", "SRV_Taaleem_TService", "Ta3leem@PR321");
                var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
                HttpResponseMessage postResponse;
                if (token == null)
                    postResponse = await httpClient.PostAsync(RequestUrl, content);
                else
                    postResponse = await httpClient.PostAsync(RequestUrl, content, token.Value);
               
                if (token != null && token.Value.IsCancellationRequested)
                {
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                    return response;
                }

                var stringValue = await postResponse.Content.ReadAsStringAsync();
                //response.StatusCode = postResponse.StatusCode;
                ParseResult(stringValue, response);//parse Result even when there is error
                if (!postResponse.IsSuccessStatusCode)
                {
                    response.ResponseStatus = ResponseStatus.HttpError;//Override response status value to be HttpError
                }
                return response;
            }
            catch (OperationCanceledException)
            {
                if (token != null && token.Value.IsCancellationRequested)
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                else
                    response.ResponseStatus = ResponseStatus.TimeOut;
            }
            catch (WebException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (HttpRequestException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (Exception e)
            {
                response.ResponseStatus = ResponseStatus.ClientSideError;
            }
            finally
            {
                if (httpClient != null) httpClient.Dispose();

            }
            return response;
        }
        private ContentType _resultContentType = ContentType.Json;
        public ContentType ResultContentType
        {
            get { return _resultContentType; }
            set { _resultContentType = value; }
        }
        private void ParseResult<TR>(string stringValue, RequestResponse<TR> response)
        {
            if (String.IsNullOrWhiteSpace(stringValue))
            {
                response.ResponseStatus = ResponseStatus.SuccessWithNoData;
                return;
            }
            if (ResultContentType == ContentType.Json)
            {
                try
                {
                    var result = JsonConvert.DeserializeObject<TR>(stringValue);
                    response.Result = result;
                    response.ResponseStatus = ResponseStatus.SuccessWithResult;
                }
                catch (Exception e)
                {
                    response.ResponseStatus = ResponseStatus.ParserException;
                    return;
                }
            }
            else if (ResultContentType == ContentType.Text)
            {
                try
                {
                    response.Result = (TR)Convert.ChangeType(stringValue, typeof(TR));
                }
                catch (Exception)
                {
                    response.ResponseStatus = ResponseStatus.ParserException;
                    return;
                }
            }

            if (response.Result.Equals(default(TR))) response.ResponseStatus = ResponseStatus.SuccessWithNoData;
            //Check if the result is of type Ilist, and the count ==0, if true assign ResponseStatus to no Data
            else if (response.Result is IList && (response.Result as IList).Count == 0) response.ResponseStatus = ResponseStatus.SuccessWithNoData;
        }

        public LoginDataService(IAppSettings appSettings, INetworkHelper networkHelper, Func<LoginDataService> requestFactory)
        {
            _appSettings = appSettings;
            _requestFactory = requestFactory;
            _networkHelper = networkHelper;
            NoCahce = true;
        }

        private CookieContainer GetSharedCookieContainer()
        {
            if (_sharedCookieContainer == null)
            {
                _sharedCookieContainer = new CookieContainer();
            }
            return _sharedCookieContainer;
        }

        private HttpClientHandler GetAuthHandler()
        {
            var handler = new HttpClientHandler { CookieContainer = GetSharedCookieContainer() };
            return handler;
        }
        public async Task<RequestResponse<ActivateUserResponse>> ActiveUser(ActivateUserParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/ActivateUser");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            //   request.ResultContentType = ContentType.Text;
            return await PostAsync<ActivateUserResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<CreateUserResponse>> CreateUser(CreateUserParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/CreateUser");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            //   request.ResultContentType = ContentType.Text;
            return await PostAsync<CreateUserResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<ForgetPasswordResponse>> ForgetPassword(ForgetPasswordParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/ForgetPassword");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            //   request.ResultContentType = ContentType.Text;
            return await PostAsync<ForgetPasswordResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<LoginResponse>> LoginAsync(LoginParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/CheckCredentials");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            //   request.ResultContentType = ContentType.Text;
            return await PostAsync<LoginResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<ResendActivateUserResponse>> ResendActivateUser(ResendActivateUserParameters parameters , CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/ResendActivationCode");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            //   request.ResultContentType = ContentType.Text;
            return await PostAsync<ResendActivateUserResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }
    }
}
