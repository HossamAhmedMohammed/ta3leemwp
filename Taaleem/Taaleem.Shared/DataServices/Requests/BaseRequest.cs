﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.Helpers.Interfaces;

namespace Taaleem.DataServices.Requests
{
    public class BaseRequest
    {

        public HttpMessageHandler HttpMessageHandler { get; set; }
       
        public string RequestUrl { get; set; }
        /// <summary>
        /// Avoid retrieving results from the client side cache, Default is true.
        /// </summary>
        public bool NoCahce { get; set; }

        /// <summary>
        /// Default is true, which will dispose handler automatically
        /// </summary>
        public bool DisposeHandler = true;

        private readonly INetworkHelper _networkHelper;
        private readonly IAppSettings _appSettings;

        private ContentType _resultContentType = ContentType.Json;
        /// <summary>
        /// Default is Json
        /// </summary>
        public ContentType ResultContentType
        {
            get { return _resultContentType; }
            set { _resultContentType = value; }
        }

        /// <summary>
        /// Override User Agent of request
        /// </summary>
        public string UserAgent { get; set; }


        public BaseRequest(INetworkHelper networkHelper, IAppSettings appSettings)
        {
            _networkHelper = networkHelper;
            _appSettings = appSettings;
            NoCahce = true;
        }

        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value;
            }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
            }
        }

        public async Task<RequestResponse<TR>> PostAsync<TR>(StringContent content, CancellationToken? token = null)
        {

            var response = new RequestResponse<TR>();
            HttpClient httpClient = null;
            try
            {
                if (!_networkHelper.HasInternetAccess())
                {
                    response.ResponseStatus = ResponseStatus.NoInternet;
                    return response;
                }

                httpClient = HttpMessageHandler != null ?
                    new HttpClient(HttpMessageHandler, DisposeHandler) :
                    new HttpClient(new HttpClientHandler(), DisposeHandler);

                Debug.WriteLine($"Request at {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
                Debug.WriteLine(RequestUrl);

                if (NoCahce)
                    httpClient.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue() { NoCache = true };

                if (!String.IsNullOrEmpty(UserAgent))
                    httpClient.DefaultRequestHeaders.Add("User-Agent", UserAgent);
                //httpClient.DefaultRequestHeaders.Add("Role", "3");
                var authData = string.Format("{0}:{1}", UserName, Password);
                var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage postResponse;
                if (token == null)
                    postResponse = await httpClient.PostAsync(RequestUrl, content);
                else
                    postResponse = await httpClient.PostAsync(RequestUrl, content, token.Value);
                IEnumerable<string> co;
                bool cookies = postResponse.Headers.TryGetValues("Set-Cookie", out co);
                //IEnumerable<string> cookies = postResponse.Headers.GetValues("Set-Cookie");

                if (co != null)
                {
                    foreach (var c in co)
                    {
                        _appSettings.AuthCookie = c;
                    }
                }

                if (token != null && token.Value.IsCancellationRequested)
                {
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                    return response;
                }

                var stringValue = await postResponse.Content.ReadAsStringAsync();
                response.StatusCode = postResponse.StatusCode;
                ParseResult(stringValue, response);//parse Result even when there is error
                if (!postResponse.IsSuccessStatusCode)
                {
                    response.ResponseStatus = ResponseStatus.HttpError;//Override response status value to be HttpError
                    //response.
                }
                return response;
            }
            catch (OperationCanceledException)
            {
                if (token != null && token.Value.IsCancellationRequested)
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                else
                    response.ResponseStatus = ResponseStatus.TimeOut;
            }
            catch (WebException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (HttpRequestException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (Exception e)
            {
                response.ResponseStatus = ResponseStatus.ClientSideError;
            }
            finally
            {
                if (httpClient != null) httpClient.Dispose();

            }
            return response;
        }

        public async Task<RequestResponse<TR>> PostDataAsync<TR>(StringContent content, CancellationToken? token = null)
        {

            var response = new RequestResponse<TR>();
            HttpClient httpClient = null;
            try
            {
                if (!_networkHelper.HasInternetAccess())
                {
                    response.ResponseStatus = ResponseStatus.NoInternet;
                    return response;
                }

                httpClient = HttpMessageHandler != null ?
                    new HttpClient(HttpMessageHandler, DisposeHandler) :
                    new HttpClient(new HttpClientHandler(), DisposeHandler);

                Debug.WriteLine($"Request at {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
                Debug.WriteLine(RequestUrl);
                if (NoCahce)
                    httpClient.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue() { NoCache = true };

                if (!String.IsNullOrEmpty(UserAgent))
                    httpClient.DefaultRequestHeaders.Add("User-Agent", UserAgent);
                //httpClient.DefaultRequestHeaders.Add("Role", "3");
                var authData = string.Format("{0}:{1}", UserName, Password);
                var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
                HttpResponseMessage postResponse;
                if (token == null)
                    postResponse = await httpClient.PostAsync(RequestUrl, content);
                else
                    postResponse = await httpClient.PostAsync(RequestUrl, content, token.Value);
                IEnumerable<string> co;
                bool cookies = postResponse.Headers.TryGetValues("Set-Cookie", out co);
                //IEnumerable<string> cookies = postResponse.Headers.GetValues("Set-Cookie");

                if (co != null)
                {
                    foreach (var c in co)
                    {
                        _appSettings.AuthCookie = c;
                    }
                }

                if (token != null && token.Value.IsCancellationRequested)
                {
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                    return response;
                }

                var stringValue = await postResponse.Content.ReadAsStringAsync();
                response.StatusCode = postResponse.StatusCode;
                response.Result = ParseDataResult<TR>(stringValue);//parse Result even when there is error
                if (!postResponse.IsSuccessStatusCode)
                {
                    response.ResponseStatus = ResponseStatus.HttpError;//Override response status value to be HttpError
                    //response.
                }
                return response;
            }
            catch (OperationCanceledException)
            {
                if (token != null && token.Value.IsCancellationRequested)
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                else
                    response.ResponseStatus = ResponseStatus.TimeOut;
            }
            catch (WebException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (HttpRequestException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (Exception e)
            {
                response.ResponseStatus = ResponseStatus.ClientSideError;
            }
            finally
            {
                if (httpClient != null) httpClient.Dispose();

            }
            return response;
        }



        public async Task<RequestResponse<TR>> PutAsync<TR>(StringContent content, CancellationToken? token = null)
        {

            var response = new RequestResponse<TR>();
            HttpClient httpClient = null;
            try
            {
                if (!_networkHelper.HasInternetAccess())
                {
                    response.ResponseStatus = ResponseStatus.NoInternet;
                    return response;
                }

                httpClient = HttpMessageHandler != null ?
                    new HttpClient(HttpMessageHandler, DisposeHandler) :
                    new HttpClient(new HttpClientHandler(), DisposeHandler);


                Debug.WriteLine($"Request at {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
                Debug.WriteLine(RequestUrl);
                if (NoCahce)
                    httpClient.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue() { NoCache = true };

                if (!String.IsNullOrEmpty(UserAgent))
                    httpClient.DefaultRequestHeaders.Add("User-Agent", UserAgent);
                //httpClient.DefaultRequestHeaders.Add("Role", "3");
                var authData = string.Format("{0}:{1}", UserName, Password);
                var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
                HttpResponseMessage postResponse;
                if (token == null)
                    postResponse = await httpClient.PutAsync(RequestUrl, content);
                else
                    postResponse = await httpClient.PutAsync(RequestUrl, content, token.Value);

                if (token != null && token.Value.IsCancellationRequested)
                {
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                    return response;
                }

                var stringValue = await postResponse.Content.ReadAsStringAsync();
                response.StatusCode = postResponse.StatusCode;
                ParseResult(stringValue, response);//parse Result even when there is error
                if (!postResponse.IsSuccessStatusCode)
                {
                    response.ResponseStatus = ResponseStatus.HttpError;//Override response status value to be HttpError
                   
                }
                return response;
            }
            catch (OperationCanceledException)
            {
                if (token != null && token.Value.IsCancellationRequested)
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                else
                    response.ResponseStatus = ResponseStatus.TimeOut;
            }
            catch (WebException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (HttpRequestException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (Exception  e)
            {
                response.ResponseStatus = ResponseStatus.ClientSideError;
            }
            finally
            {
                if (httpClient != null) httpClient.Dispose();

            }
            return response;
        }

        public async Task<RequestResponse<TR>> GetAsync<TR>(CancellationToken? token = null, bool requiredAuthentication = true)
        {
            var response = new RequestResponse<TR>();
            HttpClient httpClient = null;
            try
            {
                if (!_networkHelper.HasInternetAccess())
                {
                    response.ResponseStatus = ResponseStatus.NoInternet;
                    return response;
                }
                httpClient = HttpMessageHandler != null ?
                      new HttpClient(HttpMessageHandler, DisposeHandler) :
                      new HttpClient(new HttpClientHandler(), DisposeHandler);

                Debug.WriteLine($"Request at {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
                Debug.WriteLine(RequestUrl);
                if (NoCahce)
                    httpClient.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue() { NoCache = true };
                if (requiredAuthentication)
                {
                    var authData = string.Format("{0}:{1}", UserName, Password);
                    var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
                    //httpClient.DefaultRequestHeaders.Add("Set-Cookie", _appSettings.AuthCookie);
                }

                HttpResponseMessage getResponse;


                if (token == null)
                    getResponse = await httpClient.GetAsync(RequestUrl);
                else
                    getResponse = await httpClient.GetAsync(RequestUrl, token.Value);

                if (token != null && token.Value.IsCancellationRequested)
                {
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                    return response;
                }

                response.StatusCode = getResponse.StatusCode;
                var stringValue = await getResponse.Content.ReadAsStringAsync();
                ParseResult(stringValue, response);//parse Result even when there is error
                if (!getResponse.IsSuccessStatusCode)
                {
                    if (stringValue == "The HTTP request is unauthorized with client authentication scheme 'Ntlm'. The authentication header received from the server was 'NTLM,Basic realm=\"localhost\"'.")
                        response.StatusCode = HttpStatusCode.Unauthorized;
                    response.ResponseStatus = ResponseStatus.HttpError;//Override response status value to be HttpError
                }

                return response;
            }
            catch (OperationCanceledException)
            {
                if (token != null && token.Value.IsCancellationRequested)
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                else
                    response.ResponseStatus = ResponseStatus.TimeOut;
            }
            catch (WebException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (HttpRequestException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (Exception)
            {
                response.ResponseStatus = ResponseStatus.ClientSideError;
            }
            finally
            {
                if (httpClient != null) httpClient.Dispose();
            }
            return response;
        }
        public async Task<RequestResponse<TR>> GetDataAsync<TR>(CancellationToken? token = null)
        {
            var response = new RequestResponse<TR>();
            HttpClient httpClient = null;
            try
            {
                if (!_networkHelper.HasInternetAccess())
                {
                    response.ResponseStatus = ResponseStatus.NoInternet;
                    return response;
                }
                httpClient = HttpMessageHandler != null ?
                      new HttpClient(HttpMessageHandler, DisposeHandler) :
                      new HttpClient(new HttpClientHandler(), DisposeHandler);

                Debug.WriteLine($"Request at {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
                Debug.WriteLine(RequestUrl);
                if (NoCahce)
                    httpClient.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue() { NoCache = true };
                var authData = string.Format("{0}:{1}", UserName, Password);
                var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
                //httpClient.DefaultRequestHeaders.Add("Set-Cookie", _appSettings.AuthCookie);
                HttpResponseMessage getResponse;


                if (token == null)
                    getResponse = await httpClient.GetAsync(RequestUrl);
                else
                    getResponse = await httpClient.GetAsync(RequestUrl, token.Value);

                if (token != null && token.Value.IsCancellationRequested)
                {
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                    return response;
                }

                response.StatusCode = getResponse.StatusCode;
                var stringValue = await getResponse.Content.ReadAsStringAsync();
                response.Result = ParseDataResult<TR>(stringValue);//parse Result even when there is error
                if (!getResponse.IsSuccessStatusCode)
                {
                    if (stringValue == "The HTTP request is unauthorized with client authentication scheme 'Ntlm'. The authentication header received from the server was 'NTLM,Basic realm=\"localhost\"'.")
                        response.StatusCode = HttpStatusCode.Unauthorized;
                    response.ResponseStatus = ResponseStatus.HttpError;//Override response status value to be HttpError
                }

                return response;
            }
            catch (OperationCanceledException)
            {
                if (token != null && token.Value.IsCancellationRequested)
                    response.ResponseStatus = ResponseStatus.UserCanceled;
                else
                    response.ResponseStatus = ResponseStatus.TimeOut;
            }
            catch (WebException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (HttpRequestException)
            {
                response.ResponseStatus = ResponseStatus.HttpError;
            }
            catch (Exception)
            {
                response.ResponseStatus = ResponseStatus.ClientSideError;
            }
            finally
            {
                if (httpClient != null) httpClient.Dispose();
            }
            return response;
        }

        private void ParseResult<TR>(string stringValue, RequestResponse<TR> response)
        {
            Debug.WriteLine($"Response at {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
            Debug.WriteLine(stringValue);

            if (String.IsNullOrWhiteSpace(stringValue))
            {
                response.ResponseStatus = ResponseStatus.SuccessWithNoData;
                return;
            }
            if (ResultContentType == ContentType.Json)
            {
                try
                {
                    stringValue = stringValue.Replace("DayofLeave", "DayOfLeave"); //Replace("\"<", "\"").Replace(">k__BackingField", "")
                    TR result = JsonConvert.DeserializeObject<TR>(stringValue);
                    if (response.StatusCode == HttpStatusCode.InternalServerError)// PreregistrationErrorMessage
                    {
                        var x = JsonConvert.DeserializeObject<InternalServerError>(stringValue);
                        if (!string.IsNullOrEmpty(x.ErrorMessage))
                        {
                            response.InternalServerErrorMessage = x.ErrorMessage;
                        }
                    }
                    response.Result = result;
                    response.ResponseStatus = ResponseStatus.SuccessWithResult;
                }
                catch (Exception e)
                {
                    response.ResponseStatus = ResponseStatus.ParserException;
                    Debug.WriteLine($"Json value: {stringValue} \n\rParser Error: {e.Message}");
                    return;
                }
            }
            else if (ResultContentType == ContentType.Text)
            {
                try
                {
                    response.Result = (TR)Convert.ChangeType(stringValue, typeof(TR));
                }
                catch (Exception)
                {
                    response.ResponseStatus = ResponseStatus.ParserException;
                    return;
                }
            }

            if (response.Result.Equals(default(TR))) response.ResponseStatus = ResponseStatus.SuccessWithNoData;
            //Check if the result is of type Ilist, and the count ==0, if true assign ResponseStatus to no Data
            else if (response.Result is IList && (response.Result as IList).Count == 0) response.ResponseStatus = ResponseStatus.SuccessWithNoData;
        }
        private T ParseDataResult<T>(string stringValue)
        {
            Debug.WriteLine($"Response at {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
            Debug.WriteLine(stringValue);

            T response = default(T);

            if (String.IsNullOrWhiteSpace(stringValue))
            {
                //response.ResponseStatus = ResponseStatus.SuccessWithNoData;
                return response;
            }
            if (ResultContentType == ContentType.Json)
            {
                try
                {
                    stringValue = stringValue.Replace("DayofLeave", "DayOfLeave");
                    T result = JsonConvert.DeserializeObject<T>(stringValue);
                    return result;
                    //if (response.StatusCode == HttpStatusCode.InternalServerError)// PreregistrationErrorMessage
                    //{
                    //    var x = JsonConvert.DeserializeObject<InternalServerError>(stringValue);
                    //    if (!string.IsNullOrEmpty(x.ErrorMessage))
                    //    {
                    //        //response.InternalServerErrorMessage = x.ErrorMessage;
                    //    }
                    //}
                    //response.Result = result;
                    //response.ResponseStatus = ResponseStatus.SuccessWithResult;
                }
                catch (Exception e)
                {
                    //response.ResponseStatus = ResponseStatus.ParserException;
                    //return;
                }
            }
            else if (ResultContentType == ContentType.Text)
            {
                try
                {
                    response = (T)Convert.ChangeType(stringValue, typeof(T));
                }
                catch (Exception)
                {
                    //response.ResponseStatus = ResponseStatus.ParserException;
                    //return;
                }
            }

            return response;
            //if (response.Equals(default(T))) response.ResponseStatus = ResponseStatus.SuccessWithNoData;
            //Check if the result is of type Ilist, and the count ==0, if true assign ResponseStatus to no Data
            //else if (response is IList && (response as IList).Count == 0)
            //    response.ResponseStatus = ResponseStatus.SuccessWithNoData;
        }
    }

    public enum ContentType
    {
        Text,
        Json,
        Xml,
    }
    class InternalServerError
    {
        public string ErrorMessage { get; set; }
    }
}
