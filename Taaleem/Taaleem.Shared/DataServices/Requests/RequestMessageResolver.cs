﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Helpers;
using Taaleem.DataServices.Interfaces;

namespace Taaleem.DataServices.Requests
{
    public class RequestMessageResolver : IRequestMessageResolver
    {
        private readonly Resources _resources;

        public RequestMessageResolver(Resources resources)
        {
            _resources = resources;
        }

        public RequestMessage ResultToMessage(RequestResponse response)
        {

            switch (response.ResponseStatus)
            {
                case ResponseStatus.NoInternet:
                    return new RequestMessage(_resources.NoInternet, GemoetryPaths.NoInternet);
                case ResponseStatus.HttpError:
                    return new RequestMessage(_resources.ErrorConnecting, GemoetryPaths.NoInternet);
                case ResponseStatus.SuccessWithNoData:
                    return new RequestMessage(_resources.NoData, GemoetryPaths.NoResults);
                case ResponseStatus.ParserException:
                case ResponseStatus.ClientSideError:
                    return new RequestMessage(_resources.ClientSideError, GemoetryPaths.NoResults);
                case ResponseStatus.SuccessWithResult:
                    return new RequestMessage(_resources.Success, null) { MessageType = RequestMessageType.Info };
                default:
                    return new RequestMessage("", null) { MessageType = RequestMessageType.Info };
            }
        }
    }
}
