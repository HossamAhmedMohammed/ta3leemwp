﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.QueryParameters;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;

namespace Taaleem.DataServices
{
    public class ChildrenDataService : IChildrenDataService
    {
        //TODO:Review DateTime formats
        //Null values
        //Long instead of int?
        //String instead of long?
        private readonly IAppSettings _appSettings;
        private readonly Func<BaseRequest> _requestFactory;
        private const string BaseUrl = "https://mobsrvc.education.qa/";
        public String UserName
        {
            get
            {
                if(_appSettings.AmIParent)
                return _appSettings.ParentCredential.UserName;
                else
                    //return _appSettings.StudentCredential.UserName;
                return "a.alsayed";
            }
        }

        public String Password
        {
            get
            {
                if (_appSettings.AmIParent)
                    return _appSettings.ParentCredential.Password;
                else
                    //return _appSettings.StudentCredential.Password;
                 return "A123456a";
            }
        }

        public ChildrenDataService(IAppSettings appSettings, INetworkHelper networkHelper, Func<BaseRequest> requestFactory)
        {
            _appSettings = appSettings;
            _requestFactory = requestFactory;
        }

        public async Task<RequestResponse<ChildLoginResponse>> LoginAsync(ChildLoginParameters parameters, String userName, String password,
            CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/security/Authenticate").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(userName, password) };
            return await request.GetAsync<ChildLoginResponse>(token, false);
        }

        public async Task<RequestResponse<List<Announcement>>> GetAnnouncementsAsync(AnnouncementParameters parameters,
            CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/school/getschoolannouncements").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<Announcement>>(token);
        }

        public async Task<RequestResponse<List<Assignment>>> GetAssignmentsAsync(AssignmentParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/assignment/getactiveassignmentsbystudentid").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<Assignment>>(token);
        }

        public async Task<RequestResponse<List<Attendance>>> GetStudentSchedulesAsync(GetAttendanceScheduleParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/attendance/getstudentschedule").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<Attendance>>(token);
        }

        public async Task<RequestResponse<List<Vacation>>> GetSchoolVacationsAsync(GetSchoolVacationsParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/timetable/getschoolvacations").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<Vacation>>(token);
        }

        public async Task<RequestResponse<List<AttendanceStatus>>> GetAttendanceStatusesAsync(AttendanceStatusParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/attendance/getallattendancestatuses").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<AttendanceStatus>>(token);
        }

        public async Task<RequestResponse<List<ContactTeacher>>> GetStudentTeachersAsync(ContactTeacherParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/contact/GetStudentTeachers").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<ContactTeacher>>(token);
        }


        public async Task<RequestResponse<List<FeedCategory>>> GetFeedCategoriesAsync(FeedCategoryParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/multirolefeed/getcategories").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<FeedCategory>>(token);
        }

        public async Task<RequestResponse<List<Reminder>>> GetStudentRemindersAsync(ReminderParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/Reminder/GetStudentReminders").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<Reminder>>(token);
        }

        public async Task<RequestResponse<List<Feed>>> GetFeedsAsync(GetFeedsMethod feedsMethod, FeedParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + String.Format("api/feed/{0}", feedsMethod.MethodName)).AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<Feed>>(token);
        }

        public async Task<RequestResponse<List<Behavior>>> GetBehaviorsAsync(BehaviorParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/behavior/getstudentbehaviorincidents").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<Behavior>>(token);
        }

        public async Task<RequestResponse<List<Grade>>> GetGradesAsync(GradeParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/grade/GetStudentPeriodScores").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<Grade>>(token);
        }

        public async Task<RequestResponse<List<PrivateNote>>> GetPrivateNotesAsync(GetPrivateNoteParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/contact/GetPrivateConversation").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<PrivateNote>>(token);
        }

        public async Task<RequestResponse<long>> SendPrivateNoteToTeacherAsync(AddConversationPostParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "api/contact/AddConversationPost");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.PostAsync<long>(new StringContent(parameters.ToQueryString(), Encoding.UTF8, "application/x-www-form-urlencoded"), token);
        }

    }
}
