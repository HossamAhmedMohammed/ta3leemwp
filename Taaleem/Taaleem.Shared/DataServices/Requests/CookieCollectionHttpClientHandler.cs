﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Taaleem.DataServices.Requests
{
    public class CookieCollectionHttpClientHandler : HttpClientHandler
    {
        private readonly CookieCollection _cookieCollection;

        public CookieCollectionHttpClientHandler()
        {
            UseCookies = false;
            // CookieContainer = new CookieContainer();
            _cookieCollection = new CookieCollection();
        }

        public bool HasCookies
        {
            get { return _cookieCollection.Count > 0; }
        }

        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {

            //if(!UseCookies)
            //    return await base.SendAsync(request, cancellationToken);

            SetCookies(request);

            var response = await base.SendAsync(request, cancellationToken);

            ParseResponse(response);
            return response;
        }

        public void SetCookies(HttpRequestMessage request)
        {

            //if(CookieContainer.GetCookies(GetBaseUri(request.RequestUri)).Count > 0)
            //    return;

            if (_cookieCollection.Count == 0)
                return;

            var cookieString = new StringBuilder();

            foreach (var cookie in _cookieCollection)
            {
                cookieString.Append(cookie);
                cookieString.Append("; ");
            }

            request.Headers.Add("Cookie", cookieString.ToString().TrimEnd(' ', ';'));
        }



        public void ParseResponse(HttpResponseMessage responseMessage)
        {
            //if(CookieContainer.GetCookies(GetBaseUri(responseMessage.RequestMessage.RequestUri)).Count > 0)
            //    return;

            if (!responseMessage.Headers.Contains("Set-Cookie"))
                return;

            var cookies = responseMessage.Headers.GetValues("Set-Cookie");

            foreach (var cookie in cookies)
            {
                foreach (var cookieString in Regex.Split(cookie, ",  "))
                {
                    var newCookie = new Cookie();

                    foreach (var value in cookieString.Split(';').
                        Select(cookiePartString => cookiePartString.Trim().Split('=')))
                    {
                        switch (value[0].ToLower())
                        {
                            case "expires":
                                newCookie.Expires = DateTime.Parse(value[1]);
                                continue;
                            case "domain":
                                newCookie.Domain = value[1];

                                continue;
                            case "path":
                                newCookie.Path = value[1];
                                continue;
                            case "httponly":
                                newCookie.HttpOnly = true;
                                continue;
                            default:
                                newCookie.Name = value[0];
                                newCookie.Value = value[1];
                                continue;
                        }
                    }
                    _cookieCollection.Add(newCookie);
                }
            }
        }

        private static Uri GetBaseUri(Uri uri)
        {
            var fullPath = uri.OriginalString;
            string host = uri.Host;
            string result = fullPath.Substring(0, fullPath.IndexOf(host, StringComparison.Ordinal) + host.Length);
            return new Uri(result, UriKind.RelativeOrAbsolute);
        }
    }
}
