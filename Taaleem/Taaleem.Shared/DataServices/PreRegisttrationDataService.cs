﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.PreRegistration.Request;
using Taaleem.Models.PreRegistration.Response;

namespace Taaleem.DataServices
{
    public class PreRegisttrationDataService : IPreRegistrationDataService
    {
        private readonly IAppSettings _appSettings;
        private readonly Func<BaseRequest> _requestFactory;//213.130.124.65:121
        private CookieContainer _sharedCookieContainer; //10.0.190.248:121
        private const string BaseUrl = "http://213.130.124.65:121/Atlas-K12NET-EarlyRegistration-Web-EnrollmentDomainService.svc/json/";
        private const string BaseDomainUrl = "http://213.130.124.65:121/Atlas-K12NET-EarlyRegistration-Web-DomainService.svc/json/";
        private const string BaseERURL = "http://213.130.124.65:121/Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/";
        private const string BaseTransferURl = "http://213.130.124.65:121/Atlas-K12NET-EarlyRegistration-Web-TransferDomainService.svc/json/";
        public PreRegisttrationDataService(IAppSettings appSettings, INetworkHelper networkHelper, Func<BaseRequest> requestFactory)
        {
            _appSettings = appSettings;
            _requestFactory = requestFactory;
        }
        private CookieContainer GetSharedCookieContainer()
        {
            if (_sharedCookieContainer == null)
            {
                _sharedCookieContainer = new CookieContainer();
            }
            return _sharedCookieContainer;
        }

        private HttpClientHandler GetAuthHandler()
        {
            var handler = new HttpClientHandler { CookieContainer = GetSharedCookieContainer() };
           
            return handler;
        }

        public async Task<RequestResponse<LoginResponse>> Login(LoginParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = "http://213.130.124.65:121/Authentication_JSON_AppService.axd/Login";
            request.HttpMessageHandler = GetAuthHandler();
            request.ResultContentType = ContentType.Json;
            return await request.PostAsync<LoginResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);

        }
        public async Task<RequestResponse<GetRequestsResponse>>GetALLRequests( CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "GetRequests");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            return await request.GetAsync<GetRequestsResponse>(token);
        }
     
        public string AuthCookievalue { get; set; }
      
        private void getAuthCookie()
        {
            string all = _appSettings.AuthCookie;
            if (!string.IsNullOrEmpty(all))
            {
                AuthCookievalue = all.Substring(17);
                AuthCookievalue = AuthCookievalue.Split(';')[0];
            }
        }

        public Task<RequestResponse<List<EnrollmentResponse>>> GetAlternateSchools(GetAlternateSchoolsParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<List<EmployerType>>> GetEmployerTypes(CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public async Task<RequestResponse<GradeLevel>> GetGradeLevels(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseDomainUrl + "GetGradeLevels");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            return await request.PostAsync<GradeLevel>(null, token);
        }

        public Task<RequestResponse<Job>> GetJob(Guid ID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public async Task<RequestResponse<GetNearestSchoolsResponse>> GetNearestSchools(GetNearestSchoolsParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "GetNearestSchools");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA","/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            string y = JsonConvert.SerializeObject(parameters);
            if (y != null)
            {
                var x = new StringContent(y, Encoding.UTF8, "application/json");

                return await request.PostAsync<GetNearestSchoolsResponse>(x, token);
            }
            return null;
        }

        public async Task<RequestResponse<GetNearestSchoolsDetailsResponse>> GetNearestSchoolsByQars(GetNearestSchoolsByQarsParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "GetSchoolContacts");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
             // CookieCollection x =  _sharedCookieContainer.GetCookies(new Uri("/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA","/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA","/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en","/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en","/"));
            }
            request.ResultContentType = ContentType.Json;
            string y = JsonConvert.SerializeObject(parameters);
            if (y != null)
            {
                var x = new StringContent(y, Encoding.UTF8, "application/json");

                return await request.PostAsync<GetNearestSchoolsDetailsResponse>(x, token);
            }
            return null;
        }

        public async Task<RequestResponse<Parent>> GetParent(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseERURL + "GetParent");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en"));
            }
            request.ResultContentType = ContentType.Json;
            return await request.PostAsync<Parent>(null, token);
        }

        public async Task<RequestResponse<GetSchoolEnrollmentsResponsecs>> GetRegistrations(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "GetSchoolEnrollments");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            return await request.PostAsync<GetSchoolEnrollmentsResponsecs>(null, token);
        }

        public async Task<RequestResponse<Relationship>> GetRelationships(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseDomainUrl + "GetRelationships");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            return await request.PostAsync<Relationship>(null, token);
        }

        public Task<RequestResponse<List<ReceiveFileResponse>>> GetSchoolContact(Guid schoolID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<bool>> GetValidationCode(GetValidationCodeParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<List<WorkLocation>>> GetWorkLocations(GetWorkLocationsParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<bool>> IsAllNationalitiesAllowedToEnroll(CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public async Task<RequestResponse<EnrollmentResponse>> EnrollStudent(EnrollStudentParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Enroll");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            string y = JsonConvert.SerializeObject(parameters);
            if (y != null)
            {
                var x = new StringContent(y, Encoding.UTF8, "application/json");

                return await request.PostAsync<EnrollmentResponse>(x, token);
            }
            return null;
        }

        public Task<RequestResponse<Job>> PrintReceipt(Guid enrollmentID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public async Task<RequestResponse<ReceiveFileResponse>> ReceiveFile(ReceiveFileParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseERURL + "ReceiveFile");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            string y = JsonConvert.SerializeObject(parameters);
            if (y != null)
            {
                var x = new StringContent(y, Encoding.UTF8, "application/json");

                return await request.PostAsync<ReceiveFileResponse>(x, token);
            }
            return null;
        }

        public Task<RequestResponse<RegisterAsParentResponse>> RegisterAsParent(RegisterAsParentParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<bool>> RegisterStudent(RegisterStudentParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<ResetPasswordPesponse>> ResetPassword(ResetPasswordParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public async Task<RequestResponse<SearchStudentResponse>> SearchStudent(SearchStudentParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "SearchStudent");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            string y = JsonConvert.SerializeObject(parameters);
            if (y != null)
            {
                var x = new StringContent(y, Encoding.UTF8, "application/json");

                return await request.PostAsync<SearchStudentResponse>(x, token);
            }
            return null;
        }

        public Task<RequestResponse<UpdateParentResponse>> UpdateParent(UpdateParentParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }
        
        public async Task<RequestResponse<GetAllSettingsResponse>> GetSettings(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "GetSettings");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            return await request.GetAsync<GetAllSettingsResponse>(token);
        }

        public async Task<RequestResponse<GetStatesResponse>> GetStates(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseTransferURl + "GetStates");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            return await request.PostAsync<GetStatesResponse>(null, token);
        }

        public async Task<RequestResponse<TransferStudentResponse>> TransferStudent(TransferParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseTransferURl + "Transfer");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            string y = JsonConvert.SerializeObject(parameters);
            if (y != null)
            {
                var x = new StringContent(y, Encoding.UTF8, "application/json");

                return await request.PostAsync<TransferStudentResponse>(x, token);
            }
            return null;
        }

        public async Task<RequestResponse<GetSchoolsByStateResponse>> GetSchoolsByState(GetSchoolsByStateParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseTransferURl + "GetSchoolsByState");
            request.HttpMessageHandler = GetAuthHandler();
            getAuthCookie();
            Cookie c = new Cookie(".WebReg_ASPXAUTH", AuthCookievalue);
            c.HttpOnly = true;
            _sharedCookieContainer.Add(new Uri(request.RequestUrl), c);
            if (_appSettings.LanguageId == 2)
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "ar-QA", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "ar-QA", "/"));
            }
            else
            {
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("UICulture", "en", "/"));
                _sharedCookieContainer.Add(new Uri(request.RequestUrl), new Cookie("Culture", "en", "/"));
            }
            request.ResultContentType = ContentType.Json;
            string y = JsonConvert.SerializeObject(parameters);
            if (y != null)
            {
                var x = new StringContent(y, Encoding.UTF8, "application/json");

                return await request.PostAsync<GetSchoolsByStateResponse>(x, token);
            }
            return null;
        }
    }
}
