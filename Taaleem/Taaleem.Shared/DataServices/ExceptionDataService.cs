﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.QueryParameters;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Exceptions.Request;
using Taaleem.Models.Exceptions.Response;

namespace Taaleem.DataServices
{
    public class ExceptionDataService : IExceptionDataService
    {
        private readonly IAppSettings _appSettings;
        private readonly Func<BaseRequest> _requestFactory;
        private CookieContainer _sharedCookieContainer;
        private const string BaseUrl = "http://rfe-awd1-tst.edu.gov.qa:8080/api/";
        public String UserName
        {
            get
            {
               // return _appSettings.EmployeeCredential.UserName;
                return "Testuser2020";
            }
        }

        public String Password
        {
            get
            {
               // return _appSettings.EmployeeCredential.Password;
                 return "2020";
            }
        }


        public ExceptionDataService(IAppSettings appSettings, INetworkHelper networkHelper, Func<BaseRequest> requestFactory)
        {
            _appSettings = appSettings;
            _requestFactory = requestFactory;
        }

        private CookieContainer GetSharedCookieContainer()
        {
            if (_sharedCookieContainer == null)
            {
                _sharedCookieContainer = new CookieContainer();
            }
            return _sharedCookieContainer;
        }

        private HttpClientHandler GetAuthHandler()
        {
            var handler = new HttpClientHandler { CookieContainer = GetSharedCookieContainer() };
            return handler;
        }

     

        public async Task<RequestResponse<AddResponse>> AddApplicant(AddApplicantParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Applicant/Add");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            request.ResultContentType = ContentType.Json;
            
            StringContent s = new StringContent(JsonConvert.SerializeObject(parameters));
            return await request.PostAsync<AddResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<List<School>>> GetAllIndependentSchoolsAsync(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl =  (BaseUrl + "IndependentSchool/GetAll");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            request.ResultContentType = ContentType.Json;
            return await request.GetAsync<List<School>>(token);
        }

        public async Task<RequestResponse<List<Nationality>>> GetAllNationalitiesAsync(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Nationality/GetAll");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<Nationality>>(token);
        }

        public async Task<RequestResponse<List<School>>> GetAllPrivateSchoolsAsync(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "PrivateSchool/GetAll");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<School>>(token);
        }

        public async Task<RequestResponse<Applicant>> GetApplicantByID(int applicantID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Applicant/GetById/"+applicantID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<Applicant>(token);
        }

        public async Task<RequestResponse<List<MyExceptionRequestResponse>>> GetMyRequests(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Request/GetMyRequests");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            request.ResultContentType = ContentType.Json;
            return await request.GetAsync<List<MyExceptionRequestResponse>>(token);
        }

        public async Task<RequestResponse<MyExceptionRequestResponse>> GetRequestByID(GetReqestByIDParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/Request/GetById/"+parameters.requestID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<MyExceptionRequestResponse>(token);
        }

        public async Task<RequestResponse<SubmissionDate>> GetSubmissionDate(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "SubmissionDate/GetSubmissionDate");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<SubmissionDate>(token);
        }

        public async Task<RequestResponse<UpdateResponse>> UpdateApplicant(UpdateApplicantParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Applicant/Update");///.AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.PutAsync<UpdateResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<AddResponse>> AddFather(AddFatherParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Parent/Add");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            //   request.ResultContentType = ContentType.Text;
            return await request.PostAsync<AddResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<UpdateResponse>> UpdateFather(UpdateFatherParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Parent/Update");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
          string x =  JsonConvert.SerializeObject(parameters);
            return await request.PutAsync<UpdateResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<Parent>> GetFatherByID(int FatherID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/Parent/GetById/").AppendQueryString(FatherID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<Parent>(token);
        }

        public async Task<RequestResponse<Parent>> GetFatherByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/Parent/GetFatherByRequestId/" + RequestID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<Parent>(token);
        }

        public async Task<RequestResponse<AddResponse>> AddMother(AddMotherParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Parent/Add");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            //   request.ResultContentType = ContentType.Text;
            return await request.PostAsync<AddResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);

        }

        public async Task<RequestResponse<UpdateResponse>> UpdateMother(UpdateMotherParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Parent/Update");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.PutAsync<UpdateResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<ParentM>> GetMotherByID(int MotherID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/Parent/GetById/").AppendQueryString(MotherID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<ParentM>(token);
        }

        public async Task<RequestResponse<ParentM>> GetMotherByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/Parent/GetMotherByRequestId/" + RequestID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<ParentM>(token);
        }

        public async Task<RequestResponse<AddResponse>> AddBreadwinner(AddBreadwinnerParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Breadwinner/Add");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            //   request.ResultContentType = ContentType.Text;
            return await request.PostAsync<AddResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<UpdateResponse>> UpdateBreadwinner(UpdateBreadwinnerParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Breadwinner/Update");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.PutAsync<UpdateResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<Breadwinner>> GetBreadwinnerByID(int BreadwinnerID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/Breadwinner/GetById/" + BreadwinnerID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<Breadwinner>(token);
        }

        public async Task<RequestResponse<Breadwinner>> GetBreadwinnerByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/Breadwinner/GetByRequestId/" + RequestID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<Breadwinner>(token);
        }

        public async Task<RequestResponse<AddResponse>> AddStudent(AddStudentParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "EStudent/Add");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            //   request.ResultContentType = ContentType.Text;
            return await request.PostAsync<AddResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<UpdateResponse>> UpdateStudent(UpdateStudentParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "EStudent/Update");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.PutAsync<UpdateResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<Student>> GetStudentByID(int StudentID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/EStudent/GetById/" + StudentID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<Student>(token);
        }

        public async Task<RequestResponse<Student>> GetStudentByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/EStudent/GetByRequestId/" + RequestID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<Student>(token);
        }

        public async Task<RequestResponse<Student>> GetStudentTwinsByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            
           var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/EStudent/GetTwinsByRequestId/"+RequestID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<Student>(token);
        }

        public async Task<RequestResponse<AddResponse>> AddChild(AddChildParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Children/Add");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            //   request.ResultContentType = ContentType.Text;
            return await request.PostAsync<AddResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<UpdateResponse>> UpdateChild(UpdateChildParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Children/Update");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.PutAsync<UpdateResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<Child>> GetChildByID(int ChildID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/Children/GetById/" + ChildID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<Child>(token);
        }

        public async Task<RequestResponse<List<Child>>> GetAllChildrenByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/Children/GetByRequestId/" + RequestID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<List<Child>>(token);
        }

        public async Task<RequestResponse<Additionaldata>> GetAdditionalDataByID(int AdditionalDataID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/AdditionalData/GetById/" + AdditionalDataID);
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.GetAsync<Additionaldata>(token);
        }

        public async Task<RequestResponse<GetAttachmentFileResponse>> GetAttachmentFile(GetAttachmentParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Attachment/GetAttachment");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            //   request.ResultContentType = ContentType.Text;
            return await request.PostAsync<GetAttachmentFileResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<UpdateResponse>> UpdateAdditionalData(UpdateAdditionalDataParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "AdditionalData/Update");
            request.HttpMessageHandler = new HttpClientHandler { Credentials = new NetworkCredential(UserName, Password) };
            return await request.PutAsync<UpdateResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }
    }
}
