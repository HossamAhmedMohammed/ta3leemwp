﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.QueryParameters;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.PrivateSchools.Request;
using Taaleem.Models.PrivateSchools.Response;

namespace Taaleem.DataServices
{
    public class PrivateSchoolsDataService : IPrivateSchoolsDataService
    {
        private readonly IAppSettings _appSettings;
        private readonly Func<BaseRequest> _requestFactory;
        private CookieContainer _sharedCookieContainer;
        private const string BaseUrl = "https://nsis.sec.gov.qa/nSISServices/PrivateSchoolPortalService.svc/json";
        //public String UserName
        //{
        //    get
        //    {
        //        return _appSettings.EmployeeCredential.UserName;
        //        //return "a.alsayed";
        //    }
        //}

        //public String Password
        //{
        //    get
        //    {
        //        return _appSettings.EmployeeCredential.Password;
        //        // return "Testpassword";
        //    }
        //}

        public PrivateSchoolsDataService(IAppSettings appSettings, INetworkHelper networkHelper, Func<BaseRequest> requestFactory)
        {
            _appSettings = appSettings;
            _requestFactory = requestFactory;
        }

        private CookieContainer GetSharedCookieContainer()
        {
            if (_sharedCookieContainer == null)
            {
                _sharedCookieContainer = new CookieContainer();
            }
            return _sharedCookieContainer;
        }

        private HttpClientHandler GetAuthHandler()
        {
            var handler = new HttpClientHandler { CookieContainer = GetSharedCookieContainer() };
            return handler;
        }

        public async Task<RequestResponse<Region>> GetAllRegions(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/GetRegions");
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<Region>(token);
        }

        public async Task<RequestResponse<EducationalCurriculum>> GetEducationalCurriculums(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/GetEducationalCurriculums");
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<EducationalCurriculum>(token);
        }

        public async Task<RequestResponse<EducationalLevel>> GetEducationalLevels(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/GetEducationalLevels");
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<EducationalLevel>(token);
        }

        public async Task<RequestResponse<GetSchoolDetailsResponse>> GetSchoolDetailsByID(string schoolID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/GetSchoolDetails/"+ schoolID);
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<GetSchoolDetailsResponse>(token);
        }

        public async Task<RequestResponse<List<SchoolEvent>>> GetSchoolEventsByID(string schoolID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/GetSchoolEvents/" + schoolID);
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<List<SchoolEvent>> (token);
        }

        public async Task<RequestResponse<SchoolInfoDetails>> GetSchoolInfoDetailsByID(string schoolID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/GetSchoolInfoDetail/" + schoolID);
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<SchoolInfoDetails>(token);
        }

        public async Task<RequestResponse<List<SchoolReport>>> GetSchoolReportsByID(string schoolID, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/GetSchoolReports/" + schoolID);
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<List<SchoolReport>>(token);
        }

        public async Task<RequestResponse<SchoolType>> GetSchoolTypes(CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/GetSchoolTypes");
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<SchoolType>(token);
        }

        public async Task<RequestResponse<List<PrivateSchool>>> SearchSchools(SearchSchoolParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "/SearchSchools").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler();
            string y = JsonConvert.SerializeObject(parameters);
            if (y != null)
            {
                var x = new StringContent(y, Encoding.UTF8, "application/json");

                return await request.PostAsync<List<PrivateSchool>>(x, token);
            }
            return null;
        }
    }
}
