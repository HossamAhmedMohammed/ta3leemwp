﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Mock;
using Taaleem.DataServices.Requests;
using Taaleem.Models.Employees.Request;
using Taaleem.Models.Employees.Response;

namespace Taaleem.DataServices.Mix
{
    public class MixEmployeesDataService : IEmployeesDataService
    {
        private readonly IEmployeesDataService _real;
        private readonly IEmployeesDataService _mock;

        public MixEmployeesDataService(EmployeesDataService real, MockEmployeesDataService mock)
        {
#if Mock
            _real = mock;
#else
            _real = real;
#endif
            _mock = mock;
        }

        public Task<RequestResponse<EmployeeLoginResponse>> LoginAsync(EmpolyeeLoginBody parameters, CancellationToken? token = null)
        {
            return _real.LoginAsync(parameters, token);
        }

        public Task<RequestResponse<List<SalaryDetail>>> GetSalariesAsync(SalaryParameters parameters, CancellationToken? token = null)
        {
            return _real.GetSalariesAsync(parameters, token);
        }

        public Task<RequestResponse<List<EmployeeLoan>>> GetEmployeeLoansByStatusAsync(LoansByStatusParameters parameters, CancellationToken? token = null)
        {
            return _real.GetEmployeeLoansByStatusAsync(parameters, token);
        }

        public Task<RequestResponse<List<EmployeeLoan>>> GetAllEmployeeLoansAsync(string userid, CancellationToken? token = null)
        {
            return _real.GetAllEmployeeLoansAsync(userid, token);
        }

        public Task<RequestResponse<List<Evaluation>>> GetEvaluationsAsync(EvaluationParameters parameters, CancellationToken? token = null)
        {
            return _real.GetEvaluationsAsync(parameters, token);
        }

        public Task<RequestResponse<List<EmployeeVacation>>> GetEmployeeVacationsAsync(string userid, CancellationToken? token = null)
        {
            return _real.GetEmployeeVacationsAsync(userid, token);
        }

        public async Task<RequestResponse<TicketValidation>> ValidateTicketAsync(CancellationToken? token = default(CancellationToken?))
        { 
            return await _real.ValidateTicketAsync(token);
        }
    }
}
