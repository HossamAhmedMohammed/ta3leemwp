﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Mock;
using Taaleem.DataServices.Requests;
using Taaleem.Models.PublicServices;
using Taaleem.Models.PublicServices.Request;
using Taaleem.Models.PublicServices.Response;
using Windows.Web.Syndication;

namespace Taaleem.DataServices.Mix
{
    public class MixPublicSectorDataService : IPublicSectorDataService
    {
        private readonly MockPublicSectorDataService _mock;
        private readonly PublicSectorDataService _real;

        public MixPublicSectorDataService(MockPublicSectorDataService mock, PublicSectorDataService real)
        {
            _mock = mock;
            _real = real;
        }

        public Task<RequestResponse<List<SchoolInfo>>> GetAllSchoolsAsync(int typeId, CancellationToken? token = null)
        {
            return _real.GetAllSchoolsAsync(typeId, token);
        }

        public Task<RequestResponse<List<SchoolInfo>>> GetSchoolsByElectricAsync(SchoolsByElecParameters parameters, CancellationToken? token = null)
        {
            return _real.GetSchoolsByElectricAsync(parameters, token);
        }

        public Task<RequestResponse<StageResultAvailability>> GetStageAvailabilityAsync(CancellationToken? token = null)
        {
            return _real.GetStageAvailabilityAsync(token);
        }

        public async Task<RequestResponse<StudentStageResult>> GetStudentStageResultAsync(string seatNumber, CancellationToken? token = null)
        {
            var response = await _real.GetStudentStageResultAsync(seatNumber, token);
            //if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
            //{
            //    response.Result.NameAr = "الطالب";
            //    response.Result.Percentage = "88";
            //}
            return response;
        }

        public Task<RequestResponse<string>> ValidateQatarIdAsync(ValidateQidParameters parameters, CancellationToken? token = null)
        {
            return _real.ValidateQatarIdAsync(parameters, token);
        }

        public Task<RequestResponse<ActivateScholarResponse>> ActivateScholarUserAsync(ScholarshipActivateUserParameters parameters, CancellationToken? token = null)
        {
            return _real.ActivateScholarUserAsync(parameters, token);
        }

        public Task<RequestResponse<List<CountryState>>> GetCountryStatesAsync(GetStatesParameters parameters, CancellationToken? token = null)
        {
            return _real.GetCountryStatesAsync(parameters, token);
        }

        public Task<RequestResponse<List<University>>> GetUniversitiesAsync(GetUniversityParameters parameters, CancellationToken? token = null)
        {
            return _real.GetUniversitiesAsync(parameters, token);
        }

        public Task<RequestResponse<List<Major>>> GetMajorsAsync(GetMajorParameters parameters, CancellationToken? token = null)
        {
            return _real.GetMajorsAsync(parameters, token);
        }

        public Task<RequestResponse<string>> SearchForScholarshipAsync(SearchForScholarsParameters parameters, CancellationToken? token = null)
        {
            return _real.SearchForScholarshipAsync(parameters, token);
        }

        public Task<RequestResponse<NewsDetail>> GetArticleDetailAsync(string id, CancellationToken? token = null)
        {
            return _real.GetArticleDetailAsync(id, token);
        }

        public Task<RequestResponse<IList<SyndicationItem>>> GetNewsAsync(CancellationToken? token = null)
        {
            return _real.GetNewsAsync(token);

        }

        public Task<RequestResponse<List<Country>>> GetCountriesAsync(CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }
     public   Task<RequestResponse<NatejaResult>> GetSecondaryNatigaResult(SecondaryResultParameters parameters, CancellationToken? token = null)
        {
            return _real.GetSecondaryNatigaResult(parameters,token);
        }
    }
}
