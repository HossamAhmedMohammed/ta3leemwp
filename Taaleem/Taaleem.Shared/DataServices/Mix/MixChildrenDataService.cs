﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Mock;
using Taaleem.DataServices.Requests;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;

namespace Taaleem.DataServices.Mix
{
    public class MixChildrenDataService : IChildrenDataService
    {
        private readonly ChildrenDataService _real;
        private readonly MockChildrenDataService _mock;

        public MixChildrenDataService(ChildrenDataService real, MockChildrenDataService mock)
        {
            _real = real;
            _mock = mock;
        }

        public Task<RequestResponse<ChildLoginResponse>> LoginAsync(ChildLoginParameters parameters, string userName, string password, CancellationToken? token = null)
        {
            return _real.LoginAsync(parameters, userName, password, token);
        }

        public Task<RequestResponse<List<Announcement>>> GetAnnouncementsAsync(AnnouncementParameters parameters, CancellationToken? token = null)
        {
            return _real.GetAnnouncementsAsync(parameters, token);
        }

        public Task<RequestResponse<List<ContactTeacher>>> GetStudentTeachersAsync(ContactTeacherParameters parameters, CancellationToken? token = null)
        {
            return _real.GetStudentTeachersAsync(parameters, token);
        }

        public Task<RequestResponse<List<Feed>>> GetFeedsAsync(GetFeedsMethod feedsMethod, FeedParameters parameters, CancellationToken? token = null)
        {
            return _real.GetFeedsAsync(feedsMethod, parameters, token);
        }

        public Task<RequestResponse<List<Behavior>>> GetBehaviorsAsync(BehaviorParameters parameters, CancellationToken? token = null)
        {
            return _real.GetBehaviorsAsync(parameters, token);
        }

        public Task<RequestResponse<List<Assignment>>> GetAssignmentsAsync(AssignmentParameters parameters, CancellationToken? token = null)
        {
            return _real.GetAssignmentsAsync(parameters, token);
        }

        public Task<RequestResponse<List<Reminder>>> GetStudentRemindersAsync(ReminderParameters parameters, CancellationToken? token = null)
        {
            return _real.GetStudentRemindersAsync(parameters, token);
        }

        public Task<RequestResponse<List<Grade>>> GetGradesAsync(GradeParameters parameters, CancellationToken? token = null)
        {
            return _real.GetGradesAsync(parameters, token);
        }

        public Task<RequestResponse<List<Attendance>>> GetStudentSchedulesAsync(GetAttendanceScheduleParameters parameters, CancellationToken? token = null)
        {
            return _real.GetStudentSchedulesAsync(parameters, token);
        }

        public Task<RequestResponse<List<AttendanceStatus>>> GetAttendanceStatusesAsync(AttendanceStatusParameters parameters, CancellationToken? token = null)
        {
            return _real.GetAttendanceStatusesAsync(parameters, token);
        }

        public Task<RequestResponse<List<PrivateNote>>> GetPrivateNotesAsync(GetPrivateNoteParameters parameters, CancellationToken? token = null)
        {
            return _real.GetPrivateNotesAsync(parameters, token);
        }

        public Task<RequestResponse<long>> SendPrivateNoteToTeacherAsync(AddConversationPostParameters parameters, CancellationToken? token = null)
        {
            return _real.SendPrivateNoteToTeacherAsync(parameters, token);
        }

        public Task<RequestResponse<List<FeedCategory>>> GetFeedCategoriesAsync(FeedCategoryParameters parameters, CancellationToken? token = null)
        {
            return _real.GetFeedCategoriesAsync(parameters, token);
        }
    }
}
