﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Requests;
using Taaleem.Models.PrivateSchools.Request;
using Taaleem.Models.PrivateSchools.Response;

namespace Taaleem.DataServices.Interfaces
{
   public interface IPrivateSchoolsDataService
    {
        Task<RequestResponse<List<PrivateSchool>>> SearchSchools (SearchSchoolParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<GetSchoolDetailsResponse>> GetSchoolDetailsByID(string schoolID,
            CancellationToken? token = null);

        Task<RequestResponse<List<SchoolEvent>>> GetSchoolEventsByID(string schoolID ,
            CancellationToken? token = null);

        Task<RequestResponse<List<SchoolReport>>> GetSchoolReportsByID(string schoolID,
            CancellationToken? token = null);

        Task<RequestResponse<SchoolInfoDetails>> GetSchoolInfoDetailsByID(string schoolID ,
            CancellationToken? token = null);

        Task<RequestResponse<EducationalCurriculum>> GetEducationalCurriculums(
            CancellationToken? token = null);
        Task<RequestResponse<EducationalLevel>> GetEducationalLevels(
            CancellationToken? token = null);
        Task<RequestResponse<SchoolType>> GetSchoolTypes(
            CancellationToken? token = null);
        Task<RequestResponse<Region>> GetAllRegions(
            CancellationToken? token = null);
    }
}
