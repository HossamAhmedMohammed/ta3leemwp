﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Requests;
using Taaleem.Models.Employees.Request;
using Taaleem.Models.Employees.Response;

namespace Taaleem.DataServices.Interfaces
{
    public interface IEmployeesDataService
    {
        Task<RequestResponse<EmployeeLoginResponse>> LoginAsync(EmpolyeeLoginBody parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<SalaryDetail>>> GetSalariesAsync(SalaryParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<EmployeeLoan>>> GetEmployeeLoansByStatusAsync(LoansByStatusParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<EmployeeLoan>>> GetAllEmployeeLoansAsync(string userid,
            CancellationToken? token = null);

        Task<RequestResponse<List<Evaluation>>> GetEvaluationsAsync(EvaluationParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<EmployeeVacation>>> GetEmployeeVacationsAsync(string userid,
            CancellationToken? token = null);

        Task<RequestResponse<TicketValidation>> ValidateTicketAsync(CancellationToken? token = null);
    }
}
