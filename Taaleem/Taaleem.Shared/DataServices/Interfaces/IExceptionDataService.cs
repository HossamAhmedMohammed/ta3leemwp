﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Requests;
using Taaleem.Models.Exceptions.Request;
using Taaleem.Models.Exceptions.Response;

namespace Taaleem.DataServices.Interfaces
{
  public  interface IExceptionDataService
    {
        Task<RequestResponse<List<MyExceptionRequestResponse>>> GetMyRequests(
          CancellationToken? token = null);

        Task<RequestResponse<MyExceptionRequestResponse>> GetRequestByID(GetReqestByIDParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<SubmissionDate>> GetSubmissionDate(
            CancellationToken? token = null);

        Task<RequestResponse<List<Nationality>>> GetAllNationalitiesAsync(
            CancellationToken? token = null);

        Task<RequestResponse<List<School>>> GetAllIndependentSchoolsAsync(
            CancellationToken? token = null);

        Task<RequestResponse<List<School>>> GetAllPrivateSchoolsAsync(
            CancellationToken? token = null);

        Task<RequestResponse<AddResponse>> AddApplicant(AddApplicantParameters parameters,
            CancellationToken? token = null);
        Task<RequestResponse<UpdateResponse>> UpdateApplicant(UpdateApplicantParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<Applicant>> GetApplicantByID(int applicantID,
           CancellationToken? token = null);
        Task<RequestResponse<AddResponse>> AddFather(AddFatherParameters parameters,
          CancellationToken? token = null);
        Task<RequestResponse<UpdateResponse>> UpdateFather(UpdateFatherParameters parameters,
          CancellationToken? token = null);
        Task<RequestResponse<Parent>> GetFatherByID(int FatherID,
          CancellationToken? token = null);
        Task<RequestResponse<Parent>> GetFatherByRequestID(int RequestID,
        CancellationToken? token = null);
        Task<RequestResponse<AddResponse>> AddMother(AddMotherParameters parameters,
          CancellationToken? token = null);
        Task<RequestResponse<UpdateResponse>> UpdateMother(UpdateMotherParameters parameters,
         CancellationToken? token = null);
        Task<RequestResponse<ParentM>> GetMotherByID(int MotherID,
         CancellationToken? token = null);
        Task<RequestResponse<ParentM>> GetMotherByRequestID(int RequestID,
      CancellationToken? token = null);
        Task<RequestResponse<AddResponse>> AddBreadwinner(AddBreadwinnerParameters parameters,
         CancellationToken? token = null);
        Task<RequestResponse<UpdateResponse>> UpdateBreadwinner(UpdateBreadwinnerParameters parameters,
      CancellationToken? token = null);
        Task<RequestResponse<Breadwinner>> GetBreadwinnerByID(int BreadwinnerID,
       CancellationToken? token = null);
        Task<RequestResponse<Breadwinner>> GetBreadwinnerByRequestID(int RequestID,
      CancellationToken? token = null);
        Task<RequestResponse<AddResponse>> AddStudent(AddStudentParameters parameters,
        CancellationToken? token = null);
        Task<RequestResponse<UpdateResponse>> UpdateStudent(UpdateStudentParameters parameters,
    CancellationToken? token = null);
        Task<RequestResponse<Student>> GetStudentByID(int StudentID,
        CancellationToken? token = null);
        Task<RequestResponse<Student>> GetStudentByRequestID(int RequestID,
      CancellationToken? token = null);
        Task<RequestResponse<Student>> GetStudentTwinsByRequestID(int RequestID,
    CancellationToken? token = null);

        Task<RequestResponse<AddResponse>> AddChild(AddChildParameters parameters,
       CancellationToken? token = null);
        Task<RequestResponse<UpdateResponse>> UpdateChild(UpdateChildParameters parameters,
    CancellationToken? token = null);
        Task<RequestResponse<Child>> GetChildByID(int ChildID,
        CancellationToken? token = null);
        Task<RequestResponse<List<Child>>> GetAllChildrenByRequestID(int RequestID,
      CancellationToken? token = null);
        Task<RequestResponse<Additionaldata>> GetAdditionalDataByID(int AdditionalDataID,
     CancellationToken? token = null);

        Task<RequestResponse<GetAttachmentFileResponse>> GetAttachmentFile(GetAttachmentParameters parameters,
     CancellationToken? token = null);
        Task<RequestResponse<UpdateResponse>> UpdateAdditionalData(UpdateAdditionalDataParameters parameters,
    CancellationToken? token = null);
    }
}
