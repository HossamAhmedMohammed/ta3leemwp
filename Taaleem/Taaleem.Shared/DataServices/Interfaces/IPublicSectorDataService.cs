﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Requests;
using Taaleem.Models.PublicServices;
using Taaleem.Models.PublicServices.Request;
using Taaleem.Models.PublicServices.Response;
using Windows.Web.Syndication;

namespace Taaleem.DataServices.Interfaces
{
    public interface IPublicSectorDataService
    {
        Task<RequestResponse<List<SchoolInfo>>> GetAllSchoolsAsync(int typeId,
            CancellationToken? token = null);

        Task<RequestResponse<List<SchoolInfo>>> GetSchoolsByElectricAsync(SchoolsByElecParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<StageResultAvailability>> GetStageAvailabilityAsync(
            CancellationToken? token = null);

        Task<RequestResponse<StudentStageResult>> GetStudentStageResultAsync(string seatNumber,
            CancellationToken? token = null);

        Task<RequestResponse<string>> ValidateQatarIdAsync(ValidateQidParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<ActivateScholarResponse>> ActivateScholarUserAsync(ScholarshipActivateUserParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<CountryState>>> GetCountryStatesAsync(GetStatesParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<University>>> GetUniversitiesAsync(GetUniversityParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<Major>>> GetMajorsAsync(GetMajorParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<string>> SearchForScholarshipAsync(SearchForScholarsParameters parameters,
            CancellationToken? token = null);

        /// <summary>
        /// Get Article details like full description and image url
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<RequestResponse<NewsDetail>> GetArticleDetailAsync(string id,
            CancellationToken? token = null);

        Task<RequestResponse<IList<SyndicationItem>>> GetNewsAsync(
            CancellationToken? token = null);
        Task<RequestResponse<List<Country>>> GetCountriesAsync(
            CancellationToken? token = null);

        Task<RequestResponse<NatejaResult>> GetSecondaryNatigaResult(SecondaryResultParameters parameters, CancellationToken? token = null);
    }
}
