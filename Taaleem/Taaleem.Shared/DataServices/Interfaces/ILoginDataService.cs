﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Requests;
using Taaleem.Models.Login.Request;
using Taaleem.Models.Login.Response;

namespace Taaleem.DataServices.Interfaces
{
  public   interface ILoginDataService
    {
        Task<RequestResponse<LoginResponse>> LoginAsync(LoginParameters parameters,
           CancellationToken? token = null);

        Task<RequestResponse<CreateUserResponse>> CreateUser(CreateUserParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<ActivateUserResponse>> ActiveUser(ActivateUserParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<ResendActivateUserResponse>> ResendActivateUser(ResendActivateUserParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<ForgetPasswordResponse>> ForgetPassword(ForgetPasswordParameters parameters,
            CancellationToken? token = null);

    
    }
}
