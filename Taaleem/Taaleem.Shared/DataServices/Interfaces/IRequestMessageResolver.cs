﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.Requests;

namespace Taaleem.DataServices.Interfaces
{
    public interface IRequestMessageResolver
    {
        RequestMessage ResultToMessage(RequestResponse response);
    }
}
