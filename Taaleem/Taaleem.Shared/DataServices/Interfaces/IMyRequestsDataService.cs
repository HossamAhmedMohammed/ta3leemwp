﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Requests;
using Taaleem.Models.Employees.MyRequests.Request;
using Taaleem.Models.Employees.MyRequests.Response;

namespace Taaleem.DataServices.Interfaces
{
    public interface IMyRequestsDataService
    {
        Task<RequestResponse<List<RequestTypesResponse>>> GetRequestTypes(CancellationToken? token = null);
        Task<RequestResponse<RootResponse<LeaveRequestResponse>>> GetLeaveRequests(CancellationToken? token = null);
        Task<RequestResponse<RootResponse<StupidLeaveRequestResponse>>> GetLeaveReturnRequests(CancellationToken? token = null);
        Task<RequestResponse<RootResponse<StupidLeaveRequestResponse>>> GetExitPermitRequests(CancellationToken? token = null);

        Task<RequestResponse<RootResponse<LeaveRequestResponse>>> GetPersonalLoanRequests(string userId, CancellationToken? token = null);
        Task<RequestResponse<RootResponse<StupidLeaveRequestResponse>>> GetDocumentRequests(string userId, CancellationToken? token = null);

        Task<EmployeeByNameResponse> GetEmployeeProfileByQatariId(string qatariId, CancellationToken? token = default(CancellationToken?));

        // Document Request APIs
        Task<List<DocumentRequestType>> GetDocumentRequestTypes(CancellationToken? token = default(CancellationToken?));
        Task<BasicResponse> SubmitNewDocumentRequest(string qatariId, string typeId, string details, CancellationToken? token = default(CancellationToken?));

        // Personal Loan Request APIs
        Task<List<LoanRequestType>> GetLoanRequestTypes(bool isQatari, CancellationToken? token = default(CancellationToken?));
        Task<BasicResponse> SubmitNewPersonalLoanRequest(PersonalLoanRequest request, CancellationToken? token = default(CancellationToken?));
        Task<List<string>> GetLoanPeriodsList(CancellationToken? token = default(CancellationToken?));
        Task<decimal> GetMonthlyInstallment(decimal loanAmount, int paymentPeriodInMonths, CancellationToken? token = default(CancellationToken?));
        Task<bool> IsEligibleforLoan(EmployeeByNameResponse employeeProfile, string loanTypeCode, decimal loanAmount, CancellationToken? token = default(CancellationToken?));
        Task<int> GetMaximumLoanAmount(string qatariId, string loanTypeCode, CancellationToken? token = default(CancellationToken?));

        // Exit request API
        Task<BasicResponse> SubmitNewExitPermitRequest(EmployeeByNameResponse employeeProfile, string date, CancellationToken? token = default(CancellationToken?));


        // Leave Request API
        Task<List<LeaveType>> GetLeaveTypes(string qatariId, CancellationToken? token = default(CancellationToken?));
        Task<bool> IsDuplicateLeave(string qatariId, string leaveTypeId, string startDate, string endDate, CancellationToken? token = default(CancellationToken?));
        Task<BasicResponse> SubmitNewLeaveRequest(LeaveRequestRequest leaveRequest, CancellationToken? token = default(CancellationToken?));


        Task<EmployeeByNameResponse> GetEmployeeByJobId(string jobId, CancellationToken? token = default(CancellationToken?));
        Task<EmployeeByNameResponse> GetEmployeeByName(string employeeName, CancellationToken? token = default(CancellationToken?));


        // Leave Return request APIs
        Task<List<EmployeeApprovedLeave>> GetEmployeeApprovedLeaves(string jobId, CancellationToken? token = default(CancellationToken?));
        Task<bool> IsOnBehalfMember(string jobId);
        Task<bool> IsDuplicateLeaveReturn(string leaveSequence, CancellationToken? token = default(CancellationToken?));
        Task<BasicResponse> SubmitNewLeaveReturnRequest(LeaveReturnRequestRequest leaveReturnRequest, CancellationToken? token = default(CancellationToken?));
    }
}
