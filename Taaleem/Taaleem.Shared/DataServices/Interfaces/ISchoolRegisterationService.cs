﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Requests;
using Taaleem.Models.SchoolRegisteration.Request;
using Taaleem.Models.SchoolRegisteration.Response;

namespace Taaleem.DataServices.Interfaces
{
    public interface ISchoolRegisterationService
    {
        Task<RequestResponse<EmployerTypesResponse>> GetEmployerTypesAsync(CancellationToken? token = null);
        Task<RequestResponse<WorkLocationsResponse>> GetWorkLocationsAsync(WorkLocationsParameters parameters, CancellationToken? token = null);
        Task<RequestResponse<RegisterParentResponse>> RegisterAsParentAsync(RegisterParentParameters parameters, CancellationToken? token = null);
        Task<RequestResponse<ParentLoginResponse>> LoginAsync(ParentLoginParameters parameters,
            CancellationToken? token = null);
        Task<RequestResponse<ParentInfoResponse>> GetParentInfoAsync(CancellationToken? token = null);
        Task<RequestResponse<GradeLevelResponse>> GetGradeLevelsAsync(CancellationToken? token = null);
        Task<RequestResponse<GetRelationshipsResponse>> GetRelationshipsAsync(CancellationToken? token = null);
        Task<RequestResponse<RegisterStudentAvailability>> GetRegisterStudentAvailabilityAsync(CancellationToken? token = null);
        Task<RequestResponse<StudentSearchResponse>> SearchForStudentAsync(SearchStudentParameters parameters, CancellationToken? token = null);
        Task<RequestResponse<GetRegistrationsResponse>> GetRegisterationsStatusAsync(CancellationToken? token = null);
        Task<RequestResponse<GetSchoolsByElecResponse>> GetSchoolsByElectAsync(GetSchoolsByElecParameters parameters, CancellationToken? token = null);
        Task<RequestResponse<GetSchoolsByQarResponse>> GetSchoolsByQarAsync(GetSchoolsByQarParameters parameters, CancellationToken? token = null);
        Task<RequestResponse<GetSchoolContactResponse>> GetSchoolContactAsync(GetSchoolContactQuery parameters, CancellationToken? token = null);
        Task<RequestResponse<UploadDocumentResponse>> UploadDocumentAsync(UploadDocumentParamters parameters, CancellationToken? token = null);
        Task<RequestResponse<RegisterStudentResponse>> RegisterStudentAsync(RegisterStudentParameters parameters, CancellationToken? token = null);
    }
}
