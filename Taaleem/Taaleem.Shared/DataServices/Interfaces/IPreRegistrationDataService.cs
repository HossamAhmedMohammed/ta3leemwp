﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Requests;
using Taaleem.Models.PreRegistration.Request;
using Taaleem.Models.PreRegistration.Response;

namespace Taaleem.DataServices.Interfaces
{
  public  interface IPreRegistrationDataService
    {

        Task<RequestResponse<LoginResponse>> Login(LoginParameters parameters, CancellationToken? token = null);
        Task<RequestResponse<GetRequestsResponse>> GetALLRequests(CancellationToken? token = default(CancellationToken?));
        ////general methods
        Task<RequestResponse<List<EmployerType>>> GetEmployerTypes(CancellationToken? token = null);


        Task<RequestResponse<GetAllSettingsResponse>> GetSettings(CancellationToken? token = null);
        Task<RequestResponse<List<WorkLocation>>> GetWorkLocations(GetWorkLocationsParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<GradeLevel>>GetGradeLevels(CancellationToken? token = null);

        Task<RequestResponse<Relationship>>GetRelationships(CancellationToken? token = null);

        Task<RequestResponse<EnrollmentResponse>>EnrollStudent(EnrollStudentParameters parameters, CancellationToken? token = null);

        Task<RequestResponse<bool>>IsAllNationalitiesAllowedToEnroll(CancellationToken? token = null);

        /// User Methods

        Task<RequestResponse<ResetPasswordPesponse>> ResetPassword(ResetPasswordParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<RegisterAsParentResponse>>RegisterAsParent(RegisterAsParentParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<UpdateParentResponse>> UpdateParent(UpdateParentParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<Parent>> GetParent(CancellationToken? token = null);

        Task<RequestResponse<GetSchoolEnrollmentsResponsecs>> GetRegistrations(CancellationToken? token = null);
        Task<RequestResponse<GetStatesResponse>> GetStates(CancellationToken? token = null);
        Task<RequestResponse<TransferStudentResponse>> TransferStudent(TransferParameters parameters, CancellationToken? token = null);
        Task<RequestResponse<GetSchoolsByStateResponse>> GetSchoolsByState(GetSchoolsByStateParameters parameters, CancellationToken? token = null);
        Task<RequestResponse<SearchStudentResponse>> SearchStudent(SearchStudentParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<GetNearestSchoolsResponse>>GetNearestSchools(GetNearestSchoolsParameters parameters,
          CancellationToken? token = null);
        Task<RequestResponse<GetNearestSchoolsDetailsResponse>> GetNearestSchoolsByQars(GetNearestSchoolsByQarsParameters parameters,
        CancellationToken? token = null);
        Task<RequestResponse<List<EnrollmentResponse>>> GetAlternateSchools(GetAlternateSchoolsParameters parameters,
             CancellationToken? token = null);
        Task<RequestResponse<List<ReceiveFileResponse>>> GetSchoolContact(Guid schoolID,
             CancellationToken? token = null);
        Task<RequestResponse<bool>> GetValidationCode(GetValidationCodeParameters parameters,
            CancellationToken? token = null);
        Task<RequestResponse<bool>> RegisterStudent(RegisterStudentParameters parameters,
            CancellationToken? token = null);
        Task<RequestResponse<ReceiveFileResponse>> ReceiveFile(ReceiveFileParameters parameters,
            CancellationToken? token = null);
        Task<RequestResponse<Job>> PrintReceipt(Guid enrollmentID,
             CancellationToken? token = null);
        Task<RequestResponse<Job>> GetJob(Guid ID,
             CancellationToken? token = null);


    }
}
