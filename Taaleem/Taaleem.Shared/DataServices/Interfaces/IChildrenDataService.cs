﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using System.Text;
using Taaleem.DataServices.Requests;
using Taaleem.Models.Children.Response;
using Taaleem.Models.Children.Request;

namespace Taaleem.DataServices.Interfaces
{
 public   interface IChildrenDataService
    {
        Task<RequestResponse<ChildLoginResponse>> LoginAsync(ChildLoginParameters parameters, String userName, String password, CancellationToken? token = null);

        Task<RequestResponse<List<Announcement>>> GetAnnouncementsAsync(AnnouncementParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<ContactTeacher>>> GetStudentTeachersAsync(ContactTeacherParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<Feed>>> GetFeedsAsync(GetFeedsMethod feedsMethod, FeedParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<Behavior>>> GetBehaviorsAsync(BehaviorParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<Assignment>>> GetAssignmentsAsync(AssignmentParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<Reminder>>> GetStudentRemindersAsync(ReminderParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<Grade>>> GetGradesAsync(GradeParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<Attendance>>> GetStudentSchedulesAsync(GetAttendanceScheduleParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<AttendanceStatus>>> GetAttendanceStatusesAsync(AttendanceStatusParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<PrivateNote>>> GetPrivateNotesAsync(GetPrivateNoteParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<long>> SendPrivateNoteToTeacherAsync(AddConversationPostParameters parameters,
            CancellationToken? token = null);

        Task<RequestResponse<List<FeedCategory>>> GetFeedCategoriesAsync(FeedCategoryParameters parameters,
            CancellationToken? token = null);
    }
}
