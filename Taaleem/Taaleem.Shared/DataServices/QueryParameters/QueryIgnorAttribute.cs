﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.DataServices.QueryParameters
{
    [AttributeUsage(AttributeTargets.Property)]
    internal sealed class QueryIgnoreAttribute : Attribute
    {
    }
}

