﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.QueryParameters;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.SchoolRegisteration.Request;
using Taaleem.Models.SchoolRegisteration.Response;

namespace Taaleem.DataServices
{
    public class SchoolRegisterationService : ISchoolRegisterationService
    {
        private readonly Func<BaseRequest> _requestFactory;
        private readonly IAppSettings _appSettings;
        private CookieCollectionHttpClientHandler _handler;
        private const string BaseUrl = "https://tasjeel.sec.gov.qa/";

        public String UserName
        {
            get
            {
                return _appSettings.SchoolRegisterationCredential.UserName;
            }
        }

        public String Password
        {
            get
            {
                return _appSettings.SchoolRegisterationCredential.Password;
            }
        }

        public SchoolRegisterationService(Func<BaseRequest> requestFactory, IAppSettings appSettings)
        {
            _requestFactory = requestFactory;
            _appSettings = appSettings;
        }

        private CookieCollectionHttpClientHandler GetAuthHandler()
        {
            return _handler ?? (_handler = new CookieCollectionHttpClientHandler());
        }

        public async Task<RequestResponse<ParentLoginResponse>> LoginAsync(ParentLoginParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Authentication_JSON_AppService.axd/Login");
            request.HttpMessageHandler = GetAuthHandler();
            request.DisposeHandler = false;
            return await request.PostAsync<ParentLoginResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        private async Task<RequestResponse<ParentLoginResponse>> InternalLoginAsync(CancellationToken? token = null)
        {
            return await LoginAsync(new ParentLoginParameters() { UserName = UserName, Password = Password, CreatePersistentCookie = false }, token);
        }

        public async Task<RequestResponse<T>> PostAsyncWithAuth<T>(BaseRequest request, string content, Encoding encoding, string mediaType, CancellationToken? token = null)
        {
            var handler = GetAuthHandler();
            RequestResponse<T> firstAttempt = null;
            if (handler.HasCookies)
                firstAttempt = await request.PostAsync<T>(new StringContent(content, encoding, mediaType), token);

            //Login in two condition, when there are no cookies in handler
            //Or when the first attempt returns Http Server error[Currently Commented]
            if (firstAttempt == null /*|| firstAttempt.StatusCode == HttpStatusCode.InternalServerError*/)
            {
                var loginResult = await InternalLoginAsync(token);

                //if login failed, return the response status and code
                if (!(loginResult.ResponseStatus == ResponseStatus.SuccessWithResult && loginResult.Result.Success))
                    return new RequestResponse<T>() { ResponseStatus = loginResult.ResponseStatus, StatusCode = loginResult.StatusCode };

                //reaching here means login completed, so retry the request again
                var secondAttempt = await request.PostAsync<T>(new StringContent(content, encoding, mediaType), token);
                return secondAttempt;
            }

            return firstAttempt;
        }

        public async Task<RequestResponse<ParentInfoResponse>> GetParentInfoAsync(CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/GetParent");
            request.HttpMessageHandler = GetAuthHandler();
            request.DisposeHandler = false;
            return await PostAsyncWithAuth<ParentInfoResponse>(request, "", Encoding.UTF8, "application/json", token);
        }

        public async Task<RequestResponse<GetRegistrationsResponse>> GetRegisterationsStatusAsync(CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/GetRegistrations");
            request.HttpMessageHandler = GetAuthHandler();
            request.DisposeHandler = false;
            return await PostAsyncWithAuth<GetRegistrationsResponse>(request, "", Encoding.UTF8, "application/json", token);
        }

        public async Task<RequestResponse<StudentSearchResponse>> SearchForStudentAsync(SearchStudentParameters parameters, CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/SearchStudent");
            request.HttpMessageHandler = GetAuthHandler();
            request.DisposeHandler = false;
            return await PostAsyncWithAuth<StudentSearchResponse>(request, JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json", token);
        }

        public async Task<RequestResponse<GetSchoolsByQarResponse>> GetSchoolsByQarAsync(GetSchoolsByQarParameters parameters, CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/GetNearestSchoolsByQars");
            request.HttpMessageHandler = GetAuthHandler();
            request.DisposeHandler = false;
            return await PostAsyncWithAuth<GetSchoolsByQarResponse>(request, JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json", token);
        }


        public async Task<RequestResponse<GetSchoolsByElecResponse>> GetSchoolsByElectAsync(GetSchoolsByElecParameters parameters, CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/GetNearestSchools");
            request.HttpMessageHandler = GetAuthHandler();
            request.DisposeHandler = false;
            return await PostAsyncWithAuth<GetSchoolsByElecResponse>(request, JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json", token);
        }

        public async Task<RequestResponse<UploadDocumentResponse>> UploadDocumentAsync(UploadDocumentParamters parameters, CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/ReceiveFile");
            request.HttpMessageHandler = GetAuthHandler();
            request.DisposeHandler = false;
            return await PostAsyncWithAuth<UploadDocumentResponse>(request, JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json", token);
        }

        public async Task<RequestResponse<RegisterStudentResponse>> RegisterStudentAsync(RegisterStudentParameters parameters, CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/RegisterStudent");
            request.HttpMessageHandler = GetAuthHandler();
            request.DisposeHandler = false;
            return await PostAsyncWithAuth<RegisterStudentResponse>(request, JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json", token);
        }

        public async Task<RequestResponse<GetSchoolContactResponse>> GetSchoolContactAsync(GetSchoolContactQuery parameters, CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/GetSchoolContact".AppendQueryString(parameters));
            request.HttpMessageHandler = GetAuthHandler();
            request.DisposeHandler = false;
            return await request.GetAsync<GetSchoolContactResponse>(token);
        }

        #region  No Authentication methods

        public async Task<RequestResponse<RegisterStudentAvailability>> GetRegisterStudentAvailabilityAsync(CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/IsWebRegistrationEnabled");
            return await request.PostAsync<RegisterStudentAvailability>(new StringContent("", Encoding.UTF8), token);
        }

        public async Task<RequestResponse<GetRelationshipsResponse>> GetRelationshipsAsync(CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/GetRelationships");
            return await request.PostAsync<GetRelationshipsResponse>(new StringContent("", Encoding.UTF8), token);
        }

        public async Task<RequestResponse<GradeLevelResponse>> GetGradeLevelsAsync(CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/GetGradeLevels");
            return await request.PostAsync<GradeLevelResponse>(new StringContent("", Encoding.UTF8), token);
        }


        public async Task<RequestResponse<EmployerTypesResponse>> GetEmployerTypesAsync(CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/GetEmployerTypes");
            return await request.PostAsync<EmployerTypesResponse>(new StringContent("", Encoding.UTF8), token);
        }

        public async Task<RequestResponse<WorkLocationsResponse>> GetWorkLocationsAsync(WorkLocationsParameters parameters, CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/GetWorkLocations");
            return await request.PostAsync<WorkLocationsResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<RegisterParentResponse>> RegisterAsParentAsync(RegisterParentParameters parameters, CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "Atlas-K12NET-EarlyRegistration-Web-ERDomainService.svc/json/RegisterAsParent");
            return await request.PostAsync<RegisterParentResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }
        #endregion
    }
}
