﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.QueryParameters;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.PublicServices;
using Taaleem.Models.PublicServices.Request;
using Taaleem.Models.PublicServices.Response;
using Windows.Web.Syndication;

namespace Taaleem.DataServices
{
    public class PublicSectorDataService : IPublicSectorDataService
    {
        private readonly Func<BaseRequest> _requestFactory;
        private const string BaseUrl = "http://eservices.sec.gov.qa/";
        private const string NatigaBaseUrl = "http://eservices-dev1.sec.gov.qa/";//Mock Server - TODO:Change to real when it's available from sec
        private const string ScholarshipBaseUrl = "https://scholarship.sec.gov.qa/";


        public PublicSectorDataService(INetworkHelper networkHelper, Func<BaseRequest> requestFactory)
        {
            _requestFactory = requestFactory;
        }

        public async Task<RequestResponse<List<SchoolInfo>>> GetAllSchoolsAsync(int typeId,
            CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "WhereIsMySchoolRest/Service1.svc/GetAllSchools/" + typeId);
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<List<SchoolInfo>>(token);
        }

        public async Task<RequestResponse<List<SchoolInfo>>> GetSchoolsByElectricAsync(SchoolsByElecParameters parameters,
            CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "WhereIsMySchoolRest/Service1.svc/GetSchoolsByElec").AppendQueryString(parameters);
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<List<SchoolInfo>>(token);
        }

        public async Task<RequestResponse<StageResultAvailability>> GetStageAvailabilityAsync(
           CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (NatigaBaseUrl + "natija_web_service/ServiceStatus.svc/GetNatijaStatus");
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<StageResultAvailability>(token);
        }

        public async Task<RequestResponse<StudentStageResult>> GetStudentStageResultAsync(string seatNumber,
          CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (NatigaBaseUrl + "natija_web_service/Service1.svc/GetStudentData/" + seatNumber);
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.GetAsync<StudentStageResult>(token);
        }


        public async Task<RequestResponse<string>> ValidateQatarIdAsync(ValidateQidParameters parameters,
       CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (ScholarshipBaseUrl + "Public/ValidateQid");
            request.HttpMessageHandler = new HttpClientHandler();
            request.ResultContentType = ContentType.Text;
            return await request.PostAsync<string>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<ActivateScholarResponse>> ActivateScholarUserAsync(ScholarshipActivateUserParameters parameters,
      CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (ScholarshipBaseUrl + "Public/ActivateUser");
            request.HttpMessageHandler = new HttpClientHandler();
            return await request.PostAsync<ActivateScholarResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<List<CountryState>>> GetCountryStatesAsync(GetStatesParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (ScholarshipBaseUrl + "Public/GetStates");
            request.HttpMessageHandler = new HttpClientHandler();
            request.ResultContentType = ContentType.Json;
            return await request.PostAsync<List<CountryState>>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }
        public async Task<RequestResponse<List<Country>>> GetCountriesAsync(
      CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = @"https://scholarship.sec.gov.qa/public/GetCountries";
            request.HttpMessageHandler = new HttpClientHandler();
            request.ResultContentType = ContentType.Json;
            // var stringReponse = 
            return await request.GetAsync<List<Country>>(token);
        }
        
        public async Task<RequestResponse<List<University>>> GetUniversitiesAsync(GetUniversityParameters parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (ScholarshipBaseUrl + "Public/GetUniversity");
            request.HttpMessageHandler = new HttpClientHandler();
            request.ResultContentType = ContentType.Json;
            return await request.PostAsync<List<University>>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<List<Major>>> GetMajorsAsync(GetMajorParameters parameters,
       CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (ScholarshipBaseUrl + "Public/GetMajor4Unv");
            request.HttpMessageHandler = new HttpClientHandler();
            request.ResultContentType = ContentType.Json;
            return await request.PostAsync<List<Major>>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<string>> SearchForScholarshipAsync(SearchForScholarsParameters parameters,
       CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (ScholarshipBaseUrl + "Public/GetMajors4University");
            request.HttpMessageHandler = new HttpClientHandler();
            request.ResultContentType = ContentType.Text;
            return await request.PostAsync<string>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<IList<SyndicationItem>>> GetNewsAsync(
          CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                request.RequestUrl = @"http://www.sec.gov.qa/Ar/Media/News/_layouts/Mannai.CustomControls.SharePoint/CustomRSSFeeds.aspx?ListID=47DE0792-6CCF-490C-95E4-DC468F9FC4BF&DispUrl=/Ar/Media/News/Pages/NewsDetails.aspx?NewsID&ItemsUrl=/Ar/Media/News/Pages/NewsListing.aspx&lan=ar&view=5bf3d518-fa72-4bb1-ac8f-971ebb4566e6";
            else
                request.RequestUrl = @"http://www.edu.gov.qa/En/Media/News/_layouts/listfeed.aspx?List=3a28a9c9-68b4-4234-9b12-b341883e0b2d&View=45e4111b-7aa0-4006-860d-d4ec9e776f1e";
            request.HttpMessageHandler = new HttpClientHandler();
            request.ResultContentType = ContentType.Text;
            var stringReponse = await request.GetAsync<string>(token);
            var syndicationResponse = new RequestResponse<IList<SyndicationItem>>()
            {
                ResponseStatus = stringReponse.ResponseStatus,
                StatusCode = stringReponse.StatusCode
            };

            if (syndicationResponse.ResponseStatus != ResponseStatus.SuccessWithResult) return syndicationResponse;

            try
            {
                //Parse Feeds
                var feedNews = new SyndicationFeed();
                feedNews.Load(stringReponse.Result);
                //foreach(var item in feedNews.Items)
                //{
                //    string temp = item.
                //}
                if (feedNews.Items.Count == 0)
                {
                    syndicationResponse.ResponseStatus = ResponseStatus.SuccessWithNoData;
                }
                else
                {
                    syndicationResponse.Result = feedNews.Items;
                    syndicationResponse.ResponseStatus = ResponseStatus.SuccessWithResult;
                }
            }
            catch (Exception)
            {
                syndicationResponse.ResponseStatus = ResponseStatus.ParserException;
            }
            return syndicationResponse;
        }

        /// <summary>
        /// Get Article details like full description and image url
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<RequestResponse<NewsDetail>> GetArticleDetailAsync(string id, CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = "http://svncode.cloudapp.net:9000/news/" + id;
            request.HttpMessageHandler = new HttpClientHandler();
            request.ResultContentType = ContentType.Json;
            return await request.GetAsync<NewsDetail>(token);
        }

        /// <summary>
        /// Get Article details like full description and image url
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<RequestResponse<NatejaResult>> GetSecondaryNatigaResult(SecondaryResultParameters parameters, CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl =  "https://nateeja.edu.gov.qa/Atlas-K12NET-ScorePublisher-nSIS-ScorePublisherDomainService.svc/json/GetEnrollment";
            request.HttpMessageHandler = new HttpClientHandler();
            request.ResultContentType = ContentType.Json;
            return await request.PostAsync<NatejaResult>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }
    }
}
