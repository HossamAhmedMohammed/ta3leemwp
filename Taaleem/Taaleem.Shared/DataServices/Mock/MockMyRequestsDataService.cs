﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Models.Employees.MyRequests.Request;
using Taaleem.Models.Employees.MyRequests.Response;

namespace Taaleem.DataServices.Mock
{
    class MockMyRequestsDataService : MockServiceBase, IMyRequestsDataService
    {
        public async Task<RequestResponse<RootResponse<StupidLeaveRequestResponse>>> GetDocumentRequests(string userId, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<RootResponse<StupidLeaveRequestResponse>>("ms-appx:///ModelsMock/Employees/MyRequests/DocumentRequestResponse.json");
            return new RequestResponse<RootResponse<StupidLeaveRequestResponse>>() { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<RootResponse<StupidLeaveRequestResponse>>> GetExitPermitRequests(CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<RootResponse<StupidLeaveRequestResponse>>("ms-appx:///ModelsMock/Employees/MyRequests/ExitPermitRequestResponse.json");
            return new RequestResponse<RootResponse<StupidLeaveRequestResponse>>() { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<RootResponse<LeaveRequestResponse>>> GetLeaveRequests(CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<RootResponse<LeaveRequestResponse>>("ms-appx:///ModelsMock/Employees/MyRequests/LeaveRequestResponse.json");
            return new RequestResponse<RootResponse<LeaveRequestResponse>>() { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<RootResponse<StupidLeaveRequestResponse>>> GetLeaveReturnRequests(CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<RootResponse<StupidLeaveRequestResponse>>("ms-appx:///ModelsMock/Employees/MyRequests/LeaveReturnRequestResponse.json");
            return new RequestResponse<RootResponse<StupidLeaveRequestResponse>>() { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<RootResponse<LeaveRequestResponse>>> GetPersonalLoanRequests(string userId, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<RootResponse<LeaveRequestResponse>>("ms-appx:///ModelsMock/Employees/MyRequests/PersonalLoanRequestResponse.json");
            return new RequestResponse<RootResponse<LeaveRequestResponse>>() { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<RequestTypesResponse>>> GetRequestTypes(CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<List<RequestTypesResponse>>("ms-appx:///ModelsMock/Employees/MyRequests/RequestTypes.json");
            return new RequestResponse<List<RequestTypesResponse>>() { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<List<DocumentRequestType>> GetDocumentRequestTypes(CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<List<DocumentRequestType>>("ms-appx:///ModelsMock/Employees/MyRequests/DocumentRequest/DocumentRequestTypesResponse.json");
            return response;
        }

        public async Task<EmployeeByNameResponse> GetEmployeeProfileByQatariId(string qatariId, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<EmployeeByNameResponse>("ms-appx:///ModelsMock/Employees/MyRequests/DocumentRequest/EmployeeProfileByQatariId.json");
            return response;
        }

        public async Task<BasicResponse> SubmitNewDocumentRequest(string qatariId, string typeId, string details, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<List<BasicResponse>>("ms-appx:///ModelsMock/Employees/MyRequests/DocumentRequest/DocumentRequestTSubmitResponse.json");
            return response!= null && response.Count>0? response[0]: null;
        }

        public async Task<List<LoanRequestType>> GetLoanRequestTypes(bool isQatari, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<List<LoanRequestType>>("ms-appx:///ModelsMock/Employees/MyRequests/PersonalLoanRequest/PersonalLoanTypesResponse.json");
            return response;
        }

        public async Task<BasicResponse> SubmitNewPersonalLoanRequest(PersonalLoanRequest request, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<List<BasicResponse>>("ms-appx:///ModelsMock/Employees/MyRequests/PersonalLoanRequest/PersonaLoanResponse.json");
            return response != null && response.Count > 0 ? response[0] : null;
        }

        public async Task<List<string>> GetLoanPeriodsList(CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<List<string>>("ms-appx:///ModelsMock/Employees/MyRequests/PersonalLoanRequest/LoanPeriodsList.json");
            return response;
        }

        public async Task<decimal> GetMonthlyInstallment(decimal loanAmount, int paymentPeriodInMonths, CancellationToken? token = default(CancellationToken?))
        {
            await Task.Delay(100);
            if (paymentPeriodInMonths == 0)
                paymentPeriodInMonths = 1;
            return Math.Round(loanAmount / paymentPeriodInMonths, 0);
        }

        public async Task<bool> IsEligibleforLoan(EmployeeByNameResponse employeeProfile, string loanTypeCode, decimal loanAmount, CancellationToken? token = default(CancellationToken?))
        {
            await Task.Delay(50);
            return true;
        }

        public async Task<BasicResponse> SubmitNewExitPermitRequest(EmployeeByNameResponse employeeProfile, string date, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<List<BasicResponse>>("ms-appx:///ModelsMock/Employees/MyRequests/ExitPermitRequest/SubmitResponse.json");
            return response != null && response.Count > 0 ? response[0] : null;
        }

        public async Task<List<LeaveType>> GetLeaveTypes(string qatariId, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<List<LeaveType>>("ms-appx:///ModelsMock/Employees/MyRequests/LeaveRequest/GetLeaveTypesResponse.json");
            return response;
        }

        public async Task<bool> IsDuplicateLeave(string qatariId, string leaveTypeId, string startDate, string endDate, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<bool>("ms-appx:///ModelsMock/Employees/MyRequests/LeaveRequest/IsDuplicateLeaveResponse.json");
            return response;
        }

        public async Task<BasicResponse> SubmitNewLeaveRequest(LeaveRequestRequest leaveRequest, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<List<BasicResponse>>("ms-appx:///ModelsMock/Employees/MyRequests/LeaveRequest/LeaveRequestResponse.json");
            return response[0];
        }

        public async Task<EmployeeByNameResponse> GetEmployeeByJobId(string jobId, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<EmployeeByNameResponse>("ms-appx:///ModelsMock/Employees/MyRequests/DocumentRequest/EmployeeProfileByQatariId.json");
            return response;
        }

        public async Task<EmployeeByNameResponse> GetEmployeeByName(string employeeName, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<EmployeeByNameResponse>("ms-appx:///ModelsMock/Employees/MyRequests/DocumentRequest/EmployeeProfileByQatariId.json");
            return response;
        }

        public async Task<int> GetMaximumLoanAmount(string qatariId, string loanTypeCode, CancellationToken? token = default(CancellationToken?))
        {
            await Task.Delay(50);
            return 40000;
        }

        public async Task<List<EmployeeApprovedLeave>> GetEmployeeApprovedLeaves(string jobId, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<List<EmployeeApprovedLeave>>("ms-appx:///ModelsMock/Employees/MyRequests/LeaveReturn/EmployeeApprovedLeaves.json");
            return response;
        }

        public async Task<bool> IsDuplicateLeaveReturn(string leaveSequence, CancellationToken? token = default(CancellationToken?))
        {
            await Task.Delay(50);
            return false;
        }

        public async Task<BasicResponse> SubmitNewLeaveReturnRequest(LeaveReturnRequestRequest leaveReturnRequest, CancellationToken? token = default(CancellationToken?))
        {
            await Task.Delay(50);
            return new BasicResponse
            {
                ResponseCode = "2",
                ResponseNameAr = "الفشل",
                ResponseNameEn = "Failure"
            };
        }

        public async Task<bool> IsOnBehalfMember(string jobId)
        {
            await Task.Delay(50);
            return true;
        }
    }
}
