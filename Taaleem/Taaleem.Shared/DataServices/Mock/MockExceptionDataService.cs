﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Models.Exceptions.Request;
using Taaleem.Models.Exceptions.Response;

namespace Taaleem.DataServices.Mock
{
    public class MockExceptionDataService :  MockServiceBase, IExceptionDataService
    {
        public async Task<RequestResponse<AddResponse>> AddApplicant(AddApplicantParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            var response = await GetResponse<AddResponse>("ms-appx:///ModelsMock/Exception/Request/AddApplicant.json");
            return new RequestResponse<AddResponse>() { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public Task<RequestResponse<AddResponse>> AddBreadwinner(AddBreadwinnerParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<AddResponse>> AddChild(AddChildParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<AddResponse>> AddFather(AddFatherParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<AddResponse>> AddMother(AddMotherParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<AddResponse>> AddStudent(AddStudentParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<Additionaldata>> GetAdditionalDataByID(int AdditionalDataID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<List<Child>>> GetAllChildrenByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<List<School>>> GetAllIndependentSchoolsAsync(CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<List<Nationality>>> GetAllNationalitiesAsync(CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<List<School>>> GetAllPrivateSchoolsAsync(CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<Applicant>> GetApplicantByID(int applicantID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<GetAttachmentFileResponse>> GetAttachmentFile(GetAttachmentParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<Breadwinner>> GetBreadwinnerByID(int BreadwinnerID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<Breadwinner>> GetBreadwinnerByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<Child>> GetChildByID(int ChildID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<Parent>> GetFatherByID(int FatherID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<Parent>> GetFatherByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<ParentM>> GetMotherByID(int MotherID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<ParentM>> GetMotherByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<List<MyExceptionRequestResponse>>> GetMyRequests(CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<MyExceptionRequestResponse>> GetRequestByID(GetReqestByIDParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<Student>> GetStudentByID(int StudentID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<Student>> GetStudentByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<Student>> GetStudentTwinsByRequestID(int RequestID, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<SubmissionDate>> GetSubmissionDate(CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<UpdateResponse>> UpdateAdditionalData(UpdateAdditionalDataParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<UpdateResponse>> UpdateApplicant(UpdateApplicantParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<UpdateResponse>> UpdateBreadwinner(UpdateBreadwinnerParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<UpdateResponse>> UpdateChild(UpdateChildParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<UpdateResponse>> UpdateFather(UpdateFatherParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<UpdateResponse>> UpdateMother(UpdateMotherParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }

        public Task<RequestResponse<UpdateResponse>> UpdateStudent(UpdateStudentParameters parameters, CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }
    }
}
