﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Models.Employees.Request;
using Taaleem.Models.Employees.Response;

namespace Taaleem.DataServices.Mock
{
    public class MockEmployeesDataService : MockServiceBase, IEmployeesDataService
    {
        public async Task<RequestResponse<EmployeeLoginResponse>> LoginAsync(EmpolyeeLoginBody parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<EmployeeLoginResponse>("ms-appx:///ModelsMock/Employees/Login.json");
            return new RequestResponse<EmployeeLoginResponse>() { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<SalaryDetail>>> GetSalariesAsync(SalaryParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<SalaryDetail>>("ms-appx:///ModelsMock/Employees/SalaryDetails.json");
            return new RequestResponse<List<SalaryDetail>>() { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<EmployeeLoan>>> GetEmployeeLoansByStatusAsync(LoansByStatusParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<EmployeeLoan>>("ms-appx:///ModelsMock/Employees/Loans.json");
            string status = parameters.Status == "1" ? "Active" : "Completed";
            var filtered = response.Where(item => item.Status == status).ToList();
            return new RequestResponse<List<EmployeeLoan>>() { Result = filtered, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<EmployeeLoan>>> GetAllEmployeeLoansAsync(string userid, CancellationToken? token = null)
        {
            var response = await GetResponse<List<EmployeeLoan>>("ms-appx:///ModelsMock/Employees/Loans.json");
            return new RequestResponse<List<EmployeeLoan>>() { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<Evaluation>>> GetEvaluationsAsync(EvaluationParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<Evaluation>>("ms-appx:///ModelsMock/Employees/Evaluations.json");
            var filtered = response;
            if (parameters.Year != "All")
            {
                filtered = response.Where(item => String.Equals(item.Year, parameters.Year, StringComparison.OrdinalIgnoreCase))
                        .ToList();
            }
            return new RequestResponse<List<Evaluation>>() { Result = filtered, ResponseStatus = filtered.Count > 0 ? ResponseStatus.SuccessWithResult : ResponseStatus.SuccessWithNoData };
        }


        public async Task<RequestResponse<List<EmployeeVacation>>> GetEmployeeVacationsAsync(string userid, CancellationToken? token = null)
        {
            var response = await GetResponse<List<EmployeeVacation>>("ms-appx:///ModelsMock/Employees/Vacations.json");
            return new RequestResponse<List<EmployeeVacation>>() { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public Task<RequestResponse<TicketValidation>> ValidateTicketAsync(CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }
    }
}
