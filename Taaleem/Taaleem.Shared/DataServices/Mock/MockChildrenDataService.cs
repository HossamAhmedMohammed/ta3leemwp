﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;

namespace Taaleem.DataServices.Mock
{
    public class MockChildrenDataService : MockServiceBase, IChildrenDataService
    {
        public async Task<RequestResponse<ChildLoginResponse>> LoginAsync(ChildLoginParameters parameters, string userName, string password, CancellationToken? token = null)
        {
            var response = await GetResponse<ChildLoginResponse>("ms-appx:///ModelsMock/Children/Login.json");
            return new RequestResponse<ChildLoginResponse> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<Announcement>>> GetAnnouncementsAsync(AnnouncementParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<Announcement>>("ms-appx:///ModelsMock/Children/Announcements.json");
            return new RequestResponse<List<Announcement>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<ContactTeacher>>> GetStudentTeachersAsync(ContactTeacherParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<ContactTeacher>>("ms-appx:///ModelsMock/Children/Teachers.json");
            return new RequestResponse<List<ContactTeacher>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<Feed>>> GetFeedsAsync(GetFeedsMethod feedsMethod, FeedParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<Feed>>("ms-appx:///ModelsMock/Children/Feeds.json");
            return new RequestResponse<List<Feed>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<Behavior>>> GetBehaviorsAsync(BehaviorParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<Behavior>>("ms-appx:///ModelsMock/Children/Behaviors.json");
            return new RequestResponse<List<Behavior>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<Assignment>>> GetAssignmentsAsync(AssignmentParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<Assignment>>("ms-appx:///ModelsMock/Children/Assignments.json");
            return new RequestResponse<List<Assignment>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<Reminder>>> GetStudentRemindersAsync(ReminderParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<Reminder>>("ms-appx:///ModelsMock/Children/Reminders.json");
            return new RequestResponse<List<Reminder>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<Grade>>> GetGradesAsync(GradeParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<Grade>>("ms-appx:///ModelsMock/Children/Grades.json");
            return new RequestResponse<List<Grade>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<Attendance>>> GetStudentSchedulesAsync(GetAttendanceScheduleParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<Attendance>>("ms-appx:///ModelsMock/Children/Attendances.json");
            return new RequestResponse<List<Attendance>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }
        public async Task<RequestResponse<List<AttendanceStatus>>> GetAttendanceStatusesAsync(AttendanceStatusParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<AttendanceStatus>>("ms-appx:///ModelsMock/Children/AttendanceStatuses.json");
            return new RequestResponse<List<AttendanceStatus>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<PrivateNote>>> GetPrivateNotesAsync(GetPrivateNoteParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<PrivateNote>>("ms-appx:///ModelsMock/Children/PrivateNotes.json");
            return new RequestResponse<List<PrivateNote>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<long>> SendPrivateNoteToTeacherAsync(AddConversationPostParameters parameters, CancellationToken? token = null)
        {
            await Task.Delay(1500);
            return new RequestResponse<long> { Result = 1, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<FeedCategory>>> GetFeedCategoriesAsync(FeedCategoryParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<FeedCategory>>("ms-appx:///ModelsMock/Children/FeedCategories.json");
            return new RequestResponse<List<FeedCategory>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }
    }
}
