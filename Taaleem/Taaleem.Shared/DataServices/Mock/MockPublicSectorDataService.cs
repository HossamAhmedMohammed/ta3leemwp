﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Models.PublicServices;
using Taaleem.Models.PublicServices.Request;
using Taaleem.Models.PublicServices.Response;
using Windows.Storage;
using Windows.Web.Syndication;

namespace Taaleem.DataServices.Mock
{
    public class MockPublicSectorDataService : MockServiceBase, IPublicSectorDataService
    {
        public async Task<RequestResponse<List<SchoolInfo>>> GetAllSchoolsAsync(int typeId, CancellationToken? token = null)
        {
            var response = await GetResponse<List<SchoolInfo>>("ms-appx:///ModelsMock/PublicServices/Schools.json");
            foreach (var schoolInfo in response)
            {
                schoolInfo.ElecX = null;
                schoolInfo.ElecY = null;
                schoolInfo.Elec = null;
            }
            return new RequestResponse<List<SchoolInfo>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<SchoolInfo>>> GetSchoolsByElectricAsync(SchoolsByElecParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<SchoolInfo>>("ms-appx:///ModelsMock/PublicServices/Schools.json");
            return new RequestResponse<List<SchoolInfo>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<StageResultAvailability>> GetStageAvailabilityAsync(CancellationToken? token = null)
        {
            var response = await GetResponse<StageResultAvailability>("ms-appx:///ModelsMock/PublicServices/NatigaStatus.json");
            return new RequestResponse<StageResultAvailability> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<StudentStageResult>> GetStudentStageResultAsync(string seatNumber, CancellationToken? token = null)
        {
            var response = await GetResponse<StudentStageResult>("ms-appx:///ModelsMock/PublicServices/StudentNatigaResult.json");
            return new RequestResponse<StudentStageResult> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<string>> ValidateQatarIdAsync(ValidateQidParameters parameters, CancellationToken? token = null)
        {
            await DelayAsync();
            return new RequestResponse<string> { /*Result = "رقم غير صحيح,"*/ ResponseStatus = ResponseStatus.SuccessWithNoData };
        }

        public async Task<RequestResponse<ActivateScholarResponse>> ActivateScholarUserAsync(ScholarshipActivateUserParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<ActivateScholarResponse>("ms-appx:///ModelsMock/PublicServices/activatescholar.json");
            return new RequestResponse<ActivateScholarResponse> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<CountryState>>> GetCountryStatesAsync(GetStatesParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<CountryState>>("ms-appx:///ModelsMock/PublicServices/states.json");
            return new RequestResponse<List<CountryState>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<University>>> GetUniversitiesAsync(GetUniversityParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<University>>("ms-appx:///ModelsMock/PublicServices/universities.json");
            return new RequestResponse<List<University>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<List<Major>>> GetMajorsAsync(GetMajorParameters parameters, CancellationToken? token = null)
        {
            var response = await GetResponse<List<Major>>("ms-appx:///ModelsMock/PublicServices/majors.json");
            return new RequestResponse<List<Major>> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<string>> SearchForScholarshipAsync(SearchForScholarsParameters parameters, CancellationToken? token = null)
        {
            await DelayAsync();
            Uri dataUri = new Uri(@"ms-appx:///ModelsMock/PublicServices/scholarshipSearch.txt");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string text = await FileIO.ReadTextAsync(file);
            return new RequestResponse<string> { Result = text, ResponseStatus = ResponseStatus.SuccessWithResult };

        }

        public async Task<RequestResponse<NewsDetail>> GetArticleDetailAsync(string id, CancellationToken? token = null)
        {
            var response = await GetResponse<NewsDetail>("ms-appx:///ModelsMock/PublicServices/ArticleDetails.json");
            return new RequestResponse<NewsDetail> { Result = response, ResponseStatus = ResponseStatus.SuccessWithResult };
        }

        public async Task<RequestResponse<IList<SyndicationItem>>> GetNewsAsync(CancellationToken? token = null)
        {
            await DelayAsync();
            Uri dataUri = new Uri("ms-appx:///ModelsMock/PublicServices/News.txt");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string text = await FileIO.ReadTextAsync(file);
            var synd = new SyndicationFeed();
            synd.Load(text);
            return new RequestResponse<IList<SyndicationItem>> { Result = synd.Items, ResponseStatus = ResponseStatus.SuccessWithResult };

        }

        public Task<RequestResponse<List<Country>>> GetCountriesAsync(CancellationToken? token = default(CancellationToken?))
        {
            throw new NotImplementedException();
        }
        public Task<RequestResponse<NatejaResult>> GetSecondaryNatigaResult(SecondaryResultParameters parameters, CancellationToken? token = null)
        {
            throw new NotImplementedException();
        }
    }
}