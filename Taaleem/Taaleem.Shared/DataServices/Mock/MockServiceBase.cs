﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Taaleem.DataServices.Mock
{
    public class MockServiceBase
    {
        public async Task<T> GetResponse<T>(String path)
        {
            Uri dataUri = new Uri(path);

            Debug.WriteLine($"Request at {DateTime.Now.ToString("yyyy-MM-dd")}");
            Debug.WriteLine(path);

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            await DelayAsync();
            T ret = await Task.Factory.StartNew<T>(() =>
            {
                jsonText = jsonText.Replace("DayofLeave", "DayOfLeave");

                Debug.WriteLine($"Response at {DateTime.Now.ToString("yyyy-MM-dd")}");
                Debug.WriteLine(jsonText);

                T result = JsonConvert.DeserializeObject<T>(jsonText);
                return result;
            });
            return ret;
        }

        protected async Task DelayAsync()
        {
            await Task.Delay(1500);
        }
    }
}