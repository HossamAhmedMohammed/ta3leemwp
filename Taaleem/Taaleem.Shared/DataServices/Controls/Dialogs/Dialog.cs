﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.System;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace Taaleem.Controls.Dialogs
{
    public class Dialog<T>
    {
        #region Fields

        private bool IsPaneAppeared;
        protected TaskCompletionSource<T> _taskCompletionSource;
        protected Popup _popup;
        protected Grid _container;
        protected Border _overlay;
        protected Button _dummyButton;//This will be used to hide Input Pane and steal first focus 
        protected IDismissRequested<T> dismissableElement;
        protected IDismissRequested<T> dismissableViewModel;
        protected InputPane _inputPane;
        protected Rect _inputPaneRect;
        protected bool _wasAppBarVisible;

        // private bool isPopupDismissed;

        /// <summary>
        /// double value used to control the opacity of dialog.
        /// Default is 0.7
        /// </summary>
        public double OverLayOpacity = 0.7;

        /// <summary>
        /// This is the minimum value that need to be available below the focused textBox/passwordbox....
        /// Default is 20
        /// </summary>
        public int SoftKeyBoardBuffer = 20;

        /// <summary>
        /// A value of true will close soft keyboards on Enter.
        /// Default is true
        /// </summary>
        public bool HideSoftKeyBoardOnEnter = true;

        /// <summary>
        /// A value of true will close soft keyboard on Tapping inside Content layout.
        /// Default is true
        /// </summary>
        public bool HideSoftKeyBoardOnTap = true;

        /// <summary>
        /// A value of true will  make popup light dimissable
        /// Default is false
        /// </summary>
        public bool OverlayDimissal = false;

        /// <summary>
        /// Default is true
        /// </summary>
        public bool HandleInputPaneShow = true;

        /// <summary>
        /// A value of true will lock user from closing dialog on pressing Escape
        /// Default is false
        /// </summary>
        public bool LockDismissOnKeyPress = false;


        #endregion

        #region Properties
        /// <summary>
        /// This element will hosted inside popup as full screen
        /// </summary>
        public FrameworkElement Content { get; set; }

        public object DataContext { get { return Content == null ? null : Content.DataContext; } }
        #endregion

        #region Initialization
        public Dialog(FrameworkElement content)
        {
            this.Content = content;
        }

        public Dialog()
            : this(null)
        {

        }
        #endregion

        #region Methods

        /// <summary>
        /// This method will show the popup and waits for input from control or viewmodel(If any of them implements IDismissRequested)
        /// It will return default value of T if user pressed Esc or dialog.Dismiss is called.
        /// </summary>
        /// <returns></returns>
        public IAsyncOperation<T> ShowAsync()
        {
            FrameworkElement element = CreateDialog();
            _popup = new Popup { Child = element };
            if (_popup.Child != null)
            {
                SubscribeEvents();
                _popup.IsOpen = true;
            }
            return AsyncInfo.Run(WaitForInput);
        }

        public void Dismiss()
        {
            OnCanceled();
        }

        private FrameworkElement CreateDialog()
        {
            var window = Window.Current.Content as FrameworkElement;
            if (window == null)
            {
                // The dialog is being shown before content has been created for the window
                Window.Current.Activated += OnWindowActivated;
                return null;
            }

            //Container that will hold overlay and Content
            _container = new Grid
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = Window.Current.Bounds.Width,
                Height = Window.Current.Bounds.Height
            };

            //Add zero size dummy button which will steal focus and will be used to hide input pane
            _dummyButton = new Button() { Height = 0, Width = 0, BorderBrush = new SolidColorBrush(Colors.Transparent) };
            _container.Children.Add(_dummyButton);

            //Overlay
            _overlay = new Border
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Background = new SolidColorBrush(Colors.Black),
                Opacity = OverLayOpacity
            };
            _container.Children.Add(_overlay);

            //Prepare Content

            if (Content == null) throw new ArgumentNullException("Content");


            if (Content is IDismissRequested<T>)
            {
                dismissableElement = Content as IDismissRequested<T>;
            }
            if (Content.DataContext is IDismissRequested<T>)
            {
                dismissableViewModel = Content.DataContext as IDismissRequested<T>;
            }
            //Adapt The Content Flow Direction to Window Flow Direction(LTR | RTL).
            Content.FlowDirection = window.FlowDirection;
            Content.HorizontalAlignment = HorizontalAlignment.Stretch;
            Content.VerticalAlignment = VerticalAlignment.Stretch;
            _container.Children.Add(Content);
            return _container;
        }

        #region Input Pane & Size Changed

        private void pane_Showing(InputPane sender, InputPaneVisibilityEventArgs args)
        {
            if (HandleInputPaneShow)
            {
                args.EnsuredFocusedElementInView = true;
                _inputPaneRect = args.OccludedRect;
                AdjustSize();
            }
            IsPaneAppeared = true;
        }

        private void pane_hiding(InputPane sender, InputPaneVisibilityEventArgs args)
        {
            if (HandleInputPaneShow)
            {
                args.EnsuredFocusedElementInView = false;
                _inputPaneRect = args.OccludedRect;
                AdjustSize();
            }
            IsPaneAppeared = false;
        }

        private void AdjustSize()
        {
            if (_popup == null) return;
            Rect rect = Window.Current.CoreWindow.Bounds;
            if (_inputPaneRect.Top == 0)
            {
                rect.Y = _inputPaneRect.Bottom;
            }

            rect.Height -= _inputPaneRect.Height;

            //_popup.HorizontalOffset = rect.Left;
            //_popup.VerticalOffset = rect.Top;
            _container.Width = rect.Width;
            _container.Height = rect.Height;
        }

        #endregion
        private Task<T> WaitForInput(CancellationToken token)
        {
            _taskCompletionSource = new TaskCompletionSource<T>();

            token.Register(OnCanceled);

            return _taskCompletionSource.Task;
        }

        // adjust for different view states
        private void OnWindowSizeChanged(object sender, WindowSizeChangedEventArgs e)
        {
            if (_popup.IsOpen == false) return;

            var child = _popup.Child as FrameworkElement;
            if (child == null) return;
            AdjustSize();
            //child.Width = e.Size.Width;
            //child.Height = e.Size.Height;
        }

        void _overlay_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (OverlayDimissal)
                OnCanceled();
        }

        private void Content_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            //Hide Soft key on  Key Press
            if (e.Key == VirtualKey.Enter && HideSoftKeyBoardOnEnter)
            {
                _dummyButton.Focus(FocusState.Programmatic);
                e.Handled = true;
            }

            if (LockDismissOnKeyPress) return;
            if (e.Key == VirtualKey.Escape)
            {
                OnCanceled();
            }
        }

        private void Content_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //TODO:R&D on issue happened in contentsLabelsDialog
            //TODO:Keyborad was open while cliking on combo box between arrow and text
            //TODO:Reproduced in Touch Mode
            //TODO:Issue wasn't reproduced when  _dummyButton.Focus(FocusState.Programmatic); is commented
            if (HideSoftKeyBoardOnTap)
            {
                if (IsPaneAppeared)
                    _dummyButton.Focus(FocusState.Programmatic);
            }
        }

        private void OnWindowActivated(object sender, WindowActivatedEventArgs windowActivatedEventArgs)
        {
            Window.Current.Activated -= OnWindowActivated;
            _popup.Child = CreateDialog();
            SubscribeEvents();
            _popup.IsOpen = true;
        }

        private void _dismissRequested(object sender, T e)
        {
            //Un subscribe before setting isOpen with false, to prevent calling un subscription twice
            _popup.Closed -= _popup_Closed;
            //Close PopUp
            _popup.IsOpen = false;
            //SetValue
            _taskCompletionSource.SetResult(e);
            //CleanUp
            UnsubscribeEvents();
        }

        void _popup_Closed(object sender, object e)
        {
            //If popup was close outside the control of Dialog, It means it's canceled
            OnCanceled();
        }

        private void OnCanceled()
        {
            _dismissRequested(this, default(T));
        }

        private void SubscribeEvents()
        {
            _popup.Closed += _popup_Closed;
            Window.Current.SizeChanged += OnWindowSizeChanged;
            Window.Current.Content.KeyDown += Content_KeyDown;

            if (HideSoftKeyBoardOnTap)
            {
                Content.Tapped += Content_Tapped;
            }

            if (OverlayDimissal)
            {
                _overlay.Tapped += _overlay_Tapped;
            }

            //register for dismiss events
            if (dismissableElement != null)
            {
                dismissableElement.DismissRequested += _dismissRequested;
            }

            if (dismissableViewModel != null)
            {
                dismissableViewModel.DismissRequested += _dismissRequested;
            }
            //Register for input pane
            _inputPane = InputPane.GetForCurrentView();
            _inputPane.Showing += pane_Showing;
            _inputPane.Hiding += pane_hiding;

            InitializeAppBarState();
        }

        private void UnsubscribeEvents()
        {
            _popup.Closed -= _popup_Closed;
            Window.Current.SizeChanged -= OnWindowSizeChanged;
            Window.Current.Content.KeyDown -= Content_KeyDown;
            Content.Tapped -= Content_Tapped;
            _overlay.Tapped -= _overlay_Tapped;

            //unregister for dismiss events
            if (dismissableElement != null)
            {
                dismissableElement.DismissRequested -= _dismissRequested;
                dismissableElement = null;
            }

            if (dismissableViewModel != null)
            {
                dismissableViewModel.DismissRequested -= _dismissRequested;
                dismissableViewModel = null;
            }

            //unsubscribe for input pane
            if (_inputPane != null)
            {
                _inputPane.Showing -= pane_Showing;
                _inputPane.Hiding -= pane_hiding;
                _inputPane = null;
            }
            RestoreAppBarState();
        }

        private void InitializeAppBarState()
        {
            var frame = (Frame)Window.Current.Content;
            if (frame != null)
            {
                var page = (Page)frame.Content;
                if (page != null && page.BottomAppBar != null)
                {
                    if (page.BottomAppBar.Visibility == Visibility.Visible)
                    {
                        _wasAppBarVisible = true;
                        page.BottomAppBar.Visibility = Visibility.Collapsed;
                    }
                    page.BottomAppBar.IsEnabled = false;
                }
            }
        }

        private void RestoreAppBarState()
        {
            var frame = (Frame)Window.Current.Content;
            if (frame != null)
            {
                var page = (Page)frame.Content;
                if (page != null && page.BottomAppBar != null)
                {
                    if (_wasAppBarVisible)
                    {
                        _wasAppBarVisible = false;
                        page.BottomAppBar.Visibility = Visibility.Visible;
                    }
                    page.BottomAppBar.IsEnabled = true;
                }
            }
        }
        #endregion
    }
}
