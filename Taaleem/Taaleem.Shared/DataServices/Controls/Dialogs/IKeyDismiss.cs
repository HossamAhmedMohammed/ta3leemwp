﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Controls.Dialogs
{
    public interface IKeyDismiss
    {
        bool IsDismissableOnKey { get; set; }
        event EventHandler<bool> IsDismissableOnKeyChanged;
    }
}
