﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Controls.Dialogs
{
    /// <summary>
    /// This will be used to dismiss popup from inside user control or view model hosted inside popup
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDismissRequested<T>
    {
        event EventHandler<T> DismissRequested;
    }
}
