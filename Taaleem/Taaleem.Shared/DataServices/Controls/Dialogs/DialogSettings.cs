﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Controls.Dialogs
{
    public class DialogSettings
    {
        //TODO:Not Used Yet
        public bool OverlayDismissal { get; protected set; }
        public double OverlayOpacity { get; protected set; }
        public bool HideSoftKeyBoardOnTap { get; set; }
        public bool HideSoftKeyBoardOnEnter { get; set; }

    }
}
