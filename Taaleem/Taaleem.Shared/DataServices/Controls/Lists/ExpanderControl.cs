﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Taaleem.Controls.Lists
{
    /*
     Original Control in this link:https://github.com/telerik/win8-xaml-sdk/blob/master/Blogs/ExpanderControl/ExpanderControl/ExpanderControl.Shared/ExpanderControl/ExpanderControl.xaml
     */
    //[TemplateVisualState(Name = ExpandedStateName, GroupName = ExpandStateGroupName)]
    //[TemplateVisualState(Name = CollapsedStateName, GroupName = ExpandStateGroupName)]
    //TODO:Improve Control
    public class ExpanderControl : ContentControl
    {
        //private const string ExpandedStateName = "Expanded";
        //private const string CollapsedStateName = "Collapsed";
        //private const string ExpandStateGroupName = "ExpandStateGroup";

        // Using a DependencyProperty as the backing store for Header.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(object), typeof(ExpanderControl), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for HeaderContentTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HeaderContentTemplateProperty =
            DependencyProperty.Register("HeaderContentTemplate", typeof(object), typeof(ExpanderControl), new PropertyMetadata(null));


        // Using a DependencyProperty as the backing store for IsExpander.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsExpandedProperty =
            DependencyProperty.Register("IsExpanded", typeof(bool), typeof(ExpanderControl), new PropertyMetadata(false, OnIsExpandedChanged));



        public object Header
        {
            get
            {
                return (object)GetValue(HeaderProperty);
            }
            set
            {
                SetValue(HeaderProperty, value);
            }
        }

        public object HeaderContentTemplate
        {
            get
            {
                return (object)GetValue(HeaderContentTemplateProperty);
            }
            set
            {
                SetValue(HeaderContentTemplateProperty, value);
            }
        }

        public bool IsExpanded
        {
            get
            {
                return (bool)GetValue(IsExpandedProperty);
            }
            set
            {
                SetValue(IsExpandedProperty, value);
            }
        }

        public ExpanderControl()
        {
            DefaultStyleKey = typeof(ExpanderControl);
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            //this.ChangeVisualState();
        }

        private static void OnIsExpandedChanged(DependencyObject sender, DependencyPropertyChangedEventArgs arg)
        {
            var expander = sender as ExpanderControl;
            if (expander != null) expander.OnExpandingChanged(expander.IsExpanded);
        }

        public event EventHandler<bool> ExpandingChanged;

        protected virtual void OnExpandingChanged(bool e)
        {
            var handler = ExpandingChanged;
            if (handler != null) handler(this, e);
        }
    }
}
