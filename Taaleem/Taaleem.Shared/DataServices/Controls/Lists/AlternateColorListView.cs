﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Taaleem.Controls.Lists
{
    public class AlternateColorListView : CustomListView
    {

        public SolidColorBrush EvenBackgroundBrush { get; set; }
        public SolidColorBrush OddBackgroundBrush { get; set; }

        private List<SolidColorBrush> _backgroundBrushes = new List<SolidColorBrush>();
        public List<SolidColorBrush> BackgroundBrushes
        {
            get { return _backgroundBrushes; }
        }

        public AlternateColorListView()
        {
            ContainerContentChanging += AlternateColorListView_ContainerContentChanging;

        }

        void AlternateColorListView_ContainerContentChanging(ListViewBase sender, ContainerContentChangingEventArgs args)
        {
            if (BackgroundBrushes != null && BackgroundBrushes.Count > 0)
                SetListItemcolor(args.ItemContainer, args.ItemIndex);
        }

        private void SetListItemcolor(DependencyObject element, int index)
        {
            var listViewItem = (ListViewItem)element;
            listViewItem.Background = BackgroundBrushes[index % BackgroundBrushes.Count];
            //if(index % 2 == 0)
            //{
            //    listViewItem.Background = this.EvenBackgroundBrush;
            //}
            //else
            //{
            //    listViewItem.Background = this.OddBackgroundBrush;
            //}
        }

        protected override void OnItemsChanged(object e)
        {
            base.OnItemsChanged(e);
            if (e is IVectorChangedEventArgs)
            {
                IVectorChangedEventArgs changedArgs = (IVectorChangedEventArgs)e;
                if (changedArgs.CollectionChange == CollectionChange.ItemRemoved)
                {
                    if (BackgroundBrushes == null || BackgroundBrushes.Count == 0) return;
                    int index = (int)changedArgs.Index;
                    int count = Items.Count;
                    if (count > 0)
                    {
                        for (int i = index; i < count; i++)
                        {
                            try
                            {
                                var container = ContainerFromIndex(i);
                                SetListItemcolor(container, i);
                            }
                            catch (Exception)
                            {
                                return;
                            }
                        }
                    }
                }

            }
        }

    }
}
