﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Common;
using Taaleem.ViewModels;
using Windows.UI.Xaml.Navigation;

namespace Taaleem.Controls
{
    public class ParentsExtendedPage : ExtendedPage
    {
        #region Fields
        private int lastCheckedChildIndex = -1;
        protected bool ChildChangedOutsidePage;
        #endregion

        #region Properties

        /// <summary>
        /// Overrided from base NeedsRefresh to add ChildChangedOutsidePage factor.
        /// You can also override this property to add new factors that trigger refresh.
        /// </summary>
        public override bool NeedsRefresh
        {
            get { return ChildChangedOutsidePage || base.NeedsRefresh; }
        }

        #endregion

        #region Methods
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            int currentChildIndex = ViewModelLocator.Locator.AppSettings.SelectedChildIndex;
            if (e.NavigationMode == NavigationMode.Back)
            {
                if (lastCheckedChildIndex != currentChildIndex)
                {
                    lastCheckedChildIndex = currentChildIndex;
                    ChildChangedOutsidePage = true;
                }
            }
            else
            {
                lastCheckedChildIndex = currentChildIndex;
            }
            //Listen for selected child index changed event to detect if user changed child while active on this page
            ViewModelLocator.Locator.ChildrenSelectionVM.SelectedChildIndexChanged += ChildrenSelectionViewModel_SelectedChildIndexChanged;

            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
            {
                ChildChangedOutsidePage = false;
            }

            //Don't listen any more when this page isn't active
            ViewModelLocator.Locator.ChildrenSelectionVM.SelectedChildIndexChanged -= ChildrenSelectionViewModel_SelectedChildIndexChanged;
            base.OnNavigatedFrom(e);
        }
        #endregion

        /// <summary>
        /// This is called when child selection change while this page is Active.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ChildrenSelectionViewModel_SelectedChildIndexChanged(object sender, int index)
        {
            lastCheckedChildIndex = index;

          //  Navigate back to Primary section, 1 means Home(1 in back stack) +Section(Current)
          //  Nested Sections will be directed to first primary section
          //  Example:
            //if (Frame.BackStackDepth > 1)
            //{
            //    while (Frame.BackStackDepth > 2)
            //    {
            //        Frame.BackStack.RemoveAt(Frame.BackStack.Count - 1);
            //    }
            //    Frame.GoBack();
            //}
            //else
            //{
                //Refresh page
                LoadState(this, new LoadStateEventArgs(null, new Dictionary<string, object>()));
            //}
        }

        /// <summary>
        /// Override this method to handle child selection changed in current page.
        /// This is called when child selection change while this page is Active.
        /// </summary>
        protected virtual void RefreshPageForChangedChild()
        {

        }
    }

    public class ChildSelectionChangedEventArgs
    {
        /// <summary>
        /// Determines if selection change is from this page or from other page.
        /// </summary>
        public bool IsThisPageTheOrigin { get; set; }
    }

    public class ParentsLoadStateEventArgs : ExtendedLoadStateEventArgs
    {
        public bool ChildChanged { get; private set; }

        /// <summary>
        /// Overrided from base NeedsRefresh to add ChildChangedOutsidePage factor.
        /// You can also override this property to add new factors that trigger refresh.
        /// </summary>
        public override bool NeedsRefresh
        {
            get { return ChildChanged || base.NeedsRefresh; }
        }

        public ParentsLoadStateEventArgs(object navigationParameter, Dictionary<string, object> pageState, NavigationMode navigationMode, bool isNewInstance, bool childChanged)
            : base(navigationParameter, pageState, navigationMode, isNewInstance)
        {
            ChildChanged = childChanged;
        }
    }
}
