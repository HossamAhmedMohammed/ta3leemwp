﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Taaleem.Controls.Validation
{

    /// <summary>
    /// Control which wraps another control to add visual validation information. If a value
    /// becomes invalid (<see cref="Errors"/>is not empty), a red border will be drawn
    /// around the inner control. Additionally, the list of validation errors will be shown below.
    /// </summary>
    [TemplateVisualState(GroupName = "CommonStates", Name = "Valid")]
    [TemplateVisualState(GroupName = "CommonStates", Name = "Error")]
    public sealed class ValidationPanel : ContentControl
    {
        #region Fields

        /// <summary>
        /// The original border brush of the inner control
        /// (will be replaced when a red border is shown because of invalid data).
        /// </summary>
        private Brush _originalControlBorderBrush;

        /// <summary>
        /// The original border thickness of the inner control
        /// (will be replaced when a red border is shown because of invalid data).
        /// </summary>
        private Thickness _originalControlBorderThickness = new Thickness(0);

        #endregion

        #region Message dp
        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Message.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(ValidationPanel), new PropertyMetadata(null, OnMessageChanged));

        private static void OnMessageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var panel = d as ValidationPanel;
            if (panel != null)
                UpdateValidationInformation(panel);
        }

        #endregion
        public ValidationPanel()
        {
            this.DefaultStyleKey = typeof(ValidationPanel);
            this.Loaded += this.OnLoaded;
        }

        /// <summary>
        /// Invoked when the control has been loaded. In this case the control UI
        /// will update its validation state.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="routedEventArgs">Event arguments.</param>
        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            UpdateValidationInformation(this);
        }

        /// <summary>
        /// Invoked when the inner content changed. In this case, if the content is a control, 
        /// the control's border information will be stored, hence it could be restored later.
        /// </summary>
        /// <param name="oldContent"> The old content. </param>
        /// <param name="newContent"> The new content. </param>
        protected override void OnContentChanged(object oldContent, object newContent)
        {
            var control = newContent as Control;
            if (control != null)
            {
                this._originalControlBorderBrush = control.BorderBrush;
                this._originalControlBorderThickness = control.BorderThickness;
            }

            base.OnContentChanged(oldContent, newContent);
        }

        /// <summary>
        /// Updates the validation information to show on the control.
        /// </summary>
        /// <param name="panel"><c>ValidationPanel</c> that should be rendered.</param>
        private static void UpdateValidationInformation(ValidationPanel panel)
        {
            // go to the appropriate validation state
            VisualStateManager.GoToState(
              panel,
              ComputeVisualState(!String.IsNullOrEmpty(panel.Message)),
              true);

            var control = panel.Content as Control;
            //if(control != null)
            //{
            //    // state is invalid: clear the border information (colored border is drawn around);
            //    // state is valid:   restore the initial border information of the control
            //    var isValid = !String.IsNullOrEmpty(panel.Message);
            //    control.BorderBrush = isValid ? panel._originalControlBorderBrush : null;
            //    control.BorderThickness = isValid ? panel._originalControlBorderThickness : new Thickness(0);
            //}
        }

        /// <summary>
        /// Computes the visual state to represent the validation state of the control.
        /// </summary>
        /// <param name="hasErrors"> The indicator whether there are errors. </param>
        /// <returns> The <see cref="string"/> containing the name of the visual state. </returns>
        private static string ComputeVisualState(bool hasErrors)
        {
            return hasErrors
                ? "Error" : "Valid";
        }
    }
}
