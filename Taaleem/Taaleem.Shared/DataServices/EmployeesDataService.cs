﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.QueryParameters;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees.Request;
using Taaleem.Models.Employees.Response;

namespace Taaleem.DataServices
{
    public class EmployeesDataService : IEmployeesDataService
    {
        private readonly IAppSettings _appSettings;
        private readonly Func<BaseRequest> _requestFactory;
        private CookieContainer _sharedCookieContainer;
        private const string BaseUrl = "http://eservices.sec.gov.qa/";
        public String UserName
        {
            get
            {
                return _appSettings.EmployeeCredential.UserName;
                //return "a.alsayed";
            }
        }

        public String Password
        {
            get
            {
                return _appSettings.EmployeeCredential.Password;
                // return "Testpassword";
            }
        }

        public EmployeesDataService(IAppSettings appSettings, INetworkHelper networkHelper, Func<BaseRequest> requestFactory)
        {
            _appSettings = appSettings;
            _requestFactory = requestFactory;
        }

        private CookieContainer GetSharedCookieContainer()
        {
            if (_sharedCookieContainer == null)
            {
                _sharedCookieContainer = new CookieContainer();
            }
            return _sharedCookieContainer;
        }

        private HttpClientHandler GetAuthHandler()
        {
            var handler = new HttpClientHandler { CookieContainer = GetSharedCookieContainer() };
            return handler;
        }



        public async Task<RequestResponse<EmployeeLoginResponse>> LoginAsync(EmpolyeeLoginBody parameters,
        CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke(); 
            request.RequestUrl = (BaseUrl + "MobileServices/Service/auth");
            request.HttpMessageHandler = GetAuthHandler();
            return await request.PostAsync<EmployeeLoginResponse>(new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json"), token);
        }

        public async Task<RequestResponse<TicketValidation>> ValidateTicketAsync(CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (BaseUrl + "MobileServices/Service/ValidateTicket");
            request.HttpMessageHandler = GetAuthHandler();
            return await request.PostAsync<TicketValidation>(new StringContent("", Encoding.UTF8), token);
        }

        public async Task<RequestResponse<T>> GetAsyncWithAuth<T>(string requestUrl, CancellationToken? token = null)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = requestUrl;
            request.HttpMessageHandler = GetAuthHandler();

            var firstAttempt = await request.GetAsync<T>(token);
            //There is no data, This means that it can be no data or auth failed
            if ((firstAttempt.ResponseStatus == ResponseStatus.SuccessWithNoData || firstAttempt.StatusCode == HttpStatusCode.BadRequest) && firstAttempt.ResponseStatus != ResponseStatus.NoInternet)
            {
                var validateTicket = await ValidateTicketAsync(token);

                //after checking on validation, it returned valid so it was no data.
                if (validateTicket.ResponseStatus == ResponseStatus.SuccessWithResult && validateTicket.Result.Validated)
                    return firstAttempt;

                //Now try to login to set auth ticket in the cookie container
                var login = await LoginAsync(new EmpolyeeLoginBody() { UserName = UserName, Password = Password }, token);

                //if login failed, return the response status and code
                if (!(login.ResponseStatus == ResponseStatus.SuccessWithResult && login.Result.Success))
                    return new RequestResponse<T>() { ResponseStatus = login.ResponseStatus, StatusCode = login.StatusCode };

                //reaching here means login completed, so retry the request again
                var secondRequest = _requestFactory.Invoke();
                secondRequest.RequestUrl = requestUrl;
                secondRequest.HttpMessageHandler = GetAuthHandler();
                var secondAttempt = await secondRequest.GetAsync<T>(token);
                return secondAttempt;
            }
            
            return firstAttempt;
        }


        public async Task<RequestResponse<List<SalaryDetail>>> GetSalariesAsync(SalaryParameters parameters,
        CancellationToken? token = null)
        {
            return await GetAsyncWithAuth<List<SalaryDetail>>((BaseUrl + "MobileServices/Service/GetEmloyeeSalary").AppendQueryString(parameters), token);
        }

        public async Task<RequestResponse<List<EmployeeLoan>>> GetEmployeeLoansByStatusAsync(LoansByStatusParameters parameters,
        CancellationToken? token = null)
        {
            return await GetAsyncWithAuth<List<EmployeeLoan>>((BaseUrl + "MobileServices/Service/GetEmloyeeLoansWithStatus").AppendQueryString(parameters), token);
        }

        public async Task<RequestResponse<List<EmployeeLoan>>> GetAllEmployeeLoansAsync(string userid,
        CancellationToken? token = null)
        {
            return await GetAsyncWithAuth<List<EmployeeLoan>>((BaseUrl + "MobileServices/Service/GetEmloyeeLoans/" + userid), token);
        }

        public async Task<RequestResponse<List<Evaluation>>> GetEvaluationsAsync(EvaluationParameters parameters,
        CancellationToken? token = null)
        {
            return await GetAsyncWithAuth<List<Evaluation>>((BaseUrl + "MobileServices/Service/GetEmployeeEvaluation").AppendQueryString(parameters), token);
        }

        //The second user emp2 is the only user which has vacations
        public async Task<RequestResponse<List<EmployeeVacation>>> GetEmployeeVacationsAsync(string userid,
            CancellationToken? token = null)
        {
            return await GetAsyncWithAuth<List<EmployeeVacation>>(BaseUrl + "MobileServices/Service/GetEmployeeVacations/" + userid, token);
        }


    }
}
