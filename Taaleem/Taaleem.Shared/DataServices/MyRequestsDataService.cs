﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees.MyRequests.Request;
using Taaleem.Models.Employees.MyRequests.Response;
using Taaleem.ViewModels.Employees;

namespace Taaleem.DataServices
{
    public class MyRequestsDataService : IMyRequestsDataService
    {
        private readonly IAppSettings _appSettings;
        private readonly Func<BaseRequest> _requestFactory;
        private CookieContainer _sharedCookieContainer;
        IEmployeesDataService _employeeDataService;
        private const string BaseUrl = "http://intra16-stg-web.secedu.qa:99/api";
        //private const string MyRequestsBaseUrl = "http://intra16-stg-web.secedu.qa:99/api/Employee/GetMyRequests";
        //private const string DocumentBaseUrl = "http://intra16-stg-web.secedu.qa:99/api/Document";
        //private const string PersonalLoanBaseUrl = "http://intra16-stg-web.secedu.qa:99/api/PersonalLoan";

        public MyRequestsDataService(IAppSettings appSettings, INetworkHelper networkHelper, Func<BaseRequest> requestFactory, IEmployeesDataService employeeDataService)
        {
            _appSettings = appSettings;
            _requestFactory = requestFactory;
            _employeeDataService = employeeDataService;
        }

        private CookieContainer GetSharedCookieContainer()
        {
            if (_sharedCookieContainer == null)
            {
                _sharedCookieContainer = new CookieContainer();
            }
            return _sharedCookieContainer;
        }
        private HttpClientHandler GetAuthHandler()
        {
            var handler = new HttpClientHandler
            {
                CookieContainer = GetSharedCookieContainer(),
                Credentials = new NetworkCredential("qsec\\emp2", "123456")
            };

            return handler;
        }

        async Task<RequestResponse<T>> GetResponse<T>(string url, CancellationToken? token, bool isResponseOfTypeRequestResponse = true)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (url);
            request.UserName = "qsec\\emp2";// _appSettings.EmployeeCredential.UserName;
            request.Password = "123456";// _appSettings.EmployeeCredential.Password;
            request.HttpMessageHandler = GetAuthHandler();
            request.ResultContentType = ContentType.Json;

            var result = await GetAsyncWithAuth<T>(request, token, isResponseOfTypeRequestResponse); //isResponseOfTypeRequestResponse ? await request.GetAsync<T>(token) : await request.GetDataAsync<T>(token);
            return result;
        }

        async Task<RequestResponse<T>> PostResponse<T>(string url, StringContent content, CancellationToken? token)
        {
            var request = _requestFactory.Invoke();
            request.RequestUrl = (url);
            request.HttpMessageHandler = GetAuthHandler();
            request.ResultContentType = ContentType.Json;
            var result = await request.PostDataAsync<T>(content, token);
            return result;
        }

        async Task<RequestResponse<T>> GetPageResponse<T>(MyRequestTypeEnum type, int page, CancellationToken? token)
        {
            var url = $"{BaseUrl}/Employee/GetMyRequests/{_appSettings.EmployeeId},{(int)type},{page},none,none,none/";
            return await GetResponse<T>(url, token);
        }

        async Task<RequestResponse<RootResponse<T>>> GetFullPagesResponse<T>(MyRequestTypeEnum type, CancellationToken? token)
        {
            int page = 0;
            var response = new RequestResponse<RootResponse<T>>
            {
                Result = new RootResponse<T>
                {
                    Requests = new List<T>()
                }
            };

            do
            {
                page++;
                var data = await GetPageResponse<RootResponse<T>>(type, page, token);
#if Mock
                data.Result.Requests.Clear();
                data.Result.Count = 0;
#endif
                if (data.Result.Count == 0 || (data.Result.Requests != null && data.Result.Requests.Count == 0))
                    data.ResponseStatus = ResponseStatus.SuccessWithNoData;

                if (data.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    response.Result.Requests.AddRange(data.Result.Requests);
                    response.Result.Count = data.Result.Count;
                    response.Result.Error = data.Result.Error;
                }
            } while (response.Result.Count != response.Result.Requests.Count);
            return response;
        }

        public async Task<RequestResponse<RootResponse<StupidLeaveRequestResponse>>> GetDocumentRequests(string userId, CancellationToken? token = default(CancellationToken?))
        {
            return await GetFullPagesResponse<StupidLeaveRequestResponse>(MyRequestTypeEnum.DocumentRequest, token);
        }

        public async Task<RequestResponse<RootResponse<StupidLeaveRequestResponse>>> GetExitPermitRequests(CancellationToken? token = default(CancellationToken?))
        {
            return await GetFullPagesResponse<StupidLeaveRequestResponse>(MyRequestTypeEnum.ExitPermit, token);
        }

        public async Task<RequestResponse<RootResponse<LeaveRequestResponse>>> GetLeaveRequests(CancellationToken? token = default(CancellationToken?))
        {
            return await GetFullPagesResponse<LeaveRequestResponse>(MyRequestTypeEnum.LeaveRequest, token);
        }

        public async Task<RequestResponse<RootResponse<StupidLeaveRequestResponse>>> GetLeaveReturnRequests(CancellationToken? token = default(CancellationToken?))
        {
            return await GetFullPagesResponse<StupidLeaveRequestResponse>(MyRequestTypeEnum.LeaveReturn, token);
        }

        public async Task<RequestResponse<RootResponse<LeaveRequestResponse>>> GetPersonalLoanRequests(string userId, CancellationToken? token = default(CancellationToken?))
        {
            return await GetFullPagesResponse<LeaveRequestResponse>(MyRequestTypeEnum.PersonalLoan, token);
        }

        public async Task<RequestResponse<List<RequestTypesResponse>>> GetRequestTypes(CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/Employee/RequestTypes";
            return await GetResponse<List<RequestTypesResponse>>(url, token);
        }

        public async Task<List<DocumentRequestType>> GetDocumentRequestTypes(CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/Document/Types/Qatar";
            var response = await GetResponse<List<DocumentRequestType>>(url, token);
            return response.Result;
        }

        public async Task<EmployeeByNameResponse> GetEmployeeProfileByQatariId(string qatariId, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/Employee/EmployeeProfile/{qatariId}";
            var response = await GetResponse<EmployeeByNameResponse>(url, token);
            return response.Result ?? new EmployeeByNameResponse { qatariID = qatariId };
        }

        public async Task<BasicResponse> SubmitNewDocumentRequest(string qatariId, string typeId, string details, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/Document/{qatariId},{typeId},{details}";
            var response = await PostResponse<List<BasicResponse>>(url, null, token);
            return response.Result != null && response.Result.Count > 0 ? response.Result[0] : null;
        }

        public async Task<List<LoanRequestType>> GetLoanRequestTypes(bool isQatari, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/PersonalLoan/LoanTypes/{isQatari}";
            var response = await GetResponse<List<LoanRequestType>>(url, token, false);
            return response.Result;
        }
        //PersonalLoan/GetLoanPeriod/

        public async Task<BasicResponse> SubmitNewPersonalLoanRequest(PersonalLoanRequest request, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/PersonalLoan";
            var response = await PostResponse<List<BasicResponse>>(url, new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json"), token);
            return response.Result != null && response.Result.Count > 0 ? response.Result[0] : null;
        }

        public async Task<List<string>> GetLoanPeriodsList(CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/PersonalLoan/GetLoanPeriod";
            var response = await GetResponse<List<string>>(url, token, false);
            return response.Result;
        }

        public async Task<decimal> GetMonthlyInstallment(decimal loanAmount, int paymentPeriodInMonths, CancellationToken? token = default(CancellationToken?))
        {
            if (loanAmount == 0 || paymentPeriodInMonths == 0)
                return 0;

            var url = $"{BaseUrl}/PersonalLoan/LoanInstallment/{loanAmount},{paymentPeriodInMonths}";
            var response = await GetResponse<decimal>(url, token, false);
            return response.Result;
        }

        public async Task<bool> IsEligibleforLoan(EmployeeByNameResponse employeeProfile, string loanTypeCode, decimal loanAmount, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/PersonalLoan/IsEligibleforLoan/{loanTypeCode},{loanAmount}";
            var response = await PostResponse<bool>(url, new StringContent(JsonConvert.SerializeObject(employeeProfile), Encoding.UTF8, "application/json"), token);
            return response.Result;
        }

        public async Task<BasicResponse> SubmitNewExitPermitRequest(EmployeeByNameResponse employeeProfile, string date, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/ExitPermit/{date}";
            var response = await PostResponse<List<BasicResponse>>(url, new StringContent(JsonConvert.SerializeObject(employeeProfile), Encoding.UTF8, "application/json"), token);
            return response.Result != null && response.Result.Count > 0 ? response.Result[0] : null;
        }

        public async Task<List<LeaveType>> GetLeaveTypes(string qatariId, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/LeaveRequest/LeaveTypes/{qatariId}";
            var response = await GetResponse<List<LeaveType>>(url, token, false);
            return response.Result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="qatariId"></param>
        /// <param name="leaveTypeId"></param>
        /// <param name="startDate">MM-dd-yyyy</param>
        /// <param name="endDate">MM-dd-yyyy</param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> IsDuplicateLeave(string qatariId, string leaveTypeId, string startDate, string endDate, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/LeaveRequest/IsDuplicate/{qatariId},{leaveTypeId},{startDate},{endDate}";
            var response = await PostResponse<bool>(url, null, token);
            return response.Result;
        }

        public async Task<BasicResponse> SubmitNewLeaveRequest(LeaveRequestRequest leaveRequest, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/LeaveRequest";
            var response = await PostResponse<List<BasicResponse>>(url, new StringContent(JsonConvert.SerializeObject(leaveRequest), Encoding.UTF8, "application/json"), token);
            return response.Result != null && response.Result.Count > 0 ? response.Result[0] : null;
        }

        public async Task<EmployeeByNameResponse> GetEmployeeByJobId(string jobId, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/Employee/employeebyjobid/{jobId}";
            var response = await GetResponse<EmployeeByNameResponse>(url, token, false);
            return response.Result;
        }

        public async Task<EmployeeByNameResponse> GetEmployeeByName(string employeeName, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/Employee/?empUserName={employeeName}";
            var response = await GetResponse<EmployeeByNameResponse>(url, token, false);
            return response.Result;
        }

        public async Task<int> GetMaximumLoanAmount(string qatariId, string loanTypeCode, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/PersonalLoan/LoanAmount/{qatariId},{loanTypeCode}";
            var response = await GetResponse<int>(url, token, false);
            return response.Result;
        }

        public async Task<List<EmployeeApprovedLeave>> GetEmployeeApprovedLeaves(string jobId, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/LeaveReturn/EmployeeApprovedLeaves/{jobId}";
            var response = await GetResponse<List<EmployeeApprovedLeave>>(url, token, false);
            return response.Result;
        }

        public async Task<bool> IsDuplicateLeaveReturn(string leaveSequence, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/LeaveReturn/IsDuplicate/{leaveSequence}";
            var response = await PostResponse<List<BasicResponse>>(url, null, token);
            return response.StatusCode == HttpStatusCode.OK;
        }

        public async Task<BasicResponse> SubmitNewLeaveReturnRequest(LeaveReturnRequestRequest leaveReturnRequest, CancellationToken? token = default(CancellationToken?))
        {
            var url = $"{BaseUrl}/LeaveReturn";
            var response = await PostResponse<List<BasicResponse>>(url, new StringContent(JsonConvert.SerializeObject(leaveReturnRequest), Encoding.UTF8, "application/json"), token);
            return response.Result != null && response.Result.Count > 0 ? response.Result[0] : null;
        }

        public async Task<bool> IsOnBehalfMember(string jobId)
        {
            var url = $"{BaseUrl}/Employee/IsOnBehalfMember/{jobId}";
            var response = await GetResponse<bool>(url, default(CancellationToken?), false);
            return response.Result;
        }


        public async Task<RequestResponse<T>> GetAsyncWithAuth<T>(BaseRequest request, CancellationToken? token = null, 
            bool isResponseOfTypeRequestResponse = true)
        {
            var firstAttempt = isResponseOfTypeRequestResponse ? await request.GetAsync<T>(token): await request.GetDataAsync<T>(token);
            //There is no data, This means that it can be no data or auth failed
            if ((firstAttempt.ResponseStatus == ResponseStatus.SuccessWithNoData || firstAttempt.StatusCode == HttpStatusCode.BadRequest) && firstAttempt.ResponseStatus != ResponseStatus.NoInternet)
            {
                var validateTicket = await _employeeDataService.ValidateTicketAsync(token);

                //after checking on validation, it returned valid so it was no data.
                if (validateTicket.ResponseStatus == ResponseStatus.SuccessWithResult && validateTicket.Result.Validated)
                    return firstAttempt;

                //Now try to login to set auth ticket in the cookie container
                var login = await _employeeDataService.LoginAsync(new Taaleem.Models.Employees.Request.EmpolyeeLoginBody()
                {
                    UserName = "qsec\\emp2", // _appSettings.EmployeeCredential.UserName;
                    Password = "123456"
                    //UserName = _appSettings.EmployeeCredential.UserName,
                    //    Password = _appSettings.EmployeeCredential.Password
                }, token);

                //if login failed, return the response status and code
                if (!(login.ResponseStatus == ResponseStatus.SuccessWithResult && login.Result.Success))
                    return new RequestResponse<T>() { ResponseStatus = login.ResponseStatus, StatusCode = login.StatusCode };

                //reaching here means login completed, so retry the request again
                var secondRequest = _requestFactory.Invoke();
                secondRequest.UserName = "qsec\\emp2";// _appSettings.EmployeeCredential.UserName;
                secondRequest.Password = "123456";// _appSettings.EmployeeCredential.Password;
                secondRequest.HttpMessageHandler = GetAuthHandler();
                secondRequest.ResultContentType = ContentType.Json; 

                var secondAttempt = isResponseOfTypeRequestResponse ? await secondRequest.GetAsync<T>(token) : await secondRequest.GetDataAsync<T>(token);
                return secondAttempt;
            }

            return firstAttempt;
        }

    }
}
