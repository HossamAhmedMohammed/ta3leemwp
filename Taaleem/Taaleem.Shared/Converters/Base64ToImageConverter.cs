﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace Taaleem.Converters
{
    public class Base64ToImageConverter : IValueConverter
    {
        public static BitmapImage Base64StringToBitmap ( string source )
        {
            //Avoid Dummy Data
            if(source.StartsWith("<")) return new BitmapImage();
            try
            {
                var ims = new InMemoryRandomAccessStream();
                var bytes = System.Convert.FromBase64String(source);
                var dataWriter = new DataWriter(ims);
                dataWriter.WriteBytes(bytes);
                dataWriter.StoreAsync();
                ims.Seek(0);
                var img = new BitmapImage();
                img.SetSource(ims);
                return img;
            }
            catch(Exception)
            {
                return new BitmapImage();
            }
        }

        public object Convert ( object value, Type targetType, object parameter, string language )
        {
            String stringVal;
            if(value == null || (stringVal = value.ToString()).Length == 0) return new BitmapImage();
            return Base64StringToBitmap(stringVal);
        }



        public object ConvertBack ( object value, Type targetType, object parameter, string language )
        {
            throw new NotImplementedException();
        }
    }
}
