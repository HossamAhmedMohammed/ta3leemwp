﻿
//Modified 
//﻿Added support for arabic and change most of the logic to be similar to .getRelativeTimeSpanString in android
//Original Code:https://github.com/timheuer/callisto/tree/master/src/Callisto/Converters


// BASE CODE PROVIDED UNDER THIS LICENSE
// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Public License (Ms-PL).
// Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.UI.Xaml.Data;

namespace Taaleem.Converters
{
    /// <summary>
    /// Simliar to realtive date time converter inside Android
    /// </summary>
    public class RelativeDateTimeTransationConverter : IValueConverter
    {

        /// <summary>
        /// A minute defined in seconds.
        /// </summary>
        private const double Minute = 60.0;

        /// <summary>
        /// An hour defined in seconds.
        /// </summary>
        private const double Hour = 60.0 * Minute;

        /// <summary>
        /// A day defined in seconds.
        /// </summary>
        private const double Day = 24 * Hour;

        /// <summary>
        /// A week defined in seconds.
        /// </summary>
        private const double Week = 7 * Day;

        /// <summary>
        /// A month defined in seconds.
        /// </summary>
        private const double Month = 30.5 * Day;

        /// <summary>
        /// A year defined in seconds.
        /// </summary>
        private const double Year = 365 * Day;

        /// <summary>
        /// The minimum elapsed time (in milliseconds) to report when showing relative times.
        /// </summary>
        private const double MinResolutionSeconds = 2;
        /// <summary>
        /// The elapsed time (in milliseconds) at which to stop reporting relative measurements. Elapsed times greater than this resolution will default to normal date formatting. For example, will transition from "7 days ago" to "Dec 12" when using WEEK_IN_MILLIS.
        /// </summary>
        private const double TransitionResolutionSeconds = 7 * Day;

        /// <summary>
        /// Abbreviation for the default culture used by resources files.
        /// </summary>
        private const string DefaultCulture = "en-US";

        /// <summary>
        /// Four different strings to express hours in plural.
        /// </summary>
        private string[] _pluralHourStrings;

        /// <summary>
        /// Four different strings to express minutes in plural.
        /// </summary>
        private string[] _pluralMinuteStrings;

        /// <summary>
        /// Four different strings to express seconds in plural.
        /// </summary>
        private string[] _pluralSecondStrings;

        /// <summary>
        /// Four different strings to express seconds in plural.
        /// </summary>
        private string[] _pluralDaysStrings;

        /// <summary>
        /// Four different strings to express seconds in plural.
        /// </summary>
        private string[] _pluralMonthsStrings;

        /// <summary>
        /// Wrapper for the ResourceLoader to get the string resources
        /// </summary>
        private static ResourceLoader TimeResources
        {
            get
            {
                return ResourceLoader.GetForCurrentView(@"DateResources");
            }
        }

        /// <summary>
        /// CultureInfo object using the user's preferred language list
        /// </summary>
        private static CultureInfo PreferredCulture
        {
            get
            {
                return new CultureInfo(CurrentLanguageTag);
            }
        }

        /// <summary>
        /// Gets the current BCP-47 language from the user's language preference list
        /// </summary>
        public static string CurrentLanguageTag
        {
            get
            {
                return ResourceContext.GetForCurrentView().Languages[0].ToString();
            }
        }

        /// <summary>
        /// Resources use the culture in the system locale by default.
        /// The converter must use the culture specified the ConverterCulture.
        /// The ConverterCulture defaults to en-US when not specified.
        /// Thus, change the resources culture only if ConverterCulture is set.
        /// </summary>
        /// <param name="culture">The culture to use in the converter.</param>
        private void SetLocalizationCulture ( CultureInfo culture )
        {
            //if (!culture.Name.Equals(DefaultCulture, StringComparison.Ordinal))
            //{
            //    ControlResources.Culture = culture;
            //}

            _pluralHourStrings = new string[3] { 
                  TimeResources.GetString(@"XHoursAgo_2"), 
                  TimeResources.GetString(@"XHoursAgo_3To10"), 
                  TimeResources.GetString(@"XHoursAgo_Other") 
              };

            _pluralMinuteStrings = new string[3] { 
                  TimeResources.GetString(@"XMinutesAgo_2"), 
                  TimeResources.GetString(@"XMinutesAgo_3To10"), 
                  TimeResources.GetString(@"XMinutesAgo_Other")
              };

            _pluralSecondStrings = new string[3] { 
                  TimeResources.GetString(@"XSecondsAgo_2"), 
                  TimeResources.GetString(@"XSecondsAgo_3To10"), 
                  TimeResources.GetString(@"XSecondsAgo_Other") 
              };

            _pluralDaysStrings = new string[3] { 
                  TimeResources.GetString(@"XDaysAgo_2"), 
                  TimeResources.GetString(@"XDaysAgo_3To10"), 
                  TimeResources.GetString(@"XDaysAgo_Other") 
              };

            _pluralMonthsStrings = new string[3] { 
                  TimeResources.GetString(@"XMonthsAgo_2"), 
                  TimeResources.GetString(@"XMonthsAgo_3To10"), 
                  TimeResources.GetString(@"XMonthsAgo_Other") 
              };
        }

        /// <summary>
        /// Returns a localized text string to express months in plural.
        /// </summary>
        /// <param name="month">Number of months.</param>
        /// <returns>Localized text string.</returns>
        private string GetPluralMonth ( int month )
        {
            return GetPluralTimeUnits(month, _pluralMonthsStrings);
        }

        /// <summary>
        /// Returns a localized text string to express time units in plural.
        /// </summary>
        /// <param name="units">
        /// Number of time units, e.g. 5 for five months.
        /// </param>
        /// <param name="resources">
        /// Resources related to the specified time unit.
        /// </param>
        /// <returns>Localized text string.</returns>
        private static string GetPluralTimeUnits ( int units, string[] resources )
        {
            if(units <= 1)
            {
                throw new ArgumentException(TimeResources.GetString(@"InvalidNumberOfTimeUnits"));
            }
            else if(units == 2)
            {
                return resources[0];
            }
            else if(units >= 3 && units <= 10)
            {
                return string.Format(PreferredCulture, resources[1], units.ToString(PreferredCulture));
            }
            else
            {
                return string.Format(PreferredCulture, resources[2], units.ToString(PreferredCulture));
            }
        }



        /// <summary>
        /// Converts a 
        /// <see cref="T:System.DateTime"/>
        /// object into a string the represents the elapsed time 
        /// relatively to the present.
        /// </summary>
        /// <param name="value">The given date and time.</param>
        /// <param name="targetType">
        /// The type corresponding to the binding property, which must be of
        /// <see cref="T:System.String"/>.
        /// </param>
        /// <param name="parameter">(Not used).</param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// When not specified, the converter uses the current culture
        /// as specified by the system locale.
        /// </param>
        /// <returns>The given date and time as a string.</returns>
        public object Convert ( object value, Type targetType, object parameter, string culture )
        {
            // Target value must be a System.DateTime object.
            if(!(value is DateTime))
            {
                throw new ArgumentException(TimeResources.GetString(@"InvalidDateTimeArgument"));
            }

            string result;

            DateTime given = ((DateTime)value);

            DateTime current = DateTime.Now;

            TimeSpan difference = current - given;

            SetLocalizationCulture(PreferredCulture);

            if(current < given)
            {
                // Future dates and times are not supported, but to prevent crashing an app
                // if the time they receive from a server is slightly ahead of the phone's clock
                // we'll just default to the minimum, which is "2 seconds ago".
                result = GetPluralTimeUnits(2, _pluralSecondStrings);
            }

            if(difference.TotalSeconds >= TransitionResolutionSeconds)
            {
                result = ShortFormatDate(given, CurrentLanguageTag);
            }
            else if(difference.TotalSeconds >= 2 * Day)
            {
                int days = (int)(difference.TotalSeconds / Day);
                result = GetPluralTimeUnits(days, _pluralDaysStrings);
            }
            else if(difference.TotalSeconds >= Day)
            {
                result = TimeResources.GetString(@"Yesterday");
            }
            else if(difference.TotalSeconds >= (2 * Hour))
            {
                // "x hours ago"
                int nHours = (int)(difference.TotalSeconds / Hour);
                result = GetPluralTimeUnits(nHours, _pluralHourStrings);
            }
            else if(difference.TotalSeconds >= Hour)
            {
                // "about an hour ago"
                result = TimeResources.GetString(@"AboutAnHourAgo");
            }
            else if(difference.TotalSeconds >= (2 * Minute))
            {
                // "x minutes ago"
                int nMinutes = (int)(difference.TotalSeconds / Minute);
                result = GetPluralTimeUnits(nMinutes, _pluralMinuteStrings);
            }
            else if(difference.TotalSeconds >= Minute)
            {
                // "about a minute ago"
                result = TimeResources.GetString(@"AboutAMinuteAgo");
            }
            else
            {
                // "x seconds ago" or default to "2 seconds ago" if less than two seconds.
                int nSeconds = ((int)difference.TotalSeconds > 1.0) ? (int)difference.TotalSeconds : 2;
                result = GetPluralTimeUnits(nSeconds, _pluralSecondStrings);
            }

            return result;
        }

        private string ShortFormatDate ( DateTime given, string cultureString )
        {
            cultureString = cultureString == "ar" ? "ar-EG" : cultureString;
            return given.ToString("dd MMMM yyyy", new CultureInfo(cultureString));
        }

        public object ConvertBack ( object value, Type targetType, object parameter, string language )
        {
            throw new NotImplementedException();
        }


    }
}
