﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace Taaleem.Converters
{
    public sealed class ListViewIndexToBrushConverter : IValueConverter
    {
        public Brush EvenBrush { get; set; }
        public Brush OddBrush { get; set; }

        public object Convert ( object value, Type targetType, object parameter, string language )
        {
            ListViewItem item = (ListViewItem)value;
            ListView listView =
                ItemsControl.ItemsControlFromItemContainer(item) as ListView;
            // Get the index of a ListViewItem
            if(listView == null) return EvenBrush;
            int index = listView.ItemContainerGenerator.IndexFromContainer(item);

            if(index % 2 == 0)
            {
                return EvenBrush;
            }
            else
            {
                return OddBrush;
            }
        }

        public object ConvertBack ( object value, Type targetType, object parameter, string language )
        {
            throw new NotImplementedException();
        }
    }

}
