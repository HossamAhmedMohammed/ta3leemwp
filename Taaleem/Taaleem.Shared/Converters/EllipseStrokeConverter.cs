﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace Taaleem.Converters
{
    public class EllipseStrokeConverter : IValueConverter
    {


        private double CalculatePercentage(double width, double percent)
        {
            return 2 * 3.14 * (width / 2) * percent;
        }



        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var width = (double)value;

            var strokeDashArray = new DoubleCollection();

            strokeDashArray.Add(CalculatePercentage(width, 0.1));
            strokeDashArray.Add(CalculatePercentage(width, 0.5));
            strokeDashArray.Add(CalculatePercentage(width, 0.25));
            strokeDashArray.Add(CalculatePercentage(width, 0.15));

            return strokeDashArray;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}