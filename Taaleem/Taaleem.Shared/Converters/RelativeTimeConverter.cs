﻿//Modified 
//﻿Added support for arabic
//Original Code:https://github.com/timheuer/callisto/tree/master/src/Callisto/Converters


// BASE CODE PROVIDED UNDER THIS LICENSE
// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Public License (Ms-PL).
// Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
// All other rights reserved.

using System;
using System.Globalization;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.UI.Xaml.Data;

namespace Taaleem.Converters
{
    /// <summary>
    /// Time converter to display elapsed time relatively to the present.
    /// </summary>
    /// <QualityBand>Preview</QualityBand>
    public class RelativeTimeConverter : IValueConverter
    {
        /// <summary>
        /// A minute defined in seconds.
        /// </summary>
        private const double Minute = 60.0;

        /// <summary>
        /// An hour defined in seconds.
        /// </summary>
        private const double Hour = 60.0 * Minute;

        /// <summary>
        /// A day defined in seconds.
        /// </summary>
        private const double Day = 24 * Hour;

        /// <summary>
        /// A week defined in seconds.
        /// </summary>
        private const double Week = 7 * Day;

        /// <summary>
        /// A month defined in seconds.
        /// </summary>
        private const double Month = 30.5 * Day;

        /// <summary>
        /// A year defined in seconds.
        /// </summary>
        private const double Year = 365 * Day;

        /// <summary>
        /// Abbreviation for the default culture used by resources files.
        /// </summary>
        private const string DefaultCulture = "en-US";

        /// <summary>
        /// Four different strings to express hours in plural.
        /// </summary>
        private string[] _pluralHourStrings;

        /// <summary>
        /// Four different strings to express minutes in plural.
        /// </summary>
        private string[] _pluralMinuteStrings;

        /// <summary>
        /// Four different strings to express seconds in plural.
        /// </summary>
        private string[] _pluralSecondStrings;

        /// <summary>
        /// Four different strings to express seconds in plural.
        /// </summary>
        private string[] _pluralMonthsStrings;

        /// <summary>
        /// Wrapper for the ResourceLoader to get the string resources
        /// </summary>
        private static ResourceLoader TimeResources
        {
            get
            {
                return ResourceLoader.GetForCurrentView(@"DateResources");
            }
        }

        /// <summary>
        /// CultureInfo object using the user's preferred language list
        /// </summary>
        private static CultureInfo PreferredCulture
        {
            get
            {
                return new CultureInfo(CurrentLanguageTag);
            }
        }

        /// <summary>
        /// Gets the current BCP-47 language from the user's language preference list
        /// </summary>
        public static string CurrentLanguageTag
        {
            get
            {
                return ResourceContext.GetForCurrentView().Languages[0].ToString();
            }
        }

        /// <summary>
        /// Resources use the culture in the system locale by default.
        /// The converter must use the culture specified the ConverterCulture.
        /// The ConverterCulture defaults to en-US when not specified.
        /// Thus, change the resources culture only if ConverterCulture is set.
        /// </summary>
        /// <param name="culture">The culture to use in the converter.</param>
        private void SetLocalizationCulture ( CultureInfo culture )
        {
            //if (!culture.Name.Equals(DefaultCulture, StringComparison.Ordinal))
            //{
            //    ControlResources.Culture = culture;
            //}

            _pluralHourStrings = new string[3] { 
                  TimeResources.GetString(@"XHoursAgo_2"), 
                  TimeResources.GetString(@"XHoursAgo_3To10"), 
                  TimeResources.GetString(@"XHoursAgo_Other") 
              };

            _pluralMinuteStrings = new string[3] { 
                  TimeResources.GetString(@"XMinutesAgo_2"), 
                  TimeResources.GetString(@"XMinutesAgo_3To10"), 
                  TimeResources.GetString(@"XMinutesAgo_Other")
              };

            _pluralSecondStrings = new string[3] { 
                  TimeResources.GetString(@"XSecondsAgo_2"), 
                  TimeResources.GetString(@"XSecondsAgo_3To10"), 
                  TimeResources.GetString(@"XSecondsAgo_Other") 
              };

            _pluralMonthsStrings = new string[3] { 
                  TimeResources.GetString(@"XMonthsAgo_2"), 
                  TimeResources.GetString(@"XMonthsAgo_3To10"), 
                  TimeResources.GetString(@"XMonthsAgo_Other") 
              };
        }

        /// <summary>
        /// Returns a localized text string to express months in plural.
        /// </summary>
        /// <param name="month">Number of months.</param>
        /// <returns>Localized text string.</returns>
        private string GetPluralMonth ( int month )
        {
            return GetPluralTimeUnits(month, _pluralMonthsStrings);
        }

        /// <summary>
        /// Returns a localized text string to express time units in plural.
        /// </summary>
        /// <param name="units">
        /// Number of time units, e.g. 5 for five months.
        /// </param>
        /// <param name="resources">
        /// Resources related to the specified time unit.
        /// </param>
        /// <returns>Localized text string.</returns>
        private static string GetPluralTimeUnits ( int units, string[] resources )
        {
            if(units <= 1)
            {
                throw new ArgumentException(TimeResources.GetString(@"InvalidNumberOfTimeUnits"));
            }
            else if(units == 2)
            {
                return resources[0];
            }
            else if(units >= 3 && units <= 10)
            {
                return string.Format(PreferredCulture, resources[1], units.ToString(PreferredCulture));
            }
            else
            {
                return string.Format(PreferredCulture, resources[2], units.ToString(PreferredCulture));
            }
        }

        /// <summary>
        /// Returns a localized text string for the "ast" + "day of week as {0}".
        /// </summary>
        /// <param name="dow">Last Day of week.</param>
        /// <returns>Localized text string.</returns>
        private static string GetLastDayOfWeek ( DayOfWeek dow )
        {
            string result;

            switch(dow)
            {
                case DayOfWeek.Monday:
                    result = TimeResources.GetString(@"last Monday");
                    break;
                case DayOfWeek.Tuesday:
                    result = TimeResources.GetString(@"last Tuesday");
                    break;
                case DayOfWeek.Wednesday:
                    result = TimeResources.GetString(@"last Wednesday");
                    break;
                case DayOfWeek.Thursday:
                    result = TimeResources.GetString(@"last Thursday");
                    break;
                case DayOfWeek.Friday:
                    result = TimeResources.GetString(@"last Friday");
                    break;
                case DayOfWeek.Saturday:
                    result = TimeResources.GetString(@"last Saturday");
                    break;
                case DayOfWeek.Sunday:
                    result = TimeResources.GetString(@"last Sunday");
                    break;
                default:
                    result = TimeResources.GetString(@"last Sunday");
                    break;
            }

            return result;
        }

        /// <summary>
        /// Returns a localized text string to express "on {0}"
        /// where {0} is a day of the week, e.g. Sunday.
        /// </summary>
        /// <param name="dow">Day of week.</param>
        /// <returns>Localized text string.</returns>
        private static string GetOnDayOfWeek ( DayOfWeek dow )
        {
            string result;

            switch(dow)
            {
                case DayOfWeek.Monday:
                    result = TimeResources.GetString(@"on Monday");
                    break;
                case DayOfWeek.Tuesday:
                    result = TimeResources.GetString(@"on Tuesday");
                    break;
                case DayOfWeek.Wednesday:
                    result = TimeResources.GetString(@"on Wednesday");
                    break;
                case DayOfWeek.Thursday:
                    result = TimeResources.GetString(@"on Thursday");
                    break;
                case DayOfWeek.Friday:
                    result = TimeResources.GetString(@"on Friday");
                    break;
                case DayOfWeek.Saturday:
                    result = TimeResources.GetString(@"on Saturday");
                    break;
                case DayOfWeek.Sunday:
                    result = TimeResources.GetString(@"on Sunday");
                    break;
                default:
                    result = TimeResources.GetString(@"on Sunday");
                    break;
            }

            return result;
        }

        /// <summary>
        /// Converts a 
        /// <see cref="T:System.DateTime"/>
        /// object into a string the represents the elapsed time 
        /// relatively to the present.
        /// </summary>
        /// <param name="value">The given date and time.</param>
        /// <param name="targetType">
        /// The type corresponding to the binding property, which must be of
        /// <see cref="T:System.String"/>.
        /// </param>
        /// <param name="parameter">(Not used).</param>
        /// <param name="culture">
        /// The culture to use in the converter.
        /// When not specified, the converter uses the current culture
        /// as specified by the system locale.
        /// </param>
        /// <returns>The given date and time as a string.</returns>
        public object Convert ( object value, Type targetType, object parameter, string culture )
        {
            // Target value must be a System.DateTime object.
            if(!(value is DateTime))
            {
                throw new ArgumentException(TimeResources.GetString(@"InvalidDateTimeArgument"));
            }

            string result;

            DateTime given = ((DateTime)value).ToLocalTime();

            DateTime current = DateTime.Now;

            TimeSpan difference = current - given;

            SetLocalizationCulture(PreferredCulture);

            if(current < given)
            {
                // Future dates and times are not supported, but to prevent crashing an app
                // if the time they receive from a server is slightly ahead of the phone's clock
                // we'll just default to the minimum, which is "2 seconds ago".
                result = GetPluralTimeUnits(2, _pluralSecondStrings);
            }

            if(difference.TotalSeconds > Year)
            {
                // "over a year ago"
                result = TimeResources.GetString(@"OverAYearAgo");
            }
            else if(difference.TotalSeconds > (1.5 * Month))
            {
                // "x months ago"
                int nMonths = (int)((difference.TotalSeconds + Month / 2) / Month);
                result = GetPluralMonth(nMonths);
            }
            else if(difference.TotalSeconds >= (3.5 * Week))
            {
                // "about a month ago"
                result = TimeResources.GetString(@"AboutAMonthAgo");
            }
            else if(difference.TotalSeconds >= Week)
            {
                int nWeeks = (int)(difference.TotalSeconds / Week);
                if(nWeeks == 1)
                {
                    // "about a week ago"
                    result = TimeResources.GetString(@"AboutAWeekAgo");
                }
                else if(nWeeks == 2)
                {
                    // "2 weeks ago"
                    result = TimeResources.GetString(@"XWeeksAgo_2");
                }
                else
                {
                    // "x weeks ago"
                    result = string.Format(PreferredCulture, TimeResources.GetString(@"XWeeksAgo_3To4"), nWeeks.ToString(PreferredCulture));

                }

            }
            else if(difference.TotalSeconds >= (5 * Day))
            {
                // "last <dayofweek>"    
                result = GetLastDayOfWeek(given.DayOfWeek);
            }
            else if(difference.TotalSeconds >= Day)
            {
                // "on <dayofweek>"
                result = GetOnDayOfWeek(given.DayOfWeek);
            }
            else if(difference.TotalSeconds >= (2 * Hour))
            {
                // "x hours ago"
                int nHours = (int)(difference.TotalSeconds / Hour);
                result = GetPluralTimeUnits(nHours, _pluralHourStrings);
            }
            else if(difference.TotalSeconds >= Hour)
            {
                // "about an hour ago"
                result = TimeResources.GetString(@"AboutAnHourAgo");
            }
            else if(difference.TotalSeconds >= (2 * Minute))
            {
                // "x minutes ago"
                int nMinutes = (int)(difference.TotalSeconds / Minute);
                result = GetPluralTimeUnits(nMinutes, _pluralMinuteStrings);
            }
            else if(difference.TotalSeconds >= Minute)
            {
                // "about a minute ago"
                result = TimeResources.GetString(@"AboutAMinuteAgo");
            }
            else
            {
                // "x seconds ago" or default to "2 seconds ago" if less than two seconds.
                int nSeconds = ((int)difference.TotalSeconds > 1.0) ? (int)difference.TotalSeconds : 2;
                result = GetPluralTimeUnits(nSeconds, _pluralSecondStrings);
            }

            return result;
        }

        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="value">(Not used).</param>
        /// <param name="targetType">(Not used).</param>
        /// <param name="parameter">(Not used).</param>
        /// <param name="culture">(Not used).</param>
        /// <returns>null</returns>
        public object ConvertBack ( object value, Type targetType, object parameter, string culture )
        {
            throw new NotImplementedException();
        }
    }
}