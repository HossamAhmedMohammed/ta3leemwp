﻿using System;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace Taaleem.Converters
{
    public class BooleanToBrushConverter : IValueConverter
    {
        public Brush FalseBrush { get; set; }
        public Brush TrueBrush { get; set; }
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!(value is bool)) return FalseBrush;
            var val = (bool)value;
            return val ? TrueBrush : FalseBrush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
