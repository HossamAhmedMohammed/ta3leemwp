﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;

namespace Taaleem.Converters
{
    public class ItemClickedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var args = value as TappedRoutedEventArgs;

            if (args != null)
            {
                var temp = args.OriginalSource.GetType();
                if(temp.Name == "Image")
                return (args.OriginalSource as Windows.UI.Xaml.Controls.Image).DataContext;
                if (temp.Name == "Border")
                    return (args.OriginalSource as Windows.UI.Xaml.Controls.Border).DataContext;
                if (temp.Name == "TextBlock")
                    return (args.OriginalSource as Windows.UI.Xaml.Controls.TextBlock).DataContext;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            string language)
        {
            throw new NotImplementedException();
        }
    }
}
