﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Taaleem.Models.Children;

namespace Taaleem.Converters
{
    public class AttendanceStatusToBrushConverter : IValueConverter
    {
        public Brush PresentBrush { get; set; }
        public Brush AbsenceBrush { get; set; }
        public Brush AuthorizedAbsenceBrush { get; set; }
        public Brush UnrecordedBrush { get; set; }
        public Brush VacationBrush { get; set; }
        public Brush NoPeriodBrush { get; set; }

        public object Convert ( object value, Type targetType, object parameter, string language )
        {
            if(value == null || typeof(int) != value.GetType())
            {
                return NoPeriodBrush;
            }
            int statusId = (int)value;
            switch(statusId)
            {
                case AttendanceStatusType.Present:
                    return PresentBrush;
                case AttendanceStatusType.Absence:
                    return AbsenceBrush;
                case AttendanceStatusType.AuthorizedAbsence:
                    return AuthorizedAbsenceBrush;
                case AttendanceStatusType.Unrecorded:
                    return UnrecordedBrush;
                case AttendanceStatusType.Vacation:
                    return VacationBrush;
                default:
                    return NoPeriodBrush;
            }
        }

        public object ConvertBack ( object value, Type targetType, object parameter, string language )
        {
            throw new NotImplementedException();
        }

    }
}
