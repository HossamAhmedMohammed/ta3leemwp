﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Taaleem.ViewModels;

namespace Taaleem.Converters
{
    public class DateFormatConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert ( object value, Type targetType, object parameter, string language )
        {
            if(!(value is DateTime || value is DateTimeOffset) || parameter == null) return value;
            //convert ar to ar-EG to display gregorian date but in arabic format
            //leaving it as it is only will display Higri date
            var cultureString = ViewModelLocator.Resources.Culture == "ar"
     ? "ar-EG"
     : ViewModelLocator.Resources.Culture;



            string result;
            if(value is DateTime)
            {
                var date = (DateTime)value;
                result = date.ToString(parameter.ToString(), new CultureInfo(cultureString));
            }
            else
            {
                var date = (DateTimeOffset)value;
                result = date.ToString(parameter.ToString(), new CultureInfo(cultureString));
            }

            //Correct character that breaks direction of arabic RTL like -
            if(cultureString.StartsWith("ar"))
            {
                //const string ltrMark = "\u200E";
                const string rtlMark = "\u200F";

                try
                {
                    if(result.Contains("-") || result.Contains("/") || result.Contains(" "))
                    {

                        //result = Regex.Replace(result, "[!-/]", "$&" + rtlMark);
                        ////replace matched char with RTLcode after it - R&D to make sure it works with other chars
                        ////link:http://stackoverflow.com/questions/12630566/parsing-through-arabic-rtl-text-from-left-to-right
                        StringBuilder builder = new StringBuilder();
                        builder.Append(rtlMark);
                        for(int i = 0; i < result.Length; i++)
                        {
                            char c = result[i];
                            if(c == '-' || c == '/' || c == ' ')
                            {
                                builder.Append(c);
                                builder.Append(rtlMark);
                            }
                            else
                            {
                                builder.Append(c);
                            }
                        }
                        result = builder.ToString();
                    }
                }
                catch(Exception)
                {
                    // ignored
                }
            }
            return result;
        }

        public object ConvertBack ( object value, Type targetType, object parameter, string language )
        {
            try
            {
                DateTimeOffset dto = (DateTimeOffset)value;
                return dto.DateTime;
            }
            catch (Exception ex)
            {
                return DateTime.MinValue;
            }
        }

        #endregion
    }
}
