﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace Taaleem.Converters
{
    public class IndexToBrushConverter : IValueConverter
    {
        public Brush EvenBrush { get; set; }
        public Brush OddBrush { get; set; }
        #region IValueConverter Members

        public object Convert ( object value, Type targetType, object parameter, string language )
        {
            if(value == null || typeof(int) != value.GetType())
            {
                return EvenBrush;
            }
            return ((int)value) % 2 == 0 ? EvenBrush : OddBrush;
        }

        public object ConvertBack ( object value, Type targetType, object parameter, string language )
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
