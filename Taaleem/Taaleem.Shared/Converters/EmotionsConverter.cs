﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;

namespace Taaleem.Converters
{
    class EmotionsConverter : IValueConverter
    {
       

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                int emotionNumber = (int)value;
                switch(emotionNumber)
                {
                    case -1:                     
                        return "ms-appx:///Assets/Parent/behaviour_smily_sad_ic.png";
                    case 0:
                        return "ms-appx:///Assets/Parent/behaviour_smily_sad_ic.png";
                    case 1:
                        return "ms-appx:///Assets/Parent/behaviour_smily_happy_ic.png";
                   
                    default:
                        return null;
                }

            }
            else return null;
        }



        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
