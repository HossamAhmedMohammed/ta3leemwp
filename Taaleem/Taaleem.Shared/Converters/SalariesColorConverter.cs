﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace Taaleem.Converters
{
    public class SalariesColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
          
                if (double.Parse(value.ToString()) >= 0.0)
                {
                    return new SolidColorBrush(Colors.Green);
                }
                else
                {
                    return new SolidColorBrush(Colors.Red);
                }
           
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
