﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Taaleem.Converters
{
    public class LefToRightWhenTrueCoverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert ( object value, Type targetType, object parameter, string language )
        {
            return (value is bool && (bool)value) ? FlowDirection.LeftToRight : FlowDirection.RightToLeft;
        }

        public object ConvertBack ( object value, Type targetType, object parameter, string language )
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
