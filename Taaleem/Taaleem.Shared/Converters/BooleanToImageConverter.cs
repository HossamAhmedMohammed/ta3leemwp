﻿using System;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace Taaleem.Converters
{
    public class BooleanToImageConverter : IValueConverter
    {
        public BitmapImage FalseImage { get; set; }
        public BitmapImage TrueImage { get; set; }
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!( value is bool )) return FalseImage;
            var val = (bool)value;
            return val ? TrueImage : FalseImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
