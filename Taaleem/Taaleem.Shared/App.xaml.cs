﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.ViewModels.Children;
using Taaleem.Views;
using Taaleem.Views.Children;
using Taaleem.Views.Exceptions;
using Taaleem.Views.PublicServices;
using Taaleem.Views.PublicServices.Login;
//using Taaleem.Views;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Globalization;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace Taaleem
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : Application
    {
#if WINDOWS_PHONE_APP
        private TransitionCollection transitions;
#endif

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += this.OnSuspending;
            UnhandledException += App_UnhandledException;
        }


        void App_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //TODO:This to prevent rare issue in subclassing list view, Which affects Notifications page
            //TODO:R&D on workaround to prevent the issue from the root (if any)
            //This isn't the best way as it may suppress other important issues
            //http://stackoverflow.com/questions/10148820/setting-itemssource-of-derived-listbox-throws-catastrophic-failure
            //https://social.msdn.microsoft.com/Forums/windowsapps/en-US/295d7ee6-8bc4-4326-9ea7-b68ee4c98a7a/deriving-from-gridview-throws-exception?forum=winappswithcsharp
            if (e.Exception.HResult == -2147418113)
            {
                e.Handled = true;
                Debug.WriteLine("Suprressed Catastrophic failure");
                return;
            }
           if(e.Exception.HResult == -2147467259)
            {
                e.Handled = true;
                return;
            }

#if DEBUG
            e.Handled = true;
            Debug.WriteLine("Exception Suprressed: \n" + e.Message);
            return;
            //#else 
            //            e.Handled = true;
                     //  new ContentDialog() {Title = e.Message}.ShowAsync();
#endif
        }
        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            ViewModels.ViewModelLocator.Locator = new ViewModels.ViewModelLocator();
           

            Window.Current.Activate();

#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = false;
            }
#endif

            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();
                
                // TODO: change this value to a cache size that is appropriate for your application
                rootFrame.CacheSize = 0;
                //if (e.PreviousExecutionState != ApplicationExecutionState.Running)
                //{
                //    bool loadState = (e.PreviousExecutionState == ApplicationExecutionState.Terminated);
                //    H extendedSplash = new Splash(e.SplashScreen, loadState);
                //    rootFrame.Content = extendedSplash;
                   
                //}
                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    // TODO: Load state from previously suspended application
                }
                //TODO:[Future work]Check on app settings.LanguageId to set app language
                string language = "en-US";
               // const string language = "ar";
                SetLanguage(rootFrame, language);
                // Place the frame in the current Window
                ////////////////////////// Window.Current.Content = rootFrame;
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
#if WINDOWS_PHONE_APP
                // Removes the turnstile navigation for startup.
                //if (rootFrame.ContentTransitions != null)
                //{
                //    this.transitions = new TransitionCollection();
                //    foreach (var c in rootFrame.ContentTransitions)
                //    {
                //        this.transitions.Add(c);
                //    }
                //}

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;
                Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().QualifierValues.MapChanged += QualifierValues_MapChanged;
#endif

                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                var homePageScreen = typeof(Splash);
#if Mock
                homePageScreen = typeof(Views.Employee.EmployeeMenuPage);
#endif
                if (!rootFrame.Navigate(homePageScreen, e.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }

            // Ensure the current window is active
            Window.Current.Activate();
            //ViewModels.ViewModelLocator.Locator.NavigationService.Register<AssignmentsViewModel,Taaleem.Views.Children.Assignments> ();
            //ViewModels.ViewModelLocator.Locator.NavigationService.Register <,> ();
            //ViewModels.ViewModelLocator.Locator.NavigationService.Register <,> ();
            //ViewModels.ViewModelLocator.Locator.NavigationService.Register <,> ();
            //ViewModels.ViewModelLocator.Locator.NavigationService.Register <,> ();
        }

        private void QualifierValues_MapChanged(IObservableMap<string, string> sender, IMapChangedEventArgs<string> @event)
        {
            try
            {
                Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
            }
            catch
            {
                Debug.WriteLine("Crach on start here");
            }
        }

#if WINDOWS_PHONE_APP
        /// <summary>
        /// Restores the content transitions after the app has launched.
        /// </summary>
        /// <param name="sender">The object where the handler is attached.</param>
        /// <param name="e">Details about the navigation event.</param>
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }

        
#endif

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();

            // TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        public  void SetLanguage(Frame rootFrame, string language)
        {
            //Set Frame Language
            rootFrame.Language = language;


            //Be Aware that this value is persisted between sessions
            ApplicationLanguages.PrimaryLanguageOverride = language;

            //On Windows 8.1 
            //

            try
            {
                ResourceContext.ResetGlobalQualifierValues();// need to be call to reset the language.
            }
            catch (Exception)
            {
                //Check this link for more info https://timheuer.com/blog/archive/2013/03/26/howto-refresh-languages-winrt-xaml-windows-store.aspx
            }

            ////reset the context of the resource manager.
            var resourceContext = ResourceContext.GetForCurrentView();
            resourceContext.Reset();

            //ResourceContext.ResetGlobalQualifierValues();// need to be call to reset the language.

            //Set Flow Direction
            FlowDirection flow = FlowDirection.LeftToRight;
            try
            {
                var loader = ResourceLoader.GetForCurrentView();
                var direction = loader.GetString("FlowDirection");
                Enum.TryParse<FlowDirection>(direction, out flow);
            }
            catch (Exception)
            {
                // ignored
            }
            rootFrame.FlowDirection = flow;
        }
    }
}