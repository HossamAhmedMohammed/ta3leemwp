﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem
{
    /// <summary>
    /// Used across app to represent supported languages.
    /// </summary>
    public class Lanuage
    {
        public const int English = 1;
        public const int Arabic = 2;
        public const int Third = 3;
    }

    /// <summary>
    /// Used with https://tasjeel.sec.gov.qa 
    /// </summary>
    public class Culture
    {
        public const string English = "en";
        public const string Arabic = "ar";
        public const string Third = "-";

        /// <summary>
        /// Maps app language id to language resource used with school registeration Service.
        /// </summary>
        /// <param name="languageId"></param>
        /// <returns></returns>
        public static string LanguageToCulture(int languageId)
        {
            switch (languageId)
            {
                case Lanuage.Arabic:
                    return Arabic;
                case Lanuage.English:
                    return English;
                case Lanuage.Third:
                    return Third;
                default:
                    return Arabic;
            }
        }
    }
}
