﻿using Taaleem.Models.Employees.MyRequests;
using Taaleem.ViewModels.Employees;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Taaleem.Selectors
{

    public abstract class TemplateSelector : ContentControl
    {
        public abstract DataTemplate SelectTemplate(object item, DependencyObject container);

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);

            ContentTemplate = SelectTemplate(newContent, this);
        }
    }

    public class ContactTemplateSelector : TemplateSelector
    {
        public DataTemplate LeaveRequestTemplate { get; set; }

        public DataTemplate LeaveReturnRequestTemplate { get; set; }

        public DataTemplate ExitPermitRequestTemplate { get; set; }

        public DataTemplate PersonalLoanRequestTemplate { get; set; }

        public DataTemplate DocumentRequestTemplate { get; set; }

        public DataTemplate OverTimeRequestTemplate { get; set; }


        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var contact = item as SectionsListWrapper;
            if (contact != null)
            {
                switch (contact.Type)
                {
                    case MyRequestTypeEnum.MyRequests:
                        break;
                    case MyRequestTypeEnum.LeaveRequest:
                        return LeaveRequestTemplate;
                    case MyRequestTypeEnum.LeaveReturn:
                        return LeaveReturnRequestTemplate;
                    case MyRequestTypeEnum.ExitPermit:
                        return ExitPermitRequestTemplate;
                    case MyRequestTypeEnum.PersonalLoan:
                        return PersonalLoanRequestTemplate;
                    case MyRequestTypeEnum.DocumentRequest:
                        return DocumentRequestTemplate;
                    case MyRequestTypeEnum.OverTime:
                        return OverTimeRequestTemplate;
                    default:
                        break;
                }
            }

            return null;
        }
    }
}
