﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.Children;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Taaleem.Selectors
{
    public class PrivateNoteTemplateSelector : DataTemplateSelector
    {
        public DataTemplate FirstPersonTemplate { get; set; }
        public DataTemplate OtherPersonTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            if (!(item is PrivateNoteWrapper)) return FirstPersonTemplate;
            var noteWrapper = (PrivateNoteWrapper)item;
            if (noteWrapper.IsTeacher) return OtherPersonTemplate;
            return FirstPersonTemplate;
        }
    }
}
