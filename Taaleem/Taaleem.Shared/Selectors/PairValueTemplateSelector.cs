﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.SchoolRegisteration;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Taaleem.Selectors
{
    public class PairValueTemplateSelector : DataTemplateSelector
    {
        public DataTemplate TitleOnlyTemplate { get; set; }
        public DataTemplate ValueOnlyTemplate { get; set; }
        public DataTemplate FullTemplate { get; set; }
        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var propertyPair = item as PropertyPair;
            if (propertyPair != null)
            {
                var pair = propertyPair;
                if (string.IsNullOrEmpty(pair.Name))
                {
                    return ValueOnlyTemplate;
                }
                else if (string.IsNullOrEmpty(pair.Value))
                {
                    return TitleOnlyTemplate;
                }
                else
                {
                    return FullTemplate;
                }
            }
            return base.SelectTemplateCore(item, container);
        }
    }
}
