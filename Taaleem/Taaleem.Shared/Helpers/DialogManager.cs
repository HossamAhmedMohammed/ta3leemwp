﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Controls.Dialogs;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children.Response;
using Taaleem.ViewModels.Children;
using Taaleem.ViewModels.Employees;
using Taaleem.ViewModels.PreRegistration;
using Taaleem.Views.Children;
using Taaleem.Views.Employee;
using Taaleem.Views.PublicServices.PreRegistration;
using Taaleem.Views.Student;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
//using Taaleem.Viewmodels.Children;

namespace Taaleem.Helpers
{
    public class DialogManager : IDialogManager
    {
        private readonly Resources _resources;
        Dialog<bool> dialog = new Dialog<bool>();
        public DialogManager(Resources resources)
        {
            this._resources = resources;
           
        }
        #region IDialogManager Members


        #endregion

        public async Task<bool> ShowMessage(string title, string content)
        {
            TextBlock txtBlock = new TextBlock();
            txtBlock.Text = content;
            txtBlock.TextWrapping = TextWrapping.Wrap;
            ContentDialog dialog = new ContentDialog
            {
                Title = title,
                Content = txtBlock,
                PrimaryButtonText = _resources.Close
            };
            
            var result = await dialog.ShowAsync();
            if (result == ContentDialogResult.Primary) return true;
            return false;
        }

        public async Task ShowContactTeacherDialog(ContactTeacher contactTeacher)
        {
            //var control = new TeacherUserControl();
            //var viewmodel = control.DataContext as TeacherViewModel;
            //viewmodel.ContactTeacher = contactTeacher;
            //var dialog = new Dialog<bool>(control) { OverlayDimissal = true };
            //await dialog.ShowAsync();
        }


        public async Task ShowParentLoginDialog()
        {
            var control = new ParentLogin();
            var viewmodel = control.DataContext as ParentLoginViewModel;

            dialog = new Dialog<bool>(control) { OverlayDimissal = true };
            await dialog.ShowAsync();
        }

        public async Task ShowStudentLoginDialog()
        {
            var control = new StudentLogin();
            var viewmodel = control.DataContext as ParentLoginViewModel;

            dialog = new Dialog<bool>(control) { OverlayDimissal = true };
            await dialog.ShowAsync();
        }

        public async Task ShowEmployeeLoginDialog()
        {
            var control = new EmployeeLogin();
            var viewmodel = control.DataContext as EmployeeLoginViewModel;

            dialog = new Dialog<bool>(control) { OverlayDimissal = true };
            await dialog.ShowAsync();
        }

        /// <summary>
        /// Returns selected child index
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ShowChildrenSelectionDialog()
        {
            var dialog = new Dialog<bool>(new ChildrenSelectionUserControl()) { OverlayDimissal = true };
            bool result = await dialog.ShowAsync();
            return result;
        }

        public void CloseDialog()
        {
            dialog.Dismiss();
        }

        public async Task ShowTansferMyChiledrenDialog()
        {
            var control = new MyChildren();
            var viewmodel = control.DataContext as PreRegistrationViewModel;

            dialog = new Dialog<bool>(control) { OverlayDimissal = true };
            await dialog.ShowAsync();

        }
    }
}
