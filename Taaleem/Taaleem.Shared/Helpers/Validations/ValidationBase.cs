﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using Taaleem.Common;

namespace Taaleem.Helpers.Validations
{
    public class ValidationBase : BindableBase
    {

        #region PropertyChanged

        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            Type type = this.GetType();
            PropertyInfo property = type.GetRuntimeProperty(propertyName);
            if (property != null)
            {
                bool hasValidationAttribute = property.IsDefined(typeof(ValidationAttribute));
                if (hasValidationAttribute)
                {
                    ValidationAttribute validationAttribute = property.GetCustomAttribute<ValidationAttribute>();
                    if (validationAttribute.Trigger == ValidationTrigger.Default)
                        ValidateProperty(property, validationAttribute);
                }
            }
        }

        #endregion

        #region Validation Errors
        private readonly ValidationErrors _validationErrors = new ValidationErrors();
        public ValidationErrors ValidationErrors
        {
            get { return _validationErrors; }
        }
        #endregion

        #region AttributesBasedValidation

        public bool ValidationSuprressed { get; set; }

        public bool ValidateAll()
        {
            bool result = true;
            Type type = this.GetType();
            var properties = type.GetRuntimeProperties().Where(prop => prop.IsDefined(typeof(ValidationAttribute), false));
            foreach (var property in properties)
            {
                ValidationAttribute validationAttribute = property.GetCustomAttribute<ValidationAttribute>();
                result &= ValidateProperty(property, validationAttribute);
            }
            return result;
        }

        public bool ValidateByTag(string tag)
        {
            if (String.IsNullOrEmpty(tag))
            {
                throw new Exception("Tag can't be a null or empty");
            }
            bool result = true;
            Type type = this.GetType();
            var properties = type.GetRuntimeProperties().Where(prop => prop.IsDefined(typeof(ValidationAttribute), false));
            foreach (var property in properties)
            {
                ValidationAttribute validationAttribute = property.GetCustomAttribute<ValidationAttribute>();
                if (validationAttribute.Tag == tag)
                {
                    result &= ValidateProperty(property, validationAttribute);
                }
            }
            return result;
        }

        //public bool ValidateProperty([CallerMemberName]string propertyName = null, Func<object, bool> IsValid, String  ErrorMessage)
        //{
        //    if (String.IsNullOrEmpty(propertyName)) throw new Exception("Property name can't be null or empty");

        //    Type type = this.GetType();
        //    PropertyInfo property = type.GetProperty(propertyName);
        //    object value = property.GetValue(this, null);
        //    if (!IsValid(value))
        //    {
        //        AddError(property.Name, ErrorMessage);
        //        return false;
        //    }
        //    else
        //    {s
        //        ClearError(property.Name);
        //        return true;
        //    }
        //}

        public bool ValidateProperty(Expression<Func<object>> expression)
        {
            string propertyName = GetPropertyName(expression);
            return ValidateProperty(propertyName);
        }

        public bool ValidateProperty([CallerMemberName]string propertyName = null)
        {
            if (String.IsNullOrEmpty(propertyName)) throw new Exception("Property name can't be null or empty");

            Type type = this.GetType();
            PropertyInfo property = type.GetRuntimeProperty(propertyName);
            if (property != null)
            {
                bool hasValidationAttribute = property.IsDefined(typeof(ValidationAttribute));
                if (hasValidationAttribute)
                {
                    ValidationAttribute validationAttribute = property.GetCustomAttribute<ValidationAttribute>();
                    return ValidateProperty(property, validationAttribute);
                }
                throw new Exception(String.Format("{0} Property doesn't exist", propertyName));
            }
            else
            {
                throw new Exception("Property doesn't have ValidationAttribute");
                //you Can choose to throw exception if property doesn't have attribute
                //Or you can choose to return true If There is no Validation Required for it
                //return true;
            }
        }

        protected bool ValidateProperty(PropertyInfo property, ValidationAttribute validationAttribute)
        {
            object value = property.GetValue(this, null);
            if (!validationAttribute.IsValid(value))
            {
                ValidationErrors[property.Name] = validationAttribute.ErrorMessageString;
                return false;
            }
            else
            {
                ValidationErrors.ClearError(property.Name);
                return true;
            }
        }
        #endregion

        #region Helper methods
        public static string GetPropertyName(Expression<Func<object>> expression)
        {
            if (expression == null)
                throw new ArgumentNullException("expression");

            MemberExpression memberExpression;

            if (expression.Body is UnaryExpression)
                memberExpression = ((UnaryExpression)expression.Body).Operand as MemberExpression;
            else
                memberExpression = expression.Body as MemberExpression;

            if (memberExpression == null)
                throw new ArgumentException("The expression is not a member access expression", "expression");

            var property = memberExpression.Member as PropertyInfo;

            if (property == null)
                throw new ArgumentException("The member access expression does not access a property", "expression");

            var getMethod = property.GetMethod;
            if (getMethod.IsStatic)
                throw new ArgumentException("The referenced property is a static property", "expression");
            return memberExpression.Member.Name;
        }

        public static Object GetPropValue(Object obj, String name)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetRuntimeProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        public static T GetPropValue<T>(Object obj, String name)
        {
            Object retval = GetPropValue(obj, name);
            if (retval == null) { return default(T); }
            ////extra , If we can change to this type, change it
            //var genericReturnValue = retval;
            //if (retval != null && retval.GetType() != typeof(T))
            //{
            //    if (retval is IConvertible)
            //    {
            //        genericReturnValue = Convert.ChangeType(retval, typeof(T), null);
            //    }
            //}
            //

            //throws InvalidCastException if types are incompatible
            return (T)retval;
        }

        #endregion
    }
}