﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Helpers.Validations
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class RequiredAttribute : ValidationAttribute
    {
        public RequiredAttribute()
        {
            ErrorMessageResource = "Required";
        }

        public override bool IsValid(object value)
        {
            if (value == null) return false;
            string text = value.ToString();
            if (string.IsNullOrEmpty(text)) return false;
            return true;
        }
    }
}