﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Common;

namespace Taaleem.Helpers.Validations
{
    public class ValidationErrors : BindableBase
    {
        private readonly Dictionary<string, string> _validationErrors = new Dictionary<string, string>();

        public bool IsValid
        {
            get
            {
                return this._validationErrors.Count < 1;
            }
        }

        public string this[string fieldName]
        {
            get
            {
                return this._validationErrors.ContainsKey(fieldName)
                    ? this._validationErrors[fieldName]
                    : string.Empty;
            }

            set
            {
                if (this._validationErrors.ContainsKey(fieldName))
                {
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        this._validationErrors.Remove(fieldName);
                    }
                    else
                    {
                        this._validationErrors[fieldName] = value;
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        this._validationErrors.Add(fieldName, value);
                    }
                }
                this.OnPropertyChanged(String.Format("Item[{0}]", fieldName));
                this.OnPropertyChanged("IsValid");
            }
        }

        public void ClearError(string propertyName)
        {
            if (_validationErrors.ContainsKey(propertyName))
            {
                _validationErrors.Remove(propertyName);
            }

            this.OnPropertyChanged(String.Format("Item[{0}]", propertyName));
            this.OnPropertyChanged("IsValid");
        }

    }

}
