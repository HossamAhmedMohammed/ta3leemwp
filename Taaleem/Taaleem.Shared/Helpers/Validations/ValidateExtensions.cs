﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace Taaleem.Helpers.Validations
{
    public static class ValidateExtensions
    {
        #region Lontidue Latitude
        public static bool ValidateLongtiudeLatitude(this ValidationBase validationBase, Expression<Func<object>> expression, String errorMessage)
        {
            //get property Name
            string propertyName = ValidationBase.GetPropertyName(expression);
            return validationBase.ValidateLongtiudeLatitude(propertyName, errorMessage);
        }

        public static bool ValidateLongtiudeLatitude(this ValidationBase validationBase, string propertyName, string errorMessage)
        {
            string value = ValidationBase.GetPropValue<string>(validationBase, propertyName);
            bool result;
            if (String.IsNullOrEmpty(value) || !IsValidLongtiudeLatitude(value))
            {

                validationBase.ValidationErrors[propertyName] = errorMessage;
                result = false;
            }
            else
            {
                validationBase.ValidationErrors.ClearError(propertyName);
                result = true;
            }
            return result;

        }


        public static bool IsValidLongtiudeLatitude(string value)
        {
            if (String.IsNullOrEmpty(value)) return false;
            String[] parts = value.Split(',');
            if (parts.Length != 2) return false;
            double longtuide, latitude;
            if (double.TryParse(parts[0].Trim(), out longtuide)
                && double.TryParse(parts[1].Trim(), out latitude))
                return true;
            return false;
        }
        #endregion

        public static bool ValidateEMailFormat(this ValidationBase validationBase, Expression<Func<object>> expression, String errorMessage)
        {
            //get property Name
            string propertyName = ValidationBase.GetPropertyName(expression);
            return validationBase.ValidateEMailFormat(propertyName, errorMessage);
        }

        public static bool ValidateEMailFormat(this ValidationBase validationBase, string propertyName, String errorMessage)
        {
            string value = ValidationBase.GetPropValue<string>(validationBase, propertyName);
            bool result;
            if (String.IsNullOrEmpty(value) || !IsValidEmail(value))
            {

                validationBase.ValidationErrors[propertyName] = errorMessage;
                result = false;
            }
            else
            {
                validationBase.ValidationErrors.ClearError(propertyName);
                result = true;
            }
            return result;
        }

        public static bool IsValidEmail(string value)
        {
            if (String.IsNullOrEmpty(value)) return false;
            return Regex.IsMatch(value, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }

        #region Required Validations
        public static bool ValidateRequired(this ValidationBase validationBase, Expression<Func<object>> expression, String errorMessage)
        {
            //get property Name
            string propertyName = ValidationBase.GetPropertyName(expression);
            return validationBase.ValidateRequired(propertyName, errorMessage);
        }

        public static bool ValidateRequired(this ValidationBase validationBase, string propertyName, String errorMessage)
        {
            string value = ValidationBase.GetPropValue<string>(validationBase, propertyName);
            bool result;
            if (String.IsNullOrEmpty(value))
            {
                validationBase.ValidationErrors[propertyName] = errorMessage;
                result = true;
            }
            else
            {
                validationBase.ValidationErrors.ClearError(propertyName);
                result = false;
            }
            return result;
        }
        #endregion


        #region Integer Validations
        public static bool ValidatePositiveInteger(this ValidationBase validationBase, Expression<Func<object>> expression, String errorMessage)
        {
            //get property Name
            string propertyName = ValidationBase.GetPropertyName(expression);
            return validationBase.ValidatePositiveInteger(propertyName, errorMessage);
        }

        public static bool ValidatePositiveInteger(this ValidationBase validationBase, string propertyName, String errorMessage, int minNumber = 1)
        {
            string value = ValidationBase.GetPropValue<string>(validationBase, propertyName);
            bool result;
            if (!IsPositiveInteger(value, minNumber))
            {

                validationBase.ValidationErrors[propertyName] = errorMessage;
                result = false;
            }
            else
            {
                validationBase.ValidationErrors.ClearError(propertyName);
                result = true;
            }
            return result;
        }

        public static bool IsPositiveInteger(string value, int minNumber = 1)
        {
            if (String.IsNullOrWhiteSpace(value)) return false;
            if (value.Length < minNumber) return false;
            for (int i = 0; i < value.Length; i++)
            {
                if (!Char.IsDigit(value[i])) return false;
            }
            return true;
        }

        #endregion
    }
}
