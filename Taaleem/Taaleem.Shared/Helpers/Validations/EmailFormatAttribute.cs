﻿using System;
using System.Text.RegularExpressions;

namespace Taaleem.Helpers.Validations
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class EmailFormatAttribute : ValidationAttribute
    {
        readonly bool allowEmpty;

        public EmailFormatAttribute(bool allowEmpty = false)
        {
            this.allowEmpty = allowEmpty;
            ErrorMessageResource = "EmailFormatError";
        }

        public override bool IsValid(object value)
        {
            if (value == null) return allowEmpty;
            string text = value.ToString();
            if (String.IsNullOrEmpty(text)) return allowEmpty;
            return IsValidEmail(text);
        }

        private bool IsValidEmail(string value)
        {
            return Regex.IsMatch(value, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }
    }
}