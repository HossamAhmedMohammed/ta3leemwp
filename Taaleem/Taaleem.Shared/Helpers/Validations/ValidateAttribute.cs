﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Helpers.Validations
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public abstract class ValidationAttribute : Attribute
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ValidationAttribute()
        {
        }

        /// <summary>
        /// Use Tag to group Properties so you can validate them in batch later
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// Gets or sets the error message resource name to use in order to look up the ErrorMessageResourceType property value if validation fails.
        /// </summary>
        public string ErrorMessageResource { get; set; }

        /// <summary>
        /// Localized Error Message
        /// </summary>
        public string ErrorMessageString
        {
            get
            {
                if (ErrorMessageAccessor == null) throw new Exception("errorMessageAccessor is null \n Assign it at the start of application with your Localized Resources Manager");
                return ErrorMessageAccessor(ErrorMessageResource);
            }
        }

        /// <summary>
        /// Assign it at the start of application with your Localized Resources Manager
        /// </summary>
        public static Func<string, string> ErrorMessageAccessor;

        /// <summary>
        /// Override this based on validation logic in your class
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public abstract bool IsValid(object value);

        private ValidationTrigger _trigger = ValidationTrigger.Default;

        /// <summary>
        /// Determines whether Validation will be automatically or explicitly
        /// Default means Validation will be automatically
        /// </summary>
        public ValidationTrigger Trigger
        {
            get { return _trigger; }
            set { _trigger = value; }
        }

    }

    /// <summary>
    /// Determines whether Validation will be automatically or explicitly
    /// Default means Validation will be automatically
    /// </summary>
    public enum ValidationTrigger
    {
        /// <summary>
        /// Will be validated automatically on property changed
        /// </summary>
        Default = 0,
        /// <summary>
        /// You need to call  ValidateProperty, ValidateByTag Or ValidateAll explicitly,
        /// No Validation for the marked property is done on property changed
        /// </summary>
        Explicit = 1
    }
}

