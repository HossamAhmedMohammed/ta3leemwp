﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Helpers.Validations
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class LongtiudeLatitudeAttribute : ValidationAttribute
    {
        readonly bool allowEmpty;

        public LongtiudeLatitudeAttribute(bool allowEmpty = false)
        {
            this.allowEmpty = allowEmpty;

            ErrorMessageResource = "LongtiudeLatitudeError";
        }

        public override bool IsValid(object value)
        {
            if (value == null) return allowEmpty;
            string text = value.ToString();
            if (String.IsNullOrEmpty(text)) return allowEmpty;
            return IsValidLongtiudeLatitude(text);
        }

        private bool IsValidLongtiudeLatitude(string value)
        {
            if (String.IsNullOrEmpty(value)) return false;
            String[] parts = value.Split(',');
            if (parts.Length != 2) return false;
            double longtuide, latitude;
            if (double.TryParse(parts[0].Trim(), out longtuide)
                && double.TryParse(parts[1].Trim(), out latitude))
                return true;
            return false;
        }

    }
}