﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Helpers.Validations
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class PositiveIntegerNumberAttribute : ValidationAttribute
    {
        readonly bool allowEmpty;

        public PositiveIntegerNumberAttribute(bool allowEmpty = false)
        {
            this.allowEmpty = allowEmpty;
        }
        public override bool IsValid(object value)
        {
            if (value == null) return allowEmpty;
            string text = value.ToString();
            if (String.IsNullOrEmpty(text)) return allowEmpty;
            return IsIntegerNumber(text);
        }
        private bool IsIntegerNumber(string value)
        {
            foreach (char ch in value)
            {
                if (!char.IsDigit(ch)) return false;
            }
            return true;
        }

    }
}