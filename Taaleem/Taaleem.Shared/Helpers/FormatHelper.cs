﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Taaleem.Helpers
{
    public static class FormatHelper
    {
        public static string SingleFractionOrIntCurrentCulture(this double number)
        {

            return Math.Abs((number % 1)) < 1e-6 ? number.ToString("0", CultureInfo.InvariantCulture) : number.ToString("0.0", CultureInfo.InvariantCulture);
            // return number.ToString(Math.Abs((number % 1)) < 1e-6 ? "0" : "0.0", CultureInfo.CurrentCulture);
            //return string.Format(CultureInfo.CurrentCulture, Math.Abs((number % 1)) < 1e-6 ? "{0:0}" : "{0:0.0}", number);
        }
    }
}
