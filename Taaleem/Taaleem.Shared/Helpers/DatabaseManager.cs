﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children.Database;
using Windows.Storage;

namespace Taaleem.Helpers
{
    public class DatabaseManager : IDatabaseManager
    {
        private const int LatestDataBaseVersion = 0;
        public readonly static string DbPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "Storage.sqlite");

        private static bool IsMigrationEnabled = false;
        public async Task CreateOrMigrateDatabaseAsync(string dbPath)
        {
            var databaseExists = await GetIfFileExistsAsync(dbPath) != null;
            if (IsMigrationEnabled && databaseExists)
            {
                //Migrate based on version
                MigrateDatabase(dbPath);
            }
            else
            {
                //Create new database
                CreateDataBase(dbPath);
            }
        }

        private bool MigrateDatabase(string dbPath)
        {

            var con = new SQLiteConnection(dbPath);
            using (con)
            {
                int currentVersion = GetDatabaseVersion(con);
                if (currentVersion == LatestDataBaseVersion) return true;
                try
                {
                    con.BeginTransaction();
                    if (currentVersion == 0)
                        MigrateToVersion1(con);

                    con.Commit();

                    return true;
                }
                catch (Exception)
                {
                    con.Rollback();
                    return false;
                }
            }
        }


        #region Migration Methods
        private void MigrateToVersion1(SQLiteConnection con)
        {
            SetDatabaseVersion(con, 1);
        }

        #endregion

        private int GetDatabaseVersion(SQLiteConnection con)
        {
            var query = "pragma user_version";
            int version = con.ExecuteScalar<int>(query);
            return version;
        }

        private void SetDatabaseVersion(SQLiteConnection con, int version)
        {
            var query = String.Format("pragma user_version={0}", version);
            int count = con.Execute(query);
        }
        private async Task<StorageFile> GetIfFileExistsAsync(string key)
        {
            try
            {
                return await StorageFile.GetFileFromPathAsync(key);
            }
            catch (FileNotFoundException) { return default(StorageFile); }
        }

        private void CreateDataBase(string dbPath)
        {

            // Initialize the database if necessary
            using (var db = new SQLiteConnection(dbPath))
            {
                // Create the tables if they don't exist

                SetDatabaseVersion(db, LatestDataBaseVersion);
                db.CreateTable<FeedData>();
            }
        }
    }
}
