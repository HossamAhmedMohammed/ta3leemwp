﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace Taaleem.Helpers
{
    public class FileSystemUtils
    {
        #region Constants
        private const string SettingsPath = @"settings\{0}";//{0] key     
        #endregion

        #region Methods



        public async static Task<StorageFolder> GetLocalDirectoryAsync(StorageFolder folder, string path)
        {
            return await folder.GetItemAsync(path) as StorageFolder;
        }

        //TODO:Catch File Not found exception caused by replacing TryGetItemAsync to GetItemAsync

        /// <summary>
        /// Gets a folder with relative path to application local folder
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public async static Task<StorageFolder> GetLocalDirectoryAsync(string path)
        {
            return await ApplicationData.Current.LocalFolder.GetItemAsync(path) as StorageFolder;
        }

        public async static Task<StorageFolder> CreateLocalFolderAsync(string path)
        {
            try
            {
                var folder = ApplicationData.Current.LocalFolder;
                var directories = GetDirectories(path);
                foreach (var directory in directories)
                {
                    folder = await folder.CreateFolderAsync(directory, CreationCollisionOption.OpenIfExists);
                }
                return folder;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async Task<bool> DeleteLocalFolder(StorageFolder folder, string path)
        {
            try
            {
                folder = await folder.GetItemAsync(path) as StorageFolder;
                if (folder != null)
                {
                    await folder.DeleteAsync(StorageDeleteOption.PermanentDelete);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static async Task<bool> DeleteLocalFolder(string path)
        {
            return await DeleteLocalFolder(ApplicationData.Current.LocalFolder, path);
        }

        public static async Task<bool> DeleteFileAsync(IStorageFile file)
        {
            try
            {
                if (file == null) return false;
                await file.DeleteAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static async Task<bool> DeleteFolderAsync(IStorageFolder folder)
        {
            try
            {
                if (folder == null) return false;
                await folder.DeleteAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async static Task<StorageFile> GetLocalFile(StorageFolder folder, string path)
        {
            try
            {
                return await folder.GetItemAsync(path) as StorageFile;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async static Task<StorageFile> GetLocalFile(string path)
        {
            try
            {
                return await ApplicationData.Current.LocalFolder.GetItemAsync(path) as StorageFile;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async static Task<StorageFile> CreateLocalFile(StorageFolder folder, string path)
        {
            try
            {
                var directories = GetDirectories(Path.GetDirectoryName(path));
                foreach (var directory in directories)
                {
                    folder = await folder.CreateFolderAsync(directory, CreationCollisionOption.OpenIfExists);
                }
                var fileName = Path.GetFileName(path);
                return await folder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async static Task<StorageFile> CreateLocalFile(string path)
        {
            return await CreateLocalFile(ApplicationData.Current.LocalFolder, path);
        }

        private static string[] GetDirectories(string path)
        {
            var directories = path.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
            return directories;
        }



        public async static Task<BitmapImage> GetBitMapFromFile(StorageFile image)
        {
            BitmapImage bitmapImage = null;
            if (image != null)
            {
                bitmapImage = new BitmapImage();
                using (var fileStream = await image.OpenAsync(FileAccessMode.Read))
                {
                    bitmapImage.SetSource(fileStream);
                }

            }
            return bitmapImage;
        }


        public async static Task<StorageFile> CopyFileToFolder(StorageFile anyFile, StorageFolder anyFolder)
        {
            var newDirectory = await anyFile.CopyAsync(anyFolder, anyFile.Name);
            return newDirectory;
        }

        public async static Task<StorageFile> DuplicateFileToFolder(StorageFile anyFile, StorageFolder anyFolder, string fileName)
        {
            var newDirectory = await anyFile.CopyAsync(anyFolder, fileName, NameCollisionOption.ReplaceExisting);
            return newDirectory;
        }
        public async static Task MoveFileToFolder(StorageFile anyFile, StorageFolder anyFolder)
        {
            await anyFile.MoveAsync(anyFolder, anyFile.Name);
        }

        public async static Task<StorageFile> ZipContentFile(StorageFile zipFile, StorageFile file)
        {

            using (var zipToOpen = await zipFile.OpenStreamForWriteAsync())
            {
                using (var archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                {
                    var readmeEntry = archive.CreateEntry(file.Name);
                    using (var writer = readmeEntry.Open())
                    {
                        //writer.WriteLine("Information about this package.");
                        //writer.WriteLine("========================");
                        var data = await GetByteFromFile(file);
                        writer.Write(data, 0, data.Length);
                    }
                }
            }
            return zipFile;
        }

        public async static Task<StorageFile> CreateEmptyZipFile(string zipFileName, StorageFolder folder)
        {
            var zipFile = await folder.CreateFileAsync("compressed.zip", CreationCollisionOption.ReplaceExisting);
            return zipFile;
        }


        private async static Task<byte[]> GetByteFromFile(StorageFile storageFile)
        {
            var stream = await storageFile.OpenReadAsync();

            using (var dataReader = new DataReader(stream))
            {
                var bytes = new byte[stream.Size];
                await dataReader.LoadAsync((uint)stream.Size);
                dataReader.ReadBytes(bytes);

                return bytes;
            }
        }

        #endregion
    }

}
