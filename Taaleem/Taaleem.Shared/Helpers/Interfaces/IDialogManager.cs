﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Models.Children.Response;

namespace Taaleem.Helpers.Interfaces
{
    public interface IDialogManager
    {
        Task ShowContactTeacherDialog(ContactTeacher contactTeacher);
        Task<bool> ShowMessage(string title, string content);

        /// <summary>
        /// Returns selected child index
        /// </summary>
        /// <returns></returns>
        Task<bool> ShowChildrenSelectionDialog();
        Task ShowParentLoginDialog();
        Task ShowStudentLoginDialog();
        Task ShowEmployeeLoginDialog();
        Task ShowTansferMyChiledrenDialog();
        void CloseDialog();
    }
}
