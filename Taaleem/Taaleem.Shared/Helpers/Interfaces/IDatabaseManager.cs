﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Taaleem.Helpers.Interfaces
{
    public interface IDatabaseManager
    {
        Task CreateOrMigrateDatabaseAsync(string dbPath);
    }
}