﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Models.Children;
using Taaleem.Models.Employees.Response;
using Taaleem.Models.SchoolRegisteration.Response;
using Windows.Security.Credentials;

namespace Taaleem.Helpers.Interfaces
{
    public interface IAppSettings
    {

        //Children Service
        int LanguageId { get; set; }
        int SelectedChildIndex { get; set; }
        bool IsParentMode { get; set; }
        bool IsStudentMode { get; set; }
        bool AmIParent { get; set; }


        Task<ParentLoginWrapper> GetParentLoginResponseAsync();
        Task SetParentLoginResponseAsync(ParentLoginWrapper value);

        Task<ParentLoginWrapper> GetStudentLoginResponseAsync();
        Task SetStudentLoginResponseAsync(ParentLoginWrapper value);

        Task<EmployeeLoginResponse> GetEmployeeLoginResponseAsync();
        Task SetEmployeeLoginResponseAsync(EmployeeLoginResponse value); 

        PasswordCredential ParentCredential { get; }
        void SetParentCredential(String userName, String password);
        void RemoveParentCredential();

        PasswordCredential StudentCredential { get; }
        void SetStudentCredential(String userName, String password);
        void RemoveStudentCredential();

        //Employee Service
        string EmployeeId { get; set; }
        PasswordCredential EmployeeCredential { get; }
        void SetEmployeeCredential(String userName, String password);
        void RemoveEmployeeCredential();

        /// <summary>
        /// Parent Info of school registeration Service
        /// </summary>
        ParentInfo ParentRegisterationInfo { get; set; }
        /// <summary>
        /// Credentials of parent of school registeration Service
        /// </summary>
        PasswordCredential SchoolRegisterationCredential { get; }
        void SetSchoolRegisterationCredential(String userName, String password);
        void RemoveSchoolRegisterationredential();

        int SelectedStageIndex { get; set; }

        string LoginUserName { get; set; }
        string LoginPassword { get; set; }
        int CurrentModule { get; set; }
        string AuthCookie { get; set; }
    }
}
