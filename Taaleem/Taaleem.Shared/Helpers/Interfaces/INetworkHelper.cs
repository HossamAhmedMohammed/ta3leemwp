﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Helpers.Interfaces
{
    public interface INetworkHelper
    {
        bool HasInternetAccess();
    }
}
