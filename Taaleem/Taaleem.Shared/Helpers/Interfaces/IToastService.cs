﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Helpers.Interfaces
{
    public interface IToastService
    {
        void ShowToastThreeLines(string message);
        void ShowToastTwoLinesBody(string title, string message);
    }
}
