﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Collections.Extended;
using Taaleem.Models;
using Windows.UI.Xaml.Data;

namespace Taaleem.Helpers
{
    public class ListIndexer
    {
        public static void AssignIndex<T>(IList<T> list) where T : IIndex
        {
            if (list == null || list.Count == 0) return;
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                if (item != null) item.IndexInList = i;
            }
        }

        public static void AssignIndex(IList<object> list)
        {
            if (list == null || list.Count == 0) return;
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                if (item is IIndex) (item as IIndex).IndexInList = i;
            }
        }


        /// <summary>
        /// Call this method after you set collectionView.Source, This is used with Grouped collections
        /// </summary>
        public static void AssignIndex(ICollectionView collectionView)
        {
            if (collectionView.CollectionGroups != null && collectionView.CollectionGroups.Count > 0)
            {
                foreach (var collectionGroup in collectionView.CollectionGroups)
                {
                    var groupList = collectionGroup as GroupList;
                    if (groupList == null) continue;
                    AssignIndex(groupList.GroupItems);
                }
            }
        }
    }
}
