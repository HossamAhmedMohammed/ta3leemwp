﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Taaleem.Helpers
{
    public static class ReflectionExensions
    {
        public static object GetPropertyValue(this object obj, string propertyName)
        {
            if (String.IsNullOrEmpty(propertyName)) return null;
            var propertyNames = propertyName.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            if (propertyNames == null) return null;
            foreach (var name in propertyNames)
            {
                var typeInfo = obj.GetType().GetTypeInfo();
                var property = typeInfo.GetDeclaredProperty(name);
                obj = property.GetValue(obj);
                if (obj == null) return null;
            }
            return obj;
        }
    }
}
