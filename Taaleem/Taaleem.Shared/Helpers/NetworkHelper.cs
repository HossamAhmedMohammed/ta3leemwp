﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Networking.Connectivity;

namespace Taaleem.Helpers.Interfaces
{
    public class NetworkHelper : INetworkHelper
    {
        public bool HasInternetAccess()
        {

            ConnectionProfile connections = NetworkInformation.GetInternetConnectionProfile();
            bool internet = connections != null && connections.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
            return internet;
        }
    }
}
