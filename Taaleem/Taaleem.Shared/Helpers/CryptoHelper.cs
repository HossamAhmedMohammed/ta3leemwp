﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.DataProtection;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Taaleem.Helpers
{
    public class CryptoHelper
    {
        public const String CryptoExtension = ".cryto";
        private async Task<StorageFile> EncryptStorageFileAsync(StorageFile file, String encryptedFileName, IStorageFolder folder, String protectionDescriptor)
        {
            try
            {

                IBuffer fileStreamBuffer = await FileIO.ReadBufferAsync(file);
                if (fileStreamBuffer.Length == 0)
                {
                    return null;
                }
                DataProtectionProvider provider =
                    String.IsNullOrEmpty(protectionDescriptor)
                        ? new DataProtectionProvider()
                        : new DataProtectionProvider(protectionDescriptor);

                IBuffer encryptedBuffer = await provider.ProtectAsync(fileStreamBuffer);

                StorageFile encryptedFile =
                    await folder.CreateFileAsync(encryptedFileName, CreationCollisionOption.ReplaceExisting);

                await FileIO.WriteBufferAsync(encryptedFile, encryptedBuffer);
                return encryptedFile;
            }
            catch (Exception)
            {
                return null;
            }

        }

        private async Task<StorageFile> DecryptStorageFileAsync(StorageFile file, String plainFileName, IStorageFolder folder, String protectionDescriptor)
        {
            try
            {

                IBuffer fileStreamBuffer = await FileIO.ReadBufferAsync(file);
                if (fileStreamBuffer.Length == 0)
                {
                    return null;
                }
                DataProtectionProvider provider =
                    String.IsNullOrEmpty(protectionDescriptor)
                        ? new DataProtectionProvider()
                        : new DataProtectionProvider(protectionDescriptor);

                IBuffer decryptedStreamBuffer = await provider.UnprotectAsync(fileStreamBuffer);

                StorageFile plainFile =
                    await folder.CreateFileAsync(plainFileName, CreationCollisionOption.ReplaceExisting);

                await FileIO.WriteBufferAsync(plainFile, decryptedStreamBuffer);
                //GC.Collect();
                return plainFile;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public async Task<Stream> DecryptFileToStreamAsync(IStorageFile file, String protectionDescriptor = "LOCAL = machine")
        {
            try
            {

                IBuffer fileStreamBuffer = await FileIO.ReadBufferAsync(file);
                if (fileStreamBuffer.Length == 0)
                {
                    return null;
                }
                DataProtectionProvider provider =
                    String.IsNullOrEmpty(protectionDescriptor)
                        ? new DataProtectionProvider()
                        : new DataProtectionProvider(protectionDescriptor);

                IBuffer decryptedStreamBuffer = await provider.UnprotectAsync(fileStreamBuffer);
                return decryptedStreamBuffer.AsStream();
            }
            catch (Exception)
            {
                return null;
            }

        }

        public async Task EncryptFile(StorageFile fileForEncryption, StorageFolder folder)
        {
            await EncryptStorageFileAsync(fileForEncryption, fileForEncryption.Name + CryptoExtension, folder, "LOCAL = machine");
        }

        public async Task<StorageFile> DecryptFile(StorageFile encryptedFile, IStorageFolder parentFolder)
        {
            return await DecryptStorageFileAsync(encryptedFile, Path.GetFileNameWithoutExtension(encryptedFile.Name), parentFolder, "LOCAL = machine");
        }

        public async Task<string> ProtectAsync(string clearText, string protectionDescriptor = "LOCAL = machine")
        {
            if (clearText == null)
                throw new ArgumentNullException("clearText");
            if (protectionDescriptor == null)
                throw new ArgumentNullException("protectionDescriptor");

            var clearBuffer = CryptographicBuffer.ConvertStringToBinary(clearText, BinaryStringEncoding.Utf8);
            var provider = new DataProtectionProvider(protectionDescriptor);
            var encryptedBuffer = await provider.ProtectAsync(clearBuffer);
            return CryptographicBuffer.EncodeToBase64String(encryptedBuffer);
        }

        public async Task<string> UnprotectAsync(string encryptedText, string protectionDescriptor = "LOCAL = machine")
        {
            if (encryptedText == null)
                throw new ArgumentNullException("encryptedText");

            if (protectionDescriptor == null)
                throw new ArgumentNullException("protectionDescriptor");

            var encryptedBuffer = CryptographicBuffer.DecodeFromBase64String(encryptedText);
            var provider = new DataProtectionProvider(protectionDescriptor);
            var clearBuffer = await provider.UnprotectAsync(encryptedBuffer);
            return CryptographicBuffer.ConvertBinaryToString(BinaryStringEncoding.Utf8, clearBuffer);
        }
    }
}
