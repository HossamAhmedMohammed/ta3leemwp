﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Helpers
{
    using Windows.ApplicationModel.Resources;


    public partial class Resources
    {

        private ResourceLoader resourceLoader;

        public Resources()
        {
            string executingAssemblyName = Windows.UI.Xaml.Application.Current.GetType().AssemblyQualifiedName;
            string[] executingAssemblySplit = executingAssemblyName.Split(',');
            executingAssemblyName = executingAssemblySplit[1];
            string currentAssemblyName = typeof(Resources).AssemblyQualifiedName;
            string[] currentAssemblySplit = currentAssemblyName.Split(',');
            currentAssemblyName = currentAssemblySplit[1];
            if (executingAssemblyName.Equals(currentAssemblyName) || Windows.ApplicationModel.DesignMode.DesignModeEnabled)
            {
                resourceLoader = ResourceLoader.GetForViewIndependentUse("Resources");
            }
            else
            {
                resourceLoader = ResourceLoader.GetForViewIndependentUse(currentAssemblyName + "/Resources");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ??????"
        /// </summary>
        public virtual string AboutMagles
        {
            get
            {
                return resourceLoader.GetString("AboutMagles");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????? ???????"
        /// </summary>
        public virtual string ActiveLoans
        {
            get
            {
                return resourceLoader.GetString("ActiveLoans");
            }
        }

        /// <summary>
        /// Localized resource similar to "????"
        /// </summary>
        public virtual string All
        {
            get
            {
                return resourceLoader.GetString("All");
            }
        }

        /// <summary>
        /// Localized resource similar to "????"
        /// </summary>
        public virtual string AllNotifications
        {
            get
            {
                return resourceLoader.GetString("AllNotifications");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????????"
        /// </summary>
        public virtual string Announcements
        {
            get
            {
                return resourceLoader.GetString("Announcements");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Apply
        {
            get
            {
                return resourceLoader.GetString("Apply");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Attendance
        {
            get
            {
                return resourceLoader.GetString("Attendance");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Balance
        {
            get
            {
                return resourceLoader.GetString("Balance");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Behavior
        {
            get
            {
                return resourceLoader.GetString("Behavior");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ??????"
        /// </summary>
        public virtual string BehaviorDetails
        {
            get
            {
                return resourceLoader.GetString("BehaviorDetails");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Calculated
        {
            get
            {
                return resourceLoader.GetString("Calculated");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Cancel
        {
            get
            {
                return resourceLoader.GetString("Cancel");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ????? ?????? ???????"
        /// </summary>
        public virtual string ChooseChildToFollow
        {
            get
            {
                return resourceLoader.GetString("ChooseChildToFollow");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ???????"
        /// </summary>
        public virtual string ChooseFilter
        {
            get
            {
                return resourceLoader.GetString("ChooseFilter");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string ClassPeriod
        {
            get
            {
                return resourceLoader.GetString("ClassPeriod");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??? ??? ????? ???????? ??? ???? ?????"
        /// </summary>
        public virtual string ClientSideError
        {
            get
            {
                return resourceLoader.GetString("ClientSideError");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Close
        {
            get
            {
                return resourceLoader.GetString("Close");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string ComingSoon
        {
            get
            {
                return resourceLoader.GetString("ComingSoon");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????????"
        /// </summary>
        public virtual string Comments
        {
            get
            {
                return resourceLoader.GetString("Comments");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????? ????????"
        /// </summary>
        public virtual string CompletedLoans
        {
            get
            {
                return resourceLoader.GetString("CompletedLoans");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string Connecting
        {
            get
            {
                return resourceLoader.GetString("Connecting");
            }
        }

        /// <summary>
        /// Localized resource similar to "ar"
        /// </summary>
        public virtual string Culture
        {
            get
            {
                return resourceLoader.GetString("Culture");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Degree
        {
            get
            {
                return resourceLoader.GetString("Degree");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????? ??"
        /// </summary>
        public virtual string DeliveredAt
        {
            get
            {
                return resourceLoader.GetString("DeliveredAt");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ??????  "
        /// </summary>
        public virtual string DeliveryDueStart
        {
            get
            {
                return resourceLoader.GetString("DeliveryDueStart");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ????"
        /// </summary>
        public virtual string DoesnotExist
        {
            get
            {
                return resourceLoader.GetString("DoesnotExist");
            }
        }

        /// <summary>
        /// Localized resource similar to "????"
        /// </summary>
        public virtual string Due
        {
            get
            {
                return resourceLoader.GetString("Due");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????????"
        /// </summary>
        public virtual string EmployeeServices
        {
            get
            {
                return resourceLoader.GetString("EmployeeServices");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????? {0}"
        /// </summary>
        public virtual string EnterField
        {
            get
            {
                return resourceLoader.GetString("EnterField");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????? ??? ???????? ? ???? ??????"
        /// </summary>
        public virtual string EnterUserAndPassword
        {
            get
            {
                return resourceLoader.GetString("EnterUserAndPassword");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??? ?? ???????? ????? ????? ????????"
        /// </summary>
        public virtual string ErrorConnecting
        {
            get
            {
                return resourceLoader.GetString("ErrorConnecting");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string Evaluation
        {
            get
            {
                return resourceLoader.GetString("Evaluation");
            }
        }

        /// <summary>
        /// Localized resource similar to " ??????? ????: "
        /// </summary>
        public virtual string EvaluationForYear
        {
            get
            {
                return resourceLoader.GetString("EvaluationForYear");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ?????? ?? ??? ???????? ? ???? ??????"
        /// </summary>
        public virtual string FailedToLogin
        {
            get
            {
                return resourceLoader.GetString("FailedToLogin");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string Filtration
        {
            get
            {
                return resourceLoader.GetString("Filtration");
            }
        }

        /// <summary>
        /// Localized resource similar to "RightToLeft"
        /// </summary>
        public virtual string FlowDirection
        {
            get
            {
                return resourceLoader.GetString("FlowDirection");
            }
        }

        /// <summary>
        /// Localized resource similar to "????"
        /// </summary>
        public virtual string ForYear
        {
            get
            {
                return resourceLoader.GetString("ForYear");
            }
        }

        /// <summary>
        /// Localized resource similar to "??"
        /// </summary>
        public virtual string From
        {
            get
            {
                return resourceLoader.GetString("From");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ???? ???"
        /// </summary>
        public virtual string Future
        {
            get
            {
                return resourceLoader.GetString("Future");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Grade
        {
            get
            {
                return resourceLoader.GetString("Grade");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string Grades
        {
            get
            {
                return resourceLoader.GetString("Grades");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????? ??????"
        /// </summary>
        public virtual string HighSchool
        {
            get
            {
                return resourceLoader.GetString("HighSchool");
            }
        }

        /// <summary>
        /// Localized resource similar to "????????"
        /// </summary>
        public virtual string HomeWork
        {
            get
            {
                return resourceLoader.GetString("HomeWork");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string ImportantNotifications
        {
            get
            {
                return resourceLoader.GetString("ImportantNotifications");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Late
        {
            get
            {
                return resourceLoader.GetString("Late");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Later
        {
            get
            {
                return resourceLoader.GetString("Later");
            }
        }

        /// <summary>
        /// Localized resource similar to "????????"
        /// </summary>
        public virtual string Loans
        {
            get
            {
                return resourceLoader.GetString("Loans");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ????????"
        /// </summary>
        public virtual string LoansStatus
        {
            get
            {
                return resourceLoader.GetString("LoansStatus");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??????"
        /// </summary>
        public virtual string Login
        {
            get
            {
                return resourceLoader.GetString("Login");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string MonthlySalary
        {
            get
            {
                return resourceLoader.GetString("MonthlySalary");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string News
        {
            get
            {
                return resourceLoader.GetString("News");
            }
        }

        /// <summary>
        /// Localized resource similar to "??"
        /// </summary>
        public virtual string No
        {
            get
            {
                return resourceLoader.GetString("No");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ???? ???????"
        /// </summary>
        public virtual string NoComments
        {
            get
            {
                return resourceLoader.GetString("NoComments");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ???? ?????"
        /// </summary>
        public virtual string NoConsequences
        {
            get
            {
                return resourceLoader.GetString("NoConsequences");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ???? ?????"
        /// </summary>
        public virtual string NoData
        {
            get
            {
                return resourceLoader.GetString("NoData");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ???? ???"
        /// </summary>
        public virtual string NoDescription
        {
            get
            {
                return resourceLoader.GetString("NoDescription");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ?????? ?? ??????? ?????????"
        /// </summary>
        public virtual string NoInternet
        {
            get
            {
                return resourceLoader.GetString("NoInternet");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??????"
        /// </summary>
        public virtual string NoOfDays
        {
            get
            {
                return resourceLoader.GetString("NoOfDays");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ???? ????"
        /// </summary>
        public virtual string NoPeriod
        {
            get
            {
                return resourceLoader.GetString("NoPeriod");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????????"
        /// </summary>
        public virtual string Notifications
        {
            get
            {
                return resourceLoader.GetString("Notifications");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Ok
        {
            get
            {
                return resourceLoader.GetString("Ok");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string Paid
        {
            get
            {
                return resourceLoader.GetString("Paid");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??????"
        /// </summary>
        public virtual string ParentsService
        {
            get
            {
                return resourceLoader.GetString("ParentsService");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ??????"
        /// </summary>
        public virtual string Password
        {
            get
            {
                return resourceLoader.GetString("Password");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Period
        {
            get
            {
                return resourceLoader.GetString("Period");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Previously
        {
            get
            {
                return resourceLoader.GetString("Previously");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????"
        /// </summary>
        public virtual string PublicServices
        {
            get
            {
                return resourceLoader.GetString("PublicServices");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Refresh
        {
            get
            {
                return resourceLoader.GetString("Refresh");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????????"
        /// </summary>
        public virtual string Reminders
        {
            get
            {
                return resourceLoader.GetString("Reminders");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string Result
        {
            get
            {
                return resourceLoader.GetString("Result");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string Results
        {
            get
            {
                return resourceLoader.GetString("Results");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Returned
        {
            get
            {
                return resourceLoader.GetString("Returned");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Salary
        {
            get
            {
                return resourceLoader.GetString("Salary");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ???????"
        /// </summary>
        public virtual string ScholarshipsRegisteration
        {
            get
            {
                return resourceLoader.GetString("ScholarshipsRegisteration");
            }
        }

        /// <summary>
        /// Localized resource similar to "????"
        /// </summary>
        public virtual string Send
        {
            get
            {
                return resourceLoader.GetString("Send");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????? ????????"
        /// </summary>
        public virtual string SendEmail
        {
            get
            {
                return resourceLoader.GetString("SendEmail");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ????? ????"
        /// </summary>
        public virtual string SendPrivateNote
        {
            get
            {
                return resourceLoader.GetString("SendPrivateNote");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ???????"
        /// </summary>
        public virtual string ShowResults
        {
            get
            {
                return resourceLoader.GetString("ShowResults");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??????"
        /// </summary>
        public virtual string SituationDescription
        {
            get
            {
                return resourceLoader.GetString("SituationDescription");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ??? ????????"
        /// </summary>
        public virtual string SpeakToTeacher
        {
            get
            {
                return resourceLoader.GetString("SpeakToTeacher");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Status
        {
            get
            {
                return resourceLoader.GetString("Status");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Subject
        {
            get
            {
                return resourceLoader.GetString("Subject");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Submitted
        {
            get
            {
                return resourceLoader.GetString("Submitted");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??? ?? ???????"
        /// </summary>
        public virtual string SubmittingFailed
        {
            get
            {
                return resourceLoader.GetString("SubmittingFailed");
            }
        }

        /// <summary>
        /// Localized resource similar to "????"
        /// </summary>
        public virtual string Success
        {
            get
            {
                return resourceLoader.GetString("Success");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????????"
        /// </summary>
        public virtual string Teachers
        {
            get
            {
                return resourceLoader.GetString("Teachers");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string TheHour
        {
            get
            {
                return resourceLoader.GetString("TheHour");
            }
        }

        /// <summary>
        /// Localized resource similar to "???"
        /// </summary>
        public virtual string To
        {
            get
            {
                return resourceLoader.GetString("To");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Today
        {
            get
            {
                return resourceLoader.GetString("Today");
            }
        }

        /// <summary>
        /// Localized resource similar to "????"
        /// </summary>
        public virtual string Tomorrow
        {
            get
            {
                return resourceLoader.GetString("Tomorrow");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string TotalGrade
        {
            get
            {
                return resourceLoader.GetString("TotalGrade");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ????????"
        /// </summary>
        public virtual string UniversitiesGuide
        {
            get
            {
                return resourceLoader.GetString("UniversitiesGuide");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ????????"
        /// </summary>
        public virtual string UserName
        {
            get
            {
                return resourceLoader.GetString("UserName");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Vacation
        {
            get
            {
                return resourceLoader.GetString("Vacation");
            }
        }

        /// <summary>
        /// Localized resource similar to "????????"
        /// </summary>
        public virtual string Vacations
        {
            get
            {
                return resourceLoader.GetString("Vacations");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ???????"
        /// </summary>
        public virtual string VacationType
        {
            get
            {
                return resourceLoader.GetString("VacationType");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??????"
        /// </summary>
        public virtual string WhereMySchool
        {
            get
            {
                return resourceLoader.GetString("WhereMySchool");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Year
        {
            get
            {
                return resourceLoader.GetString("Year");
            }
        }

        /// <summary>
        /// Localized resource similar to "???"
        /// </summary>
        public virtual string Yes
        {
            get
            {
                return resourceLoader.GetString("Yes");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????? ????? ?"
        /// </summary>
        public virtual string FilterBy
        {
            get
            {
                return resourceLoader.GetString("FilterBy");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Days
        {
            get
            {
                return resourceLoader.GetString("Days");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Value
        {
            get
            {
                return resourceLoader.GetString("Value");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ???? ??????? ???? ???.
        ///????? ????? ????? ?????? ??? ??? ??????."
        /// </summary>
        public virtual string NoNotificationFavourites
        {
            get
            {
                return resourceLoader.GetString("NoNotificationFavourites");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string ChooseGender
        {
            get
            {
                return resourceLoader.GetString("ChooseGender");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????? ????????"
        /// </summary>
        public virtual string ChooseSchoolStage
        {
            get
            {
                return resourceLoader.GetString("ChooseSchoolStage");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ???????"
        /// </summary>
        public virtual string ChooseStage
        {
            get
            {
                return resourceLoader.GetString("ChooseStage");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ??? {0} {1}"
        /// </summary>
        public virtual string DistanceFromYou
        {
            get
            {
                return resourceLoader.GetString("DistanceFromYou");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????? ??????????"
        /// </summary>
        public virtual string Eelementary
        {
            get
            {
                return resourceLoader.GetString("Eelementary");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ????????"
        /// </summary>
        public virtual string ElectNumber
        {
            get
            {
                return resourceLoader.GetString("ElectNumber");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????? ???? ???? ????? ??? ???? ??????? ??????? ?? ??????? ??? ???????? ?????? ?????? ????."
        /// </summary>
        public virtual string ElectNumberGuide
        {
            get
            {
                return resourceLoader.GetString("ElectNumberGuide");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????? ????? ?????"
        /// </summary>
        public virtual string ExamResultsAvailable
        {
            get
            {
                return resourceLoader.GetString("ExamResultsAvailable");
            }
        }

        /// <summary>
        /// Localized resource similar to "????"
        /// </summary>
        public virtual string Female
        {
            get
            {
                return resourceLoader.GetString("Female");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??????"
        /// </summary>
        public virtual string Garden
        {
            get
            {
                return resourceLoader.GetString("Garden");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??? ????. ???? ?? ?????? ???? ????"
        /// </summary>
        public virtual string InvalidNumber
        {
            get
            {
                return resourceLoader.GetString("InvalidNumber");
            }
        }

        /// <summary>
        /// Localized resource similar to "??"
        /// </summary>
        public virtual string KiloMeterAbbrevation
        {
            get
            {
                return resourceLoader.GetString("KiloMeterAbbrevation");
            }
        }

        /// <summary>
        /// Localized resource similar to "???"
        /// </summary>
        public virtual string Male
        {
            get
            {
                return resourceLoader.GetString("Male");
            }
        }

        /// <summary>
        /// Localized resource similar to "?"
        /// </summary>
        public virtual string MeterAbbrevation
        {
            get
            {
                return resourceLoader.GetString("MeterAbbrevation");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ???? ????? ????? ?????"
        /// </summary>
        public virtual string NoExamResults
        {
            get
            {
                return resourceLoader.GetString("NoExamResults");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string Percentange
        {
            get
            {
                return resourceLoader.GetString("Percentange");
            }
        }

        /// <summary>
        /// Localized resource similar to "????????????????"
        /// </summary>
        public virtual string Preparatory
        {
            get
            {
                return resourceLoader.GetString("Preparatory");
            }
        }

        /// <summary>
        /// Localized resource similar to "????"
        /// </summary>
        public virtual string Search
        {
            get
            {
                return resourceLoader.GetString("Search");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??????"
        /// </summary>
        public virtual string SeatNumber
        {
            get
            {
                return resourceLoader.GetString("SeatNumber");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ?????? / ????? ??????"
        /// </summary>
        public virtual string SeatOrPersonalNumber
        {
            get
            {
                return resourceLoader.GetString("SeatOrPersonalNumber");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????? ????????"
        /// </summary>
        public virtual string Secondary
        {
            get
            {
                return resourceLoader.GetString("Secondary");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ????????"
        /// </summary>
        public virtual string UseElectNumber
        {
            get
            {
                return resourceLoader.GetString("UseElectNumber");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??????????"
        /// </summary>
        public virtual string ExamResults
        {
            get
            {
                return resourceLoader.GetString("ExamResults");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ???? ????? ????? ?????"
        /// </summary>
        public virtual string ExamsResultsNotAvailable
        {
            get
            {
                return resourceLoader.GetString("ExamsResultsNotAvailable");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string Activate
        {
            get
            {
                return resourceLoader.GetString("Activate");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ???????? ???"
        /// </summary>
        public virtual string EmailFormatError
        {
            get
            {
                return resourceLoader.GetString("EmailFormatError");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ????? ??????"
        /// </summary>
        public virtual string EnterPersonalNumber
        {
            get
            {
                return resourceLoader.GetString("EnterPersonalNumber");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string MobilePhone
        {
            get
            {
                return resourceLoader.GetString("MobilePhone");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????? ??? ???? ?? ??? ?? ???? ?????"
        /// </summary>
        public virtual string MobilePhoneError
        {
            get
            {
                return resourceLoader.GetString("MobilePhoneError");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ??? ??????"
        /// </summary>
        public virtual string MobilePhoneHint
        {
            get
            {
                return resourceLoader.GetString("MobilePhoneHint");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??????"
        /// </summary>
        public virtual string PersonalNumber
        {
            get
            {
                return resourceLoader.GetString("PersonalNumber");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????? ???????"
        /// </summary>
        public virtual string PrimaryEmail
        {
            get
            {
                return resourceLoader.GetString("PrimaryEmail");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ???? ??????? ????????"
        /// </summary>
        public virtual string PrimaryEmailHint
        {
            get
            {
                return resourceLoader.GetString("PrimaryEmailHint");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string Required
        {
            get
            {
                return resourceLoader.GetString("Required");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ???????"
        /// </summary>
        public virtual string ScholarshipActivation
        {
            get
            {
                return resourceLoader.GetString("ScholarshipActivation");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ??????"
        /// </summary>
        public virtual string HotLine
        {
            get
            {
                return resourceLoader.GetString("HotLine");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ?????? ???????"
        /// </summary>
        public virtual string MOEFullName
        {
            get
            {
                return resourceLoader.GetString("MOEFullName");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ???????"
        /// </summary>
        public virtual string OfficeLine
        {
            get
            {
                return resourceLoader.GetString("OfficeLine");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ???? ???? ??? ?????? ????? ???? ??? ???? ?????? ??????? ????? ?? ???? ????? ????? ???? ????????? ??????? ?????? ??? ??????? ?????? ?????????? ???????. ??? ????? ???? ??? ???? ?????? ??? ??? ?????? ?? ???????? ?? ?????? ???????? ???? ???? ?????? ???? ???? ?? ???? ??? ???? ???? ?????? ?? ????? ?????? ?????? ??? ???? ??? ??? ???? ????? ???? ??????? ??? ???? ??????? ????? ?????? ??? ????? ??????. ??? ??? ????? ?????? ?????? ??????? ????? ??????? ??????? ??? 37 ?? ??? ?????? ?? ??? 2002 ??? ??? ?????? ??????? ????? ????? ??????? ?? ??????.
        ///
        ///??? ?????? ?????? ??????? ????? ??????? ???????? ?? ????? ??????? ???? ????? ??? ?????? ? ???? ????? ???????? ???? ??? ?? ???? ??????? ????????? ??????? ???????? ?? ????? ????????. ??? ??? ?????? ??????? ?? ??? ??????? ????????? ??????? ???? ??? ??????? ???????? ?????? ???????? ????????? ??? ??????? ?????? ??????.
        ///
        ///????? ??? ???? ????? ?????? ?????? ??????? ????? ???? ??????? ????????? ?? ??????? ??? ???? ?????? ????? ??????? ?? ????? ?????? ????????? ????????? ?? ???? ??????? ???? ??? ??? ?????: ???? ??????? ????? ??????? ????? ??????? ??????. ??? ??? ?????? ?????? ??????? ?????? ?? ????? ?????? ???????? ????? ????? ???? ????? ?????? ?????? ??????????? ??? ???? ??? ?????? ????? ?????? ?????? ?????"
        /// </summary>
        public virtual string AboutMaglesDescription
        {
            get
            {
                return resourceLoader.GetString("AboutMaglesDescription");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????"
        /// </summary>
        public virtual string ContactUs
        {
            get
            {
                return resourceLoader.GetString("ContactUs");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Country
        {
            get
            {
                return resourceLoader.GetString("Country");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string Major
        {
            get
            {
                return resourceLoader.GetString("Major");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????/????????"
        /// </summary>
        public virtual string StateProvince
        {
            get
            {
                return resourceLoader.GetString("StateProvince");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string University
        {
            get
            {
                return resourceLoader.GetString("University");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string YourLocation
        {
            get
            {
                return resourceLoader.GetString("YourLocation");
            }
        }

        ///// <summary>
        ///// Localized resource similar to "???? ??????"
        ///// </summary>
        //public virtual string ChooseCountry
        //{
        //    get
        //    {
        //        return resourceLoader.GetString("ChooseCountry");
        //    }
        //}

        ///// <summary>
        ///// Localized resource similar to "???? ??????"
        ///// </summary>
        //public virtual string ChooseMajor
        //{
        //    get
        //    {
        //        return resourceLoader.GetString("ChooseMajor");
        //    }
        //}

        ///// <summary>
        ///// Localized resource similar to "???? ???????"
        ///// </summary>
        //public virtual string ChooseState
        //{
        //    get
        //    {
        //        return resourceLoader.GetString("ChooseState");
        //    }
        //}

        ///// <summary>
        ///// Localized resource similar to "???? ???????"
        ///// </summary>
        //public virtual string ChooseUniversity
        //{
        //    get
        //    {
        //        return resourceLoader.GetString("ChooseUniversity");
        //    }
        //}

        /// <summary>
        /// Localized resource similar to "????"
        /// </summary>
        public virtual string Select
        {
            get
            {
                return resourceLoader.GetString("Select");
            }
        }

        ///// <summary>
        ///// Localized resource similar to ""
        ///// </summary>
        //public virtual string s
        //{
        //    get
        //    {
        //        return resourceLoader.GetString("s");
        //    }
        //}

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string MyLocation
        {
            get
            {
                return resourceLoader.GetString("MyLocation");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ??"
        /// </summary>
        public virtual string StartsWith
        {
            get
            {
                return resourceLoader.GetString("StartsWith");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ?????? ???"
        /// </summary>
        public virtual string WriteYourMessage
        {
            get
            {
                return resourceLoader.GetString("WriteYourMessage");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ???? ?????"
        /// </summary>
        public virtual string NoMessages
        {
            get
            {
                return resourceLoader.GetString("NoMessages");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??????"
        /// </summary>
        public virtual string ChildRegisteration
        {
            get
            {
                return resourceLoader.GetString("ChildRegisteration");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????? ??? ???? ?? ??? ?? ???? ?????"
        /// </summary>
        public virtual string MobilePhoneMin8
        {
            get
            {
                return resourceLoader.GetString("MobilePhoneMin8");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ???? , ????? ????? ??????"
        /// </summary>
        public virtual string NewUserRegisteration
        {
            get
            {
                return resourceLoader.GetString("NewUserRegisteration");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ?????? ???? ?????"
        /// </summary>
        public virtual string ParentQId
        {
            get
            {
                return resourceLoader.GetString("ParentQId");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??????"
        /// </summary>
        public virtual string ServiceActivation
        {
            get
            {
                return resourceLoader.GetString("ServiceActivation");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ?????"
        /// </summary>
        public virtual string WorkDetails
        {
            get
            {
                return resourceLoader.GetString("WorkDetails");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ?????"
        /// </summary>
        public virtual string WorkName
        {
            get
            {
                return resourceLoader.GetString("WorkName");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ??? ?????"
        /// </summary>
        public virtual string WorkSection
        {
            get
            {
                return resourceLoader.GetString("WorkSection");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????"
        /// </summary>
        public virtual string NewRegisteration
        {
            get
            {
                return resourceLoader.GetString("NewRegisteration");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ?????? ???????"
        /// </summary>
        public virtual string ParentBackToLogin
        {
            get
            {
                return resourceLoader.GetString("ParentBackToLogin");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ???? ??? ??? ?? ??? ???? ??? ?????"
        /// </summary>
        public virtual string ParentNewRegisterationHint
        {
            get
            {
                return resourceLoader.GetString("ParentNewRegisterationHint");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string ParentNext
        {
            get
            {
                return resourceLoader.GetString("ParentNext");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??????? ????? ????? ???? ??????? ?????? ??? ?????"
        /// </summary>
        public virtual string ParentSmsHint
        {
            get
            {
                return resourceLoader.GetString("ParentSmsHint");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ??????? ????? ????? ????? ???? ????? ?????? ?? ???? ?????"
        /// </summary>
        public virtual string ParentSucessfullRegsiteration
        {
            get
            {
                return resourceLoader.GetString("ParentSucessfullRegsiteration");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????? ?????? ?????? ???? ???"
        /// </summary>
        public virtual string ParentUserAndPassword
        {
            get
            {
                return resourceLoader.GetString("ParentUserAndPassword");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????? ?? ???? ??? ?????"
        /// </summary>
        public virtual string ToRegisterAsParent
        {
            get
            {
                return resourceLoader.GetString("ToRegisterAsParent");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????? ????"
        /// </summary>
        public virtual string AddNewRegisteration
        {
            get
            {
                return resourceLoader.GetString("AddNewRegisteration");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string DocumentDownload
        {
            get
            {
                return resourceLoader.GetString("DocumentDownload");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??? ?????"
        /// </summary>
        public virtual string ParentStudentRelation
        {
            get
            {
                return resourceLoader.GetString("ParentStudentRelation");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string School
        {
            get
            {
                return resourceLoader.GetString("School");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ???????"
        /// </summary>
        public virtual string SchoolName
        {
            get
            {
                return resourceLoader.GetString("SchoolName");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string Student
        {
            get
            {
                return resourceLoader.GetString("Student");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????????"
        /// </summary>
        public virtual string StudentElectricityDocument
        {
            get
            {
                return resourceLoader.GetString("StudentElectricityDocument");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ??????"
        /// </summary>
        public virtual string StudentExtraDetails
        {
            get
            {
                return resourceLoader.GetString("StudentExtraDetails");
            }
        }

        /// <summary>
        /// Localized resource similar to "????"
        /// </summary>
        public virtual string StudentGradeLevel
        {
            get
            {
                return resourceLoader.GetString("StudentGradeLevel");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??????? ?????"
        /// </summary>
        public virtual string StudentHealthDocuemt
        {
            get
            {
                return resourceLoader.GetString("StudentHealthDocuemt");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??????"
        /// </summary>
        public virtual string StudentName
        {
            get
            {
                return resourceLoader.GetString("StudentName");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ?????? ??????"
        /// </summary>
        public virtual string StudentPersonalId
        {
            get
            {
                return resourceLoader.GetString("StudentPersonalId");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ????? ??? ???????"
        /// </summary>
        public virtual string StudentRegisterationActive
        {
            get
            {
                return resourceLoader.GetString("StudentRegisterationActive");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string StudentRegisterationDetails
        {
            get
            {
                return resourceLoader.GetString("StudentRegisterationDetails");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??????"
        /// </summary>
        public virtual string StudentsRegisteration
        {
            get
            {
                return resourceLoader.GetString("StudentsRegisteration");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ????????  {0}"
        /// </summary>
        public virtual string StudentTransactionNo
        {
            get
            {
                return resourceLoader.GetString("StudentTransactionNo");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string Address
        {
            get
            {
                return resourceLoader.GetString("Address");
            }
        }

        /// <summary>
        /// Localized resource similar to "???????"
        /// </summary>
        public virtual string AddressArea
        {
            get
            {
                return resourceLoader.GetString("AddressArea");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string AddressPlot
        {
            get
            {
                return resourceLoader.GetString("AddressPlot");
            }
        }

        /// <summary>
        /// Localized resource similar to "??????"
        /// </summary>
        public virtual string AddressStreet
        {
            get
            {
                return resourceLoader.GetString("AddressStreet");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ?????"
        /// </summary>
        public virtual string BlueBoard
        {
            get
            {
                return resourceLoader.GetString("BlueBoard");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ????????"
        /// </summary>
        public virtual string ElectircityWaterNumber
        {
            get
            {
                return resourceLoader.GetString("ElectircityWaterNumber");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ?????"
        /// </summary>
        public virtual string QatariMother
        {
            get
            {
                return resourceLoader.GetString("QatariMother");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string SchoolSelection
        {
            get
            {
                return resourceLoader.GetString("SchoolSelection");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ?????? ???????"
        /// </summary>
        public virtual string SchoolSelectionConfirmation
        {
            get
            {
                return resourceLoader.GetString("SchoolSelectionConfirmation");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ?????"
        /// </summary>
        public virtual string EnterNumber
        {
            get
            {
                return resourceLoader.GetString("EnterNumber");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ?????? ????"
        /// </summary>
        public virtual string MotherPersonalNumber
        {
            get
            {
                return resourceLoader.GetString("MotherPersonalNumber");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ???????"
        /// </summary>
        public virtual string SchoolCapacity
        {
            get
            {
                return resourceLoader.GetString("SchoolCapacity");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ???????"
        /// </summary>
        public virtual string SchoolCode
        {
            get
            {
                return resourceLoader.GetString("SchoolCode");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??????"
        /// </summary>
        public virtual string SchoolPhone
        {
            get
            {
                return resourceLoader.GetString("SchoolPhone");
            }
        }

        /// <summary>
        /// Localized resource similar to "???? ???????"
        /// </summary>
        public virtual string SchoolPrincipal
        {
            get
            {
                return resourceLoader.GetString("SchoolPrincipal");
            }
        }

        /// <summary>
        /// Localized resource similar to "?? ???? ????? ?? ????? ????  {0} ????"
        /// </summary>
        public virtual string CantExceedeMaxFileSize
        {
            get
            {
                return resourceLoader.GetString("CantExceedeMaxFileSize");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????????"
        /// </summary>
        public virtual string ElectricityDocument
        {
            get
            {
                return resourceLoader.GetString("ElectricityDocument");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ????? ????"
        /// </summary>
        public virtual string MotherBirthDocument
        {
            get
            {
                return resourceLoader.GetString("MotherBirthDocument");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ???????? ??????? ??? ????"
        /// </summary>
        public virtual string NoLocationForSchool
        {
            get
            {
                return resourceLoader.GetString("NoLocationForSchool");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??? ??? ??? ?????"
        /// </summary>
        public virtual string ParentWorkDocument
        {
            get
            {
                return resourceLoader.GetString("ParentWorkDocument");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ?????????"
        /// </summary>
        public virtual string SchoolAddingDocuments
        {
            get
            {
                return resourceLoader.GetString("SchoolAddingDocuments");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ??? ?????"
        /// </summary>
        public virtual string ParentName
        {
            get
            {
                return resourceLoader.GetString("ParentName");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ???????"
        /// </summary>
        public virtual string BirthDate
        {
            get
            {
                return resourceLoader.GetString("BirthDate");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ??????????"
        /// </summary>
        public virtual string Email
        {
            get
            {
                return resourceLoader.GetString("Email");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ?????? ??????"
        /// </summary>
        public virtual string ParentMobilePhone
        {
            get
            {
                return resourceLoader.GetString("ParentMobilePhone");
            }
        }

        /// <summary>
        /// Localized resource similar to "????? ??? ??? ????? ???? ??? ????????"
        /// </summary>
        public virtual string RegisterationAgreement
        {
            get
            {
                return resourceLoader.GetString("RegisterationAgreement");
            }
        }

        /// <summary>
        /// Localized resource similar to "??? ?????"
        /// </summary>
        public virtual string RegisterationParent
        {
            get
            {
                return resourceLoader.GetString("RegisterationParent");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????"
        /// </summary>
        public virtual string StudentGender
        {
            get
            {
                return resourceLoader.GetString("StudentGender");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ????????"
        /// </summary>
        public virtual string InformationConfirmation
        {
            get
            {
                return resourceLoader.GetString("InformationConfirmation");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string RegisterationConfirmation
        {
            get
            {
                return resourceLoader.GetString("RegisterationConfirmation");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string StudentTransfer
        {
            get
            {
                return resourceLoader.GetString("StudentTransfer");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string Welcome
        {
            get
            {
                return resourceLoader.GetString("Welcome");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string MyRequests
        {
            get
            {
                return resourceLoader.GetString("MyRequests");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string ForgetPassword
        {
            get
            {
                return resourceLoader.GetString("ForgetPassword");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string RemindMeLater
        {
            get
            {
                return resourceLoader.GetString("RemindMeLater");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string Done
        {
            get
            {
                return resourceLoader.GetString("Done");
            }
        }

        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string ShowThisWeek
        {
            get
            {
                return resourceLoader.GetString("ShowThisWeek");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string AttendanceDetails
        {
            get
            {
                return resourceLoader.GetString("AttendanceDetails");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string StartNewConversation
        {
            get
            {
                return resourceLoader.GetString("StartNewConversation");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string Settings
        {
            get
            {
                return resourceLoader.GetString("Settings");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string TaleemQatar
        {
            get
            {
                return resourceLoader.GetString("TaleemQatar");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string TotalEvaluation
        {
            get
            {
                return resourceLoader.GetString("TotalEvaluation");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string SearchForSchool
        {
            get
            {
                return resourceLoader.GetString("SearchForSchool");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string SearchForNearbySchool

        {
            get
            {
                return resourceLoader.GetString("SearchForNearbySchool");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string RemindMeAfter

        {
            get
            {
                return resourceLoader.GetString("RemindMeAfter");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string ShowThisMonth

        {
            get
            {
                return resourceLoader.GetString("ShowThisMonth");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string NetSalary

        {
            get
            {
                return resourceLoader.GetString("NetSalary");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string QatariRyal

        {
            get
            {
                return resourceLoader.GetString("QatariRyal");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string Remaining

        {
            get
            {
                return resourceLoader.GetString("Remaining");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string Repaid

        {
            get
            {
                return resourceLoader.GetString("Repaid");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string WriteSomething

        {
            get
            {
                return resourceLoader.GetString("WriteSomething");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string Hour

        {
            get
            {
                return resourceLoader.GetString("Hour");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string Hours

        {
            get
            {
                return resourceLoader.GetString("Hours");
            }
        }
        /// <summary>
        /// Localized resource similar to "?????? ???????"
        /// </summary>
        public virtual string ChooseAnotherTime

        {
            get
            {
                return resourceLoader.GetString("ChooseAnotherTime");
            }
        }
        public virtual string StudentService

        {
            get
            {
                return resourceLoader.GetString("StudentService");
            }
        }
        public virtual string SearchForScholarship

        {
            get
            {
                return resourceLoader.GetString("SearchForScholarship");
            }
        }
        public virtual string AvailableScholarships

        {
            get
            {
                return resourceLoader.GetString("AvailableScholarships");
            }
        }
        public virtual string GoToWebsite

        {
            get
            {
                return resourceLoader.GetString("GoToWebsite");
            }
        }
        public virtual string TheMinistryofEducationandHigherEducation

        {
            get
            {
                return resourceLoader.GetString("TheMinistryofEducationandHigherEducation");
            }
        }
        public virtual string SecondaryNatija

        {
            get
            {
                return resourceLoader.GetString("SecondaryNatija");
            }
        }
        public virtual string NatijaText

        {
            get
            {
                return resourceLoader.GetString("NatijaText");
            }
        }
        public virtual string Total

        {
            get
            {
                return resourceLoader.GetString("Total");
            }
        }
        public virtual string None

        {
            get
            {
                return resourceLoader.GetString("None");
            }
        }
        public virtual string Road

        {
            get
            {
                return resourceLoader.GetString("Road");
            }
        }
        public virtual string Aerial

        {
            get
            {
                return resourceLoader.GetString("Aerial");
            }
        }
        public virtual string AerialWithRoads

        {
            get
            {
                return resourceLoader.GetString("AerialWithRoads");
            }
        }
        public virtual string Terrain

        {
            get
            {
                return resourceLoader.GetString("Terrain");
            }
        }
        public virtual string NearbySchools

        {
            get
            {
                return resourceLoader.GetString("NearbySchools");
            }
        }
        public virtual string LeaveRequest

        {
            get
            {
                return resourceLoader.GetString("LeaveRequest");
            }
        }
        public virtual string LeaveReturnRequest

        {
            get
            {
                return resourceLoader.GetString("LeaveReturnRequest");
            }
        }
        public virtual string LoanRequest

        {
            get
            {
                return resourceLoader.GetString("LoanRequest");
            }
        }
        public virtual string ExitPermitRequest

        {
            get
            {
                return resourceLoader.GetString("ExitPermitRequest");
            }
        }
        public virtual string DocumentRequest

        {
            get
            {
                return resourceLoader.GetString("DocumentRequest");
            }
        }
        public virtual string OvertimeRequest

        {
            get
            {
                return resourceLoader.GetString("OvertimeRequest");
            }
        }
        public virtual string EducationCenters

        {
            get
            {
                return resourceLoader.GetString("EducationCenters");
            }
        }
        public virtual string ListOfAccreditedUniversites

        {
            get
            {
                return resourceLoader.GetString("ListOfAccreditedUniversites");
            }
        }

        public virtual string NationalCertificates

        {
            get
            {
                return resourceLoader.GetString("NationalCertificates");
            }
        }
        public virtual string NeedHelp

        {
            get
            {
                return resourceLoader.GetString("NeedHelp");
            }
        }
        public virtual string OtherScholarships

        {
            get
            {
                return resourceLoader.GetString("OtherScholarships");
            }
        }
        public virtual string ScholarshipRequest

        {
            get
            {
                return resourceLoader.GetString("ScholarshipRequest");
            }
        }
        public virtual string SchoolLicenses

        {
            get
            {
                return resourceLoader.GetString("SchoolLicenses");
            }
        }
        public virtual string SchoolsGuide

        {
            get
            {
                return resourceLoader.GetString("SchoolsGuide");
            }
        }
        public virtual string SearchUniversity

        {
            get
            {
                return resourceLoader.GetString("SearchUniversity");
            }
        }
        public virtual string StudentGuideScholarship

        {
            get
            {
                return resourceLoader.GetString("StudentGuideScholarship");
            }
        }
        public virtual string PrivateSchools

        {
            get
            {
                return resourceLoader.GetString("PrivateSchools");
            }
        }

        public virtual string AllRightsReservedTheMinistryofEducationandHigherEducation

        {
            get
            {
                return resourceLoader.GetString("AllRightsReservedTheMinistryofEducationandHigherEducation");
            }
        }
        public virtual string Delivered

        {
            get
            {
                return resourceLoader.GetString("Delivered");
            }
        }
        public virtual string Promise

        {
            get
            {
                return resourceLoader.GetString("Promise");
            }
        }
        public virtual string Applicaant

        {
            get
            {
                return resourceLoader.GetString("Applicaant");
            }
        }
        public virtual string Parents

        {
            get
            {
                return resourceLoader.GetString("Parents");
            }
        }
        public virtual string ToBeExceptioned

        {
            get
            {
                return resourceLoader.GetString("ToBeExceptioned");
            }
        }
        public virtual string Children

        {
            get
            {
                return resourceLoader.GetString("Children");
            }
        }
        public virtual string Other

        {
            get
            {
                return resourceLoader.GetString("Other");
            }
        }
        public virtual string Abroad

        {
            get
            {
                return resourceLoader.GetString("Abroad");
            }
        }
        public virtual string AddChild

        {
            get
            {
                return resourceLoader.GetString("AddChild");
            }
        }
        public virtual string AddTwinsText

        {
            get
            {
                return resourceLoader.GetString("AddTwinsText");
            }
        }
        public virtual string AdiitionalData

        {
            get
            {
                return resourceLoader.GetString("AdiitionalData");
            }
        }
        public virtual string Applicant

        {
            get
            {
                return resourceLoader.GetString("Applicant");
            }
        }
        public virtual string ApplicantData

        {
            get
            {
                return resourceLoader.GetString("ApplicantData");
            }
        }
        public virtual string ApplicantName

        {
            get
            {
                return resourceLoader.GetString("ApplicantName");
            }
        }
        public virtual string AreaNumber

        {
            get
            {
                return resourceLoader.GetString("AreaNumber");
            }
        }
        public virtual string BirthCertificate

        {
            get
            {
                return resourceLoader.GetString("BirthCertificate");
            }
        }
        public virtual string Breadwinner

        {
            get
            {
                return resourceLoader.GetString("Breadwinner");
            }
        }
        public virtual string BreadwinnerData

        {
            get
            {
                return resourceLoader.GetString("BreadwinnerData");
            }
        }
        public virtual string BreadwinnerName

        {
            get
            {
                return resourceLoader.GetString("BreadwinnerName");
            }
        }
        public virtual string Brevious

        {
            get
            {
                return resourceLoader.GetString("Brevious");
            }
        }
        public virtual string BuildingNumber

        {
            get
            {
                return resourceLoader.GetString("BuildingNumber");
            }
        }

        public virtual string CertifiedbytheCourt

        {
            get
            {
                return resourceLoader.GetString("CertifiedbytheCourt");
            }
        }
        public virtual string CertifiedRentPaper

        {
            get
            {
                return resourceLoader.GetString("CertifiedRentPaper");
            }
        }
        public virtual string CommitmentText

        {
            get
            {
                return resourceLoader.GetString("CommitmentText");
            }
        }
        public virtual string Create

        {
            get
            {
                return resourceLoader.GetString("Create");
            }
        }
        public virtual string Dead

        {
            get
            {
                return resourceLoader.GetString("Dead");
            }
        }

        public virtual string DeathCertification

        {
            get
            {
                return resourceLoader.GetString("DeathCertification");
            }
        }
        public virtual string Employer

        {
            get
            {
                return resourceLoader.GetString("Employer");
            }
        }
        public virtual string ExceptionRequests

        {
            get
            {
                return resourceLoader.GetString("ExceptionRequests");
            }
        }
        public virtual string FatherData

        {
            get
            {
                return resourceLoader.GetString("FatherData");
            }
        }
        public virtual string FatherName

        {
            get
            {
                return resourceLoader.GetString("FatherName");
            }
        }
        public virtual string Grandfather

        {
            get
            {
                return resourceLoader.GetString("Grandfather");
            }
        }
        public virtual string Brother

        {
            get
            {
                return resourceLoader.GetString("Brother");
            }
        }
        public virtual string Uncle

        {
            get
            {
                return resourceLoader.GetString("Uncle");
            }
        }
        public virtual string HasException

        {
            get
            {
                return resourceLoader.GetString("HasException");
            }
        }

        public virtual string HasTwins

        {
            get
            {
                return resourceLoader.GetString("HasTwins");
            }
        }
        public virtual string IcubationCertificate

        {
            get
            {
                return resourceLoader.GetString("IcubationCertificate");
            }
        }
        public virtual string IdentityType

        {
            get
            {
                return resourceLoader.GetString("IdentityType");
            }
        }
        public virtual string InCountry

        {
            get
            {
                return resourceLoader.GetString("InCountry");
            }
        }
        public virtual string IncubatorCertificate

        {
            get
            {
                return resourceLoader.GetString("IncubatorCertificate");
            }
        }
        public virtual string IndependentSchool

        {
            get
            {
                return resourceLoader.GetString("IndependentSchool");
            }
        }
        public virtual string IsDevorsed

        {
            get
            {
                return resourceLoader.GetString("IsDevorsed");
            }
        }
        public virtual string IsIncubator

        {
            get
            {
                return resourceLoader.GetString("IsIncubator");
            }
        }
        public virtual string IsWorking

        {
            get
            {
                return resourceLoader.GetString("IsWorking");
            }
        }
        public virtual string Job

        {
            get
            {
                return resourceLoader.GetString("Job");
            }
        }
        public virtual string LastSchoolDegree

        {
            get
            {
                return resourceLoader.GetString("LastSchoolDegree");
            }
        }
        public virtual string Mother

        {
            get
            {
                return resourceLoader.GetString("Mother");
            }
        }
        public virtual string MotherData

        {
            get
            {
                return resourceLoader.GetString("MotherData");
            }
        }
        public virtual string MotherName

        {
            get
            {
                return resourceLoader.GetString("MotherName");
            }
        }
        public virtual string Nationality

        {
            get
            {
                return resourceLoader.GetString("Nationality");
            }
        }
        public virtual string NewBankSalaryCertificate

        {
            get
            {
                return resourceLoader.GetString("NewBankSalaryCertificate");
            }
        }
        public virtual string NEWEmployerCertificate

        {
            get
            {
                return resourceLoader.GetString("NEWEmployerCertificate");
            }
        }

        public virtual string NewStudent

        {
            get
            {
                return resourceLoader.GetString("NewStudent");
            }
        }
        public virtual string Next

        {
            get
            {
                return resourceLoader.GetString("Next");
            }
        }
        public virtual string NextGrade

        {
            get
            {
                return resourceLoader.GetString("NextGrade");
            }
        }
        public virtual string NonStudent

        {
            get
            {
                return resourceLoader.GetString("NonStudent");
            }
        }
        public virtual string NotPaid

        {
            get
            {
                return resourceLoader.GetString("NotPaid");
            }
        }
        public virtual string NoWork

        {
            get
            {
                return resourceLoader.GetString("NoWork");
            }
        }
        public virtual string ParentsData

        {
            get
            {
                return resourceLoader.GetString("ParentsData");
            }
        }
        public virtual string Passport

        {
            get
            {
                return resourceLoader.GetString("Passport");
            }
        }
        public virtual string PrivateSchool

        {
            get
            {
                return resourceLoader.GetString("PrivateSchool");
            }
        }
        public virtual string QatarID

        {
            get
            {
                return resourceLoader.GetString("QatarID");
            }
        }
        public virtual string QIDImageFromALLSides

        {
            get
            {
                return resourceLoader.GetString("QIDImageFromALLSides");
            }
        }
        public virtual string Relationship

        {
            get
            {
                return resourceLoader.GetString("Relationship");
            }
        }
        public virtual string Rent

        {
            get
            {
                return resourceLoader.GetString("Rent");
            }
        }
        public virtual string RentAmount

        {
            get
            {
                return resourceLoader.GetString("RentAmount");
            }
        }
        public virtual string RentCertificate

        {
            get
            {
                return resourceLoader.GetString("RentCertificate");
            }
        }
        public virtual string RequestNumber

        {
            get
            {
                return resourceLoader.GetString("RequestNumber");
            }
        }
        public virtual string SchoolType

        {
            get
            {
                return resourceLoader.GetString("SchoolType");
            }
        }
        public virtual string SendRequest

        {
            get
            {
                return resourceLoader.GetString("SendRequest");
            }
        }
        public virtual string Sent

        {
            get
            {
                return resourceLoader.GetString("Sent");
            }
        }
        public virtual string StreetNumber

        {
            get
            {
                return resourceLoader.GetString("StreetNumber");
            }
        }
        public virtual string StudentFullName

        {
            get
            {
                return resourceLoader.GetString("StudentFullName");
            }
        }
        public virtual string ToBeExceptionedStudentData

        {
            get
            {
                return resourceLoader.GetString("ToBeExceptionedStudentData");
            }
        }
        public virtual string TotalSalary

        {
            get
            {
                return resourceLoader.GetString("TotalSalary");
            }
        }
        public virtual string Work

        {
            get
            {
                return resourceLoader.GetString("Work");
            }
        }
        public virtual string AccountActivation

        {
            get
            {
                return resourceLoader.GetString("AccountActivation");
            }
        }

        public virtual string AcctivationAccountSentToURMobile

        {
            get
            {
                return resourceLoader.GetString("AcctivationAccountSentToURMobile");
            }
        }
        public virtual string ActivationCode

        {
            get
            {
                return resourceLoader.GetString("ActivationCode");
            }
        }
        public virtual string ConfirmPassword

        {
            get
            {
                return resourceLoader.GetString("ConfirmPassword");
            }
        }
        public virtual string CreateNewAccount

        {
            get
            {
                return resourceLoader.GetString("CreateNewAccount");
            }
        }
        public virtual string CurrentEmail

        {
            get
            {
                return resourceLoader.GetString("CurrentEmail");
            }
        }
        public virtual string CurrentMobileQ

        {
            get
            {
                return resourceLoader.GetString("CurrentMobileQ");
            }
        }
        public virtual string CurrentPassword

        {
            get
            {
                return resourceLoader.GetString("CurrentPassword");
            }
        }
        public virtual string InsertDataAndPressSaveToResetPassword

        {
            get
            {
                return resourceLoader.GetString("InsertDataAndPressSaveToResetPassword");
            }
        }
        public virtual string InsertDataAndPressSendToSendNewAcctivationCode

        {
            get
            {
                return resourceLoader.GetString("InsertDataAndPressSendToSendNewAcctivationCode");
            }
        }
        public virtual string InsertDataToGetPasswordBack

        {
            get
            {
                return resourceLoader.GetString("InsertDataToGetPasswordBack");
            }
        }
        public virtual string InsertNewAccountData

        {
            get
            {
                return resourceLoader.GetString("InsertNewAccountData");
            }
        }
        public virtual string InsertURLoginData

        {
            get
            {
                return resourceLoader.GetString("InsertURLoginData");
            }
        }
        public virtual string MobileQ

        {
            get
            {
                return resourceLoader.GetString("MobileQ");
            }
        }
        public virtual string NewEmail

        {
            get
            {
                return resourceLoader.GetString("NewEmail");
            }
        }
        public virtual string NewMobileQ

        {
            get
            {
                return resourceLoader.GetString("NewMobileQ");
            }
        }
        public virtual string NewPassword

        {
            get
            {
                return resourceLoader.GetString("NewPassword");
            }
        }

        public virtual string ReSendActivationCode

        {
            get
            {
                return resourceLoader.GetString("ReSendActivationCode");
            }
        }

        public virtual string ResetPassword

        {
            get
            {
                return resourceLoader.GetString("ResetPassword");
            }
        }
        public virtual string Save

        {
            get
            {
                return resourceLoader.GetString("Save");
            }
        }
        public virtual string UpdatePersonalData

        {
            get
            {
                return resourceLoader.GetString("UpdatePersonalData");
            }
        }
        public virtual string AvailablePlaces

        {
            get
            {
                return resourceLoader.GetString("AvailablePlaces");
            }
        }

        public virtual string Curreculum

        {
            get
            {
                return resourceLoader.GetString("Curreculum");
            }
        }

        public virtual string EducationalQasaem

        {
            get
            {
                return resourceLoader.GetString("EducationalQasaem");
            }
        }
        public virtual string GeneralManager

        {
            get
            {
                return resourceLoader.GetString("GeneralManager");
            }
        }
        public virtual string GradeLevel

        {
            get
            {
                return resourceLoader.GetString("GradeLevel");
            }
        }
        public virtual string HowToGo

        {
            get
            {
                return resourceLoader.GetString("HowToGo");
            }
        }
        public virtual string LessDetails

        {
            get
            {
                return resourceLoader.GetString("LessDetails");
            }
        }
        public virtual string Mixed

        {
            get
            {
                return resourceLoader.GetString("Mixed");
            }
        }
        public virtual string MoreDetails

        {
            get
            {
                return resourceLoader.GetString("MoreDetails");
            }
        }
        public virtual string Region

        {
            get
            {
                return resourceLoader.GetString("Region");
            }
        }
        public virtual string SchoolFees

        {
            get
            {
                return resourceLoader.GetString("SchoolFees");
            }
        }
        public virtual string SchoolGrades

        {
            get
            {
                return resourceLoader.GetString("SchoolGrades");
            }
        }
        public virtual string SchoolsList

        {
            get
            {
                return resourceLoader.GetString("SchoolsList");
            }
        }
        public virtual string SchoolsMap

        {
            get
            {
                return resourceLoader.GetString("SchoolsMap");
            }
        }
        public virtual string SearchResult

        {
            get
            {
                return resourceLoader.GetString("SearchResult");
            }
        }
        public virtual string StudentsNumberTillNow

        {
            get
            {
                return resourceLoader.GetString("StudentsNumberTillNow");
            }
        }
        public virtual string InsertAllDataMessage

        {
            get
            {
                return resourceLoader.GetString("InsertAllDataMessage");
            }
        }
        public virtual string PasswordAndConfirmPasswordRntSame

        {
            get
            {
                return resourceLoader.GetString("PasswordAndConfirmPasswordRntSame");
            }
        }
        public virtual string Enrollment

        {
            get
            {
                return resourceLoader.GetString("Enrollment");
            }
        }
        public virtual string YouCanEnrollNewORTransfer

        {
            get
            {
                return resourceLoader.GetString("YouCanEnrollNewORTransfer");
            }
        }
        public virtual string EnrollSudentForThisYear

        {
            get
            {
                return resourceLoader.GetString("EnrollSudentForThisYear");
            }
        }
        public virtual string EnrollmentNumber

        {
            get
            {
                return resourceLoader.GetString("EnrollmentNumber");
            }
        }
        public virtual string RelationshipWithStudent

        {
            get
            {
                return resourceLoader.GetString("RelationshipWithStudent");
            }
        }

        public virtual string Receipt

        {
            get
            {
                return resourceLoader.GetString("Receipt");
            }
        }
        public virtual string EnterRequiredStudentData

        {
            get
            {
                return resourceLoader.GetString("EnterRequiredStudentData");
            }
        }
        public virtual string IsMotherQatarian

        {
            get
            {
                return resourceLoader.GetString("IsMotherQatarian");
            }
        }
        public virtual string SchoolInformation

        {
            get
            {
                return resourceLoader.GetString("SchoolInformation");
            }
        }
        public virtual string Municipal

        {
            get
            {
                return resourceLoader.GetString("Municipal");
            }
        }
        public virtual string Area

        {
            get
            {
                return resourceLoader.GetString("Area");
            }
        }
        public virtual string Choose

        {
            get
            {
                return resourceLoader.GetString("Choose");
            }
        }
        public virtual string UploadDocuments

        {
            get
            {
                return resourceLoader.GetString("UploadDocuments");
            }
        }
        public virtual string PleaseUploadDocWith4MB

        {
            get
            {
                return resourceLoader.GetString("PleaseUploadDocWith4MB");
            }
        }
        public virtual string PleaseUploadAllDocuments

        {
            get
            {
                return resourceLoader.GetString("PleaseUploadAllDocuments");
            }
        }
        public virtual string PleaseReviseEnteredDataAndConfirm

        {
            get
            {
                return resourceLoader.GetString("PleaseReviseEnteredDataAndConfirm");
            }
        }
        public virtual string StudentInformation

        {
            get
            {
                return resourceLoader.GetString("StudentInformation");
            }
        }
        public virtual string ParentInformation

        {
            get
            {
                return resourceLoader.GetString("ParentInformation");
            }
        }
        public virtual string CommitmentTextForEnrollment

        {
            get
            {
                return resourceLoader.GetString("CommitmentTextForEnrollment");
            }
        }
        public virtual string PleaseConfirmEnteredData

        {
            get
            {
                return resourceLoader.GetString("PleaseConfirmEnteredData");
            }
        }
        public virtual string AdoptionOfTheData

        {
            get
            {
                return resourceLoader.GetString("AdoptionOfTheData");
            }
        }
        public virtual string PleaseContactSchoolToCheckTransformation

        {
            get
            {
                return resourceLoader.GetString("PleaseContactSchoolToCheckTransformation");
            }
        }
        public virtual string PreRegistration

        {
            get
            {
                return resourceLoader.GetString("PreRegistration");
            }
        }
        public virtual string MobilePhoneShouldStartWith55

        {
            get
            {
                return resourceLoader.GetString("MobilePhoneShouldStartWith55");
            }
        }
        public virtual string ExplainingTheJobAndTheTotalSalary

        {
            get
            {
                return resourceLoader.GetString("ExplainingTheJobAndTheTotalSalary");
            }
        }
        public virtual string YouCantEditThisRequest

        {
            get
            {
                return resourceLoader.GetString("YouCantEditThisRequest");
            }
        }
        public virtual string RequiredDays

        {
            get
            {
                return resourceLoader.GetString("RequiredDays");
            }
        }
        public virtual string EndDate

        {
            get
            {
                return resourceLoader.GetString("EndDate");
            }
        }
        public virtual string StartDate

        {
            get
            {
                return resourceLoader.GetString("StartDate");
            }
        }
        public virtual string LeaveType

        {
            get
            {
                return resourceLoader.GetString("LeaveType");
            }
        }
        public virtual string DayDays

        {
            get
            {
                return resourceLoader.GetString("DayDays");
            }
        }

        public virtual string DepartureDate

        {
            get
            {
                return resourceLoader.GetString("DepartureDate");
            }
        }

        public virtual string ExitPermit

        {
            get
            {
                return resourceLoader.GetString("ExitPermit");
            }
        }
        public virtual string LoanType

        {
            get
            {
                return resourceLoader.GetString("LoanType");
            }
        }
        public virtual string LoanAmount

        {
            get
            {
                return resourceLoader.GetString("LoanAmount");
            }
        }
        public virtual string Installment

        {
            get
            {
                return resourceLoader.GetString("Installment");
            }
        }
        public virtual string PaymentPeriod

        {
            get
            {
                return resourceLoader.GetString("PaymentPeriod");
            }
        }

        public virtual string RequestedDocuments
        {
            get
            {
                return resourceLoader.GetString("RequestedDocuments");
            }
        }

        public virtual string EmployeeName

        {
            get
            {
                return resourceLoader.GetString("EmployeeName");
            }
        }
        public virtual string JoinedDate

        {
            get
            {
                return resourceLoader.GetString("JoinedDate");
            }
        }
        public virtual string DeptOfficeInstitute

        {
            get
            {
                return resourceLoader.GetString("DeptOfficeInstitute");
            }
        }
        public virtual string JobTitle

        {
            get
            {
                return resourceLoader.GetString("JobTitle");
            }
        }
        public virtual string PersonalIDNo

        {
            get
            {
                return resourceLoader.GetString("PersonalIDNo");
            }
        }

        public virtual string StaffNo
        {
            get
            {
                return resourceLoader.GetString("StaffNo");
            }
        }

        public virtual string Back

        {
            get
            {
                return resourceLoader.GetString("Back");
            }
        }
        public virtual string Submit

        {
            get
            {
                return resourceLoader.GetString("Submit");
            }
        }
        public virtual string RequestOwner

        {
            get
            {
                return resourceLoader.GetString("RequestOwner");
            }
        }
        public virtual string RequiredDocumentType

        {
            get
            {
                return resourceLoader.GetString("RequiredDocumentType");
            }
        }
        public virtual string DocumentRequestHint

        {
            get
            {
                return resourceLoader.GetString("DocumentRequestHint");
            }
        }

        public virtual string NextWithArrow
        {
            get
            {
                return resourceLoader.GetString("NextWithArrow");
            }
        }

        public virtual string Details
        {
            get
            {
                return resourceLoader.GetString("Details");
            }
        }

        public virtual string ErrorMsg_InputIsRequired
        {
            get
            {
                return resourceLoader.GetString("ErrorMsg_InputIsRequired");
            }
        }

        public virtual string Error
        {
            get
            {
                return resourceLoader.GetString("Error");
            }
        }

        public virtual string MonthlyInstallment
        {
            get
            {
                return resourceLoader.GetString("MonthlyInstallment");
            }
        }

        public virtual string PaymentPeriodMonths
        {
            get
            {
                return resourceLoader.GetString("PaymentPeriodMonths");
            }
        }

        public virtual string QatarImageId
        {
            get
            {
                return resourceLoader.GetString("QatarImageId");
            }
        }
        public virtual string RequiredLoanType
        {
            get
            {
                return resourceLoader.GetString("RequiredLoanType");
            }
        }

        public virtual string LoanRequester
        {
            get
            {
                return resourceLoader.GetString("LoanRequester");
            }
        }

        public virtual string BasicSalary
        {
            get
            {
                return resourceLoader.GetString("BasicSalary");
            }
        }

        public virtual string PersonalLoan
        {
            get
            {
                return resourceLoader.GetString("PersonalLoan");
            }
        }
        
        public virtual string ErrorMsg_NotEligableLoan
        {
            get
            {
                return resourceLoader.GetString("ErrorMsg_NotEligableLoan");
            }
        } 

        public virtual string ExitRequester
        {
            get
            {
                return resourceLoader.GetString("ExitRequester");
            }
        }




        public virtual string JobNumber
        {
            get
            {
                return resourceLoader.GetString("JobNumber");
            }
        }

        public virtual string GradeCategory
        {
            get
            {
                return resourceLoader.GetString("GradeCategory");
            }
        }
        public virtual string FirstDateOfLeaving
        {
            get
            {
                return resourceLoader.GetString("FirstDateOfLeaving");
            }
        }

        public virtual string LastDateOfLeaving
        {
            get
            {
                return resourceLoader.GetString("LastDateOfLeaving");
            }
        }

        public virtual string AlternativeEmployee
        {
            get
            {
                return resourceLoader.GetString("AlternativeEmployee");
            }
        }

        public virtual string LeaveRequestHint
        {
            get
            {
                return resourceLoader.GetString("LeaveRequestHint");
            }
        }

        public virtual string Proof
        {
            get
            {
                return resourceLoader.GetString("Proof");
            }
        }
        
        public virtual string LeavingRequest
        {
            get
            {
                return resourceLoader.GetString("LeavingRequest");
            }
        }

        public virtual string ReplacemenetEmployeeDoesntExist
        {
            get
            {
                return resourceLoader.GetString("ReplacemenetEmployeeDoesntExist");
            }
        }

        public virtual string LeaveReturn
        {
            get
            {
                return resourceLoader.GetString("LeaveReturn");
            }
        }

        public virtual string ErrorMsg_MaximumLoanAmount
        {
            get
            {
                return resourceLoader.GetString("ErrorMsg_MaximumLoanAmount");
            }
        }

        public virtual string ReturnInfo
        {
            get
            {
                return resourceLoader.GetString("ReturnInfo");
            }
        }

        public virtual string ProvidingSickLeave
        {
            get
            {
                return resourceLoader.GetString("ProvidingSickLeave");
            }
        }

        public virtual string ProvidingALenientVacation
        {
            get
            {
                return resourceLoader.GetString("ProvidingALenientVacation");
            }
        }

        public virtual string NewEndDateOfLeaving
        {
            get
            {
                return resourceLoader.GetString("NewEndDateOfLeaving");
            }
        }

        public virtual string UnitDept
        {
            get
            {
                return resourceLoader.GetString("UnitDept");
            }
        }

        public virtual string SelectedLeave
        {
            get
            {
                return resourceLoader.GetString("SelectedLeave");
            }
        }

        public virtual string ReturnRequester
        {
            get
            {
                return resourceLoader.GetString("ReturnRequester");
            }
        }

        public virtual string LeavingDetails
        {
            get
            {
                return resourceLoader.GetString("LeavingDetails");
            }
        }

        public virtual string IncreaseLeaveDays
        {
            get
            {
                return resourceLoader.GetString("IncreaseLeaveDays");
            }
        }

        public virtual string ErrorMsg_ThisLeaveHadFinished
        {
            get
            {
                return resourceLoader.GetString("ErrorMsg_ThisLeaveHadFinished");
            }
        }

        public virtual string DaysNumber
        {
            get
            {
                return resourceLoader.GetString("DaysNumber");
            }
        }

        public virtual string CutLeaving
        {
            get
            {
                return resourceLoader.GetString("CutLeaving");
            }
        }

        public virtual string ChangeReturnDate
        {
            get
            {
                return resourceLoader.GetString("ChangeReturnDate");
            }
        }

        public virtual string Attachments
        {
            get
            {
                return resourceLoader.GetString("Attachments");
            }
        }

        public virtual string SubmitRequestOnBehalfTo
        {
            get
            {
                return resourceLoader.GetString("SubmitRequestOnBehalfTo");
            }
        }

        public virtual string ErrorMsg_NoApprovedLeaves
        {
            get
            {
                return resourceLoader.GetString("ErrorMsg_NoApprovedLeaves");
            }
        }

        //NumberOfOtherEmployee

        public virtual string NumberOfOtherEmployee
        {
            get
            {
                return resourceLoader.GetString("NumberOfOtherEmployee");
            }
        }

        public virtual string GetString(string name)
        {
            return resourceLoader.GetString(name);
        }
    }
}
