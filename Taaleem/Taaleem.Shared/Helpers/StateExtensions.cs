﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.UI.Xaml;

namespace Taaleem.Helpers
{
    public static class StateExtensions
    {
        public static VisualStateGroup Get(this IList<VisualStateGroup> stateGroups, string name)
        {
            return stateGroups.Single(x => x.Name == name);
        }

        public static VisualState Get(this IList<VisualState> stateGroups, string name)
        {
            return stateGroups.Single(x => x.Name == name);
        }
    }

}
