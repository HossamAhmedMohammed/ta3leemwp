﻿using Autofac.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.Storage.Pickers;

namespace Taaleem.Helpers
{
    public class FilePicker
    {
        private readonly CoreApplicationView view;
        private String ImagePath;
        private TaskCompletionSource<StorageFile> _taskCompletionSource;

        public FilePicker()
        {
            if (!Windows.ApplicationModel.DesignMode.DesignModeEnabled)
                view = CoreApplication.GetCurrentView();
        }
        public Task<StorageFile> PickImageStorageFileAsync()
        {
            _taskCompletionSource = new TaskCompletionSource<StorageFile>();
            ImagePath = string.Empty;
            FileOpenPicker filePicker = new FileOpenPicker();
            filePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            filePicker.ViewMode = PickerViewMode.Thumbnail;

            // Filter to include a sample subset of file types
            filePicker.FileTypeFilter.Clear();
            filePicker.FileTypeFilter.Add(".bmp");
            filePicker.FileTypeFilter.Add(".png");
            filePicker.FileTypeFilter.Add(".jpeg");
            filePicker.FileTypeFilter.Add(".jpg");
            filePicker.PickSingleFileAndContinue();
            view.Activated += ViewActivated;
            return _taskCompletionSource.Task;
        }

#if WINDOWS_PHONE_APP
        private void ViewActivated(CoreApplicationView sender, IActivatedEventArgs args1)
        {
            Windows.ApplicationModel.Activation.FileOpenPickerContinuationEventArgs args = args1 as FileOpenPickerContinuationEventArgs;
            view.Activated -= ViewActivated;
            if (args != null)
            {
                if (args.Files.Count == 0)
                {
                    _taskCompletionSource.TrySetResult(null);
                    return;
                }

                StorageFile storageFile = args.Files[0];
                _taskCompletionSource.TrySetResult(storageFile);

            }

        }
#endif
    }
}
