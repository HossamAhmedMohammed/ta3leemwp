﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children;
using Taaleem.Models.Employees.Response;
using Taaleem.Models.SchoolRegisteration.Response;
using Windows.Security.Credentials;
using Windows.Storage;

namespace Taaleem.Helpers
{
    public class AppSettings : IAppSettings
    {
        #region Fields
        private const string ParentResource = "[MOE-Parent-P]";
        private const string StudentResource = "[MOE-Student-P]";
        private const string EmployeesResource = "[MOE-Employees-P]";
        private const string SchoolRegisterationResource = "[MOE-Schools-Reg]";
        private readonly ApplicationDataContainer _localSettings;
        private readonly PasswordVault _vault;
        private readonly CryptoHelper _cryptoHelper;
        #endregion

        public AppSettings(CryptoHelper cryptoHelper)
        {
            //This will be used to store user name and password
            _vault = new PasswordVault();
            _localSettings = ApplicationData.Current.LocalSettings;
            _cryptoHelper = cryptoHelper;
          
        }

     

        private int _languageId;
        public int LanguageId
        {
            get
            {
                if (_languageId == 0)
                    _languageId = LoadFromSettings("LanguageId", Lanuage.Arabic);
                return _languageId;
            }
            set
            {
                SaveToSettings("LanguageId", value);
                _languageId = value;
            }
        }

        private bool _amIParent;

        public bool AmIParent
        {
            get { return _amIParent; }
            set { _amIParent = value; }
        }
        private bool isParentMode;

        public bool IsParentMode
        {
            get { return isParentMode; }
            set { isParentMode = value; }
        }
        private bool isStudentMode;

        public bool IsStudentMode
        {
            get { return isStudentMode; }
            set { isStudentMode = value; }
        }
        private int _selectedChildIndex = -1;
        public int SelectedChildIndex
        {
            get { if (isParentMode && AmIParent) { return _selectedChildIndex; }
                else { return 0; } }
            set { _selectedChildIndex = value; }
        }
    
        #region Login Responses


        //Parent
        private ParentLoginWrapper _childLoginResponse;

        public async Task<ParentLoginWrapper> GetParentLoginResponseAsync()
        {
            if (_childLoginResponse == null)
            {
                var encrypted = await GetSettingsFromFileAsync("ParentLoginResponse");
                if (encrypted == null) return null;
                try
                {
                    var decrypted = await _cryptoHelper.UnprotectAsync(encrypted);
                    _childLoginResponse = JsonConvert.DeserializeObject<ParentLoginWrapper>(decrypted);
                }
                catch (Exception)
                {
                    _childLoginResponse = null;
                }
            }
            return _childLoginResponse;
        }

        public async Task SetParentLoginResponseAsync(ParentLoginWrapper value)
        {
            string encrypted = null;
            if (value != null)
            {
                try
                {
                    var json = JsonConvert.SerializeObject(value);
                    encrypted = await _cryptoHelper.ProtectAsync(json);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            await SaveSettingsInFileAsync("ParentLoginResponse", encrypted);
            _childLoginResponse = value;
        }

        //Student
        private ParentLoginWrapper _studentLoginResponse;

        public async Task<ParentLoginWrapper> GetStudentLoginResponseAsync()
        {
            if ( IsStudentMode == true)//_childLoginResponse == null &&
            {
                var encrypted = await GetSettingsFromFileAsync("StudentLoginResponse");
                if (encrypted == null) return null;
                try
                {
                    var decrypted = await _cryptoHelper.UnprotectAsync(encrypted);
                    _studentLoginResponse = JsonConvert.DeserializeObject<ParentLoginWrapper>(decrypted);
                }
                catch (Exception)
                {
                    _studentLoginResponse = null;
                }
            }
            return _studentLoginResponse;
        }

        public async Task SetStudentLoginResponseAsync(ParentLoginWrapper value)
        {
            string encrypted = null;
            if (value != null)
            {
                try
                {
                    var json = JsonConvert.SerializeObject(value);
                    encrypted = await _cryptoHelper.ProtectAsync(json);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            await SaveSettingsInFileAsync("StudentLoginResponse", encrypted);
            _studentLoginResponse = value;
        }
        //Employee
        private string _employeeId;
        public string EmployeeId
        {
            get
            {
                if (_employeeId == null)
                    _employeeId = LoadFromSettings<string>("EmployeeId", null);
                return _employeeId;
            }
            set
            {
                SaveToSettings("EmployeeId", value);
                _employeeId = value;
            }
        }
      
      

       

    


        private ParentInfo _parentRegisterationInfo;
        /// <summary>
        /// Parent Info of school registeration Service
        /// </summary>
        public ParentInfo ParentRegisterationInfo
        {
            get
            {
                if (_parentRegisterationInfo == null)
                {
                    string json = LoadFromSettings<string>("ParentInfo", null);
                    if (json == null) return null;
                    _parentRegisterationInfo = JsonConvert.DeserializeObject<ParentInfo>(json);
                }
                return _parentRegisterationInfo;
            }
            set
            {

                _parentRegisterationInfo = value;
                var json = value == null ? null : JsonConvert.SerializeObject(value);
                SaveToSettings("ParentInfo", json);
            }
        }

        #endregion

        #region Save and retieve Files
        public async Task SaveSettingsInFileAsync(string key, string value)
        {
            var file = await FileSystemUtils.CreateLocalFile(key);
            if (file == null) throw new FileNotFoundException(key);
            using (var stream = await file.OpenStreamForWriteAsync())
            {
                using (var streamWriter = new StreamWriter(stream))
                {
                    await streamWriter.WriteAsync(value);
                }
            }
        }

        public async Task<string> GetSettingsFromFileAsync(string key)
        {
            try
            {
                var file = await FileSystemUtils.GetLocalFile(key);
                if (file == null) return null;
                using (var stream = await file.OpenStreamForReadAsync())
                {
                    using (var streamReader = new StreamReader(stream))
                    {
                        return await streamReader.ReadToEndAsync();
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion

        #region Password Credentials

        //Parent Credential
        private PasswordCredential _parentCredential;
        public PasswordCredential ParentCredential
        {
            get
            {
                GetCredential(ParentResource, ref _parentCredential);
                return _parentCredential;
            }
        }

        public void SetParentCredential(String userName, String password)
        {
            SetCredential(ParentResource, userName, password, out _parentCredential);
        }

        public void RemoveParentCredential()
        {
            RemoveVaultCredentials(ParentResource);
            _parentCredential = null;
        }

        //Student Credential
        private PasswordCredential _studentCredential;
        public PasswordCredential StudentCredential
        {
            get
            {
                GetCredential(StudentResource, ref _studentCredential);
                return _studentCredential;
            }
        }

        public void SetStudentCredential(String userName, String password)
        {
            SetCredential(StudentResource, userName, password, out _studentCredential);
        }

        public void RemoveStudentCredential()
        {
            RemoveVaultCredentials(StudentResource);
            _studentCredential = null;
        }

        //Employee Credential
        private PasswordCredential _employeeCredential;
        public PasswordCredential EmployeeCredential
        {
            get
            {
                GetCredential(EmployeesResource, ref _employeeCredential);
                return _employeeCredential;
            }
        }

        public void SetEmployeeCredential(String userName, String password)
        {
            SetCredential(EmployeesResource, userName, password, out _employeeCredential);
        }

        public void RemoveEmployeeCredential()
        {
            RemoveVaultCredentials(EmployeesResource);
            _employeeCredential = null;
        }

        //School Registeration Credential
        private PasswordCredential _schoolRegisterationCredential;
        /// <summary>
        /// Credentials of parent of school registeration Service
        /// </summary>
        public PasswordCredential SchoolRegisterationCredential
        {
            get
            {
                GetCredential(SchoolRegisterationResource, ref _schoolRegisterationCredential);
                return _schoolRegisterationCredential;
            }
        }

        private int _selectedStageIndex = -1;
        public int SelectedStageIndex
        {
            get
            {
                return _selectedStageIndex;
            }

            set
            {
                _selectedStageIndex = value;
            }
        }

        private string _loginUserName;
        public string LoginUserName
        {
            get
            {
                if (_loginUserName == null)
                    _loginUserName = LoadFromSettings<string>("LoginUserName", null);
                return _loginUserName;
            }
            set
            {
                SaveToSettings("LoginUserName", value);
                _loginUserName = value;
            }
        }

        private string _loginPassword;
        public string LoginPassword
        {
            get
            {
                if (_loginPassword == null)
                    _loginPassword = LoadFromSettings<string>("LoginPassword", null);
                return _loginPassword;
            }
            set
            {
                SaveToSettings("LoginPassword", value);
                _loginPassword = value;
            }
        }
        private int _currentModule;
        public int CurrentModule
        {
            get
            {
                if (_currentModule == 0)
                    _currentModule = LoadFromSettings<int>("CurrentModule", 0);
                return _currentModule;
            }
            set
            {
                SaveToSettings("CurrentModule", value);
                _currentModule = value;
            }
        }
        private string _authCookie;
        public string AuthCookie
        {
            get
            {
                if (_authCookie == null)
                    _authCookie = LoadFromSettings<string>("AuthCookie", null);
                return _authCookie;
            }
            set
            {
                SaveToSettings("AuthCookie", value);
                _authCookie = value;
            }
        }

        public void SetSchoolRegisterationCredential(String userName, String password)
        {
            SetCredential(SchoolRegisterationResource, userName, password, out _schoolRegisterationCredential);
        }

        public void RemoveSchoolRegisterationredential()
        {
            RemoveVaultCredentials(SchoolRegisterationResource);
            _schoolRegisterationCredential = null;
        }

        private void GetCredential(string resource, ref PasswordCredential outputCredential)
        {

            if (outputCredential == null)
            {
                try
                {
                    var credentials = _vault.FindAllByResource(resource);
                    outputCredential = credentials.FirstOrDefault();
                    outputCredential.RetrievePassword();
                }
                catch
                {
                    outputCredential = null;
                }
            }
        }

        private void SetCredential(string resource, String userName, String password, out PasswordCredential outputCredential)
        {
            RemoveVaultCredentials(resource);
            if (userName != null && password != null)
            {

                outputCredential = new PasswordCredential(resource, userName, password);
                //Add the new credential
                _vault.Add(outputCredential);
            }
            else
            {
                outputCredential = null;
            }
        }

        private void RemoveVaultCredentials(string resource)
        {
            try
            {
                var credentials = _vault.FindAllByResource(resource);
                foreach (var passwordCredential in credentials)
                {
                    _vault.Remove(passwordCredential);
                }
            }
            catch
            {
                // ignored
            }
        }

        #endregion

        #region Local settings Helpers

        public void SaveToSettings(string key, Object value)
        {
            _localSettings.Values[key] = value;
        }

        public void DeleteSetting(string key)
        {
            if (_localSettings.Values.ContainsKey(key))
                _localSettings.Values.Remove(key);
        }

        public T LoadFromSettings<T>(string key, T defaultValue)
        {
            if (!_localSettings.Values.ContainsKey(key)) return defaultValue;
            var obj = _localSettings.Values[key];
            if (obj == null) return default(T);
            if (obj.GetType() == typeof(T)) return (T)obj;
            try
            {
                var convertedValue = Convert.ChangeType(obj, typeof(T), null);
                return (T)convertedValue;
            }
            catch (Exception)
            {
                //Failed to Convert to the required type
                //Log The error here and rethrow or silently return the default 
                return defaultValue;
            }
        }
        private EmployeeLoginResponse _employeeLoginResponse;
        public async Task<EmployeeLoginResponse> GetEmployeeLoginResponseAsync()
        {
            if (_employeeLoginResponse == null)
            {
                var encrypted = await GetSettingsFromFileAsync("EmployeeLoginResponse");
                if (encrypted == null) return null;
                try
                {
                    var decrypted = await _cryptoHelper.UnprotectAsync(encrypted);
                    _employeeLoginResponse = JsonConvert.DeserializeObject<EmployeeLoginResponse>(decrypted);
                }
                catch (Exception)
                {
                    _employeeLoginResponse = null;
                }
            }
            return _employeeLoginResponse;
        }

        public async Task SetEmployeeLoginResponseAsync(EmployeeLoginResponse value)
        {
            string encrypted = null;
            if (value != null)
            {
                try
                {
                    var json = JsonConvert.SerializeObject(value);
                    encrypted = await _cryptoHelper.ProtectAsync(json);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            await SaveSettingsInFileAsync("EmployeeLoginResponse", encrypted);
            _employeeLoginResponse = value;
        }


        #endregion

      
    }
}
