﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Helpers;
using Taaleem.Models.Children.Database;

namespace Taaleem.Dao.Children
{
    public class FeedsDao
    {
        public void AddFeed(FeedData feedData)
        {
            using (var db = new SQLiteConnection(DatabaseManager.DbPath))
            {
                db.Insert(feedData);
            }
        }

        public bool UpdateFeed(FeedData feedData)
        {
            try
            {
                using (var db = new SQLiteConnection(DatabaseManager.DbPath))
                {
                    db.Update(feedData);
                    return true;

                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteFeedServerId(int serverId)
        {
            using (var db = new SQLiteConnection(DatabaseManager.DbPath))
            {
                FeedData spaceContent = db.Table<FeedData>().Where(s => s.ServerId == serverId).FirstOrDefault();
                if (spaceContent != null)
                {
                    db.Delete(spaceContent);
                    return true;
                }
                else
                    return false;

            }
        }

        public async Task<List<FeedData>> GetFavouritedFeedsDataAsync(int studentId)
        {
            return await Task.Run<List<FeedData>>(() =>
            {
                using (var db = new SQLiteConnection(DatabaseManager.DbPath))
                {
                    List<FeedData> feedsDataList = db.Table<FeedData>().Where(s => s.IsFavourite && s.StudentId == studentId).ToList();
                    return feedsDataList;
                }
            });
        }
    }
}
