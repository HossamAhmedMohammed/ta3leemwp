﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Collections
{
    public class KeyedList<TKeyType, TItem> : List<TItem>
    {
        public TKeyType Key { get; set; }
    }
}
