﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;

namespace Taaleem.Collections.Extended
{
    /// <summary>
    /// Extends the WinRT ICollectionView to provide sorting and filtering.
    /// </summary>
    public interface IExtendedCollectionView : ICollectionView
    {
    }

}
