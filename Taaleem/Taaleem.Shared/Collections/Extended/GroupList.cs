﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Data;

namespace Taaleem.Collections.Extended
{
    public class GroupList : ICollectionViewGroup
    {
        //public GroupInfo Info { get; set; }
        private Object _group { get; set; }

        private readonly ObservableVector<object> items;

        public GroupList()
        {
            items = new ObservableVector<object>();
        }

        public GroupList(IList<object> collection)
        {
            items = new ObservableVector<object>(collection);
        }

        public object Group
        {
            get { return _group; }
            set { _group = value; }
        }

        public IObservableVector<object> GroupItems
        {
            get { return items; }
        }
    }
}
