﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using Windows.Foundation.Collections;

namespace Taaleem.Collections.Extended
{
    public class ObservableVector<T> : Collection<T>, IObservableVector<T>, INotifyPropertyChanged
    {
        #region Vector Implementation
        protected int Updating { get; set; }
        public event VectorChangedEventHandler<T> VectorChanged;

        /// <summary>
        /// Raises the <see cref="VectorChanged"/> event.
        /// </summary>
        protected virtual void OnVectorChanged(IVectorChangedEventArgs e)
        {
            if (Updating <= 0)
            {
                if (VectorChanged != null)
                    VectorChanged(this, e);
                // OnPropertyChanged("Count");
            }
        }

        #endregion

        #region New methods
        //TODO:Supress Notifications, Reset View
        public void AddRange(IEnumerable<T> collection)
        {
            foreach (var item in collection)
            {
                Add(item);
            }
        }
        #endregion

        #region Contructors

        public ObservableVector()
        { }

        public ObservableVector(IList<T> list)
            : base(list)
        { }


        #endregion

        #region Protected methods

        protected override void ClearItems()
        {
            base.ClearItems();
            OnPropertyChanged("Count");
            this.OnPropertyChanged("Items");
            OnVectorChanged(new VectorChangedEventArgs(CollectionChange.Reset, 0));
        }

        protected override void InsertItem(int index, T item)
        {
            base.InsertItem(index, item);
            this.OnPropertyChanged("Count");
            this.OnPropertyChanged("Items");
            OnVectorChanged(new VectorChangedEventArgs(CollectionChange.ItemInserted, index));
        }

        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
            this.OnPropertyChanged("Count");
            this.OnPropertyChanged("Items");
            OnVectorChanged(new VectorChangedEventArgs(CollectionChange.ItemRemoved, index));
        }

        protected override void SetItem(int index, T item)
        {
            base.SetItem(index, item);
            this.OnPropertyChanged("Items");
            OnVectorChanged(new VectorChangedEventArgs(CollectionChange.ItemChanged, index));
        }
        #endregion

        #region Nested classes (for EventArgs)

        /// <summary>
        /// Class that implements IVectorChangedEventArgs so we can fire VectorChanged events.
        /// </summary>
        class VectorChangedEventArgs : IVectorChangedEventArgs
        {
            readonly CollectionChange _cc;
            readonly uint _index;

            public VectorChangedEventArgs(CollectionChange cc, int index)
            {
                _cc = cc;
                _index = (uint)index;
            }
            public CollectionChange CollectionChange
            {
                get { return _cc; }
            }
            public uint Index
            {
                get { return _index; }
            }
        }

        #endregion

        #region Binary Search
        /*
         Binary Search adopted from this answer on stack overflow 
         http://stackoverflow.com/a/2948872/2086772
         */
        /// <summary>
        /// Performs a binary search on the specified collection.
        /// </summary>
        /// <param name="value">The value to search for.</param>
        /// <param name="comparer">The comparer that is used to compare the value with the list items.</param>
        /// <returns></returns>
        public int BinarySearch(T value, Func<T, T, int> comparer)
        {
            if (comparer == null)
            {
                throw new ArgumentNullException("comparer");
            }

            int lower = 0;
            int upper = this.Count - 1;

            while (lower <= upper)
            {
                int middle = lower + (upper - lower) / 2;
                int comparisonResult = comparer(value, this[middle]);
                if (comparisonResult < 0)
                {
                    upper = middle - 1;
                }
                else if (comparisonResult > 0)
                {
                    lower = middle + 1;
                }
                else
                {
                    return middle;
                }
            }

            return ~lower;
        }

        /// <summary>
        /// Performs a binary search on this collection.
        /// </summary>
        /// <param name="value">The value to search for.</param>
        /// <returns></returns>
        public int BinarySearch(T value)
        {
            return BinarySearch(value, Comparer<T>.Default);
        }

        /// <summary>
        /// Performs a binary search on the specified collection.
        /// </summary>
        /// <param name="value">The value to search for.</param>
        /// <param name="comparer">The comparer that is used to compare the value with the list items.</param>
        /// <returns></returns>
        public int BinarySearch(T value, IComparer<T> comparer)
        {
            return BinarySearch(value, comparer.Compare);
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var eventHandler = this.PropertyChanged;
            if (eventHandler != null)
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
