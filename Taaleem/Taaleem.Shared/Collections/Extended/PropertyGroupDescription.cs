﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Windows.UI.Xaml.Data;

namespace Taaleem.Collections.Extended
{
    public class PropertyGroupDescription : GroupDescription
    {
        public PropertyGroupDescription(string propertyName)
            : this(propertyName, null)
        {
        }

        public PropertyGroupDescription(IValueConverter converter)
            : this(null, converter)
        {
        }

        public PropertyGroupDescription(String propertyName, IValueConverter converter)
        {
            PropertyName = propertyName;
            Converter = converter;
        }

        public string PropertyName { get; private set; }

        /// <summary>
        /// supplies a list of groups, or implicit, where the groups are generated dynamically depending on the data.
        /// PropertyGroupDescription allows you to create implicit groups based on a PropertyName. 
        /// If you simply want to group by a property you can set the PropertyName property.
        /// If you want to change the value that is eventually used for group you can use the Converter property to supply a value converter. 
        /// For example, you may want to group items based on the first letter of a name. 
        /// If the PropertyName property is not set, the item itself is passed to the value converter.
        /// </summary>
        public IValueConverter Converter { get; private set; }

        public override object GroupNameFromItem(object item)
        {
            if (PropertyName == null) return Converter.Convert(item, null, null, null);

            object propertyValue = GetPropertyValue(item, PropertyName);
            if (propertyValue == null) return null;
            if (Converter == null) return propertyValue;
            return Converter.Convert(propertyValue, null, null, null);
        }

        private object GetPropertyValue(object obj, string propertyName)
        {
            if (String.IsNullOrEmpty(propertyName)) return null;
            var propertyNames = propertyName.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            if (propertyNames == null) return null;
            foreach (var name in propertyNames)
            {
                var typeInfo = obj.GetType().GetTypeInfo();
                var property = typeInfo.GetDeclaredProperty(name);
                obj = property.GetValue(obj);
                if (obj == null) return null;
            }
            return obj;
        }
    }
}
