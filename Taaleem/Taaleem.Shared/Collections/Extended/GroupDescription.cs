﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Collections.Extended
{
    public abstract class GroupDescription
    {
        public abstract Object GroupNameFromItem(Object item);
    }
}
