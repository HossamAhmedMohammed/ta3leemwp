﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Exceptions.Request;
using Taaleem.Models.Exceptions.Response;
using Taaleem.Views.Exceptions;

namespace Taaleem.ViewModels.Exceptions
{
  public  class OtherViewModel : BindableBase
    {
        #region Fields
        private readonly IExceptionDataService _exceptionDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        #endregion

        #region Properties
        private Additionaldata _additionalData;

        public Additionaldata AdditionalData
        {
            get { return _additionalData; }
            set { _additionalData = value;
                OnPropertyChanged();
            }
        }
            
        private Houserentcontract _houseRentContract;

        public Houserentcontract HouseRentContract
        {
            get { return _houseRentContract; }
            set { _houseRentContract = value;
                OnPropertyChanged();
            }
        }
        private MyExceptionRequestResponse _request;

        public MyExceptionRequestResponse Request
        {
            get { return _request; }
            set { _request = value;
                OnPropertyChanged();
            }
        }
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        #endregion

        #region Initialization

        public OtherViewModel(Resources resources, IExceptionDataService exceptionDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _exceptionDataService = exceptionDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _dialogManager = dialogManager;

            AdditionalData = new Additionaldata();
            HouseRentContract = new Houserentcontract();
            //Initialize Commands
           // AddApplicantCommand = new AsyncExtendedCommand(AddApplicantAcync);
        }



        #endregion

        #region Commands
       // public AsyncExtendedCommand AddApplicantCommand { get; set; }
        #endregion

        #region Methods
        public async Task UpdateAdditionalDataAcync()
        {
            IsBusy = true;
            try
            {

                var parameters = new UpdateAdditionalDataParameters()
                {
                    AdditionalData  = AdditionalData,//new Additionaldata {  Area = Area , BuildingNumber = BuildingNumber, ElectricityNumber = ElectricityNumber, HasException = HasException, IsPayingRent= IsPayingRent, RentAmount= RentAmount, RequestId= 15, Street=Street},
                     HouseRentContract = HouseRentContract
                };
                parameters.AdditionalData.RequestId = Request.Id;
                
                var response = await _exceptionDataService.UpdateAdditionalData(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if(response.Result.IsValid == false)
                    {
                        if (_appSettings.LanguageId == 2)
                            await _dialogManager.ShowMessage("", response.Result.ErrorMessageAr.ToString());
                        else if (_appSettings.LanguageId == 1)
                            await _dialogManager.ShowMessage("", response.Result.ErrorMessageEn.ToString());
                    }
                    else
                    {
                        if (_appSettings.LanguageId == 2)
                            await _dialogManager.ShowMessage("", "تم ارسال طلبك بنجاح");
                        else if (_appSettings.LanguageId == 1)
                            await _dialogManager.ShowMessage("", "Request has been sent successfully");
                        _navigationService.NavigateByPage(typeof(MyRequests));
                    }
                }
                else
                {

                 // ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

               IsBusy = false;
            }
        }

        public async Task GetAdditionalDataAsync()
        {

            try
            {

                

                var response = await _exceptionDataService.GetAdditionalDataByID(Request.Id);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result != null)
                    {
                        AdditionalData = response.Result;
                    }
                }
                else
                {

                    // ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                ////  IsBusy = false;
            }
        }

        #endregion
    }
}