﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Exceptions.Request;
using Taaleem.Models.Exceptions.Response;
using Taaleem.Views.Exceptions;

namespace Taaleem.ViewModels.Exceptions
{
   public class MyRequestsViewModel : BindableBase
    {
        #region Fields
        private readonly IExceptionDataService _exceptionDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        #endregion

        #region Properties
       
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private List<MyExceptionRequestResponse> _myRequests;

        public List<MyExceptionRequestResponse> MyRequests
        {
            get { return _myRequests; }
            set { _myRequests = value;
                OnPropertyChanged();
            }
        }
 




        #endregion

        #region Initialization

        public MyRequestsViewModel(Resources resources, IExceptionDataService exceptionDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _exceptionDataService = exceptionDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _dialogManager = dialogManager;
            //Initialize Commands
            AddCommend = new ExtendedCommand(addNewReuset);
            ItemClickedCommand = new ExtendedCommand<MyExceptionRequestResponse>(EditRequest);
            GetMyRequestsAcync();
           
        }

       




        #endregion

        #region Commands
        public     ExtendedCommand AddCommend { get; set; }
        public ExtendedCommand <MyExceptionRequestResponse>ItemClickedCommand { get; set; }
        #endregion

        #region Methods


        private void EditRequest(MyExceptionRequestResponse arg)
        {
            if (arg != null)
            {
                if(arg.IsEditable == true)
                {
                    if (arg.RequestNextStep == 1)// none
                    {
                       
                        _navigationService.NavigateByPage(typeof(Stage1Commitment),arg);
                    }
                    if (arg.RequestNextStep == 2)// Applicant
                    {
                     
                        _navigationService.NavigateByPage(typeof(Stage2RequestOwner),arg);
                    }
                    if (arg.RequestNextStep == 3)// father
                    {
                        
                        _navigationService.NavigateByPage(typeof(Stage3Parents),arg);
                    }
                    if (arg.RequestNextStep == 4)// mother
                    {
                       
                        _navigationService.NavigateByPage(typeof(Stage3Parents),arg);
                    }
                    if (arg.RequestNextStep == 5)// breadwinner
                    {
                     
                        _navigationService.NavigateByPage(typeof(Stage3Parents),arg);
                    }
                    if (arg.RequestNextStep == 6)// Exceptional student
                    {
                       
                        _navigationService.NavigateByPage(typeof(Stage4StudentData),arg);
                    }
                    if (arg.RequestNextStep == 7)// Exceptional student twins
                    {
                      
                        _navigationService.NavigateByPage(typeof(Stage4StudentData), arg);
                    }
                    if (arg.RequestNextStep == 8)// child
                    {
                       
                        _navigationService.NavigateByPage(typeof(Stage5Children),arg);
                    }
                    if (arg.RequestNextStep == 9)// Additional Data
                    {
                        
                        _navigationService.NavigateByPage(typeof(Stage6Other), arg);
                    }
                    // _navigationService.NavigateByPage(typeof(Stage2RequestOwner), arg);
                }
                else
                {
                    _dialogManager.ShowMessage("", ViewModelLocator.Resources.YouCantEditThisRequest);
                }
            }
        }

        private void addNewReuset()
        {
            _navigationService.NavigateByPage(typeof(Stage1Commitment));
            ViewModelLocator.Locator.applicantVM.Applicant = new Applicant();
            ViewModelLocator.Locator.applicantVM.Request = null;
            ViewModelLocator.Locator.applicantVM.QIDAttachment.FileName = null;

            ViewModelLocator.Locator.parentsVM.Father = new Parent();
            ViewModelLocator.Locator.parentsVM.Father.ParentType = 1;
            ViewModelLocator.Locator.parentsVM.Mother = new ParentM();
            ViewModelLocator.Locator.parentsVM.Mother.ParentType = 2;
            ViewModelLocator.Locator.parentsVM.BreadWinner = new Breadwinner();

            ViewModelLocator.Locator.studentVM.Student = new Student();
            ViewModelLocator.Locator.childrenVM.Child = new Child();


        }
        public async Task GetMyRequestsAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var response = await _exceptionDataService.GetMyRequests();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    MyRequests = response.Result;
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

       

        #endregion
    }
}