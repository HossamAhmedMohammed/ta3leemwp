﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Exceptions.Request;
using Taaleem.Models.Exceptions.Response;
using Taaleem.Views.Exceptions;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Taaleem.ViewModels.Exceptions
{
  public  class ApplicantViewModel : BindableBase
    {
        #region Fields
        private readonly IExceptionDataService _exceptionDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        #endregion

        #region Properties
        private Applicant _applicant;

        public Applicant Applicant
        {
            get { return _applicant; }
            set { _applicant = value;
                OnPropertyChanged();
            }
        }

        private Qataridattachment _qIDAttachment;

        public Qataridattachment QIDAttachment
        {
            get { return _qIDAttachment; }
            set { _qIDAttachment = value;
                OnPropertyChanged();
            }
        }


        private MyExceptionRequestResponse _request;

        public MyExceptionRequestResponse Request
        {
            get { return _request; }
            set { _request = value;
                OnPropertyChanged();
            }
        }

        //private string _name;

        //public string Name
        //{
        //    get { return _name; }
        //    set { _name = value;
        //        OnPropertyChanged();
        //    }
        //}
        //private string _qID;
        //public string QID
        //{
        //    get { return _qID; }
        //    set
        //    {
        //        _qID = value;
        //        OnPropertyChanged();
        //    }
        //}

        private DateTimeOffset _birthdate;
        public DateTimeOffset Birthdate
        {
            get { return _birthdate; }
            set
            {
                _birthdate = value;
                Applicant.DOB = (_birthdate.Year + "-" + _birthdate.Month + "-" + _birthdate.Day);
                OnPropertyChanged();
            }
        }
        //private string _phone;
        //public string Phone
        //{
        //    get { return _phone; }
        //    set
        //    {
        //        _phone = value;
        //        OnPropertyChanged();
        //    }
        //}

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

      

       


        #endregion

        #region Initialization

        public ApplicantViewModel(Resources resources, IExceptionDataService exceptionDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _exceptionDataService = exceptionDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _dialogManager = dialogManager;
            //Initialize Commands
            AddApplicantCommand = new AsyncExtendedCommand(AddApplicantAcync);

            Applicant = new Applicant();
            QIDAttachment = new Qataridattachment();
        }

       

        #endregion

        #region Commands
        public AsyncExtendedCommand AddApplicantCommand { get; set; }
        #endregion

        #region Methods
        public async Task AddApplicantAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {
               
                var parameters = new AddApplicantParameters()
                {
                     Applicant = Applicant,
                      QatarIdAttachment = QIDAttachment//sXvHCUn329pJW9XrFW+Hn2Piix+HX7VP/BZDW01O/luo/BZnae0nv2bS/DFkVa62eRGqlrh0bzYPNRJ5V3KssgHNfXP7LP/AAb/APw5+H2iLdfFK9uvH+t3EAWSztp5tP0uxZliJCGJlnldHWUCRnRWSQZhVgDX6AUV1YbhzDQl7XEXqz7y/wAv87nzfEPjxn2Kof2fkcY5fhV8MKPuyS86iSd+7ioX6re/LfDX4G+Cvgx9t/4Q/wAH+FvCn9pbPtf9jaTBY/atm7Z5nlIu/bvfGc43Njqa6miivfjGMVyxVkfi+IxNbEVHWrycpPdttt9N2FFFFUYhRRRQAUUUUAFcD8Yf2Wfhv+0D57+NPA3hbxJdT2Lab9tvdOikvYbc7/kiuMedFgyOymN1KsxZSDzXfUVE6cJrlmrrzOrB43E4Sqq+FqShNbOLcX96sz84P2s/+De/wz4v83VPg9rf/CJXxx/xJNZllutMf/VL+7uPnuIcATOd4n3u6qPKUV8y2PxF/ap/4I362mmX8V1J4LE7QWkF+rap4YvSzXWzyJFYNbu7ebP5SPBK21WljI4r9uaq65odl4n0S803UrO11DTtQge2urW5iWWC5idSrxujAqyspIKkEEEg14GJ4coOftsI3Sn3jt93+Wh+15F48ZvDCrK+JqUMxwvWNVJz9VOzbkt1KSlLtJaW+PP2TP8AguB8JP2h/K07xRN/wrDxG+f3Os3Stpk2PNb93fYVBiONSfPWHLyqieYea+za/PT9uD/ggv4Z+LuoPr/whutL8B6xJve50a6WU6RfSPMGMiMu9rTarSDZHG8Z2xKqRAMx+Zv2ZP8Agox8af8AgmL8Qbf4efFjQ9f1HwrBPGsumayJG1DTbOMPbb9Lmd/Le3BiG1QWgfyCI2i3vJXPDN8Vgpqlmcfd6TW3z/penU9jF+GPDvFmFlmPh7XaqxV54Sq/fX/XuT3XrKS7zT90/aSiuB/Zp/aW8JftZ/CTT/GfgzUPtul3uY5YpAEudOuFAL21wgJ2SpuGRkghlZSyMrN31fT06kakVODumfz5jcFiMHiJ4XFQcKkG1KLVmmt013CiiirOUKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKK+b/+ClX/AAUI0z9gT4SWt4lj/a/jHxN50Hh6wkRxbM8QTzJ7hxjEUXmxkopDyF1VdoLyR4YnEU6FJ1qrtFHr5DkWOznMKWV5bTc61R2il97b7JJNtvRJNvQq/wDBSX/gpL4f/YM+Hywwra638Q9bgZtF0VnOyJclftd1tIZbdWBAAIaVlKKQBJJH+dX7NP7Ffxm/4K5/FvT/AIj/ABN1fVF8FTZtbnxHOIIZLmG2IU2un26gKuXZx5ixiFX89jvlVo31f+Ce/wDwT38Xf8FLPi3ffGD4wX2qXXg66vmnubmdzFc+LLhDtMEJXHlWse0Rs8e0KE8mHaVZof2Q0PQ7Lwxolnpum2drp+nafAlta2ttEsUFtEihUjRFAVVVQAFAAAAAr5ehhq2bz+sYq8aK+GPfzf8AXp3f9EZtn2WeGOFeScPclbNZJqviLXVK6/h0vNPV30uvfTfuw5b4Dfs9eDP2Yvh9H4W8CaDa+HtDjnkuTBE7yvNK5+aSSSRmkkfAVdzsSFRFGFVQOzoor62EIwioQVkuiP5oxeLr4qtLE4qbnOTvKUm3Jt7tt6tvuwoooqjnCiiigAooooAKKKKACiiigAooooAKKKKACvN/2m/2TfAn7Xfw+uPD3jfQ7XUEaCSKz1BY0XUNIZyhMtrMVLRPujjJAyr7Arq65U+kUVFSnCpFwqK6fQ68Bj8TgcRDF4Oo6dSDvGUXZp900fhl8Rfg78fP+CLnxni8Q6RqF0fCtzqohtdTt5C2i+J1jRmSG9tlfKOY5ZQElwysJmgkby/Nr9Zv2H/24PCX7dPwkTxF4df7Fqllsh1vRJpQ9zo9wwJCk4G+J9rGOUABwp4V1kjT0j4o/C7w/wDGr4far4V8VaVa634f1uA295Z3AOyVcgggghldWCsrqQyMqspDAEfjL+0f+zh8Tv8Agiz+05pvjrwLqV1e+Eb2dodL1SaPfBeRN876XqKLtUuVTPG0SCMSxGOSNlh+RlSq5LU9pSvLDvddY+a8v+Getmf0zhsxy3xXwf1HMeTD51TX7uolywxCS+GX97Tbp8UFy80F+3NFeMfsP/tweEv26fhIniLw6/2LVLLZDreiTSh7nR7hgSFJwN8T7WMcoADhTwrrJGns9fWUa0K0FVpO8Xsz+a81yrGZZjKmAx9N06tN2lF7p/1qmtGrNNphRRRWp54UUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHA/tLftLeEv2TPhJqHjPxnqH2LS7LEcUUYD3Oo3DAlLa3Qkb5X2nAyAArMxVFZl/IT9k74O+O/+CzH7aV94y8faha3Phvw7Pa3PiBTI8cEFkZHaDSrOJHEiLII5l3BhsHmys7ysBL1P/BU343+KP2//ANvjTPgh4IurqfRNA1VNCgtP3v2WbVQWW8vp0WLzAtuDJGzESKkdvNIh2ytn9SP2Tf2ZPD/7InwJ0PwR4et7VU0+BG1C8igMT6velFE13ICztvkZc4LtsUIinaigfIzTzbGuD/gUnr/el/l+nqf01hKlPw14Uji4r/hXzGF4PrQovrtdSkul/itf+G0++0PQ7Lwxolnpum2drp+nafAlta2ttEsUFtEihUjRFAVVVQAFAAAAAq1RRX122iP5mlJyblJ3bCiiigQUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABWB8Ufhd4f+NXw+1Xwr4q0q11vw/rcBt7yzuAdkq5BBBBDK6sFZXUhkZVZSGAI36KUoqS5ZK6ZrQr1KNSNajJxlFppp2aa1TTWqaezPxG0OH4nf8ENf20rNNSuLq/8AAuvTot1PbW++z8VaWkg3ukbOqpewK5IUuGidwCzwy5l/Zr4XfFHw/wDGr4faV4q8K6ra634f1uAXFneW5OyVckEEEBldWDKyMAyMrKwDAgeR/wDBRT9h/TP26P2frzw/s0u08W6d/pXhzV7uJz/Z9xlS8ZZDuEUyL5b8MBlJNjtEgr4d/wCCDf7Wet/D/wCLes/ATxjLqlvHcfaJtCsL9ZFk0e/tzI95ZiMxlo96iSVg7oiPbPhd8zZ+UwnNleNWDl/BqfD5Pt/XdeZ/SHEvsfEPhSfFFJJZngUliIxSXtKXSrZLdJXeqsozVrKmj9XaKKK+sP5qCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAr56/4Kh/tXXv7Hn7Huu+JtEvbWx8VX88GkaC9xatcp9qmbLsFwU3pbpcSqZPk3RKGDZ2N9C1+PP/Bc34q6n+0l+274S+D3h7TPPvvCnk6bbK+yGS+1LVPszhFkaTZ5Xl/ZFBYJtczZJXaa8fPsbLDYOUofFL3V6v8AVK7R+p+DXCVLiDimhh8Xb2FK9Wre1uSnZ2lfTllLljLyk2d9/wAG9/7Hv/Ib+NOuWP8Af0Twx58P0+13ce+P/dgSWKT/AJ/EYV+pFcZ+zr8GLL9nf4E+EvA9gbWSDwvpcFg09vaLaJeSog824MSkhXlk3ytyxLSMSzEkns62ynArB4WNFb9fV7nkeJfGNTifiLEZtJ+43y012px0j6XXvP8AvNhRRRXpHwYUUUUAFFFFABRRRQAV8A/8FX/+Din4H/8ABLtLvw79o/4WZ8VYvlHhHQ7xB/Z7YJ/4mF1hktBgD5NrzfOh8rY28a3/AAcY/ET4t/Dn/glt4qk+Cg8XL411jVtL0dZ/DEcz6rBBcXSRv5Hkgyh5GKRAx4b99wcmvyK/4JXf8GhfxC/aLt9N8a/tG6lqPwt8J3QS5i8MWqq3ibUo3TcPPLho7DlkysiyTfLIjxQnDVjTjUxE5wi+WMdG/Oyenya7t66K1zWbhRhGUlzOWqS9Wtfu8ktLvU/P/wD4KQf8Ff8A45f8FSvGIvPib4pYeHrScT6Z4U0kNaaHpbAMA6QbiZJQHcedM0kuHKhwuFH6wf8ABjdqE7L+0lamaU2yf8I7KsJc+Wrn+0gWC9MkKoJ9h6V59/wdy/sc/DD9h74G/syeCvhR4L0XwV4chuvEUr29jEfNvJdmlr51xM5aW4l2qo8yV2fCqM4AFd3/AMGNn/H7+0r/ALnhz+eqV3ZU6bp4hU1ok162lHVnPmcZ+zpVKj1k4v099qy+7olvY/oDooornLCiiigAooooAKKKKACiiigAr8ef+C437IOp/s+ftA2Pxs8I/wDEo0fxTfQGe4sblLa40zXUDyCWNY1Rl81YfO8xS7ecs7Mylo937DV4x/wUD/Zqt/2sP2SPGPhL+z/7Q1g2L3+gqhhSVNSgUvbhJJQVj3sPKdsr+7mkXcoYkeRnmA+t4SUF8S1Xqv8APY/TvCLjSXDXEtDFzf7mo/Z1U9vZyau3svd0l8u1zV/Yp/aJ/wCGsP2WPBfj97X7Fda/Yn7bCI/LjS7hke3uPLXe5ERmikKbmLbCu7ByB6lX5W/8G5Px5lj1v4gfDC5kupIJoI/FGnRrDH5EDIyW12Wk4k3uHstq/MoELn5Sfn/VKrybG/W8HCs97WfqtH9+5x+KnCS4b4oxeVw/hqXND/BP3or/ALdT5X5xYUUUV6h+ehRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAFXXNdsvDGiXmpaleWun6dp8D3N1dXMqxQW0SKWeR3YhVVVBJYkAAEmvxl/4JSaHZfth/wDBWPXPHt/Z2tjBZz6t47XS7iJb5PNmuQkUQdgoDwyXiSrLszut1IVSQy/pT/wVE+JX/CqP+CfnxV1T7F9v+1aI+jeV53lbft7pY+Zna2fL+0b9uPm2bcrncPlH/g2++Hd7pnwv+KHi15bU6dreq2WkQRqzeektnFLLIzDbtClb6LaQxJKvkDALfL5p+/zTDYZ7RvN/p+K/zP6H8Om8o8PM9z+Ok6vJhoO/81ue2+vLUTu19n3Wnc/SmiiivqD+eAooooAKKKKACiiigAooooAKKKKAPwM/4Pkv+QF+zZ/138Rf+g6ZWf8A8GNn/H7+0r/ueHP56pWh/wAHyX/IC/Zs/wCu/iL/ANB0ys//AIMbP+P39pX/AHPDn89Uqsj/AIWJ/wC3v/S4l5v/AAcP/wBu/wDpxn9AdFFFSQFFFFABRRRQAUUUUAFFFFABRRRQB+KHijw7/wAO8v8Agt1p00Uel6V4cm8UxXdrPe2X9naZaaVqoMcxiG9UWK2jubiJZA3lh7UkqArRj9r6/I7/AIOPPhr/AGX8a/hv4w+27/7d0S50b7J5OPI+xzibzN+75t/2/G3aNvlZyd2F/Uj4G/Er/hc/wU8H+MPsX9m/8JXollrP2TzvO+y/aIEm8vftXft343bRnGcDpXzGRpUMXicGtlJSXz/pH9C+MFSWb8NZBxPL3p1KUqM5d5UnZdndy9o3ZW7aNHU0UUV9Ofz0FFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAfGX/AAXl+JX/AAgv/BPzUdL+xfav+Ez1uw0bzfO2fY9jtfeZjad+fsezblf9Zuz8u02v+CFXw7svBP8AwTx0DUrWW6kn8X6rqGr3iyspSKVJ2sgsYCghfLs4zgljuZznBAHGf8HEv/JlPhf/ALHe0/8ASDUK9S/4It/8o0Phr/3FP/TreV8zD3s+lzdKen3r/Nn9AYuKw/g5QdHT2uNfP52pz/8AkI7dvU+pKKKK+mP5/CiiigAooooAKKKKACiiigAooooA/Az/AIPkv+QF+zZ/138Rf+g6ZWf/AMGNn/H7+0r/ALnhz+eqVof8HyX/ACAv2bP+u/iL/wBB0ys//gxs/wCP39pX/c8Ofz1SqyP+Fif+3v8A0uJeb/wcP/27/wCnGf0B0UUVJAUUUUAFFFFABRRRQAUUUUAFFFFAH56f8HGHgvTL79l7wP4iltt2s6X4pGm2tx5jjyre5tJ5Jk252nc9rbnJBI8vggM2fob/AIJR/EW9+KP/AATx+FupX8VrDPbaW+kKturKhisZ5bKJiGYneY7dCxzgsWICjAHh3/BxL/yZT4X/AOx3tP8A0g1CvUv+CLf/ACjQ+Gv/AHFP/TreV8zQ0z2ol1gvzR/QGcRVTwewFSerhjJRj5RcKsml2Tav66n1JRRRX0x/P4UUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB8Zf8F5fhr/AMJ1/wAE/NR1T7b9l/4QzW7DWfK8nf8AbN7tY+XncNmPtm/dhv8AV7cfNuFr/ghV8RbLxt/wTx0DTbWK6jn8IarqGkXjSqoSWV52vQ0ZDElfLvIxkhTuVxjABPpH/BUT4a/8LX/4J+fFXS/tv2D7Loj6z5vk+bu+wOl95eNy48z7Ps3Z+Xfuw2Np+Uf+Db74i3up/C/4oeEnitRp2iarZavBIqt57y3kUsUisd20qFsYtoCggs+ScgL8zV/dZ7Br7cGvmrv8kj+gsuj/AGh4PYuEnd4TFxmltaM4xgumt5Tn1uvSx+lNFFFfTH8+hRRRQAUUUUAFFFFABRRRQBh/Ef4meHPg74Kv/Evi7xBonhbw5pSCS91XWL6KxsrNSwUNJNKyogLMoyxHJA7143/w9i/ZZ/6OW+AH/hw9I/8AkiuT/wCC1H7BXif/AIKVf8E8/GHwl8Ha/pfh3xFrVxY3dpcanJLHYTG3uo5jFO0SO4RlU8hGwwXivwu/4gqf2pv+h++AH/g81f8A+VlZQnNzkpKyW3npv9+ny8zSUYqEWnq9/L+t/n5M9J/4PIP2tfhV+09o37Py/DX4mfD74hto02vHUB4Z8RWerGxEi6f5fm/Z5H8vdsfbuxnY2Ohqj/wZvftX/C39mC7/AGgj8SviV4A+Hg1pNAGnnxN4htNJ+3+WdR8zyvtEieZs3pu25xvXPUVwn/EFT+1N/wBD98AP/B5q/wD8rKP+IKn9qb/ofvgB/wCDzV//AJWVtg39XjUjHXnv+LT/AEIxT9vCEHpy2/CTl+p+/wAv/BWD9lp2AH7SvwBJJwAPiFpHP/kxXvscgljDKQysMgg5BFfzCr/wZU/tSlhnx98AQM8ka3q//wArK/pV+CXw7b4QfBnwj4Sa+k1NvC+i2ekG8kGHuzbwJF5pGTgtsyee9acsPZc19b7eX/A09b+TMnKXtFFLSz189LL56/cdPRRRWRoFFFFABRRRQAUUUUAFFFFAH56f8HGHjXTLH9l7wP4dludus6p4pGo2tv5bnzbe2tJ45n3Y2ja91bjBIJ8zgEK2Pob/AIJR/Du9+F3/AATx+Fum38trNPc6W+rq1uzMgivp5b2JSWUHeI7hAwxgMGALDBPwn/wcefEr+1PjX8N/B/2LZ/YWiXOs/a/Oz5/2ycQ+Xs2/Ls+wZ3bju83GBty36kfA34a/8KY+Cng/wf8Abf7S/wCEU0Sy0b7X5Pk/avs8CQ+Zs3Ns3bM7dxxnGT1r5nA/vM5xFRbRUY/fZ/mmf0Bxenl/hbkuAm7SxFWrWa3+Fyimmtlyzi7N3u/Ky6miiivpj+fwooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigCrrmh2XifRLzTdSs7XUNO1CB7a6tbmJZYLmJ1KvG6MCrKykgqQQQSDX4tf8EiPEX/AAyJ/wAFSNQ8DeIpNLe6v/7T8DXF6L3ybaG7inV1aMugMnmzWawohCMxnXuNh/a+vxv/AOC4/wAOtY/Zy/b48NfFfQJbq0n8SwWmr2d9K0EyQappxjiKxxFSdqRpZPiVWVmlfBIBVfmOJYukqWOj/wAu5a+j/q3zP6F8A69PMJ5lwfXdlj6MlFt2tUgm4/hKUnb+TVNH7IUVy3wS+MOi/tA/CTw9408Oz+fo/iSxjvbfLxtJDuHzQybGZRLG+6N1DHa6Muciupr6WMlKKlHZn4BicPVw9aWHrxcZxbTT3TTs0/NMKKKKoxCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoorxj/goH+0tb/sn/skeMfFv9of2frAsXsNBZBC8r6lOpS3KRykLJsY+a64b93DI21gpByrVo0qcqs9krnoZTlmIzLG0svwqvUqyUYrzk7L/AIJ+W/ijxF/w8N/4LdadDFJpeq+HIfFMVpawXt7/AGjpl3pWlAyTCI7GRormO2uJVjC+WXuiCxDNIf2vr8rf+Dcn4DSya38QPifcx3UcEMEfhfTpFmj8idnZLm7DR8yb0CWW1vlUiZx8xHyfqlXgcM05vDyxVT4qsnL+vxP2r6QOYYeGd4fh3A/wcvowpLW/vWTk/W3Knu+ZO7vsUUUV9IfgoUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAV89f8FQ/wBlG9/bD/Y913wzolla33iqwng1fQUuLprZPtULYdQ2Qm97d7iJRJ8m6VSxXG9foWiscRQhXpSo1NpKx6uR5zicpzGhmeDdqlGUZx7Xi72dmrp7NX1V0flv/wAG9/7YX/Ib+C2uX39/W/DHnzfT7XaR75P92dI4o/8An8djX6kV+N//AAVN+CHij9gD9vjTPjf4ItbqDRPEGqprsF3+9+yw6qSzXljO6y+YVuAJJGUmNXjuJo0G2JsfqR+yb+034f8A2u/gTofjfw9cWrJqECLqFnFOZX0i9CKZrSQlUbfGzYyUXepR1G11J+f4fxEqfNl1d+/T2849Gv62sftXjZkeHxzw/HWTx/2XHRTnbX2da3vRlbq7a95KXdHpFFFFfTH4AFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABX48/8Fxv2vtT/AGg/2gbH4J+ER/a+j+Fr6AT29jbJc3Gp664eMRRtGzs3lLN5PlqEbzmnVlYrHt/Qv/gop+3Bpn7C/wCz9eeIN+l3fi3Uf9F8OaRdyuP7QuMqHkKoNxihRvMflQcJHvRpUNfDv/BBv9kzW/iB8W9Z+PfjGLVLiO3+0Q6Ff37SNJrF/cGRLy8EhkDSbFMkTF0dHe5fDb4Wx8vntWWJqwyug9Zay8o/1+nc/ojwdyzD5Bl+K8Q84p3p4dOGHjLT2lZ6e7193bmSdryktabt+hf7FP7O3/DJ/wCyx4L8APdfbbrQLE/bZhJ5kb3c0j3Fx5bbEJiE0sgTcobYF3ZOSfUqKK+kpU404KnDZKy+R+CZlmFfH4urjsS71KspTk+8pNtv72FFFFaHEFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHA/tLfs1eEv2s/hJqHgzxnp/23S73EkUsZCXOnXCghLm3cg7JU3HBwQQzKwZGZW/I79lT4/wDxI/4I3/tbzfDv4gx+T4K1e+iOs27tLLZG3kYRrrNiyoWbCrkhUzIsTROiyxr5X7X18y/8FJf+Cbfh/wDbz+HyzQm10T4h6JAy6LrTIdkq5LfZLraCzW7MSQQC0TMXUEGSOTwc5y6pU5cXhNKsNvNdn+h+zeFXHeBwCrcN8Srny3FaS3fsp9Kke3TmaTekZLWNn9IaHrtl4n0Sz1LTby11DTtQgS5tbq2lWWC5idQySI6kqyspBDAkEEEVar8Zf+Ce/wDwUI8Xf8E0/i3ffB/4wWOqWvg61vmgubadDLc+E7hzuM8IXPm2sm4SMke4MH86HcWZZv2Q0PXbLxPolnqWm3lrqGnahAlza3VtKssFzE6hkkR1JVlZSCGBIIIIrqyrNaeNp3Wkl8UeqZ874i+HWO4Txyp1H7XD1fepVY/DUjutVdKSTV1fs03Fpu1RRRXqH52FFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVgfFH4o+H/AIK/D7VfFXirVbXRPD+iQG4vLy4J2RLkAAAAszsxVVRQWdmVVBYgE+KPxR8P/BX4far4q8Vara6J4f0SA3F5eXBOyJcgAAAFmdmKqqKCzsyqoLEA/jL+0f8AtH/E7/gtN+05pvgXwLpt1ZeEbKdptL0uaTZBZxL8j6pqLruUOFfHG4RiQRRCSSRmm8jNs2jg4qEFzVJfDH+un5n6f4a+GtfievPE4mfsMDQ1rVnoopauMW9HJr5RWsuilV/4u3/wXS/a3/6AvhbRP96XTPB9g7f8B8+6l2f7LzOn/LOGL9z+0nwu+F3h/wCCvw+0rwr4V0q10Tw/okAt7OztwdkS5JJJJLM7MWZnYlnZmZiWJJ83/Yf/AGH/AAl+wt8JE8O+HU+26pe7Jtb1uaIJc6xcKCAxGTsiTcwjiBIQMeWdpJH9nrPJsslhoutiHerP4n+i/r8Ejt8VvEGhntenlWSw9lluF92jBLlv3nJXu23e19Um2/elO5RRRXtn5GFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAfMv/BSX/gm34f/AG8/h8s0JtdE+IeiQMui60yHZKuS32S62gs1uzEkEAtEzF1BBkjk/Or9kz9vz4t/8EpPG0Xw0+JHhfVH8HJfG4uNI1GFlvdPhMkscs+mSlhE8Tyqz4y8MrRNseMyPKf2vryP9sP9iXwJ+3D8PrbQfGtpdK+nz/aNP1TT3SLUNNYlfMEUjI67ZFUK6MrK2FONyIy/P5lk851PrmClyVV90vJ/16n7XwB4oYbC4H/Vfiqj9Zy2b23nRevvU3vu72TTWrju1Ls/g98bvCX7QPgmDxF4L8Q6X4k0afaPtFlOJPJcxpJ5Uq/eilCSIWjkCuu4blBrqa/Eb4i/s4ftB/8ABGb4oReLfDOpXWq+ETOLmfUtNjuJNFv4o5Whig1W34WJ2WcbQzEBrg+TMZFLL9xfsX/8Fuvhj8f9E07S/Hd9a/Dvxp5CrdG/bydFvJQsjO8FyzFYl2xhttwUIaVY1aYjcVgc+hOf1fGL2dTs9n6P+vK5fF3gzisPhf7b4WqfX8BLaUFece6nBa6dWlp9pRPteiiivoT8RCiiigAooooAKKKKACiiigAooooAKKKq65rtl4Y0S81LUry10/TtPge5urq5lWKC2iRSzyO7EKqqoJLEgAAk0Xtqxxi5NRirtlqvN/2m/wBrLwJ+yJ8PrjxD431y105Fgkls9PWRG1DV2QoDFawlg0r7pIwSMKm8M7IuWHyP+21/wXi8GfCO0u9D+Eq2vjrxVDOsTalcQv8A2FahZHWUBldJLl8INpixERMriVtpRvlv9mT/AIJz/Gn/AIKdfEG3+IfxY1zX9O8KzzxtLqesmRdQ1KzkD3OzS4XTy0tyZRtYBYE88mNZdjx185jM+vP6tl69pUf/AICvV/16n7rwt4NunhP7e41q/UcFHW0tKtTyhHdX805do21MD4q/HP47f8Fr/i3pnhrRNB+x+GNLvsxWNksw0jRfNMpS71G5IIaUQq6ByF3eW4hiDyMj/q5+w/8AsP8AhL9hb4SJ4d8Op9t1S92Ta3rc0QS51i4UEBiMnZEm5hHECQgY8s7SSP1P7NP7NXhL9kz4Saf4M8Gaf9i0uyzJLLIQ9zqNwwAe5uHAG+V9oycAAKqqFRVVe+rbK8ndCTxOJlz1Xu+3kv6+48rxF8UI5vh45BkFH6rltJ+7TWjnrdSqd3fVRu7N3bk0miiiivdPx4KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigCrrmh2XifRLzTdSs7XUNO1CB7a6tbmJZYLmJ1KvG6MCrKykgqQQQSDXwn+2F/wAEF/h/8Yvt2tfDW6/4V54jl8yb7BtM+iXch858eX9+23SPGuYiY4448LATX3tRXHjMBh8VHkrxT/P5M+o4W40zvhzEfWcmxEqT6paxl/ii7xl81p0PxGsfiL+1T/wRv1tNMv4rqTwWJ2gtIL9W1TwxelmutnkSKwa3d282fykeCVtqtLGRxX2H+zt/wcDfCz4k/aoPH+lap8NbqLe8MuZNZsp0HlhV3wRCZZSWkO0w7AsefMywWvuzXNDsvE+iXmm6lZ2uoadqED211a3MSywXMTqVeN0YFWVlJBUgggkGvlD4/wD/AARL+BPx48TR6tDpOqeBLrn7QnhWaGytrr5I0XNu8UkUe0J/yxSPcXdn3E5Hhf2XmGC/3CrzQ/ln+j/4Y/ZJeInBPFWvGeXuhiXviMNo2+8oPRt7Xkqj2tZbfTXw6+Kvhf4v6JLqXhLxJoPijToJzbSXWkahFewRyhVYxl4mZQ4V0JXOcMp7it+vyE+Iv/BCL40/s9a3F4l+EXju18QajpsAaCSzuJPDutCWRmikSA+Y0QURPks1whZTIu08B+LuP2kf23f2EftY8Snx7LoWiX0F7qNx4g08a9pkvmeSBC2pESYifKIVhuF2u7AFZCab4gr0NMbh5R81qvv2/Ewh4J5TnD5uE88oV77QqXpVO1uXWT3ST5Em3bSx+19Ffkd8Nf8Ag488a6X9t/4TD4b+Ftd37Psn9jX0+k+Rjdv8zzRc+ZnKYxs27Wzu3Db6l8Nf+DjzwVqn23/hMPhv4p0LZs+yf2NfQat5+d2/zPNFt5eMJjG/dubO3aN3TS4ny2dv3ln5p/5W/E8LMPo+cd4Tml9S54q2sJ05XvbaPNz+vu93tqfo/RXwT/xES/BT/oV/il/4LbD/AOTKP+IiX4Kf9Cv8Uv8AwW2H/wAmV0f29l//AD9R4v8AxBnjf/oW1PuX+Z97UV+a3xF/4OQfC+ma3EnhL4X6/renGANJPq+rRaXOku5soI4o7lSu0IQ28ElmG0YBby34i/8ABxt8Q9T1uJ/CXw/8GaJpwgCyQavNc6pO8u5suJIntlC7SgC7CQVY7jkBearxRlsP+Xl/RP8Ayse7l/0eeO8VZvB+zi+s6lNfelJyT9Yn691xnxn/AGivAn7O+iC/8ceLdB8LwSQT3EC394kU94sKhpRBFnzJ3UMvyRKzEuoAJYA/kHpHjX9u/wDbP/st9OufiklrHYtqVle2kaeE7K9t5fKw4uEFtDc7gUZAXc7S7IMFzXafBj/g3T8d+K9EN1448daD4Onmgglgs7CyfWJ42ZSZIpzvhjR4ztGYnlViWw2AC3P/AG9iq+mCw0n5y0X+X4nuvwa4eyZ83FmfUabT1p0U6s+q7cy6auk+va57N+03/wAHC3gzwTd3Gm/C7w5deNp/IkVdZ1F307T4pTGhidIWTz51V2cSKwtz+7wrEMHHyjdeC/2r/wDgr7qEOr3Ntql54Oe+jNs08i6T4b08NNcIJYY2I+0+RunRpI1nuFVdjMxKg/o/+zt/wRz+BP7PH2qX/hGP+E5vrren2rxcIdT8mNvLPlpD5a24wY8h/K8wb3G/adtfUlT/AGPjcbrmNW0f5YaL7/8Ah/U1j4ocJcKpQ4Hy3nrrT6xifel6ximrN94umtNYu58Zfsmf8EP/AISfs8eVqPiiH/hZ/iNM/vtZtVXTIc+av7uxyyHMcig+e02HiV08s8V9m0UV9DhcHQw0PZ0IqK/rfq/mfiXEfFeb5/ivrmcYiVafTmei8oxVoxXlFJeQUUUV0nz4UUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQBy3xK+Bvgr4z/AGL/AITDwf4W8V/2bv8Asn9s6TBffZd+3f5fmo2zdsTOMZ2rnoK8t+JX/BLv9n74r/Yv7U+FXha1+wb/ACv7GifRd2/bnzPsbReZ90Y37tuW243Nn3uisKuFoVb+0gnfukz2sv4kzfAcv1DFVKXLe3JOUbXve1mrXu7+rPlv/hy3+zR/0TX/AMuHVf8A5Jo/4ct/s0f9E1/8uHVf/kmvqSiuf+ysF/z5h/4Cv8j2v+IkcW/9DTE/+D6v/wAkeHfDr/gmn8BPhdokthpvwn8GXME05uGbV7AaxOGKqpAlu/NkVcKMIGCgliBliT6l8OvhV4X+EGiS6b4S8N6D4X06ec3MlrpGnxWUEkpVVMhSJVUuVRAWxnCqOwrforopYajT/hwS9EkeFmGf5pj7/XsTUq3d3zzlK77u7eoUUUVueQFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH/9k=", FileName= "QatarId.jpg"}
                };
           
                var response = await _exceptionDataService.AddApplicant(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result.Id != 0)
                    {

                        ViewModelLocator.Locator.StagesSelectionVM.Stages[1].IsSelected = false;
                        ViewModelLocator.Locator.StagesSelectionVM.Stages[1].ImagePath = "ms-appx:///Assets/Exceptions/Stages/" + (1).ToString() + ".png";
                        ViewModelLocator.Locator.StagesSelectionVM.Stages[2].IsSelected = true;
                        ViewModelLocator.Locator.StagesSelectionVM.Stages[2].ImagePath = "ms-appx:///Assets/Exceptions/Stages/" + 2.ToString() + "_active.png";
                        Request = new MyExceptionRequestResponse();
                        Request.Id = response.Result.Id;
                        var res = await _exceptionDataService.GetRequestByID(new GetReqestByIDParameters { requestID = Request.Id });
                        if (res.ResponseStatus == ResponseStatus.SuccessWithResult)
                        {
                            Request = res.Result;
                        }
                        _navigationService.NavigateByPage(typeof(Stage3Parents),Request);
                    }
                    else {
                        if(_appSettings.LanguageId == 2)
                        await  _dialogManager.ShowMessage("",response.Result.ErrorMessageAr.ToString());
                        else if (_appSettings.LanguageId == 1)
                            await _dialogManager.ShowMessage("", response.Result.ErrorMessageEn.ToString());
                    }
                }
                else
                {
                   
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
              
                IsBusy = false;
            }
        }

        public async Task UpdateApplicantAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var parameters = new UpdateApplicantParameters()
                {
                    Applicant = Applicant,//new ApplicantU { ApplicantName =  Name, DOB = (Birthdate.Year + "-" + Birthdate.Month + "-" + Birthdate.Day), Mobile = Phone, QatarId = QID, Id = 9 },
                    QatarIdAttachment =QIDAttachment// new QataridattachmentU { FileContent = Identificationcard, FileName = "QID.jpg" }
                };
               
                var response = await _exceptionDataService.UpdateApplicant(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result.IsValid == true)
                    {

                        ViewModelLocator.Locator.StagesSelectionVM.Stages[1].IsSelected = false;
                    ViewModelLocator.Locator.StagesSelectionVM.Stages[1].ImagePath = "ms-appx:///Assets/Exceptions/Stages/" + (1).ToString() + ".png";
                    ViewModelLocator.Locator.StagesSelectionVM.Stages[2].IsSelected = true;
                    ViewModelLocator.Locator.StagesSelectionVM.Stages[2].ImagePath = "ms-appx:///Assets/Exceptions/Stages/" + 2.ToString() + "_active.png";


                        _navigationService.NavigateByPage(typeof(Stage3Parents), Request);
                    }
                    else {
                        if (_appSettings.LanguageId == 2)
                            await _dialogManager.ShowMessage("", response.Result.ErrorMessageAr.ToString());
                        else if (_appSettings.LanguageId == 1)
                            await _dialogManager.ShowMessage("", response.Result.ErrorMessageEn.ToString());

                    }
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task GetApplicantbyIDAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                if (Request != null)
                {

                    var response = await _exceptionDataService.GetApplicantByID(Request.Id );
                    if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                    {
                        Applicant = response.Result;
                        Birthdate = Convert.ToDateTime(Applicant.DOB);
                      await  GetQatarIDAcync();
                    }

                    else
                    {

                        ErrorMessage = _messageResolver.ResultToMessage(response);
                    }
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task GetQatarIDAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var parameters = new GetAttachmentParameters()
                {
                   AttachmentType = 11, OwnerId = Applicant.Id
                };
                var response = await _exceptionDataService.GetAttachmentFile(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result != null)
                    {
                        QIDAttachment.FileName = response.Result.FileName;
                        QIDAttachment.FileContent = response.Result.FileContent;
                    }
                }

                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
                
            }
            finally
            {

                IsBusy = false;
            }
        }
        #endregion
    }
}