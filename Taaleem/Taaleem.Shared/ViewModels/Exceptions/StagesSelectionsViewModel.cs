﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Exceptions;

namespace Taaleem.ViewModels.Exceptions
{
   public class StagesSelectionViewModel : BindableBase
    {
        #region Fields
        private readonly IAppSettings _appSettings;
        private readonly Helpers.Resources _resources;
        private readonly INavigationService _navigationService;
        #endregion

        #region Properties
        private ObservableCollection<Stage> _stages;
        public ObservableCollection<Stage> Stages
        {
            get { return _stages; }
            set { _stages = value;
                OnPropertyChanged();
            }
        }

     

  
        //TODO:Make Sure that when selection changes in other pages, that it reflects in this pages
        //As it may be cached
        public int SelectedstageIndex
        {
            get
            {
               
                    return Stages == null || Stages.Count == 0 ? -1 : _appSettings.SelectedStageIndex;
               
            }
            set
            {
               
                int previousValue = _appSettings.SelectedStageIndex;
                if (previousValue != value)
                {
                    if (previousValue != -1) Stages[previousValue].IsSelected = false;
                    try
                    {
                       
                        Stages[value].IsSelected = true;
                        Stages[value].ImagePath = "ms-appx:///Assets/Exceptions/Stages/" + value.ToString() + "_active.png";
                        
                    }
                    catch { }
                    _appSettings.SelectedStageIndex = value;
                    OnPropertyChanged();
                    OnPropertyChanged("SelectedStage");
                    OnSelectedStageIndexChanged(value);
                }
            }
          
        }



        public Stage SelectedStage
        {
            get
            {               
                    return Stages == null ? null : Stages[SelectedstageIndex];
            }
        }
        #endregion

        #region Initialization

        public  StagesSelectionViewModel(Resources resources, IAppSettings appSettings, INavigationService navigationService)
        {
            _resources = resources;
            _appSettings = appSettings;
            _navigationService = navigationService;
            //Initialize Commands
            Stages = new ObservableCollection<Stage>();
            LoadStagesAsync();
           // _appSettings.SelectedStageIndex = 0;
           // SelectedstageIndex = 0;
            //  callLoadStages();

        }

        public  void callLoadStages()
        {
             LoadStagesAsync();
            OnPropertyChanged("SelectedStage");
        }

        #endregion

        #region Commands

        #endregion

        #region Methods

        public void LoadStagesAsync()
        {
            Stages.Clear();
            Stages.Add(new Stage { StageID = 0, Name = _resources.Promise, ImagePath = "ms-appx:///Assets/Exceptions/Stages/0_active.png" , IsSelected = true});
            Stages.Add(new Stage { StageID = 1, Name = _resources.Applicaant, ImagePath = "ms-appx:///Assets/Exceptions/Stages/1.png" });
            Stages.Add(new Stage { StageID = 2, Name = _resources.Parents, ImagePath = "ms-appx:///Assets/Exceptions/Stages/2.png" });
            Stages.Add(new Stage { StageID = 3, Name = _resources.ToBeExceptioned, ImagePath = "ms-appx:///Assets/Exceptions/Stages/3.png" });
            Stages.Add(new Stage { StageID = 4, Name = _resources.Children, ImagePath = "ms-appx:///Assets/Exceptions/Stages/4.png" });
            Stages.Add(new Stage { StageID = 5, Name = _resources.Other, ImagePath = "ms-appx:///Assets/Exceptions/Stages/5.png" });
           
            //if (SelectedstageIndex == -1)
            //    SelectedstageIndex = 0;
            //else
            //    SelectedstageIndex = _appSettings.SelectedStageIndex;



        }

        public void refreshList()
        {
            Stages[0].Name = _resources.Promise;
            Stages[1].Name = _resources.Applicaant;
            Stages[2].Name = _resources.Parents;
            Stages[3].Name = _resources.ToBeExceptioned;
            Stages[4].Name = _resources.Children;
            Stages[5].Name = _resources.Other;
        
        }
        #endregion

        #region Events

        public event EventHandler<int> SelectedStageIndexChanged;
        protected virtual void OnSelectedStageIndexChanged(int e)
        {
            var handler = SelectedStageIndexChanged;
            if (handler != null) handler(this, e);
        }
        #endregion


    }
}
