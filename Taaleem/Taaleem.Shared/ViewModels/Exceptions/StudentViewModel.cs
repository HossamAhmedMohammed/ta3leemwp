﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Exceptions.Request;
using Taaleem.Models.Exceptions.Response;

namespace Taaleem.ViewModels.Exceptions
{
  public  class StudentViewModel : BindableBase
    {
        #region Fields
        private readonly IExceptionDataService _exceptionDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        #endregion

        #region Properties
        private List<Nationality> _nationalities;

        public List<Nationality> Nationalities
        {
            get { return _nationalities; }
            set { _nationalities = value;
                OnPropertyChanged();
            }
        }

        private List<School> _privateSchools;

        public List<School> PrivateSchools
        {
            get { return _privateSchools; }
            set { _privateSchools = value;
                OnPropertyChanged();
            }
        }

        private List<School> _independentSchools;

        public List<School> IndependentSchools
        {
            get { return _independentSchools; }
            set { _independentSchools = value;
                OnPropertyChanged();
            }
        }

        private List<int> _grades;

        public List<int> Grades
        {
            get { return _grades; }
            set { _grades = value; }
        }




        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private Student _student;

        public Student Student
        {
            get { return _student; }
            set { _student = value;
                OnPropertyChanged();
            }
        }
       


        private bool _hasTwins;

        public bool HasTwins
        {
            get { return _hasTwins; }
            set { _hasTwins = value;
                OnPropertyChanged();
            }
        }

        private Birthcertificate _studentBirthCer;

        public Birthcertificate StudentBirthCer
        {
            get { return _studentBirthCer; }
            set { _studentBirthCer = value;
                OnPropertyChanged();
            }
        }

        private Lastschoolcertificate _lastSchoolCer;

        public Lastschoolcertificate LastSchoolCer
        {
            get { return _lastSchoolCer; }
            set { _lastSchoolCer = value;
                OnPropertyChanged();
            }
        }
        private Passportattachment _passportAttach;

        public Passportattachment PassportAttach
        {
            get { return _passportAttach; }
            set { _passportAttach = value;
                OnPropertyChanged();
            }
        }

        private Qataridattachment _qIDAttach;

        public Qataridattachment QIDAttach
        {
            get { return _qIDAttach; }
            set { _qIDAttach = value;
                OnPropertyChanged();
            }
        }
        private DateTimeOffset _birthdate;
        public DateTimeOffset Birthdate
        {
            get { return _birthdate; }
            set
            {
                _birthdate = value;
                Student.DOB = (_birthdate.Year + "-" + _birthdate.Month + "-" + _birthdate.Day);
                OnPropertyChanged();
            }
        }
        private MyExceptionRequestResponse _request;

        public MyExceptionRequestResponse Request
        {
            get { return _request; }
            set { _request = value;
                OnPropertyChanged();
            }
        }


        #endregion

        #region Initialization

        public StudentViewModel(Resources resources, IExceptionDataService exceptionDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _exceptionDataService = exceptionDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _dialogManager = dialogManager;
            //Initialize Commands
            // AddApplicantCommand = new AsyncExtendedCommand(AddApplicantAcync);
            Student = new Student();
            StudentBirthCer = new Birthcertificate();
            LastSchoolCer = new Lastschoolcertificate();
            PassportAttach = new Passportattachment();
            QIDAttach = new Qataridattachment();

            PrivateSchools = new List<School>();
            Grades = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            GetAllNationalities();
            GetAllIndependentSchools();
            GetAllPrivateSchools();
        }



        #endregion

        #region Commands
       // public AsyncExtendedCommand AddApplicantCommand { get; set; }
        #endregion

        #region Methods
        public async Task<bool> AddStudentAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var parameters = new AddStudentParameters()
                {
                   Student = Student, //new Student {  DOB =Birthdate.ToString(), Gender = GenderID , Grade = Grade, IsTwins= IsTwins, Name = Name, NationalityId=Nationality, Passport = Passport, PrivateSchoolCode= PrivateSchoolCode, QatarId= QID, RequestId=RequestID, SchoolType=SchoolType},
                    HasTwins = HasTwins,
                     BirthCertificate = StudentBirthCer, //new Birthcertificate {  FileContent = BirthCer, FileName=""},
                      LastSchoolCertificate = LastSchoolCer,//new Lastschoolcertificate {  FileContent= LastSchoolCer, FileName=""},
                       PassportAttachment= PassportAttach,//new Passportattachment {  FileContent = PassportAttach, FileName = ""},
                        qatarIdAttachment =QIDAttach //new Qataridattachment {  FileContent = Identificationcard, FileName = ""}
                };
                parameters.Student.RequestId = Request.Id;
                if (Student.Gender != 0)
                {
                    var response = await _exceptionDataService.AddStudent(parameters);
                    if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                    {
                        if (response.Result.Id == 0)
                        {
                            if (_appSettings.LanguageId == 2)
                                await _dialogManager.ShowMessage("", response.Result.ErrorMessageAr.ToString());
                            else if (_appSettings.LanguageId == 1)
                                await _dialogManager.ShowMessage("", response.Result.ErrorMessageEn.ToString());
                            return false;
                        }
                        else
                        {
                            var res = await _exceptionDataService.GetRequestByID(new GetReqestByIDParameters { requestID = Request.Id });
                            if (res.ResponseStatus == ResponseStatus.SuccessWithResult)
                            {
                                Request = res.Result;
                                if(HasTwins == true)
                                {
                                    Request.RequestNextStep = 7;
                                }
                                else
                                {
                                    Request.RequestNextStep = 8;
                                }
                            }
                                return true;
                           
                        }
                    }
                    else
                    {

                        ErrorMessage = _messageResolver.ResultToMessage(response);
                        return false;
                    }
                }
                else
                {
                    await _dialogManager.ShowMessage("", ViewModelLocator.Resources.InsertAllDataMessage);
                    return false;
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task<bool> UpdateStudentAsync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var parameters = new UpdateStudentParameters()
                {
                    Student = Student, //new Student {  DOB =Birthdate.ToString(), Gender = GenderID , Grade = Grade, IsTwins= IsTwins, Name = Name, NationalityId=Nationality, Passport = Passport, PrivateSchoolCode= PrivateSchoolCode, QatarId= QID, RequestId=RequestID, SchoolType=SchoolType},
                    HasTwins = HasTwins,
                    BirthCertificate = StudentBirthCer, //new Birthcertificate {  FileContent = BirthCer, FileName=""},
                    LastSchoolCertificate = LastSchoolCer,//new Lastschoolcertificate {  FileContent= LastSchoolCer, FileName=""},
                    PassportAttachment = PassportAttach,//new Passportattachment {  FileContent = PassportAttach, FileName = ""},
                    qatarIdAttachment = QIDAttach //new Qataridattachment {  FileContent = Identificationcard, FileName = ""}
                };
                parameters.Student.RequestId = Request.Id;
                parameters.Student.Id = Student.Id;
                var response = await _exceptionDataService.UpdateStudent(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result.IsValid != true)
                    {
                        if (_appSettings.LanguageId == 2)
                            await _dialogManager.ShowMessage("", response.Result.ErrorMessageAr.ToString());
                        else if (_appSettings.LanguageId == 1)
                            await _dialogManager.ShowMessage("", response.Result.ErrorMessageEn.ToString());
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                    return false;
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task GetStudentByRequestID()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {

               
                var response = await _exceptionDataService.GetStudentByRequestID(Request.Id);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result.Id != 0)
                    {
                        Student = response.Result;
                        Birthdate= Convert.ToDateTime(Student.DOB);
                        GetAttachmentFileResponse result;
                        result = await GetAttachment(6, Student.Id);
                        if (result != null)
                        {
                            StudentBirthCer.FileContent = result.FileContent;
                            StudentBirthCer.FileName = result.FileName;
                        }
                        result = await GetAttachment(7, Student.Id);
                        if(result!= null)
                        {
                            LastSchoolCer.FileName = result.FileName;
                            LastSchoolCer.FileContent = result.FileContent;
                        }
                        result = await GetAttachment(13, Student.Id);
                        if(result != null)
                        {
                            QIDAttach.FileName = result.FileName;
                            QIDAttach.FileContent = result.FileContent;
                        }
                    }
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task GetStudentTwinsByRequestID()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {


                var response = await _exceptionDataService.GetStudentTwinsByRequestID(Request.Id);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result.Id != 0)
                    {
                        Student = response.Result;
                        Student.IsTwins = true;
                        Birthdate = Convert.ToDateTime(Student.DOB);
                        GetAttachmentFileResponse result;
                        result = await GetAttachment(6, Student.Id);
                        if (result != null)
                        {
                            StudentBirthCer.FileContent = result.FileContent;
                            StudentBirthCer.FileName = result.FileName;
                        }
                        result = await GetAttachment(7, Student.Id);
                        if (result != null)
                        {
                            LastSchoolCer.FileName = result.FileName;
                            LastSchoolCer.FileContent = result.FileContent;
                        }
                        result = await GetAttachment(13, Student.Id);
                        if (result != null)
                        {
                            QIDAttach.FileName = result.FileName;
                            QIDAttach.FileContent = result.FileContent;
                        }
                    }

                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                    Student.IsTwins = true;
                }
            }
            finally
            {

                IsBusy = false;
            }
        }
        public async void GetAllNationalities()
        {
            ErrorMessage = null;
            try
            {


                var response = await _exceptionDataService.GetAllNationalitiesAsync();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    Nationalities = response.Result;
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        public async void GetAllPrivateSchools()
        {
            ErrorMessage = null;
            try
            {


                var response = await _exceptionDataService.GetAllPrivateSchoolsAsync();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    PrivateSchools = response.Result;
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }


        public async void GetAllIndependentSchools()
        {
            ErrorMessage = null;
            try
            {


                var response = await _exceptionDataService.GetAllIndependentSchoolsAsync();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    IndependentSchools = response.Result;
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }
        public async Task<GetAttachmentFileResponse> GetAttachment(int typeID, int ownerID)
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var parameters = new GetAttachmentParameters()
                {
                    AttachmentType = typeID,
                    OwnerId = ownerID
                };
                var response = await _exceptionDataService.GetAttachmentFile(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    return response.Result;
                }

                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                    return null;
                }

            }
            finally
            {

                IsBusy = false;
            }
        }
        #endregion
    }
}