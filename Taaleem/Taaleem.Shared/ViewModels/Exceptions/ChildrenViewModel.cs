﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;

using Taaleem.Models.Exceptions.Request;
using Taaleem.Models.Exceptions.Response;
using Taaleem.Views.Exceptions;

namespace Taaleem.ViewModels.Exceptions
{
  public  class ChildrenViewModel : BindableBase
    {
        #region Fields
        private readonly IExceptionDataService _exceptionDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        #endregion

        #region Properties
        private List<Nationality> _nationalities;

        public List<Nationality> Nationalities
        {
            get { return _nationalities; }
            set { _nationalities = value;
                OnPropertyChanged();
            }
        }

        private List<School> _privateSchools;

        public List<School> PrivateSchools
        {
            get { return _privateSchools; }
            set { _privateSchools = value;
                OnPropertyChanged();
            }
        }

        private List<School> _independentSchools;

        public List<School> IndependentSchools
        {
            get { return _independentSchools; }
            set { _independentSchools = value;
                OnPropertyChanged();
            }
        }

        

       

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private Child _child;

        public Child Child
        {
            get { return _child; }
            set { _child = value;
                OnPropertyChanged();
            }
        }

        private Lastschoolcertificate _lastSchoolCer;

        public Lastschoolcertificate LastSchoolCer
        {
            get { return _lastSchoolCer; }
            set { _lastSchoolCer = value;
                OnPropertyChanged();
            }
        }


        private DateTimeOffset _birthdate;
        public DateTimeOffset Birthdate
        {
            get { return _birthdate; }
            set
            {
                _birthdate = value;
                Child.DOB = (_birthdate.Year + "-" + _birthdate.Month + "-" + _birthdate.Day);
                OnPropertyChanged();
            }
        }

        private MyExceptionRequestResponse _request;

        public MyExceptionRequestResponse Request
        {
            get { return _request; }
            set { _request = value;
                OnPropertyChanged();
            }
        }

        private List<int> _grades;

        public List<int> Grades
        {
            get { return _grades; }
            set { _grades = value; }
        }

        #endregion

        #region Initialization

        public ChildrenViewModel(Resources resources, IExceptionDataService exceptionDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _exceptionDataService = exceptionDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _dialogManager = dialogManager;
            //Initialize Commands

            Child = new Child();
            LastSchoolCer = new Lastschoolcertificate();
            Grades = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            GetAllNationalities();
            GetAllIndependentSchools();
            GetAllPrivateSchools();
        }



        #endregion

        #region Commands
      ///  public AsyncExtendedCommand AddApplicantCommand { get; set; }
        #endregion

        #region Methods
        public async Task AddChildAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var parameters = new AddChildParameters()
                {
                     Child = Child,//new Child {  DOB = Birthdate.ToString(), EducationType = EducationType, Gender= GenderID, Grade= Grade, IndependentSchoolCode= IndependentSchoolCode, Name= Name, NationalityId= Nationality, Passport= Passport, PrivateSchoolCode= PrivateSchoolCode, QatarId = QID, RequestId= RequestID, SchoolType= SchoolType},
                      LastSchoolCertificate =LastSchoolCer //new Lastschoolcertificate {  FileContent= LastSchoolCer, FileName= ""}
                };
                parameters.Child.RequestId = Request.Id;
                var response = await _exceptionDataService.AddChild(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                   if(response.Result.Id == 0)
                    {
                        if (_appSettings.LanguageId == 2)
                            await _dialogManager.ShowMessage("", response.Result.ErrorMessageAr.ToString());
                        else if (_appSettings.LanguageId == 1)
                            await _dialogManager.ShowMessage("", response.Result.ErrorMessageEn.ToString());
                    }
                   else
                    {
                        await _dialogManager.ShowMessage("", "Child Added Successfully!");
                        _navigationService.NavigateByPage(typeof(Stage6Other));
                        _navigationService.GoBack();
                    }
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }
        public async Task UpdateChildAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var parameters = new UpdateChildParameters()
                {
                    Child = Child,//new Child {  DOB = Birthdate.ToString(), EducationType = EducationType, Gender= GenderID, Grade= Grade, IndependentSchoolCode= IndependentSchoolCode, Name= Name, NationalityId= Nationality, Passport= Passport, PrivateSchoolCode= PrivateSchoolCode, QatarId = QID, RequestId= RequestID, SchoolType= SchoolType},
                    LastSchoolCertificate = LastSchoolCer //new Lastschoolcertificate {  FileContent= LastSchoolCer, FileName= ""}
                };
                parameters.Child.RequestId = Request.Id;
                parameters.Child.Id = Child.Id;
                var response = await _exceptionDataService.UpdateChild(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result.IsValid == false)
                    {
                        if (_appSettings.LanguageId == 2)
                            await _dialogManager.ShowMessage("", response.Result.ErrorMessageAr.ToString());
                        else if (_appSettings.LanguageId == 1)
                            await _dialogManager.ShowMessage("", response.Result.ErrorMessageEn.ToString());
                    }
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task GetChildrenByRequestID()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                

                var response = await _exceptionDataService.GetAllChildrenByRequestID(Request.Id);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result.Count== 0)
                    {
                        await _dialogManager.ShowMessage("", "No children");
                    }
                    else
                    {
                        Child = response.Result[0];
                        Birthdate = Convert.ToDateTime(Child.DOB);
                    }
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }
        public async void GetAllNationalities()
        {
            ErrorMessage = null;
            try
            {


                var response = await _exceptionDataService.GetAllNationalitiesAsync();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    Nationalities = response.Result;
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }
        public async void GetAllPrivateSchools()
        {
            ErrorMessage = null;
            try
            {


                var response = await _exceptionDataService.GetAllPrivateSchoolsAsync();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    PrivateSchools = response.Result;
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }


        public async void GetAllIndependentSchools()
        {
            ErrorMessage = null;
            try
            {


                var response = await _exceptionDataService.GetAllIndependentSchoolsAsync();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    IndependentSchools = response.Result;
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        #endregion
    }
}
