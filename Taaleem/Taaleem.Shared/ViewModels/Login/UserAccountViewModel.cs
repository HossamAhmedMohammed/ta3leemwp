﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Login.Request;
using Taaleem.Views.Exceptions;
using Taaleem.Views.PublicServices.Login;
using Taaleem.Views.PublicServices.PreRegistration;

namespace Taaleem.ViewModels.Login
{
  public  class UserAccountViewModel : BindableBase
    {
        #region Fields
        private readonly ILoginDataService _loginDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        private readonly IPreRegistrationDataService _preRegistrationDataService;
        #endregion

        #region Properties
        
        private LoginParameters _loginParamters;

        public LoginParameters LoginParamters 
        {
            get { return _loginParamters; }
            set {
                _loginParamters = value;
                OnPropertyChanged();
            }
        }

        private CreateUserParameters _registerParameters;

        public CreateUserParameters RegisterParameters
        {
            get { return _registerParameters; }
            set { _registerParameters = value;
                OnPropertyChanged();
            }
        }

        private ForgetPasswordParameters _forgetPasswordParams;

        public ForgetPasswordParameters ForgetPasswordParams
        {
            get { return _forgetPasswordParams; }
            set { _forgetPasswordParams = value;
                OnPropertyChanged();
            }
        }
        private ActivateUserParameters _activateUserParams;

        public ActivateUserParameters ActivateUserParams
        {
            get { return _activateUserParams; }
            set { _activateUserParams = value;
                OnPropertyChanged();
            }
        }

        private ResendActivateUserParameters _rasendActivationCodeParams;

        public ResendActivateUserParameters ResendActivationCodeParams
        {
            get { return _rasendActivationCodeParams; }
            set { _rasendActivationCodeParams = value;
                OnPropertyChanged();
            }
        }





        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private int _currentModule;

        public int CurrentModule
        {
            get { return _currentModule; }
            set { _currentModule = value;
                OnPropertyChanged();
            }
        }





        #endregion

        #region Initialization

        public UserAccountViewModel(Resources resources, ILoginDataService loginDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService, IPreRegistrationDataService preRegistrationDataService)
        {
            _loginDataService = loginDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _dialogManager = dialogManager;
            _preRegistrationDataService = preRegistrationDataService;
            //Initialize Commands
            LoginCommand = new AsyncExtendedCommand(LoginAcync);
            RegisterCommand = new AsyncExtendedCommand(RegisterAcync);
            ForgetPasswordCommand = new AsyncExtendedCommand(ForgetPasswordAcync);
            ActivateUserCommand = new AsyncExtendedCommand(ActivateUserAcync);
            ResendActivationCodeCommand = new AsyncExtendedCommand(ResendActivationCodeAsync);

            LoginParamters = new LoginParameters();
            RegisterParameters = new CreateUserParameters();
            ForgetPasswordParams = new ForgetPasswordParameters();
            ActivateUserParams = new ActivateUserParameters();
            ResendActivationCodeParams = new ResendActivateUserParameters();
        }



        #endregion

        #region Commands

        public AsyncExtendedCommand  LoginCommand { get; set; }
        public AsyncExtendedCommand RegisterCommand { get; set; }
        public AsyncExtendedCommand ForgetPasswordCommand { get; set; }

        public AsyncExtendedCommand ActivateUserCommand { get; set; }
        public AsyncExtendedCommand ResendActivationCodeCommand { get; set; }
        #endregion

        #region Methods
        public async Task LoginAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                if (_loginParamters.Password == null || _loginParamters.UserName == null)
                {

                    await _dialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.InsertAllDataMessage);
                }
                else {
                    var parameters = new LoginParameters()
                    {
                        UserName = LoginParamters.UserName,
                        Password = LoginParamters.Password
                    };
                    //  var responsee = await _exceptionDataService.GetAllIndependentSchoolsAsync();
                    var response = await _loginDataService.LoginAsync(parameters);
                    if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                    {
                        if (response.Result.IsValid != true)
                        {
                            if (_appSettings.LanguageId == 2)
                                await _dialogManager.ShowMessage("", response.Result.MessageAr);
                            else if (_appSettings.LanguageId == 1)
                                await _dialogManager.ShowMessage("", response.Result.MessageEn);
                        }
                        else
                        {
                            if (_appSettings.CurrentModule == 1)
                            {
                                ViewModelLocator.Locator.BaseReq.UserName = LoginParamters.UserName;
                                ViewModelLocator.Locator.BaseReq.Password = LoginParamters.Password;
                                _appSettings.LoginUserName = LoginParamters.UserName;
                                _appSettings.LoginPassword = LoginParamters.Password;
                                _navigationService.NavigateByPage(typeof(MyRequests));
                            }
                            else if (_appSettings.CurrentModule == 2)
                            {
                                ViewModelLocator.Locator.BaseReq.UserName = LoginParamters.UserName;
                                ViewModelLocator.Locator.BaseReq.Password = LoginParamters.Password;
                                _appSettings.LoginUserName = LoginParamters.UserName;
                                _appSettings.LoginPassword = LoginParamters.Password;
                              var res = await  _preRegistrationDataService.Login(new Taaleem.Models.PreRegistration.Request.LoginParameters { createPersistentCookie = "false", userName = LoginParamters.UserName, password = LoginParamters.Password });
                                if(res.ResponseStatus == ResponseStatus.SuccessWithResult)
                                {
                                    if (res.Result.d == true)
                                    {
                                          _navigationService.NavigateByPage(typeof(RegisterAndTransfer));
                                    }
                                    else
                                    {

                                    }
                                }
                              
                            }
                        }
                    }
                    else
                    {

                        ErrorMessage = _messageResolver.ResultToMessage(response);
                    }
                }
            }
            finally
            {

                IsBusy = false;
            }
        }
        public enum ValidationCode { UnexpectedError = 1, InvalidQatarId = 2, InvalidData = 3, AccessDenied = 4, ExistingUser = 5, UserCreated = 6 }
        public async Task RegisterAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var parameters = new CreateUserParameters()
                {
                    Email = RegisterParameters.Email, GroupName = "Taaleem_Users", Mobile = RegisterParameters.Mobile, OU = "Taaleem", Password = RegisterParameters.Password, QatarId = RegisterParameters.QatarId
                };
                //  var responsee = await _exceptionDataService.GetAllIndependentSchoolsAsync();
                var response = await _loginDataService.CreateUser(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result.ValidationCode != 6)
                    {
                        if (response.Result.ValidationCode == 1)
                        {
                            if (_appSettings.LanguageId == 2)
                                await _dialogManager.ShowMessage("", response.Result.MessageAr);
                            else if (_appSettings.LanguageId == 1)
                                await _dialogManager.ShowMessage("", response.Result.MessageEn);
                        }
                        else if (response.Result.ValidationCode == 2)
                        {
                            if (_appSettings.LanguageId == 2)
                                await _dialogManager.ShowMessage("", response.Result.MessageAr);
                            else if (_appSettings.LanguageId == 1)
                                await _dialogManager.ShowMessage("", response.Result.MessageEn);
                        }
                        else if (response.Result.ValidationCode == 3)
                        {
                            if (_appSettings.LanguageId == 2)
                                await _dialogManager.ShowMessage("", response.Result.MessageAr);
                            else if (_appSettings.LanguageId == 1)
                                await _dialogManager.ShowMessage("", response.Result.MessageEn);
                        }
                        else if (response.Result.ValidationCode == 4)
                        {
                            if (_appSettings.LanguageId == 2)
                                await _dialogManager.ShowMessage("", response.Result.MessageAr);
                            else if (_appSettings.LanguageId == 1)
                                await _dialogManager.ShowMessage("", response.Result.MessageEn);
                        }
                        else if (response.Result.ValidationCode == 5)
                        {
                            if (_appSettings.LanguageId == 2)
                                await _dialogManager.ShowMessage("", response.Result.MessageAr);
                            else if (_appSettings.LanguageId == 1)
                                await _dialogManager.ShowMessage("", response.Result.MessageEn);
                        }
                    }
                    else
                    {
                        if (_appSettings.LanguageId == 2)
                            await _dialogManager.ShowMessage("", response.Result.MessageAr);
                        else if (_appSettings.LanguageId == 1)
                            await _dialogManager.ShowMessage("", response.Result.MessageEn);
                        _navigationService.NavigateByPage(typeof(ActivationAccount));
                    }
                }
                else
                {

                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task ForgetPasswordAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                if (ForgetPasswordParams.UserName == null || ForgetPasswordParams.QatarId == null || ForgetPasswordParams.Mobile == null)
                {

                    await _dialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.InsertAllDataMessage);
                }
                else {
                    var parameters = new ForgetPasswordParameters()
                    {
                        Mobile = ForgetPasswordParams.Mobile,
                        QatarId = ForgetPasswordParams.QatarId,
                        UserName = ForgetPasswordParams.UserName
                    };

                    var response = await _loginDataService.ForgetPassword(parameters);
                    if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                    {
                        if (response.Result.IsValid != true)
                        {
                            if (_appSettings.LanguageId == 2)
                                await _dialogManager.ShowMessage("", response.Result.MessageAr.ToString());
                            else if (_appSettings.LanguageId == 1)
                                await _dialogManager.ShowMessage("", response.Result.MessageEn.ToString());
                        }
                        else
                        {
                            if (_appSettings.LanguageId == 2)
                                await _dialogManager.ShowMessage("", response.Result.MessageAr.ToString());
                            else if (_appSettings.LanguageId == 1)
                                await _dialogManager.ShowMessage("", response.Result.MessageEn.ToString());

                            _navigationService.NavigateByPage(typeof(Views.PublicServices.Login.Login));
                        }
                    }
                    else
                    {

                        ErrorMessage = _messageResolver.ResultToMessage(response);
                    }
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task ActivateUserAcync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                if (ActivateUserParams.ActivationCode == null || ActivateUserParams.username == null)
                {

                    await _dialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.InsertAllDataMessage);
                }
                else {
                    var parameters = new ActivateUserParameters()
                    {
                        ActivationCode = ActivateUserParams.ActivationCode,
                        username = ActivateUserParams.username
                    };

                    var response = await _loginDataService.ActiveUser(parameters);
                    if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                    {
                        if (response.Result.IsValid != true)
                        {
                            if (_appSettings.LanguageId == 2)
                                await _dialogManager.ShowMessage("", response.Result.MessageAr.ToString());
                            else if (_appSettings.LanguageId == 1)
                                await _dialogManager.ShowMessage("", response.Result.MessageEn.ToString());
                        }
                        else
                        {
                            await _dialogManager.ShowMessage("", "تم تفعيل الحساب بنجاح");
                            _navigationService.NavigateByPage<Views.PublicServices.Login.Login>();
                        }
                    }
                    else
                    {

                        ErrorMessage = _messageResolver.ResultToMessage(response);
                    }
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task ResendActivationCodeAsync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                if (ResendActivationCodeParams.Mobile == null || ResendActivationCodeParams.QatarId == null || ResendActivationCodeParams.UserName== null)
                {

                    await _dialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.InsertAllDataMessage);
                }
                else {
                    var parameters = new ResendActivateUserParameters()
                    {
                        Mobile = ResendActivationCodeParams.Mobile,
                        QatarId = ResendActivationCodeParams.QatarId,
                        UserName = ResendActivationCodeParams.UserName
                    };

                    var response = await _loginDataService.ResendActivateUser(parameters);
                    if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                    {
                        if (response.Result.IsValid != true)
                        {
                            if (_appSettings.LanguageId == 2)
                                await _dialogManager.ShowMessage("", response.Result.MessageAr.ToString());
                            else if (_appSettings.LanguageId == 1)
                                await _dialogManager.ShowMessage("", response.Result.MessageEn.ToString());
                        }
                        else
                        {
                            _navigationService.NavigateByPage(typeof(ActivationAccount));
                        }
                    }
                    else
                    {

                        ErrorMessage = _messageResolver.ResultToMessage(response);
                    }
                }
            }
            finally
            {

                IsBusy = false;
            }
        }

        
        #endregion
    }
}