﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Collections.Extended;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees;
using Taaleem.Models.Employees.Request;
using Taaleem.Models.Employees.Response;

namespace Taaleem.ViewModels.Employees
{
    public class LoansViewModel : BindableBase
    {
        #region Fields
        private readonly IEmployeesDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly Resources _resources;
        private readonly IAppSettings _appSettings;

        #endregion

        #region Properties

        private LoanType _selectedLoanType;
        public LoanType SelectedLoanType
        {
            get { return _selectedLoanType; }
            set { SetProperty(ref _selectedLoanType, value); }
        }
        private List<LoanWrapper> _loans;
        public List<LoanWrapper> Loans
        {
            get { return _loans; }
            set { SetProperty(ref _loans, value); }
        }


        private List<EmployeeLoan> loansList;
        public List<EmployeeLoan> LoansList
        {
            get { return loansList; }
            set { SetProperty(ref loansList, value); }
        }


        private ExtendedCollectionView _loansCView;
        public ExtendedCollectionView LoansCView
        {
            get { return _loansCView; }
            set { SetProperty(ref _loansCView, value); }
        }


        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private double width;

        public double Width
        {
            get { return width; }
            set { width = value;
                OnPropertyChanged();
            }
        }
        private Double totalValue ;

        public Double  TotalValue
        {
            get { return totalValue ; }
            set { totalValue = value;
                OnPropertyChanged();
            }
        }
        private Double paidValue;

        public Double PaidValue
        {
            get { return paidValue; }
            set
            {
                paidValue = value;
                OnPropertyChanged();
            }
        }
        private Double remainingValue;

        public Double RemainingValue
        {
            get { return remainingValue; }
            set
            {
                remainingValue = value;
                OnPropertyChanged();
            }
        }

        private int totalPerc;

        public int TotalPerc
        {
            get { return totalPerc; }
            set
            {
                totalPerc = value;
                OnPropertyChanged();
            }
        }
        private double paidPerc;

        public double PaidPerc
        {
            get { return paidPerc; }
            set
            {
                paidPerc = value;
                OnPropertyChanged();
            }
        }
        private double remainingPerc;

        public double RemainingPerc
        {
            get { return remainingPerc; }
            set
            {
                remainingPerc = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Initialization

        public LoansViewModel(Resources resources, IEmployeesDataService employeesDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService)
        {
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _resources = resources;
            LoansCView = new ExtendedCollectionView();
            //LoansCView.SortDescriptions.Add(new SortDescription(@"Status", ListSortDirection.Ascending));
            LoansCView.GroupDescriptions.Add(new PropertyGroupDescription(@"LocalizedStatus"));

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadLoansAsync);
            LoadLoansAsync();
            Width = 20;
        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }

        #endregion

        #region Methods

        public async Task LoadLoansAsync()
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                RequestResponse<List<EmployeeLoan>> response;
               // if (SelectedLoanType.Status == LoanType.All.Status)
              //  {
                    response = await _employeesDataService.GetAllEmployeeLoansAsync(_appSettings.EmployeeId);
               // }
               //// else
               // {
               //     var parameters = new LoansByStatusParameters
               //     {
               //         Id = _appSettings.EmployeeId,
               //         Status = SelectedLoanType.Status
               //     };
               //     response = await _employeesDataService.GetEmployeeLoansByStatusAsync(parameters);
               // }

                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    LoansList = response.Result;
                    //var list = response.Result.Select(item => LoanWrapper.Wrap(item, _appSettings.LanguageId)).ToList();
                    var sortedList = response.Result
                        .Select(item => LoanWrapper.Wrap(item, _appSettings.LanguageId))
                        .OrderBy(item => item.LocalizedStatus).ToList();
                    TotalValue = sortedList.Sum(x => double.Parse(x.Value));
                    PaidValue = sortedList.Sum(x => double.Parse(x.Paid != "لا يوجد"? x.Paid : "0"));
                    RemainingValue = TotalValue - PaidValue;
                    TotalPerc = 100;
                    PaidPerc = (PaidValue * TotalPerc ) / TotalValue;
                    RemainingPerc = totalPerc - paidPerc;
                    LoansCView.Source = sortedList;
                   

                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage();
                    LoansCView.Source = null;
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                    LoansCView.Source = null;
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }
        
        #endregion
    }
}
