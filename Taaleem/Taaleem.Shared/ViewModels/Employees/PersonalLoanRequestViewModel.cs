﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees.MyRequests;
using System.Linq;
using Taaleem.Models.Employees.MyRequests.Response;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.Storage;
using System.IO;
using Windows.UI.Xaml.Media.Imaging;

namespace Taaleem.ViewModels.Employees
{
    public class PersonalLoanRequestViewModel : BindableBase
    {
        #region Fields
        private readonly IMyRequestsDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly Resources _resources;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        private readonly FilePicker _filePicker;
        #endregion

        #region Properties
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        public AsyncExtendedCommand NextCommand { get; set; }
        public AsyncExtendedCommand BackCommand { get; set; }
        public AsyncExtendedCommand SubmitCommand { get; set; }
        public AsyncExtendedCommand LoadCommand { get; set; }
        public AsyncExtendedCommand CalculateLoanCommand { get; set; }
        public AsyncExtendedCommand SelectAttachment { get; set; }

        private bool _page2;
        public bool Page2
        {
            get { return _page2; }
            set { SetProperty(ref _page2, value); }
        }
       
        private string _employeeName;
        public string EmployeeName
        {
            get { return _employeeName; }
            set { SetProperty(ref _employeeName , value); }
        }

        private string _stuffId;
        public string StaffNo
        {
            get { return _stuffId; }
            set { SetProperty(ref _stuffId , value); }
        }

        private string _joinedDate;
        public string JoinedDate
        {
            get { return _joinedDate; }
            set { SetProperty(ref _joinedDate, value); }
        }

        private string _jobName;
        public string JobName
        {
            get { return _jobName; }
            set { SetProperty(ref _jobName ,value); }
        }

        private string _jobTitle;
        public string JobTitle
        {
            get { return _jobTitle; }
            set { SetProperty(ref _jobTitle, value); }
        }

        private string _personalIDNo;
        public string PersonalIDNo
        {
            get { return _personalIDNo; }
            set { SetProperty(ref _personalIDNo, value); }
        }

        private string _nationality;
        public string Nationality
        {
            get { return _nationality; }
            set { SetProperty(ref _nationality, value); }
        }

        private string _basicSalary;
        public string BasicSalary
        {
            get { return _basicSalary; }
            set { SetProperty(ref _basicSalary, value); }
        }
        
        private List<LoanRequestType> _loanTypes;

        public List<LoanRequestType> LoanTypes
        {
            get { return _loanTypes; }
            set { SetProperty(ref _loanTypes, value); }
        }

        //LoanTypeLanguageProperty

        private string _loanTypeLanguageProperty;

        public string LoanTypeLanguageProperty
        {
            get { return _loanTypeLanguageProperty; }
            set { SetProperty(ref _loanTypeLanguageProperty, value); }
        }

        private List<ItemTypeWrapper> _loanPeriods;

        public List<ItemTypeWrapper> LoanPeriods
        {
            get { return _loanPeriods; }
            set { SetProperty(ref _loanPeriods, value); }
        }


        private string _details;

        public string Details
        {
            get { return _details; }
            set { SetProperty(ref _details, value); }
        }

        private string _SelectedLoanType;

        public string SelectedLoanType
        {
            get { return _SelectedLoanType; }
            set { SetProperty(ref _SelectedLoanType, value); }
        }

        private decimal _loanAmount;

        public decimal LoanAmount
        {
            get { return _loanAmount; }
            set { SetProperty(ref _loanAmount, value); }
        }

        private int _loanPeriod;

        public int LoanPeriod
        {
            get { return _loanPeriod; }
            set { SetProperty(ref _loanPeriod, value); }
        }

        private decimal _monthlyInstallment;

        public decimal MonthlyInstallment
        {
            get { return _monthlyInstallment; }
            set { SetProperty(ref _monthlyInstallment, value); }
        }

        public EmployeeByNameResponse EmployeeProfile { get; set; }
        public string ImageBase64 { get; set; }

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set { SetProperty(ref _fileName, value); }
        }

        private string _displayName;
        public string DisplayName
        {
            get { return _displayName; }
            set { SetProperty(ref _displayName, value); }
        }

        private BitmapImage _thumbImage;

        public BitmapImage ThumbImage
        {
            get { return _thumbImage; }
            set { SetProperty(ref _thumbImage, value); }
        }

        #endregion

        public PersonalLoanRequestViewModel(Resources resources, 
            IMyRequestsDataService employeesDataService, 
            IRequestMessageResolver messageResolver, 
            IAppSettings appSettings, 
            IDialogManager dialogManager, 
            INavigationService navigationService,
            FilePicker filePicker)
        {
            _resources = resources;
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _dialogManager = dialogManager;
            _navigationService = navigationService;
            _filePicker = filePicker;

            NextCommand = new AsyncExtendedCommand(BackAsync);
            BackCommand = new AsyncExtendedCommand(NextAsync);
            SubmitCommand = new AsyncExtendedCommand(SubmitAsync);
            LoadCommand = new AsyncExtendedCommand(LoadDataAsync);
            CalculateLoanCommand = new AsyncExtendedCommand(CalculateLoanAsync);
            SelectAttachment = new AsyncExtendedCommand(SelectAttachmentAsync);
        }

        public async Task SelectAttachmentAsync()
        {
            StorageFile storageFile = await _filePicker.PickImageStorageFileAsync();
            if (storageFile != null)
            {
                var stream = await storageFile.GetThumbnailAsync(Windows.Storage.FileProperties.ThumbnailMode.PicturesView);
                var bitmapImage = new Windows.UI.Xaml.Media.Imaging.BitmapImage();
                await bitmapImage.SetSourceAsync(stream);
                ThumbImage = bitmapImage;

                Stream ms = await storageFile.OpenStreamForReadAsync();
                byte[] imageBytes = new byte[(int)ms.Length];
                ms.Read(imageBytes, 0, (int)ms.Length);
                ImageBase64 = Convert.ToBase64String(imageBytes);
                FileName = storageFile.Name;
                DisplayName = storageFile.DisplayName;

            }
        }

        public async Task BackAsync()
        {
            await Task.Delay(100);
            Page2 = false;
        }
        public async Task NextAsync()
        {
            await Task.Delay(100);
            Page2 = true;
        }
        public async Task<BasicResponse> SubmitAsync()
        {
            if (string.IsNullOrEmpty(SelectedLoanType))
            {
                await _dialogManager.ShowMessage(_resources.Error, string.Format(_resources.ErrorMsg_InputIsRequired, _resources.LoanType));
                return null;
            }
            if (LoanAmount == 0)
            {
                await _dialogManager.ShowMessage(_resources.Error, string.Format(_resources.ErrorMsg_InputIsRequired, _resources.LoanAmount));
                return null;
            }
            if (LoanPeriod == 0)
            {
                await _dialogManager.ShowMessage(_resources.Error, string.Format(_resources.ErrorMsg_InputIsRequired, _resources.PaymentPeriodMonths));
                return null;
            }

            IsBusy = true;
            SubmitCommand.CanExecute = false;
            BasicResponse result = null;
            var isEligable = await _employeesDataService.IsEligibleforLoan(EmployeeProfile, SelectedLoanType, LoanAmount, null);
            if (isEligable)
            {
                result = await _employeesDataService.SubmitNewPersonalLoanRequest(
                    new Models.Employees.MyRequests.Request.PersonalLoanRequest
                    {
                        PaymentPeriod = LoanPeriod,
                        LoanAmount = LoanAmount,
                        MonthlyInstallment = MonthlyInstallment,
                        RequiredLoanID = SelectedLoanType,
                        RequiredLoanType = LoanTypes.First(s => s.code == SelectedLoanType).nameEn,
                        RequiredLoanTypeAr = LoanTypes.First(s => s.code == SelectedLoanType).nameAr,

                        DateofJoin = EmployeeProfile.joiningDate,
                        DirectManager = EmployeeProfile.directManager?.nameEn,
                        EmpName = EmployeeProfile.nameEn,
                        EmpNameAr = EmployeeProfile.nameAr,
                        Grade = EmployeeProfile.financialGrade,
                        InstituteDepartmentUnit = EmployeeProfile.departmentEn,
                        InstituteDepartmentUnitAr = EmployeeProfile.departmentAr,
                        JobNo = EmployeeProfile.jobID,
                        JobTitle = EmployeeProfile.jobTitleEn,
                        JobTitleAr = EmployeeProfile.jobTitleAr,
                        Nationality = EmployeeProfile.nationalityEn,
                        NationalityAr = EmployeeProfile.nationalityAr,
                        QatarID = EmployeeProfile.qatariID,
                        Salary = int.Parse(EmployeeProfile.basicSalary),
                        Title = EmployeeProfile.jobTitleEn,
                        Attachments = string.IsNullOrEmpty(ImageBase64) ? null: new List<Models.Attachment>
                        {
                            new Models.Attachment
                            {
                                FileName = FileName,
                                DisplayName = DisplayName,
                                Data = ImageBase64
                            }
                        }

                    }, null);
                if (result != null)
                {
                    await _dialogManager.ShowMessage(_resources.Submit, _appSettings.LanguageId == 1 ? result.ResponseNameEn : result.ResponseNameAr);
                }
            }
            else
            {
                //ErrorMsg_NotEligableLoan
                await _dialogManager.ShowMessage(_resources.Error,_resources.ErrorMsg_NotEligableLoan);
            }
            SubmitCommand.CanExecute = true;
            IsBusy = false;
            
            return result;
        }
        public async Task LoadDataAsync()
        {
            IsBusy = true;

            EmployeeByNameResponse empResponse = await _employeesDataService.GetEmployeeProfileByQatariId(_appSettings.EmployeeId, null);
            EmployeeProfile = empResponse;
            if (_appSettings.LanguageId == 1) // english
            {
                EmployeeName = empResponse.nameEn;
                JobName = empResponse.departmentEn;
                JobTitle = empResponse.jobTitleEn;
                Nationality = empResponse.nationalityEn;
            }
            else
            {
                EmployeeName = empResponse.nameAr;
                JobName = empResponse.departmentAr;
                JobTitle = empResponse.jobTitleAr;
                Nationality = empResponse.nationalityAr;
            }

            BasicSalary = empResponse.basicSalary;
            StaffNo = empResponse.jobID;
            JoinedDate = empResponse.joiningDate;
            PersonalIDNo = empResponse.qatariID;

            LoanTypeLanguageProperty = _appSettings.LanguageId == 1 ? "nameEn" : "nameAr";
            LoanTypes = await _employeesDataService.GetLoanRequestTypes(EmployeeProfile.isQatari);

            var periods = await _employeesDataService.GetLoanPeriodsList();
            LoanPeriods = periods.Select(s => ItemTypeWrapper.Wrap(s, _appSettings.LanguageId)).ToList();

            IsBusy = false;
        }


        public async Task CalculateLoanAsync()
        {
            IsBusy = true;
            MonthlyInstallment = await _employeesDataService.GetMonthlyInstallment(LoanAmount, LoanPeriod, null);
            IsBusy = false;
        }

        public void ClearAttachments()
        {
            ThumbImage = null;
            ImageBase64 = FileName = DisplayName = null;
        }
    }
}
