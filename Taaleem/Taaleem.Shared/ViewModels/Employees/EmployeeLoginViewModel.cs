﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children;
using Taaleem.Models.Employees.Request;
using Taaleem.Views.Employee;

namespace Taaleem.ViewModels.Employees
{
    public class EmployeeLoginViewModel : BindableBase
    {
        #region Fields
        private readonly Resources _resources;
        private readonly IEmployeesDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly IAppSettings _appSettings;
        private readonly INavigationService _navigationService;
        #endregion

        #region Properties
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private String _userName;
        public String UserName
        {
            get { return _userName; }
            set
            {
                if (SetProperty(ref _userName, value))
                {
                    LoginCommand.CanExecute = !String.IsNullOrWhiteSpace(UserName) && !String.IsNullOrWhiteSpace(Password);
                }
                OnPropertyChanged();
            }
        }

        private String _password;
        public String Password
        {
            get { return _password; }
            set
            {
                if (SetProperty(ref _password, value))
                {
                    LoginCommand.CanExecute = !String.IsNullOrWhiteSpace(UserName) && !String.IsNullOrWhiteSpace(Password);
                }
                OnPropertyChanged();
            }
        }

        private string employeeName;

        public string EmployeeName
        {
            get { return _appSettings.EmployeeCredential?.UserName; }
            set { employeeName = value;
                OnPropertyChanged();
            }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private List<EmployeeMenuItem> employeeMenu;

        public List<EmployeeMenuItem> EmployeeMenu
        {
            get { return employeeMenu; }
            set { SetProperty(ref employeeMenu, value); }
        }

        #endregion

        #region Initialization

        public EmployeeLoginViewModel(Resources resources, IEmployeesDataService employeesDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService)
        {
            _resources = resources;
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            employeeMenu = new List<EmployeeMenuItem>();
            //Initialize Commands
            LoginCommand = new AsyncExtendedCommand(LoginAsync);
            LogoutCommand = new ExtendedCommand(LogoutMethod);
            ItemClicked = new ExtendedCommand<EmployeeMenuItem>(NavigateTo);
            LoadEmployeeMenuItems();

#if DEBUG
           // UserName = "emp1";
           // Password = "123456";

#endif
        }


        private void NavigateTo(EmployeeMenuItem obj)
        {
            if (obj.Name == _resources.Salary)
            {
                _navigationService.NavigateByPage<Salaries>();
            }
            if (obj.Name == _resources.PersonalLoan)
            {
                _navigationService.NavigateByPage<PersonalLoanRequest>();
            }
            if (obj.Name == _resources.Evaluation)
            {
                _navigationService.NavigateByPage<Evaluation>();
            }
            if (obj.Name == _resources.Vacations)
            {
                _navigationService.NavigateByPage<Vacations>();
            }
            if (obj.Name == _resources.ExitPermitRequest)
            {
                _navigationService.NavigateByPage<ExitPermitRequest>();
            }
            if (obj.Name == _resources.DocumentRequest)
            {
                _navigationService.NavigateByPage<DocumentRequest>();
            }
           
            if (obj.Name == _resources.OvertimeRequest)
            {
                _navigationService.NavigateByPage<OvertimeRequest>();
            }
            if (obj.Name == _resources.MyRequests)
            {
                _navigationService.NavigateByPage<Views.Employee.MyRequests>();
            }
            if (obj.Name == _resources.LeaveRequest)
            {
                _navigationService.NavigateByPage<LeaveRequest>();
            }
            if(obj.Name == _resources.LeaveReturn)
            {
                _navigationService.NavigateByPage<LeaveReturnRequest>();
            }
        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoginCommand { get; set; }
        public ExtendedCommand<EmployeeMenuItem> ItemClicked { get; set; }
        public ExtendedCommand LogoutCommand { get; set; }
        #endregion

        #region Methods


        private void LogoutMethod()
        {
           
            _appSettings.RemoveEmployeeCredential();
            _appSettings.SetEmployeeLoginResponseAsync(null);
            UserName = "";
            Password = "";
            _navigationService.NavigateByPage(typeof(Home));
        }
        public void LoadEmployeeMenuItems()
        {
            employeeMenu.Clear();
            employeeMenu.Add(new EmployeeMenuItem { Image = "ms-appx:///Assets/Employee/Menu/emplyees_evaluation_ic@3x.png", Name = _resources.Evaluation });
            employeeMenu.Add(new EmployeeMenuItem { Image = "ms-appx:///Assets/Employee/Menu/emplyees_salary_ic@3x.png", Name = _resources.Salary });
            employeeMenu.Add(new EmployeeMenuItem { Image = "ms-appx:///Assets/Employee/Menu/emplyees_plane_ic@3x.png", Name = _resources.Vacations });
            employeeMenu.Add(new EmployeeMenuItem { Image = "ms-appx:///Assets/Employee/Menu/employee_myrequests_ic@3x.png", Name = _resources.MyRequests });
            employeeMenu.Add(new EmployeeMenuItem { Image = "ms-appx:///Assets/Employee/Menu/employee_doc_ic@3x.png", Name = _resources.DocumentRequest });
            employeeMenu.Add(new EmployeeMenuItem { Image = "ms-appx:///Assets/Employee/Menu/emplyees_loan_ic@3x.png", Name = _resources.PersonalLoan });
            employeeMenu.Add(new EmployeeMenuItem { Image = "ms-appx:///Assets/Employee/Menu/emplyees_plane_ic@3x.png", Name = _resources.LeaveRequest });
            employeeMenu.Add(new EmployeeMenuItem { Image = "ms-appx:///Assets/Employee/Menu/emplyees_leave_return_ic@3x.png", Name = _resources.LeaveReturn });
            employeeMenu.Add(new EmployeeMenuItem { Image = "ms-appx:///Assets/Employee/Menu/employee_exit_ic@3x.png", Name = _resources.ExitPermitRequest });


            //employeeMenu.Add(new EmployeeMenuItem { Image = "ms-appx:///Assets/Employee/Menu/employee_overtime_ic@3x.png", Name = _resources.OvertimeRequest });

        }
        public async Task LoginAsync()
        {
            LoginCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var parameters = new EmpolyeeLoginBody { UserName = UserName, Password = Password };
                var response = await _employeesDataService.LoginAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result.Success)
                    {
                        _appSettings.SetEmployeeCredential(UserName, Password);
                        _appSettings.EmployeeId = response.Result.Id;
                      await  _appSettings.SetEmployeeLoginResponseAsync(response.Result);
                        _navigationService.NavigateByPage<EmployeeMenuPage>();
                        ViewModelLocator.Locator.DialogManager.CloseDialog();
                       // ViewModelLocator.Locator.MainMenuVM.CurrentMenuItem.Name = "خدمات الموظف";
                        ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = new Models.MainMenuItem { Name = "خدمات الموظف", Image = "" };

                        //  _navigationService.GoBack();
                    }
                    else
                    {
                        ErrorMessage = new RequestMessage(_resources.FailedToLogin);
                    }
                }
                else if((response.ResponseStatus == ResponseStatus.HttpError && response.StatusCode == System.Net.HttpStatusCode.BadRequest))
                    {
                    ErrorMessage = new RequestMessage(_resources.FailedToLogin);
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoginCommand.CanExecute = true;
                IsBusy = false;
            }
        }
        #endregion
    }
}