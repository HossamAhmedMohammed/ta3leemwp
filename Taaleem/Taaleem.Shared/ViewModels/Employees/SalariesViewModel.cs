﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees;
using Taaleem.Models.Employees.Request;

namespace Taaleem.ViewModels.Employees
{
    public class SalariesViewModel : BindableBase
    {
        #region Fields
        private readonly IEmployeesDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly Resources _resources;
        private readonly IAppSettings _appSettings;
        #endregion

        #region Properties
        private DateTime _currentDate;
        /// <summary>
        /// This must be sunday always
        /// </summary>
        public DateTime CurrentDate
        {
            get { return _currentDate; }
            set { SetProperty(ref _currentDate, value);
                OnPropertyChanged();
            }
        }

       
        private DateTimeOffset _salaryDate;
        public DateTimeOffset SalaryDate
        {
            get { return _salaryDate; }
            set { SetProperty(ref _salaryDate, value); }
        }

        private List<SalaryDetailWrapper> _salaryDetails;
        public List<SalaryDetailWrapper> SalaryDetails
        {
            get { return _salaryDetails; }
            set { SetProperty(ref _salaryDetails, value); }
        }

        private SalaryDetailWrapper _totalSalary;
        public SalaryDetailWrapper TotalSalary
        {
            get { return _totalSalary; }
            set { SetProperty(ref _totalSalary, value); }
        }


        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private bool showThisMonthButtonVisibilty;

        public bool ShowThisMonthButtonVisibility
        {
            get { return showThisMonthButtonVisibilty; }
            set { showThisMonthButtonVisibilty = value;
                OnPropertyChanged();
            }
        }
        


        #endregion

        #region Initialization

        public SalariesViewModel(Resources resources, IEmployeesDataService employeesDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _resources = resources;

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadSalaryDetailsAsync);
            NextMonthCommand = new AsyncExtendedCommand(LoadNextMonth);
            BreviousMonthCommand = new AsyncExtendedCommand(LoadBreviousMonth);
            CurrentDate = DateTime.Now.AddMonths(-1);
            ShowThisMonthButtonVisibility = true;
            LoadSalaryDetailsAsync();

        }

     



        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        public AsyncExtendedCommand NextMonthCommand { get; set; }
        public AsyncExtendedCommand BreviousMonthCommand { get; set; }
        #endregion

        #region Methods

        //public async Task<bool> ShowThisMonthButtonVisibility()
        //{
        //    await Task.Delay(3000);
        //    if (DateTime.Now.Month ==  CurrentDate.Month)
        //    {
        //        return false;

        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}
        public async void AssignCurrentMonthDates()
        {
           CurrentDate = DateTime.Now;
           
            await LoadSalaryDetailsAsync();
            ShowThisMonthButtonVisibility = false;
        }
        private async Task LoadBreviousMonth()
        {
           CurrentDate = CurrentDate.AddMonths(-1);
            await LoadSalaryDetailsAsync();
            ShowThisMonthButtonVisibility = true;
        }
        private async Task LoadNextMonth()
        {
           CurrentDate= CurrentDate.AddMonths(1);
            int x = CurrentDate.Date.CompareTo(DateTime.Now.Date);
            if (CurrentDate.Date.CompareTo(DateTime.Now.Date) != 0)
                ShowThisMonthButtonVisibility = true;
            else
                ShowThisMonthButtonVisibility = false;
            await LoadSalaryDetailsAsync();
           
        }
        public async Task LoadSalaryDetailsAsync()
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var parameters = new SalaryParameters
                {
                    Id = _appSettings.EmployeeId,
                    Year = CurrentDate.Year.ToString(),
                    Month = CurrentDate.Month.ToString()
                };

                var response = await _employeesDataService.GetSalariesAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    var list = response.Result.Select(item => SalaryDetailWrapper.Wrap(item, _appSettings.LanguageId)).ToList();
                    TotalSalary = list[list.Count - 1];
                    list.RemoveAt(list.Count - 1);
                    SalaryDetails = list;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage();
                    SalaryDetails = null;
                }
                else
                {
                    SalaryDetails = null;
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }

        #endregion
    }
}
