﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Collections.Extended;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees;

namespace Taaleem.ViewModels.Employees
{
    public class VacationsViewModel : BindableBase
    {
        #region Fields
        private readonly IEmployeesDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly Resources _resources;
        private readonly IAppSettings _appSettings;

        #endregion

        #region Properties

        private List<EmployeeVacationWrapper> _vacations;
        public List<EmployeeVacationWrapper> Vacations
        {
            get { return _vacations; }
            set { SetProperty(ref _vacations, value); }
        }

        private ExtendedCollectionView _vacationsCView;
        public ExtendedCollectionView VacationsCView
        {
            get { return _vacationsCView; }
            set { SetProperty(ref _vacationsCView, value); }
        }
        private readonly List<VacationGroupType> _vacationGroupTypes = VacationGroupType.GroupTypes;
        public List<VacationGroupType> VacationGroupTypes
        {
            get { return _vacationGroupTypes; }
        }

        private VacationGroupType _currentVacationType;

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }


        #endregion

        #region Initialization

        public VacationsViewModel(Resources resources, IEmployeesDataService employeesDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService)
        {
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _resources = resources;
            VacationsCView = new ExtendedCollectionView();
            ChangeGouping(VacationGroupType.Vacation);

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadVacationsAsync);
        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }

        #endregion

        #region Methods

        public void ChangeGouping(VacationGroupType type)
        {
            _currentVacationType = type;
            if (Vacations == null) return;
            using (VacationsCView.DeferRefresh())
            {
                VacationsCView.GroupDescriptions.Clear();
                VacationsCView.GroupDescriptions.Add(new PropertyGroupDescription(type.PropertyName));

                var sortedList = Vacations.OrderBy(item => item.GetPropertyValue(type.PropertyName)).ToList();
                VacationsCView.Source = sortedList;
            }
        }

        public async Task LoadVacationsAsync()
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            Vacations = null;
            VacationsCView.Source = null;
            try
            {
                var response = await _employeesDataService.GetEmployeeVacationsAsync(_appSettings.EmployeeId);

                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    Vacations = response.Result.Select(item => EmployeeVacationWrapper.Wrap(item, _appSettings.LanguageId)).ToList();
                    ChangeGouping(_currentVacationType);
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage();
                    VacationsCView.Source = null;
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                    VacationsCView.Source = null;
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }

        #endregion
    }


}
