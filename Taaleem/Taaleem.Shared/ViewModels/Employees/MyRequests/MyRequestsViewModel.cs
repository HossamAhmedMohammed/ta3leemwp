﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees.MyRequests;
using System.Linq;
using System.Net;
using Taaleem.Models.Employees.MyRequests.Response;

namespace Taaleem.ViewModels.Employees
{
    public class MyRequestsViewModel : BindableBase
    {
        #region Fields
        private readonly IMyRequestsDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly Resources _resources;
        private readonly IAppSettings _appSettings;
        #endregion

        #region Properties     

        private List<RequestTypesWrapper> _requestTypes;
        public List<RequestTypesWrapper> RequestTypes
        {
            get { return _requestTypes; }
            set { SetProperty(ref _requestTypes, value); }
        }

        List<SectionsListWrapper> _data;
        public List<SectionsListWrapper> Data
        {
            get
            {
                return _data;
            }
            set
            {
                SetProperty(ref _data, value);
            }
        }


        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private int _page;

        public int PageIndex
        {
            get { return _page; }
            set { SetProperty(ref _page, value); }
        }

        private int _qatariId;

        public int QatariId
        {
            get { return _qatariId; }
            set { SetProperty(ref _qatariId, value); }
        }

        #endregion

        #region Initialization

        public MyRequestsViewModel(Resources resources, IMyRequestsDataService employeesDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _resources = resources;

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadRequestTypesAsync);
            LeaveRequestCommand = new AsyncExtendedCommand(LeaveRequestAsync);
            LeaveReturnCommand = new AsyncExtendedCommand(LeaveReturnRequestAsync);
            DocumentRequestCommand = new AsyncExtendedCommand(DocumentRequestAsync);
            ExitPermitCommand = new AsyncExtendedCommand(ExitPermitRequestAsync);
            OverTimeCommand = new AsyncExtendedCommand(OverTimeRequestAsync);
            PersonalLoanCommand = new AsyncExtendedCommand(PersonalLoanRequestAsync);
        }

        #endregion


        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }

        public AsyncExtendedCommand LeaveRequestCommand { get; set; }
        public AsyncExtendedCommand LeaveReturnCommand { get; set; }
        public AsyncExtendedCommand ExitPermitCommand { get; set; }
        public AsyncExtendedCommand PersonalLoanCommand { get; set; }
        public AsyncExtendedCommand DocumentRequestCommand { get; set; }
        public AsyncExtendedCommand OverTimeCommand { get; set; }
                
        #endregion


        #region Methods

        public async Task LoadRequestTypesAsync()
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var response = await _employeesDataService.GetRequestTypes();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    var list = response.Result.Select(item => RequestTypesWrapper.Wrap(item, _appSettings.LanguageId))
                        .ToList();
                    // not supported for now.
                    list.RemoveAll(r => r.Id == (int)MyRequestTypeEnum.OverTime);

                    list.Insert(0, new RequestTypesWrapper { Id = 0, Name = _resources.MyRequests });
                    RequestTypes = list;

                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage(); ;
                    RequestTypes = null;
                }
                else
                {
                    RequestTypes = null;
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }

        public async Task LeaveRequestAsync()
        {
            LeaveRequestCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                Data = null;

                var response = await _employeesDataService.GetLeaveRequests();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    var list = response.Result.Requests.Select(item => SectionsListWrapper.Wrap(item, _appSettings.LanguageId, MyRequestTypeEnum.LeaveRequest))
                        .ToList();
                    Data = list;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage(); 
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LeaveRequestCommand.CanExecute = true;
                IsBusy = false;
            }
        }
        public async Task LeaveReturnRequestAsync()
        {
            LeaveReturnCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                
                Data = null;

                var response = await _employeesDataService.GetLeaveReturnRequests();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    var list = response.Result.Requests.Select(item => SectionsListWrapper.Wrap(item, _appSettings.LanguageId, MyRequestTypeEnum.LeaveReturn))
                        .ToList();
                    Data = list;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage();
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LeaveRequestCommand.CanExecute = true;
                IsBusy = false;
            }
        }
        public async Task ExitPermitRequestAsync()
        {
            ExitPermitCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            Data = null;
            try
            {

                var response = await _employeesDataService.GetExitPermitRequests();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    var list = response.Result.Requests.Select(item => SectionsListWrapper.Wrap(item, _appSettings.LanguageId, MyRequestTypeEnum.ExitPermit))
                        .ToList();
                    Data = list;

                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage(); 
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                ExitPermitCommand.CanExecute = true;
                IsBusy = false;
            }
        }
        public async Task PersonalLoanRequestAsync()
        {
            PersonalLoanCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            Data = null;
            try
            {

                var response = await _employeesDataService.GetPersonalLoanRequests(_appSettings.EmployeeId);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    var list = response.Result.Requests.Select(item => SectionsListWrapper.Wrap(item, _appSettings.LanguageId, MyRequestTypeEnum.PersonalLoan))
                        .ToList();
                    Data = list;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage(); 
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                PersonalLoanCommand.CanExecute = true;
                IsBusy = false;
            }
        }
        public async Task DocumentRequestAsync()
        {
            DocumentRequestCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            Data = null;
            try
            {

                var response = await _employeesDataService.GetDocumentRequests(_appSettings.EmployeeId);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    var list = response.Result.Requests.Select(item => SectionsListWrapper.Wrap(item, _appSettings.LanguageId, MyRequestTypeEnum.DocumentRequest))
                        .ToList();
                    Data = list;
                    
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage(); 
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                DocumentRequestCommand.CanExecute = true;
                IsBusy = false;
            }
        }
        public async Task OverTimeRequestAsync()
        {
            OverTimeCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var response = await _employeesDataService.GetRequestTypes();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    var list = response.Result.Select(item => RequestTypesWrapper.Wrap(item, _appSettings.LanguageId))
                        .ToList();
                    list.Insert(0, new RequestTypesWrapper { Id = 0, Name = _resources.MyRequests });
                    RequestTypes = list;

                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage(); ;
                    RequestTypes = null;
                }
                else
                {
                    RequestTypes = null;
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                OverTimeCommand.CanExecute = true;
                IsBusy = false;
            }
        }
        #endregion
    }

    public enum MyRequestTypeEnum
    {
        MyRequests,
        LeaveRequest,
        LeaveReturn,
        ExitPermit,
        PersonalLoan,
        DocumentRequest,
        OverTime
    }

}
