﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees.MyRequests;
using System.Linq;
using Taaleem.Models.Employees.MyRequests.Response;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using System.IO;

namespace Taaleem.ViewModels.Employees.MyRequests
{
    public class LeaveReturnRequestViewModel : BindableBase
    {
        #region Fields
        private readonly IMyRequestsDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly Resources _resources;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        private readonly FilePicker _filePicker;
        #endregion

        #region Properties
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        public AsyncExtendedCommand NextCommand { get; set; }
        public AsyncExtendedCommand BackCommand { get; set; }
        public AsyncExtendedCommand SubmitCommand { get; set; }
        public AsyncExtendedCommand LoadCommand { get; set; }
        public AsyncExtendedCommand SelectAttachment { get; set; }

        private bool _page1;

        public bool Page1
        {
            get { return _page1; }
            set { SetProperty(ref _page1, value); }
        }

        private bool _page2;

        public bool Page2
        {
            get { return _page2; }
            set { SetProperty(ref _page2, value); }
        }

        private bool _page3;

        public bool Page3
        {
            get { return _page3; }
            set { SetProperty(ref _page3, value); }
        }

        private string _employeeName;

        public string EmployeeName
        {
            get { return _employeeName; }
            set { SetProperty(ref _employeeName, value); }
        }

        private string _JobId;

        public string StaffNo
        {
            get { return _JobId; }
            set { SetProperty(ref _JobId, value); }
        }

        private string _joinedDate;

        public string JoinedDate
        {
            get { return _joinedDate; }
            set { SetProperty(ref _joinedDate, value); }
        }

        private string _deptName;

        public string DeptOfficeInstitute
        {
            get { return _deptName; }
            set { SetProperty(ref _deptName, value); }
        }

        private string _jobTitle;

        public string JobTitle
        {
            get { return _jobTitle; }
            set { SetProperty(ref _jobTitle, value); }
        }


        private List<EmployeeApprovedLeave> _employeeApprovedLeaves;

        public List<EmployeeApprovedLeave> EmployeeApprovedLeaves
        {
            get { return _employeeApprovedLeaves; }
            set { SetProperty(ref _employeeApprovedLeaves, value); }
        }


        private List<LeaveType> _documentRequestTypes;

        public List<LeaveType> LeaveRequestTypes
        {
            get { return _documentRequestTypes; }
            set { SetProperty(ref _documentRequestTypes, value); }
        }

        private string _EmployeeApprovedLeavesDisplayMember;

        public string EmployeeApprovedLeavesDisplayMember
        {
            get { return _EmployeeApprovedLeavesDisplayMember; }
            set { SetProperty(ref _EmployeeApprovedLeavesDisplayMember, value); }
        }

        private string _leaveRequestTypesDisplayMember;

        public string LeaveRequestTypesDisplayMember
        {
            get { return _leaveRequestTypesDisplayMember; }
            set { SetProperty(ref _leaveRequestTypesDisplayMember, value); }
        }

        private LeaveType _SelectedDocumentRequestType;

        public LeaveType SelectedDocumentRequestType
        {
            get { return _SelectedDocumentRequestType; }
            set
            {
                SetProperty(ref _SelectedDocumentRequestType, value);
                if (value != null)
                {
                    IncreaseLeaveDaysRequiresAttachment = value.requiredAttachment;
                }
            }
        }

        private bool _increaseLeaveDaysRequiresAttachment;

        public bool IncreaseLeaveDaysRequiresAttachment
        {
            get { return _increaseLeaveDaysRequiresAttachment; }
            set { SetProperty(ref _increaseLeaveDaysRequiresAttachment, value); }
        }


        private EmployeeApprovedLeave _SelectedEmployeeApprovedLeave;

        public EmployeeApprovedLeave SelectedEmployeeApprovedLeave
        {
            get { return _SelectedEmployeeApprovedLeave; }
            set
            {
                if (value != null)
                {
                    NewEndDateOfLeaving = value.endDate;
                }
                SetProperty(ref _SelectedEmployeeApprovedLeave, value);

            }
        }

        private string _nationality;
        public string Nationality
        {
            get { return _nationality; }
            set { SetProperty(ref _nationality, value); }
        }


        private string _unitDept;

        public string UnitDept
        {
            get { return _unitDept; }
            set { SetProperty(ref _unitDept, value); }
        }


        public string ImageBase64 { get; set; }

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set { SetProperty(ref _fileName, value); }
        }

        private string _displayName;
        public string DisplayName
        {
            get { return _displayName; }
            set { SetProperty(ref _displayName, value); }
        }

        private BitmapImage _thumbImage;

        public BitmapImage ThumbImage
        {
            get { return _thumbImage; }
            set { SetProperty(ref _thumbImage, value); }
        }

        private bool _changeReturnDate;

        public bool ChangeReturnDate
        {
            get { return _changeReturnDate; }
            set
            {
                if (value)
                {
                    Reset();
                }
                SetProperty(ref _changeReturnDate, value);

            }
        }

        private bool _cutLeave;

        public bool CutLeave
        {
            get { return _cutLeave; }
            set { SetProperty(ref _cutLeave, value); }
        }


        public int CutDays
        {
            get
            {
                if (SelectedEmployeeApprovedLeave == null || !CutLeave)
                    return 0;
                try
                {
                    int newTotalDays = (int)DateTime.Now.Subtract(SelectedEmployeeApprovedLeave.startDate).TotalDays;
                    return int.Parse(SelectedEmployeeApprovedLeave.numberOfDays) - newTotalDays;
                }
                catch (Exception ex)
                {
                    // sholud log the problem here.
                    return 0;
                }
            }

            set { }
        }

        private bool _increaseLeaveDays;

        public bool IncreaseLeaveDays
        {
            get { return _increaseLeaveDays; }
            set { SetProperty(ref _increaseLeaveDays, value); }
        }

        private DateTime? _newEndDateOfLeaving;

        public DateTime? NewEndDateOfLeaving
        {
            get { return _newEndDateOfLeaving; }
            set { SetProperty(ref _newEndDateOfLeaving, value); }
        }


        private bool _providingSickLeave;

        public bool ProvidingSickLeave
        {
            get { return _providingSickLeave; }
            set
            {
                if (value)
                {
                    Reset();
                    FirstDateOfSickLeave = SelectedEmployeeApprovedLeave.startDate;
                    LastDateOfSickLeave = SelectedEmployeeApprovedLeave.endDate;
                }
                SetProperty(ref _providingSickLeave, value);

            }
        }

        private DateTime _firstDateOfSickLeave;

        public DateTime FirstDateOfSickLeave
        {
            get { return _firstDateOfSickLeave; }
            set { SetProperty(ref _firstDateOfSickLeave, value); }
        }

        private DateTime _LastDateOfSickLeave;

        public DateTime LastDateOfSickLeave
        {
            get { return _LastDateOfSickLeave; }
            set { SetProperty(ref _LastDateOfSickLeave, value); }
        }


        private bool _providingLenientVacation;

        public bool ProvidingALenientVacation
        {
            get { return _providingLenientVacation; }
            set
            {
                if (value)
                {
                    Reset();
                    FirstDateOfLenientVacation = SelectedEmployeeApprovedLeave.startDate;
                    LastDateOfLenientVacation = SelectedEmployeeApprovedLeave.endDate;
                }
                SetProperty(ref _providingLenientVacation, value);
            }
        }

        private DateTime _firstDateOfLenientVacation;

        public DateTime FirstDateOfLenientVacation
        {
            get { return _firstDateOfLenientVacation; }
            set { SetProperty(ref _firstDateOfLenientVacation, value); }
        }

        private DateTime _LastDateOfLenientVacation;

        public DateTime LastDateOfLenientVacation
        {
            get { return _LastDateOfLenientVacation; }
            set { SetProperty(ref _LastDateOfLenientVacation, value); }
        }

        private bool _submitRequestOnBehalfToIsVisible;

        public bool SubmitRequestOnBehalfToIsVisible
        {
            get { return _submitRequestOnBehalfToIsVisible; }
            set
            {
                SetProperty(ref _submitRequestOnBehalfToIsVisible, value);
            }
        }

        //SubmitRequestOnBehalfTo
        private bool _submitRequestOnBehalfTo;

        public bool SubmitRequestOnBehalfTo
        {
            get { return _submitRequestOnBehalfTo; }
            set
            {
                SetProperty(ref _submitRequestOnBehalfTo, value);
                if(!value)
                {
                    LoadEmployeeData(LoggedInEmployeeProfile);
                }                    
            }
        }

        //SearchOnBehalfQuery
        private string _searchOnBehalfQuery;

        public string SearchOnBehalfQuery
        {
            get { return _searchOnBehalfQuery; }
            set
            {
                SetProperty(ref _searchOnBehalfQuery, value);
            }
        }


        void Reset()
        {
            ChangeReturnDate = false;
            CutLeave = false;
            CutDays = 0;

            IncreaseLeaveDays = false;
            NewEndDateOfLeaving = null;
            SelectedDocumentRequestType = null;

            ProvidingSickLeave = false;
            FirstDateOfSickLeave = LastDateOfSickLeave = DateTime.MinValue;

            ProvidingALenientVacation = false;
            FirstDateOfLenientVacation = LastDateOfLenientVacation = DateTime.MinValue;

            ImageBase64 = FileName = DisplayName = null;
            FirstDateOfSickLeave = LastDateOfSickLeave = DateTime.MinValue;
            if(Page1)
            {
                EmployeeApprovedLeaves = null;
                SelectedEmployeeApprovedLeave = null;
                LeaveRequestTypes = null;
                SelectedDocumentRequestType = null;
            }
        }

        public EmployeeByNameResponse LoggedInEmployeeProfile { get; private set; }
        public EmployeeByNameResponse EmployeeProfile { get; private set; }
        public EmployeeByNameResponse OnBehalfEmployeeProfile { get; private set; }


        #endregion

        public LeaveReturnRequestViewModel(Resources resources,
            IMyRequestsDataService employeesDataService,
            IRequestMessageResolver messageResolver,
            IAppSettings appSettings,
            IDialogManager dialogManager,
            INavigationService navigationService,
            FilePicker filePicker)
        {
            _resources = resources;
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _dialogManager = dialogManager;
            _navigationService = navigationService;
            _filePicker = filePicker;

            NextCommand = new AsyncExtendedCommand(BackAsync);
            BackCommand = new AsyncExtendedCommand(NextAsync);
            SubmitCommand = new AsyncExtendedCommand(SubmitAsync);
            LoadCommand = new AsyncExtendedCommand(LoadDataAsync);
            SelectAttachment = new AsyncExtendedCommand(SelectAttachmentAsync);

            Page1 = true;
            appSettings.LanguageId = 2;
        }

        public async Task SelectAttachmentAsync()
        {
            StorageFile storageFile = await _filePicker.PickImageStorageFileAsync();
            if (storageFile != null)
            {
                var stream = await storageFile.GetThumbnailAsync(Windows.Storage.FileProperties.ThumbnailMode.PicturesView);
                var bitmapImage = new Windows.UI.Xaml.Media.Imaging.BitmapImage();
                await bitmapImage.SetSourceAsync(stream);
                ThumbImage = bitmapImage;

                Stream ms = await storageFile.OpenStreamForReadAsync();
                byte[] imageBytes = new byte[(int)ms.Length];
                ms.Read(imageBytes, 0, (int)ms.Length);
                ImageBase64 = Convert.ToBase64String(imageBytes);
                FileName = storageFile.Name;
                DisplayName = storageFile.DisplayName;
            }
        }

        public async Task BackAsync()
        {
            await Task.Delay(100);
            if (Page3)
            {
                Page3 = false;
                Page2 = true;
            }
            else if (Page2)
            {
                Page2 = false;
                Page1 = true;
            }
        }
        public async Task NextAsync()
        {
            await Task.Delay(100);
            if (Page1)
            {
                if (EmployeeApprovedLeaves == null)
                {
                    IsBusy = true;
                    EmployeeApprovedLeaves = await _employeesDataService.GetEmployeeApprovedLeaves(EmployeeProfile.jobID, null);
                    IsBusy = false;
                }

                if (EmployeeApprovedLeaves.Count == 0)
                {
                    await _dialogManager.ShowMessage(_resources.Error, _resources.ErrorMsg_NoApprovedLeaves);
                    return;
                }
                Page1 = false;
                Page2 = true;
            }
            else if (Page2)
            {
                if (EmployeeApprovedLeaves == null || EmployeeApprovedLeaves.Count == 0)
                {
                    await _dialogManager.ShowMessage(_resources.Error, _resources.ErrorMsg_NoApprovedLeaves);
                    return;
                }
                else if(SelectedEmployeeApprovedLeave == null)
                {
                    await _dialogManager.ShowMessage(_resources.Error, _resources.InsertAllDataMessage);
                    return;
                }
                else if (SelectedEmployeeApprovedLeave != null)
                {
#if !Mock
                if(SelectedEmployeeApprovedLeave.endDate < DateTime.Now)
                {
                    await _dialogManager.ShowMessage(_resources.Error, _resources.ErrorMsg_ThisLeaveHadFinished);
                    return;
                }
#endif
                    if(LeaveRequestTypes == null)
                    {
                        IsBusy = true;
                        LeaveRequestTypes = await _employeesDataService.GetLeaveTypes(EmployeeProfile.qatariID, null);
                        SelectedDocumentRequestType = null;
                        IsBusy = false;
                    }
                    Page2 = false;
                    Page3 = true;
                }
            }
        }
        public async Task<BasicResponse> SubmitAsync()
        {
            if (!SubmitCommand.CanExecute)
                return null;


            IsBusy = true;
            SubmitCommand.CanExecute = false;
            BasicResponse response = null;
            var isDuplicate = await _employeesDataService.IsDuplicateLeaveReturn(SelectedEmployeeApprovedLeave.leaveSequence);
            if (!isDuplicate)
            {
                /*
                 * 
                    FirstDayOfLeave = FirstDateOfLeaving.ToString("MM-dd-yyyy"),
                    LastDayOfLeave = LastDateOfLeaving.ToString("MM-dd-yyyy"),
                    LeaveType = SelectedDocumentRequestType,
                    Comments = Comments,
                    NumberofDays = RequiredDays,
                 */
                var leaveRequest = new Models.Employees.MyRequests.Request.LeaveReturnRequestRequest
                {
                    DateofJoin = string.IsNullOrEmpty(EmployeeProfile.joiningDate) ? default(DateTime?) : DateTime.Parse(EmployeeProfile.joiningDate),
                    DirectManager = EmployeeProfile.directManager?.nameEn,
                    EmpName = EmployeeProfile.nameEn,
                    EmpNameAr = EmployeeProfile.nameAr,
                    Grade = EmployeeProfile.financialGrade,
                    InstituteDepartmentUnit = EmployeeProfile.departmentEn,
                    InstituteDepartmentUnitAr = EmployeeProfile.departmentAr,
                    JobNo = EmployeeProfile.jobID,
                    JobTitle = EmployeeProfile.jobTitleEn,
                    JobTitleAr = EmployeeProfile.jobTitleAr,
                    Nationality = EmployeeProfile.nationalityEn,
                    NationalityAr = EmployeeProfile.nationalityAr,
                    Title = EmployeeProfile.jobTitleEn,
                    LeaveSequence = SelectedEmployeeApprovedLeave.leaveSequence,
                    QatarID = EmployeeProfile.qatariID,
                    Attachments = string.IsNullOrEmpty(ImageBase64) ? null : new List<Models.Attachment>
                    {
                        new Models.Attachment
                        {
                            FileName = FileName,
                            DisplayName = DisplayName,
                            Data = ImageBase64
                        }
                    }
                };
                if (CutLeave)
                {
                    leaveRequest.CutLeave = true;
                    leaveRequest.CutDays = CutDays;
                }
                else if (IncreaseLeaveDays)
                {
                    leaveRequest.ExtendLeave = true;
                    leaveRequest.ExtendedDays = (int)NewEndDateOfLeaving.Value.Subtract(SelectedEmployeeApprovedLeave.endDate).TotalDays;
                    leaveRequest.LeaveType = SelectedDocumentRequestType.leaveCode;
                    leaveRequest.ExtendLeaveCode = SelectedDocumentRequestType.nameEn;
                    leaveRequest.ExtendLeaveCodeAr = SelectedDocumentRequestType.nameAR;

                }
                else if (ProvidingSickLeave)
                {
                    leaveRequest.AddSickLeave = true;
                    leaveRequest.SickLeaveStartDate = FirstDateOfSickLeave.ToString("MM-dd-yyyy");
                    leaveRequest.SickLeaveEndDate = LastDateOfSickLeave.ToString("MM-dd-yyyy");
                    leaveRequest.SickLeaveNoOfDays = ((int)LastDateOfSickLeave.Subtract(FirstDateOfSickLeave).TotalDays) + 1;
                }
                else if (ProvidingALenientVacation)
                {
                    leaveRequest.AddCompassionateLeave = true;
                    leaveRequest.CompassionateLeaveStartDate = FirstDateOfLenientVacation.ToString("MM-dd-yyyy");
                    leaveRequest.CompassionateLeaveEndDate = LastDateOfLenientVacation.ToString("MM-dd-yyyy");
                    leaveRequest.CompassionateLeaveNoOfDays = ((int)LastDateOfLenientVacation.Subtract(FirstDateOfLenientVacation).TotalDays) + 1;
                }
                else
                {
                    response = new BasicResponse
                    {
                        ResponseCode = "2",
                        ResponseNameEn = "Please select a leave return type.",
                        ResponseNameAr = "برجاء اختيار نوع العودة من الأجازة."
                    };
                }

                if (response == null)
                {
                    response = await _employeesDataService.SubmitNewLeaveReturnRequest(leaveRequest, null);
                }
            }
            else
            {
                response = new BasicResponse
                {
                    ResponseCode = "2",
                    ResponseNameEn = "Another leave return is already submitted.",
                    ResponseNameAr = "تم تقديم نفس إشعار العودة من الأجازة من قبل."
                };
            }

            IsBusy = false;
            if (response != null)
            {
                await _dialogManager.ShowMessage(_resources.Submit, _appSettings.LanguageId == 1 ? response.ResponseNameEn : response.ResponseNameAr);
            }
            SubmitCommand.CanExecute = true;
            return response;
        }

        public async Task LoadDataAsync()
        {
            IsBusy = true;

            EmployeeByNameResponse empResponse = await _employeesDataService.GetEmployeeProfileByQatariId(_appSettings.EmployeeId, null);
            LoggedInEmployeeProfile = empResponse;

            SubmitRequestOnBehalfToIsVisible = await _employeesDataService.IsOnBehalfMember(empResponse.jobID);

            LoadEmployeeData(empResponse);

            IsBusy = false;
        }

        public async Task SearchEmployee(string jobId)
        {
            int value;
            int.TryParse(jobId, out value);
            if (value == 0)
            {
                await _dialogManager.ShowMessage(_resources.Error, _resources.InvalidNumber);
                SearchOnBehalfQuery = "";
            }
            else
            {
                IsBusy = true;

                OnBehalfEmployeeProfile = await _employeesDataService.GetEmployeeByJobId(jobId, null);
#if Mock
                OnBehalfEmployeeProfile.nameAr = OnBehalfEmployeeProfile.nameEn = "Hossam Ahmed";
#endif
                LoadEmployeeData(OnBehalfEmployeeProfile);

                IsBusy = false;
            }
        }

        private void LoadEmployeeData(EmployeeByNameResponse empResponse)
        {
            IsBusy = true;

            //EmployeeApprovedLeaves = await _employeesDataService.GetEmployeeApprovedLeaves(empResponse.jobID, null);
            Reset();

            EmployeeProfile = empResponse;
            if (_appSettings.LanguageId == 1) // english
            {
                EmployeeName = empResponse.nameEn;
                DeptOfficeInstitute = empResponse.departmentEn;
                Nationality = empResponse.nationalityEn;
                JobTitle = empResponse.jobTitleEn;
                LeaveRequestTypesDisplayMember = "nameEn";
                EmployeeApprovedLeavesDisplayMember = "nameEn";
                UnitDept = empResponse.unitEn;
            }
            else
            {
                EmployeeName = empResponse.nameAr;
                DeptOfficeInstitute = empResponse.departmentAr;
                Nationality = empResponse.nationalityAr;
                JobTitle = empResponse.jobTitleAr;
                LeaveRequestTypesDisplayMember = "nameAR";
                EmployeeApprovedLeavesDisplayMember = "nameAr";
                UnitDept = empResponse.unitAr;
            }

            StaffNo = empResponse.jobID;
            JoinedDate = empResponse.joiningDate;


            //LeaveRequestTypes = await _employeesDataService.GetLeaveTypes(empResponse.qatariID);


            IsBusy = false;
        }

        public void ClearAttachments()
        {
            ThumbImage = null;
            ImageBase64 = FileName = DisplayName = null;
        }
    }
}
