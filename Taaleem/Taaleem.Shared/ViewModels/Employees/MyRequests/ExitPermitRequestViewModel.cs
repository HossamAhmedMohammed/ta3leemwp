﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees.MyRequests;
using System.Linq;
using Taaleem.Models.Employees.MyRequests.Response;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.Storage;
using System.IO;
using Windows.UI.Xaml.Media.Imaging;

namespace Taaleem.ViewModels.Employees
{
    public class ExitPermitRequestViewModel : BindableBase
    {
        #region Fields
        private readonly IMyRequestsDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly Resources _resources;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        #endregion

        #region Properties
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        public AsyncExtendedCommand SubmitCommand { get; set; }
        public AsyncExtendedCommand LoadCommand { get; set; }


        private string _employeeName;
        public string EmployeeName
        {
            get { return _employeeName; }
            set { SetProperty(ref _employeeName, value); }
        }

        private string _stuffId;
        public string StaffNo
        {
            get { return _stuffId; }
            set { SetProperty(ref _stuffId, value); }
        }

        private string _jobName;

        public string Department
        {
            get { return _jobName; }
            set { SetProperty(ref _jobName, value); }
        }

        private string _personalIDNo;
        public string PersonalIDNo
        {
            get { return _personalIDNo; }
            set { SetProperty(ref _personalIDNo, value); }
        }

        private string _mobileNumber;

        public string MobileNumber
        {
            get { return _mobileNumber; }
            set { SetProperty(ref _mobileNumber, value); }
        }

        private DateTime _departureDate;

        public DateTime DepartureDate
        {
            get { return _departureDate; }
            set { SetProperty(ref _departureDate, value); }
        }



        public EmployeeByNameResponse EmployeeProfile { get; set; }

        #endregion

        public ExitPermitRequestViewModel(Resources resources,
            IMyRequestsDataService employeesDataService,
            IRequestMessageResolver messageResolver,
            IAppSettings appSettings,
            IDialogManager dialogManager,
            INavigationService navigationService,
            FilePicker filePicker)
        {
            _resources = resources;
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _dialogManager = dialogManager;
            _navigationService = navigationService;

            SubmitCommand = new AsyncExtendedCommand(SubmitAsync);
            LoadCommand = new AsyncExtendedCommand(LoadDataAsync);
        }
        
        public async Task<BasicResponse> SubmitAsync()
        {
            IsBusy = true;
            SubmitCommand.CanExecute = false;
            BasicResponse result = await _employeesDataService.SubmitNewExitPermitRequest(EmployeeProfile, DepartureDate.ToString("dd-MM-yyyy"), null);
            if (result != null)
            {
                await _dialogManager.ShowMessage(_resources.Submit, _appSettings.LanguageId == 1 ? result.ResponseNameEn : result.ResponseNameAr);
            }

            SubmitCommand.CanExecute = true;
            IsBusy = false;

            return result;
        }
        public async Task LoadDataAsync()
        {
            IsBusy = true;

            EmployeeByNameResponse empResponse = await _employeesDataService.GetEmployeeProfileByQatariId(_appSettings.EmployeeId, null);
            EmployeeProfile = empResponse;
            if (_appSettings.LanguageId == 1) // english
            {
                EmployeeName = empResponse.nameEn;
                Department = empResponse.departmentEn;
            }
            else
            {
                EmployeeName = empResponse.nameAr;
                Department = empResponse.departmentAr;
            }

            StaffNo = empResponse.jobID;
            PersonalIDNo = empResponse.qatariID;
            MobileNumber = empResponse.mobileNumber;

            IsBusy = false;
        }
    }
}
