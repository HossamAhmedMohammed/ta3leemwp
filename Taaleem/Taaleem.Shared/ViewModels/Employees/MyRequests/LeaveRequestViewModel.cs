﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees.MyRequests;
using System.Linq;
using Taaleem.Models.Employees.MyRequests.Response;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using System.IO;

namespace Taaleem.ViewModels.Employees.MyRequests
{
    public class LeaveRequestViewModel : BindableBase
    {
        #region Fields
        private readonly IMyRequestsDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly Resources _resources;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        private readonly FilePicker _filePicker;
        #endregion

        #region Properties
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        public AsyncExtendedCommand NextCommand { get; set; }
        public AsyncExtendedCommand BackCommand { get; set; }
        public AsyncExtendedCommand SubmitCommand { get; set; }
        public AsyncExtendedCommand LoadCommand { get; set; }
        public AsyncExtendedCommand SelectAttachment { get; set; }

        private bool _page2;

        public bool Page2
        {
            get { return _page2; }
            set { SetProperty(ref _page2, value); }
        }

        public Page AfterSubmitPageType { get; set; }

        private string _employeeName;

        public string EmployeeName
        {
            get { return _employeeName; }
            set { SetProperty(ref _employeeName, value); }
        }

        private string _JobId;

        public string JobNo
        {
            get { return _JobId; }
            set { SetProperty(ref _JobId, value); }
        }

        private string _joinedDate;

        public string JoinedDate
        {
            get { return _joinedDate; }
            set { SetProperty(ref _joinedDate, value); }
        }

        private string _deptName;

        public string DeptOfficeInstitute
        {
            get { return _deptName; }
            set { SetProperty(ref _deptName, value); }
        }

        private string _jobTitle;

        public string JobTitle
        {
            get { return _jobTitle; }
            set { SetProperty(ref _jobTitle, value); }
        }

        private string _personalIDNo;

        public string PersonalIDNo
        {
            get { return _personalIDNo; }
            set { SetProperty(ref _personalIDNo, value); }
        }

        private List<LeaveType> _documentRequestTypes;

        public List<LeaveType> LeaveRequestTypes
        {
            get { return _documentRequestTypes; }
            set { SetProperty(ref _documentRequestTypes, value); }
        }

        private string _leaveRequestTypesDisplayMember;

        public string LeaveRequestTypesDisplayMember
        {
            get { return _leaveRequestTypesDisplayMember; }
            set { SetProperty(ref _leaveRequestTypesDisplayMember, value); }
        }

        private string _SelectedDocumentRequestType;

        public string SelectedDocumentRequestType
        {
            get { return _SelectedDocumentRequestType; }
            set { SetProperty(ref _SelectedDocumentRequestType, value); }
        }

        //requiredAttachment

        private bool _requiredAttachment;

        public bool RequiredAttachment
        {
            get { return _requiredAttachment; }
            set { SetProperty(ref _requiredAttachment, value); }
        }

        private bool _requiredEndDate;

        public bool RequiredEndDate
        {
            get { return _requiredEndDate; }
            set { SetProperty(ref _requiredEndDate, value); }
        }

        private string _nationality;
        public string Nationality
        {
            get { return _nationality; }
            set { SetProperty(ref _nationality, value); }
        }

        private string _grade;
        public string GradeCategory
        {
            get { return _grade; }
            set { SetProperty(ref _grade, value); }
        }

        private string _balance;
        public string Balance
        {
            get { return _balance; }
            set { SetProperty(ref _balance, value); }
        }

        //Comments
        private string _comments;
        public string Comments
        {
            get { return _comments; }
            set { SetProperty(ref _comments, value); }
        }

        private string _alternativeEmployee;
        public string AlternativeEmployee
        {
            get { return _alternativeEmployee; }
            set { SetProperty(ref _alternativeEmployee, value); }
        }

        // RequiredDays
        private string _requiredDays;
        public string RequiredDays
        {
            get { return _requiredDays; }
            set { SetProperty(ref _requiredDays, value); }
        }

        private DateTime _firstDayOfLeaving;

        public DateTime FirstDateOfLeaving
        {
            get
            {
                if (_firstDayOfLeaving == DateTime.MinValue)
                    _firstDayOfLeaving = DateTime.Now;

                if (_lastDayOfLeaving < _firstDayOfLeaving)
                    _lastDayOfLeaving = _firstDayOfLeaving.AddDays(1);

                return _firstDayOfLeaving;
            }
            set
            {
                if (value > LastDateOfLeaving)
                    LastDateOfLeaving = value.AddDays(1);

                SetProperty(ref _firstDayOfLeaving, value);
                CalculateRequiredDays();
            }
        }

        private DateTime _lastDayOfLeaving;

        public DateTime LastDateOfLeaving
        {
            get
            {
                if (_lastDayOfLeaving < FirstDateOfLeaving)
                    _lastDayOfLeaving = FirstDateOfLeaving.AddDays(1);

                return _lastDayOfLeaving;
            }
            set
            {
                if (value < FirstDateOfLeaving)
                    value = FirstDateOfLeaving.AddDays(1);

                SetProperty(ref _lastDayOfLeaving, value);
                CalculateRequiredDays();
            }
        }

        public string ImageBase64 { get; set; }

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set { SetProperty(ref _fileName, value); }
        }

        private string _displayName;
        public string DisplayName
        {
            get { return _displayName; }
            set { SetProperty(ref _displayName, value); }
        }

        private BitmapImage _thumbImage;

        public BitmapImage ThumbImage
        {
            get { return _thumbImage; }
            set { SetProperty(ref _thumbImage, value); }
        }

        public EmployeeByNameResponse EmployeeProfile { get; private set; }

        internal void LoadLeaveType(LeaveType type)
        {
            Balance = type.balance;
            SelectedDocumentRequestType = type.leaveCode;
            RequiredAttachment = type.requiredAttachment;
            RequiredEndDate = type.requireEndDate;
            //LastDateOfLeaving = FirstDateOfLeaving.AddDays(1);
        }

        private void CalculateRequiredDays()
        {
            if (LastDateOfLeaving.Subtract(LastDateOfLeaving.TimeOfDay) == FirstDateOfLeaving.Subtract(FirstDateOfLeaving.TimeOfDay))
                RequiredDays = "1";
            else
                RequiredDays = ((int)LastDateOfLeaving.AddDays(1).Subtract(FirstDateOfLeaving).TotalDays).ToString();
        }
        #endregion

        public LeaveRequestViewModel(Resources resources,
            IMyRequestsDataService employeesDataService,
            IRequestMessageResolver messageResolver,
            IAppSettings appSettings,
            IDialogManager dialogManager,
            INavigationService navigationService,
            FilePicker filePicker)
        {
            _resources = resources;
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _dialogManager = dialogManager;
            _navigationService = navigationService;
            _filePicker = filePicker;

            NextCommand = new AsyncExtendedCommand(BackAsync);
            BackCommand = new AsyncExtendedCommand(NextAsync);
            SubmitCommand = new AsyncExtendedCommand(SubmitAsync);
            LoadCommand = new AsyncExtendedCommand(LoadDataAsync);
            SelectAttachment = new AsyncExtendedCommand(SelectAttachmentAsync);

            FirstDateOfLeaving = DateTime.Today;
            LastDateOfLeaving = DateTime.Today;
        }

        public async Task SelectAttachmentAsync()
        {
            StorageFile storageFile = await _filePicker.PickImageStorageFileAsync();
            if (storageFile != null)
            {
                var stream = await storageFile.GetThumbnailAsync(Windows.Storage.FileProperties.ThumbnailMode.PicturesView);
                var bitmapImage = new Windows.UI.Xaml.Media.Imaging.BitmapImage();
                await bitmapImage.SetSourceAsync(stream);
                ThumbImage = bitmapImage;

                Stream ms = await storageFile.OpenStreamForReadAsync();
                byte[] imageBytes = new byte[(int)ms.Length];
                ms.Read(imageBytes, 0, (int)ms.Length);
                ImageBase64 = Convert.ToBase64String(imageBytes);
                FileName = storageFile.Name;
                DisplayName = storageFile.DisplayName;
            }
        }

        public async Task BackAsync()
        {
            await Task.Delay(100);
            Page2 = false;
        }
        public async Task NextAsync()
        {
            await Task.Delay(100);
            Page2 = true;
        }
        public async Task<BasicResponse> SubmitAsync()
        {
            if (!SubmitCommand.CanExecute)
                return null;
            if (string.IsNullOrEmpty(SelectedDocumentRequestType))
            {
                await _dialogManager.ShowMessage(_resources.Error, string.Format(_resources.ErrorMsg_InputIsRequired, _resources.LeaveType));
                return null;
            }
            if (string.IsNullOrEmpty(AlternativeEmployee))
            {
                await _dialogManager.ShowMessage(_resources.Error, string.Format(_resources.ErrorMsg_InputIsRequired, _resources.AlternativeEmployee));
                return null;
            }
            if (RequiredAttachment && string.IsNullOrEmpty(ImageBase64))
            {
                await _dialogManager.ShowMessage(_resources.Error, string.Format(_resources.ErrorMsg_InputIsRequired, _resources.Proof));
                return null;
            }
            //if (string.IsNullOrEmpty(Details))
            //{
            //    await _dialogManager.ShowMessage(_resources.Error, string.Format(_resources.ErrorMsg_InputIsRequired, _resources.Details));
            //    return null;
            //}
            IsBusy = true;
            SubmitCommand.CanExecute = false;
            int jobId = 0;
            EmployeeByNameResponse alternativeEmp = null;
            int.TryParse(AlternativeEmployee, out jobId);
            // validate the alternative employee by job id
            if (jobId > 0)
                alternativeEmp = await _employeesDataService.GetEmployeeByJobId(AlternativeEmployee, null);
            else
                alternativeEmp = await _employeesDataService.GetEmployeeByName(AlternativeEmployee, null);

            if (alternativeEmp == null || string.IsNullOrEmpty(alternativeEmp.qatariID))
            {
                IsBusy = false;
                await _dialogManager.ShowMessage(_resources.Error, _resources.ReplacemenetEmployeeDoesntExist);

                SubmitCommand.CanExecute = true;
                return null;
            }

            BasicResponse response = null;
            var isDuplicate = await _employeesDataService.IsDuplicateLeave(EmployeeProfile.qatariID,
                                                                           SelectedDocumentRequestType,
                                                                           FirstDateOfLeaving.ToString("MM-dd-yyyy"),
                                                                           LastDateOfLeaving.ToString("MM-dd-yyyy"));
            if (!isDuplicate)
            {
                response = await _employeesDataService.SubmitNewLeaveRequest(new Models.Employees.MyRequests.Request.LeaveRequestRequest
                {
                    DateofJoin = EmployeeProfile.joiningDate,
                    DirectManager = EmployeeProfile.directManager?.nameEn,
                    EmpName = EmployeeProfile.nameEn,
                    EmpNameAr = EmployeeProfile.nameAr,
                    Grade = EmployeeProfile.financialGrade,
                    InstituteDepartmentUnit = EmployeeProfile.departmentEn,
                    InstituteDepartmentUnitAr = EmployeeProfile.departmentAr,
                    JobNo = EmployeeProfile.jobID,
                    JobTitle = EmployeeProfile.jobTitleEn,
                    JobTitleAr = EmployeeProfile.jobTitleAr,
                    Nationality = EmployeeProfile.nationalityEn,
                    NationalityAr = EmployeeProfile.nationalityAr,
                    Title = EmployeeProfile.jobTitleEn,

                    FirstDayOfLeave = FirstDateOfLeaving.ToString("MM-dd-yyyy"),
                    LastDayOfLeave = LastDateOfLeaving.ToString("MM-dd-yyyy"),
                    LeaveType = SelectedDocumentRequestType,
                    Comments = Comments,
                    NoOfDaysRequired = RequiredDays,
                    QatarID = EmployeeProfile.qatariID,
                    Attachments = string.IsNullOrEmpty(ImageBase64) ? null : new List<Models.Attachment>
                        {
                            new Models.Attachment
                            {
                                FileName = FileName,
                                DisplayName = DisplayName,
                                Data = ImageBase64
                            }
                        }
                }, null);
            }
            else
            {
                response = new BasicResponse
                {
                    ResponseCode = "2",
                    ResponseNameEn = "Invalid first or last date of leaving",
                    ResponseNameAr = "خطأ في ادخال تاريخ الخروج أو العودة"
                };
            }

            IsBusy = false;
            if (response != null)
            {
                await _dialogManager.ShowMessage(_resources.Submit, _appSettings.LanguageId == 1 ? response.ResponseNameEn : response.ResponseNameAr);
            }
            SubmitCommand.CanExecute = true;
            return response;
        }

        public async Task LoadDataAsync()
        {
            IsBusy = true;

            EmployeeByNameResponse empResponse = await _employeesDataService.GetEmployeeProfileByQatariId(_appSettings.EmployeeId, null);
            EmployeeProfile = empResponse;
            if (_appSettings.LanguageId == 1) // english
            {
                EmployeeName = empResponse.nameEn;
                DeptOfficeInstitute = empResponse.departmentEn;
                Nationality = empResponse.nationalityEn;
                JobTitle = empResponse.jobTitleEn;
                LeaveRequestTypesDisplayMember = "nameEn";
            }
            else
            {
                EmployeeName = empResponse.nameAr;
                DeptOfficeInstitute = empResponse.departmentAr;
                Nationality = empResponse.nationalityAr;
                JobTitle = empResponse.jobTitleAr;
                LeaveRequestTypesDisplayMember = "nameAR";
            }

            JobNo = empResponse.jobID;
            JoinedDate = empResponse.joiningDate;
            PersonalIDNo = empResponse.qatariID;
            GradeCategory = empResponse.financialGrade;

            LeaveRequestTypes = await _employeesDataService.GetLeaveTypes(empResponse.qatariID);

            //LeaveRequestTypes = response.Select(s => ItemTypeWrapper.Wrap(s, _appSettings.LanguageId)).ToList();

            IsBusy = false;
        }

        public void ClearAttachments()
        {
            ThumbImage = null;
            ImageBase64 = FileName = DisplayName = null;
        }
    }
}
