﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees.MyRequests;
using System.Linq;
using Taaleem.Models.Employees.MyRequests.Response;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;

namespace Taaleem.ViewModels.Employees
{
    public class DocumentRequestViewModel : BindableBase
    {
        #region Fields
        private readonly IMyRequestsDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly Resources _resources;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        #endregion

        #region Properties
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        public AsyncExtendedCommand NextCommand { get; set; }
        public AsyncExtendedCommand BackCommand { get; set; }
        public AsyncExtendedCommand SubmitCommand { get; set; }
        public AsyncExtendedCommand LoadCommand { get; set; }

        private bool _page2;

        public bool Page2
        {
            get { return _page2; }
            set { SetProperty(ref _page2, value); }
        }

        public Page AfterSubmitPageType { get; set; }

        private string _employeeName;

        public string EmployeeName
        {
            get { return _employeeName; }
            set { SetProperty(ref _employeeName , value); }
        }

        private string _stuffId;

        public string StaffNo
        {
            get { return _stuffId; }
            set { SetProperty(ref _stuffId , value); }
        }

        private string _joinedDate;

        public string JoinedDate
        {
            get { return _joinedDate; }
            set { SetProperty(ref _joinedDate, value); }
        }

        private string _jobName;

        public string JobName
        {
            get { return _jobName; }
            set { SetProperty(ref _jobName ,value); }
        }

        private string _jobTitle;

        public string JobTitle
        {
            get { return _jobTitle; }
            set { SetProperty(ref _jobTitle, value); }
        }

        private string _personalIDNo;

        public string PersonalIDNo
        {
            get { return _personalIDNo; }
            set { SetProperty(ref _personalIDNo, value); }
        }




        private List<ItemTypeWrapper> _documentRequestTypes;

        public List<ItemTypeWrapper> DocumentRequestTypes
        {
            get { return _documentRequestTypes; }
            set { SetProperty(ref _documentRequestTypes, value); }
        }


        private string _details;

        public string Details
        {
            get { return _details; }
            set { SetProperty(ref _details, value); }
        }

        private string _SelectedDocumentRequestType;

        public string SelectedDocumentRequestType
        {
            get { return _SelectedDocumentRequestType; }
            set { SetProperty(ref _SelectedDocumentRequestType, value); }
        }
        
        #endregion

        public DocumentRequestViewModel(Resources resources, 
            IMyRequestsDataService employeesDataService, 
            IRequestMessageResolver messageResolver, 
            IAppSettings appSettings, 
            IDialogManager dialogManager, 
            INavigationService navigationService)
        {
            _resources = resources;
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _dialogManager = dialogManager;
            _navigationService = navigationService;

            NextCommand = new AsyncExtendedCommand(BackAsync);
            BackCommand = new AsyncExtendedCommand(NextAsync);
            SubmitCommand = new AsyncExtendedCommand(SubmitAsync);
            LoadCommand = new AsyncExtendedCommand(LoadDataAsync);
        }

        public async Task BackAsync()
        {
            await Task.Delay(100);
            Page2 = false;
        }
        public async Task NextAsync()
        {
            await Task.Delay(100);
            Page2 = true;
        }
        public async Task<BasicResponse> SubmitAsync()
        {
            if (string.IsNullOrEmpty(SelectedDocumentRequestType))
            {
                await _dialogManager.ShowMessage(_resources.Error, string.Format(_resources.ErrorMsg_InputIsRequired, _resources.RequiredDocumentType));
                return null;
            }
            //if (string.IsNullOrEmpty(Details))
            //{
            //    await _dialogManager.ShowMessage(_resources.Error, string.Format(_resources.ErrorMsg_InputIsRequired, _resources.Details));
            //    return null;
            //}

            IsBusy = true;
            var result = await _employeesDataService.SubmitNewDocumentRequest(_appSettings.EmployeeId, 
                                _SelectedDocumentRequestType, 
                                string.IsNullOrEmpty(Details)? "none": Details, 
                                null);
            IsBusy = false;
            if (result != null)
            {
                await _dialogManager.ShowMessage(_resources.Submit, _appSettings.LanguageId == 1 ? result.ResponseNameEn : result.ResponseNameAr);
            }
            return result;
        }
        public async Task LoadDataAsync()
        {
            IsBusy = true;

            EmployeeByNameResponse empResponse = await _employeesDataService.GetEmployeeProfileByQatariId(_appSettings.EmployeeId, null);
            if (_appSettings.LanguageId == 1) // english
            {
                EmployeeName = empResponse.nameEn;
                JobName = empResponse.departmentEn;
                JobTitle = empResponse.jobTitleEn;
            }
            else
            {
                EmployeeName = empResponse.nameAr;
                JobName = empResponse.departmentAr;
                JobTitle = empResponse.jobTitleAr;
            }

            StaffNo = empResponse.jobID;
            JoinedDate = empResponse.joiningDate;
            PersonalIDNo = empResponse.qatariID;

            var response = await _employeesDataService.GetDocumentRequestTypes();
            DocumentRequestTypes = response.Select(s => ItemTypeWrapper.Wrap(s, _appSettings.LanguageId)).ToList();

            IsBusy = false;
        }
    }
}
