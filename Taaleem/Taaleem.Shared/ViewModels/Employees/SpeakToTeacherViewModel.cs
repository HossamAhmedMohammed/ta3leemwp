﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Collections.Extended;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;

namespace Taaleem.ViewModels.Children
{
    public class SpeakToTeacherViewModel : BindableBase
    {
        #region Fields
        private readonly IChildrenDataService _childrenDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly Resources _resources;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        private readonly IToastService _toastService;
        #endregion

        #region Properties

        private readonly ObservableCollection<PrivateNoteWrapper> _privateNotes = new ObservableCollection<PrivateNoteWrapper>();
        public ObservableCollection<PrivateNoteWrapper> PrivateNotes
        {
            get { return _privateNotes; }
        }

        public ExtendedCollectionView PrivateNotesCView { get; set; }

        private ContactTeacher _contactTeacher;
        public ContactTeacher ContactTeacher
        {
            get { return _contactTeacher; }
            set { SetProperty(ref _contactTeacher, value);
                OnPropertyChanged();
            }
        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set
            {
                if (SetProperty(ref _message, value))
                {
                    SendPrivateNoteCommand.CanExecute = !String.IsNullOrWhiteSpace(Message);
                }
            }
        }

        private bool _isBusy;
        /// <summary>
        /// This will be used to indicate ongoing operation of loading private notes
        /// </summary>
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private bool _isSubmitting;

        /// <summary>
        /// This is used to indicate onoging operation of submitting Private note.
        /// </summary>
        public bool IsSubmitting
        {
            get { return _isSubmitting; }
            set { SetProperty(ref _isSubmitting, value); }
        }


        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }


        private RequestMessage _sendErrorMessage;
        public RequestMessage SendErrorMessage
        {
            get { return _sendErrorMessage; }
            set { SetProperty(ref _sendErrorMessage, value); }
        }
        #endregion

        #region Initialization

        public SpeakToTeacherViewModel(Resources resources, IChildrenDataService childrenDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService, IToastService toastService)
        {
            _childrenDataService = childrenDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _toastService = toastService;
            _resources = resources;
            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadPrivateNotesAsync);
            SendPrivateNoteCommand = new AsyncExtendedCommand(SendPrivateNoteAsync, false);

            PrivateNotesCView = new ExtendedCollectionView();
            //PrivateNotesCView.SortDescriptions.Add(new SortDescription(@"PostDate", ListSortDirection.Ascending));
            PrivateNotesCView.GroupDescriptions.Add(new PropertyGroupDescription(@"PostDate.Date"));
            PrivateNotesCView.Source = PrivateNotes;
          
            

        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        public AsyncExtendedCommand SendPrivateNoteCommand { get; set; }
        #endregion

        #region Methods

        public async Task LoadPrivateNotesAsync()
        {
            LoadCommand.CanExecute = false;
            SendPrivateNoteCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
                var child = loginResponse.Children[_appSettings.SelectedChildIndex];
                var parameters = new GetPrivateNoteParameters()
                {
                    LanguageId = _appSettings.LanguageId,
                    StudentId = child.UserID,
                    SchoolId = child.School.SchoolID,
                    TeacherId = ContactTeacher.Teacher.UserID
                };

                PrivateNotes.Clear();

                var response = await _childrenDataService.GetPrivateNotesAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    if (response.Result == null || response.Result.Count == 0)
                    {
                        ErrorMessage = RequestMessage.GetNoDataMessage(_resources.NoMessages);
                    }
                    else
                    {
                        using (PrivateNotesCView.DeferRefresh())
                        {
                            foreach (var privateNote in response.Result)
                            {
                                PrivateNotes.Add(PrivateNoteWrapper.FromPrivateNote(privateNote, ContactTeacher.Teacher.UserID));
                            }
                        }
                    }
                }
                else if (response.ResponseStatus == ResponseStatus.SuccessWithNoData)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage(_resources.NoMessages);
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            catch
            {
                // ignored
            }
            finally
            {
                LoadCommand.CanExecute = true;
                SendPrivateNoteCommand.CanExecute = !String.IsNullOrWhiteSpace(Message);
                IsBusy = false;
            }
        }

        private async Task SendPrivateNoteAsync()
        {
            LoadCommand.CanExecute = false;
            SendPrivateNoteCommand.CanExecute = false;
            IsSubmitting = true;
            SendErrorMessage = null;
            try
            {
                var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
                var child = loginResponse.Children[_appSettings.SelectedChildIndex];
                var parameters = new AddConversationPostParameters()
                {
                    LanguageId = _appSettings.LanguageId,
                    StudentId = child.UserID,
                    SchoolId = child.School.SchoolID,
                    TeacherId = ContactTeacher.Teacher.UserID,
                    Message = Message,
                    PrivateConversationId = PrivateNotes.Count > 0 ? PrivateNotes[PrivateNotes.Count - 1].PrivateConversationId : 0
                };

                var response = await _childrenDataService.SendPrivateNoteToTeacherAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    if (response.Result != 0)
                    {
                        //Show Unable to send Toast
                        var note = new PrivateNote()
                        {
                            UserId = 0,//TODO:Add user Id, and revise parameters
                            PostText = Message,
                            PostDate = DateTime.Now,
                            PrivateConversationId = response.Result
                        };
                        Message = null;
                        AddNote(PrivateNoteWrapper.FromPrivateNote(note, ContactTeacher.Teacher.UserID));
                    }
                    else
                    {
                        // _toastService.ShowToastThreeLines(_resources.SubmittingFailed);
                        SendErrorMessage = new RequestMessage(_resources.SubmittingFailed);
                    }
                }
                else
                {

                    var submitErrorMessage = _messageResolver.ResultToMessage(response);
                    // _toastService.ShowToastThreeLines(submitErrorMessage);
                    SendErrorMessage = submitErrorMessage;
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                SendPrivateNoteCommand.CanExecute = !String.IsNullOrWhiteSpace(Message);
                IsSubmitting = false;
            }
        }

        private void AddNote(PrivateNoteWrapper note)
        {
            PrivateNotes.Add(note);
            //var groups = PrivateNotesCView.CollectionGroups;
            //bool addNewGroup = false;

            //if(groups.Count == 0)
            //    addNewGroup = true;
            //else
            //{
            //    var lastGroup = (GroupList)groups[groups.Count - 1];
            //    var lastItem = (PrivateNoteWrapper)lastGroup.GroupItems[lastGroup.GroupItems.Count - 1];
            //    if(lastItem.PostDate.Date == note.PostDate.Date)
            //    {
            //        lastGroup.GroupItems.Add(note);
            //    }
            //    else
            //    {
            //        addNewGroup = true;
            //    }
            //}

            //if(addNewGroup)
            //{
            //    groups.Add(new GroupList() { Group = new GroupInfo(note.PostDate.Date), GroupItems = { note } });
            //}
        }
        #endregion
    }
}