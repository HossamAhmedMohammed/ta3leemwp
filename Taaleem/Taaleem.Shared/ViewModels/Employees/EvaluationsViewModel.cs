﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Employees;
using Taaleem.Models.Employees.Request;

namespace Taaleem.ViewModels.Employees
{
    public class EvaluationsViewModel : BindableBase
    {
        #region Fields
        private readonly IEmployeesDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly Resources _resources;
        private readonly IAppSettings _appSettings;
        #endregion

        #region Properties

        private string _evaluationYear;
        public string EvaluationYear
        {
            get { return _evaluationYear; }
            set { SetProperty(ref _evaluationYear, value); }
        }

        private List<EvaluationWrapper> _evaluations;
        public List<EvaluationWrapper> Evaluations
        {
            get { return _evaluations; }
            set { SetProperty(ref _evaluations, value); }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }


        #endregion

        #region Initialization

        public EvaluationsViewModel(Resources resources, IEmployeesDataService employeesDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _resources = resources;

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadEvaluationsAsync);
        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        #endregion

        #region Methods

        public async Task LoadEvaluationsAsync()
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var parameters = new EvaluationParameters
                {
                    Id = _appSettings.EmployeeId,
                    Year ="All" //_resources.All == EvaluationYear ? "All" : EvaluationYear
                };

                var response = await _employeesDataService.GetEvaluationsAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    var list = response.Result.Select(item => EvaluationWrapper.Wrap(item, _appSettings.LanguageId))
                        .OrderByDescending(item => item.Year)
                        .ToList();
                    Evaluations = list;

                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    ErrorMessage = RequestMessage.GetNoDataMessage(); ;
                    Evaluations = null;
                }
                else
                {
                    Evaluations = null;
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }

        #endregion
    }
}