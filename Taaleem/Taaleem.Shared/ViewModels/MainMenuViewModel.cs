﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models;
using Taaleem.Models.Children;
using Taaleem.Views.Children;
using Taaleem.Views.Employee;
using Taaleem.Views.PublicServices;

namespace Taaleem.ViewModels
{
    public class MainMenuViewModel:BindableBase
    {
        private INavigationService _navigationService;
        private IAppSettings _appSettings;
        private Resources _resources;
        private List<MainMenuItem> items;

        public List<MainMenuItem> Items
        {
            get { return items; }
            set { items = value; }
        }

        private MainMenuItem selectedMenuItem;

        public MainMenuItem SelectedMenuItem
        {
            get { return selectedMenuItem; }
            set
            {


                //selectedMenuItem = value;
               ChangeSelectedItem(value);
               
                  OnPropertyChanged("SelectedMenuItem");

            }
        }
private async void ChangeSelectedItem(MainMenuItem value)
        {
            if (value.Name == "الرئيسيه")
                    {
                        _navigationService.NavigateByPage<Home>();
                        loadItems();
                        if (_appSettings.LanguageId == 2)
                        {
                            items.Where(i => i.Name == "الرئيسيه").Single().Image = "ms-appx:///Assets/Menu/parent_home_pressed_ic@3x.png";
                        }
                        else
                        {
                            items.Where(i => i.Name == "الرئيسيه").Single().Image = "ms-appx:///Assets/Menu/parent_home_pressed_ic_en@3x.png";
                        }
                        currentMenuItem.Name = "الرئيسيه";
                       selectedMenuItem = value;
                        
                    }


            if (value.Name == "خدمات الاباء")

                    {
                        ViewModels.ViewModelLocator.Locator.AppSettings.IsParentMode = true;
                        ViewModels.ViewModelLocator.Locator.AppSettings.AmIParent = true;

                        if (await ViewModels.ViewModelLocator.Locator.AppSettings.GetParentLoginResponseAsync() == null)
                          await  ViewModels.ViewModelLocator.Locator.DialogManager.ShowParentLoginDialog();
                        else
                        {

                            _navigationService.NavigateByPage<ParentMenuPage>();
                            selectedMenuItem = value;
                            currentMenuItem.Name = "خدمات الاباء";
                            loadItems();
                            if (_appSettings.LanguageId == 2)
                            {
                                items.Where(i => i.Name == "خدمات الاباء").Single().Image = "ms-appx:///Assets/Menu/parent_services_pressed_ic@3x.png";
                            }
                            else
                            {
                                items.Where(i => i.Name == "خدمات الاباء").Single().Image = "ms-appx:///Assets/Menu/parent_services_pressed_ic_en@3x.png";
                            }
                           


                        }
                      

                    }
            if (value.Name == "خدمات الموظف")
                    {
                        if (await ViewModels.ViewModelLocator.Locator.AppSettings.GetEmployeeLoginResponseAsync() == null)
                          await  ViewModels.ViewModelLocator.Locator.DialogManager.ShowEmployeeLoginDialog();
                        else
                        {
                            _navigationService.NavigateByPage<EmployeeMenuPage>();
                            currentMenuItem.Name = "خدمات الموظف";
                            loadItems();
                            if (_appSettings.LanguageId == 2)
                            {
                                items.Where(i => i.Name == "خدمات الموظف").Single().Image = "ms-appx:///Assets/Menu/parent_employees_pressed_ic@3x.png";
                            }
                            else
                            {
                                items.Where(i => i.Name == "خدمات الموظف").Single().Image = "ms-appx:///Assets/Menu/parent_employees_pressed_ic_en@3x.png";
                            }
                            selectedMenuItem = value;
                        }
                       

                     
                    }

            if (value.Name == "خدمات الطلاب")
                    {
                        ViewModels.ViewModelLocator.Locator.AppSettings.IsStudentMode = true;
                        ViewModels.ViewModelLocator.Locator.AppSettings.AmIParent = false;

                        if (await ViewModels.ViewModelLocator.Locator.AppSettings.GetStudentLoginResponseAsync() == null)
                            //Task< ParentLoginWrapper> p =  ViewModels.ViewModelLocator.Locator.AppSettings.GetStudentLoginResponseAsync();
                            // if (p.Result == null)
                         await   ViewModels.ViewModelLocator.Locator.DialogManager.ShowStudentLoginDialog();
                        else
                        {

                            _navigationService.NavigateByPage<ParentMenuPage>();
                            selectedMenuItem = value;
                            currentMenuItem.Name = "خدمات الطلاب";
                            loadItems();
                            if (_appSettings.LanguageId == 2)
                            {
                                items.Where(i => i.Name == "خدمات الطلاب").Single().Image = "ms-appx:///Assets/Menu/parent_students_pressed_ic@3x.png";
                            }
                            else
                            {
                                items.Where(i => i.Name == "خدمات الطلاب").Single().Image = "ms-appx:///Assets/Menu/parent_students_pressed_ic_en@3x.png";
                            }

                            
                        }
                       
                      
                    }
            if (value.Name == "خدمات عامه")
            {

              
                    _navigationService.NavigateByPage<PublicServicesMenuPage>();

                    loadItems();
                    if (_appSettings.LanguageId == 2)
                    {
                        items.Where(i => i.Name == "خدمات عامه").Single().Image = "ms-appx:///Assets/Menu/parent_general_pressed_ic@3x.png";
                    }
                    else
                    {
                        items.Where(i => i.Name == "خدمات عامه").Single().Image = "ms-appx:///Assets/Menu/parent_general_pressed_ic_en@3x.png";
                    }
                    currentMenuItem.Name = "خدمات عامه";
                    selectedMenuItem = value;
                

            }
                if (value.Name == "الاخبار")
                {

                    
                        _navigationService.NavigateByPage<MaglesNews>();

                        loadItems();
                        if (_appSettings.LanguageId == 2)
                        {
                            items.Where(i => i.Name == "الاخبار").Single().Image = "ms-appx:///Assets/Menu/parent_news_pressed_ic@3x.png";
                        }
                        else
                        {
                            items.Where(i => i.Name == "الاخبار").Single().Image = "ms-appx:///Assets/Menu/parent_news_pressed_ic_en@3x.png";
                        }
                        currentMenuItem.Name = "الاخبار";
                        selectedMenuItem = value;
                   

                }
                if (value.Name == "عن المجلس")
                {
                   
                        _navigationService.NavigateByPage<About>();
                        loadItems();
                        if (_appSettings.LanguageId == 2)
                        {
                            items.Where(i => i.Name == "عن المجلس").Single().Image = "ms-appx:///Assets/Menu/parent_about_pressed_ic@3x.png";
                        }
                        else
                        {
                            items.Where(i => i.Name == "عن المجلس").Single().Image = "ms-appx:///Assets/Menu/parent_about_pressed_ic_en@3x.png";
                        }
                        currentMenuItem.Name = "عن المجلس";
                         selectedMenuItem = value;
                   
                }

                if (value.Name == "الاعدادات")
                {
                   
                        _navigationService.NavigateByPage<Taaleem.Views.Settings>();
                        loadItems();
                        if (_appSettings.LanguageId == 2)
                        {
                            items.Where(i => i.Name == "الاعدادات").Single().Image = "ms-appx:///Assets/Menu/parent_settings_pressed_ic@3x.png";
                        }
                        else
                        {
                            items.Where(i => i.Name == "الاعدادات").Single().Image = "ms-appx:///Assets/Menu/parent_settings_pressed_ic_en@3x.png";
                        }
                        currentMenuItem.Name = "الاعدادات";
                         selectedMenuItem = value;
                   
                }
            
        }
           
        
    

        private MainMenuItem currentMenuItem;

        public MainMenuItem CurrentMenuItem
        {
            get { return currentMenuItem; }
            set { currentMenuItem = value;

            }
        }

        #region Commands

        public ExtendedCommand<MainMenuItem> ItemClickedCommand { get; set; }
        #endregion
        public MainMenuViewModel(INavigationService navigationService, IAppSettings appSettings , Resources resources)
        {
            _navigationService = navigationService;
            _appSettings = appSettings;
            _resources = resources;
            items = new List<MainMenuItem>();
            loadItems();
            ItemClickedCommand = new ExtendedCommand<MainMenuItem>(NavigateToPage);
            currentMenuItem = new MainMenuItem();
        }

        private async void NavigateToPage(MainMenuItem obj)
        {
           // if(CurrentMenuItem != null)
          //  SelectedMenuItem = CurrentMenuItem;
        }

        public void loadItems()
        {
            items.Clear();
            if (_appSettings.LanguageId == 2)
            {
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_home_ic@3x.png", Name = "الرئيسيه", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_students_ic@3x.png", Name = "خدمات الطلاب", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_services_ic@3x.png", Name = "خدمات الاباء", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_employees_ic@3x.png", Name = "خدمات الموظف", URL = "" });
               
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_general_ic@3x.png", Name = "خدمات عامه", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_news_ic@3x.png", Name = "الاخبار", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_about_ic@3x.png", Name = "عن المجلس", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_settings_ic@3x.png", Name = "الاعدادات", URL = "" });
            }
            else if(_appSettings.LanguageId ==1)
            {
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_home_ic_en@3x.png", Name = "الرئيسيه", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_students_ic_en@3x.png", Name = "خدمات الطلاب", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_services_ic_en@3x.png", Name = "خدمات الاباء", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_employees_ic_en@3x.png", Name = "خدمات الموظف", URL = "" });
             
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_general_ic_en@3x.png", Name = "خدمات عامه", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_news_ic_en@3x.png", Name = "الاخبار", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_about_ic_en@3x.png", Name = "عن المجلس", URL = "" });
                items.Add(new MainMenuItem { Image = "ms-appx:///Assets/Menu/parent_settings_ic_en@3x.png", Name = "الاعدادات", URL = "" });
            }
        }
    }
}
