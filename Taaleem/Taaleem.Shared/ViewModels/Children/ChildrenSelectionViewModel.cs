﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children;
using Taaleem.Views.Children;

namespace Taaleem.ViewModels.Children
{
    public class ChildrenSelectionViewModel : BindableBase
    {
        #region Fields
        private readonly IAppSettings _appSettings;
        private readonly Resources _resources;
        private readonly INavigationService _navigationService;
        #endregion

        #region Properties
        private IList<ChildWrapper> _children;
        public IList<ChildWrapper> Children
        {
            get { return _children; }
            set { SetProperty(ref _children, value); }
        }

        private IList<ChildWrapper> _childrenStudent;
        public IList<ChildWrapper> ChildrenStudent
        {
            get { return _childrenStudent; }
            set { SetProperty(ref _childrenStudent, value); }
        }

        private string _noChildrenMessage;

        public string NoChildrenMessage
        {
            get { return _noChildrenMessage; }
            set { SetProperty(ref _noChildrenMessage, value); }
        }

        //TODO:Make Sure that when selection changes in other pages, that it reflects in this pages
        //As it may be cached
        public int SelectedChildIndex
        {
            get
            {
                if(_appSettings.AmIParent)
                return Children == null || Children.Count == 0 ? -1 : _appSettings.SelectedChildIndex;
                else
                    return ChildrenStudent == null || ChildrenStudent.Count == 0 ? -1 : _appSettings.SelectedChildIndex;
            }
            set
            {
                int previousValue = _appSettings.SelectedChildIndex;
                if (previousValue != value)
                {
                    if (previousValue != -1) Children[previousValue].IsSelected = false;
                    try {
                        Children[value].IsSelected = true;
                    }
                    catch { }
                    _appSettings.SelectedChildIndex = value;
                    OnPropertyChanged();
                    OnPropertyChanged("SelectedChild");
                    OnSelectedChildIndexChanged(value);
                }
            }
        }

      

        public ChildWrapper SelectedChild
        {
            get { if (_appSettings.AmIParent)
                {
                    return Children == null ? null : Children[SelectedChildIndex];
                }
                else
                {
                    return ChildrenStudent == null ? null : ChildrenStudent[SelectedChildIndex];
                }
            
            }
        }
        #endregion

        #region Initialization

        public  ChildrenSelectionViewModel(Resources resources, IAppSettings appSettings ,INavigationService navigationService)
        {
            _resources = resources;
            _appSettings = appSettings;
            _navigationService = navigationService;
            //Initialize Commands

            callLoadChildren();
          
        }

        public async void callLoadChildren()
        {
            await LoadChildrenAsync();
            OnPropertyChanged("SelectedChild");
        }

        #endregion

        #region Commands

        #endregion

        #region Methods

        public async Task LoadChildrenAsync()
        {
            ParentLoginWrapper loginResponse;
            if (_appSettings.AmIParent)
            {
                loginResponse = await _appSettings.GetParentLoginResponseAsync();
                if (loginResponse == null) return;
                else
                {
                    Children = loginResponse.Children;
                    if (SelectedChildIndex == -1)
                        SelectedChildIndex = 0;
                    else
                        SelectedChildIndex = _appSettings.SelectedChildIndex;
                }
            }
            else
            {
                loginResponse = await _appSettings.GetStudentLoginResponseAsync();
                if (loginResponse == null) return;
                else
                {
                    ChildrenStudent = loginResponse.Children;
                    if (SelectedChildIndex == -1)
                        SelectedChildIndex = 0;
                    else
                        SelectedChildIndex = _appSettings.SelectedChildIndex;
                }
            }
        }

        internal void NavigateToNotifications()
        {
            _navigationService.NavigateByPage<Notifications>();
        }
        internal void NavigateToReminders()
        {
            _navigationService.NavigateByPage<Reminders>();
        }
        internal void NavigateToMessages()
        {
            _navigationService.NavigateByPage<Teachers>();
        }
        #endregion

        #region Events

        public event EventHandler<int> SelectedChildIndexChanged;
        protected virtual void OnSelectedChildIndexChanged(int e)
        {
            var handler = SelectedChildIndexChanged;
            if (handler != null) handler(this, e);
        }
        #endregion


    }
}
