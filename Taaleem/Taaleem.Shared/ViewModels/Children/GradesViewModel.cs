﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using System.Linq;
using Taaleem.Collections.Extended;
using System.Collections.ObjectModel;

namespace Taaleem.ViewModels.Children
{
    public class GradesViewModel : BindableBase
    {
        #region Fields
        private readonly IChildrenDataService _childrenDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly Resources _resources;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;
        private readonly INetworkHelper _networkHelper;
        #endregion

        #region Properties
        private GroupList selectedGroup;

        public GroupList SelectedGroup
        {
            get { return selectedGroup; }
            set { selectedGroup = value;
                OnPropertyChanged();
            }
        }


        private ObservableCollection<GroupList> gradesGroupName;

        public ObservableCollection<GroupList> GradesGroupName
        {
            get { return gradesGroupName; }
            set { gradesGroupName = value; }
        }


        private List<Grade> _grades;
        public List<Grade> Grades
        {
            get { return _grades; }
            set { SetProperty(ref _grades, value); }
        }

        public CollectionViewSource GradesCView { get; set; }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }


        #endregion

        #region Initialization

        public GradesViewModel( INetworkHelper networkHelper,IDialogManager dialogManager,Resources resources, IChildrenDataService childrenDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService, Resources resources1)
        {
            _childrenDataService = childrenDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _networkHelper = networkHelper;
            _dialogManager = dialogManager;
            _resources = resources1;

            //GradesCView = new ExtendedCollectionView();
            //GradesCView.SortDescriptions.Add(new SortDescription(@"PeriodOrder", ListSortDirection.Ascending));
            //GradesCView.GroupDescriptions.Add(new PropertyGroupDescription(@"PeriodName"));
            GradesCView = new CollectionViewSource();
            gradesGroupName = new ObservableCollection<GroupList>();
            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadGradesAsync);
        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        #endregion

        #region Methods

        public async Task LoadGradesAsync()
        {
            if (_networkHelper.HasInternetAccess())
            {
                gradesGroupName.Clear();
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
               
                var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
                var child = loginResponse.Children[_appSettings.SelectedChildIndex];
                var parameters = new GradeParameters()
                {
                    LanguageId = _appSettings.LanguageId,
                    StudentId = child.UserID,
                    SchoolId = child.School.SchoolID
                };

                var response = await _childrenDataService.GetGradesAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    if (response.Result == null || response.Result.Count == 0)
                    {
                        ErrorMessage = RequestMessage.GetNoDataMessage(); ;
                    }
                    else
                    {
                        gradesGroupName.Clear();
                        var temp = (from grade in response.Result
                                    orderby grade.PeriodOrder
                                    group grade by grade.PeriodName into grouped
                                    select new GroupList(grouped.ToList<Object>()) { Group = grouped.Key.ToString() }).ToList();
                       
                        foreach (GroupList item in temp)
                        {
                            GradesGroupName.Add(item);
                        }
                        // SetGrades(response.Result);
                    }
                }
                else
                {
                    GradesCView.Source = null;
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }
            else
            {
                await _dialogManager.ShowMessage("", _resources.NoInternet);
    }
}

        //public void SetGrades(List<Grade> grades)
        //{ 
            
        //   var temp = (from grade in grades orderby grade.PeriodOrder
        //                         group grade by grade.PeriodName into grouped
        //                         select new GroupList(grouped.ToList<Object>()) { Group = grouped.Key.ToString() }).ToList();
        //    foreach(GroupList item in temp)
        //    {
        //        GradesGroupName.Add(item.Group.ToString());
        //    }
        //    //foreach( var item in gradesGrouped)
        //    //{
        //    //    gradesGroupName.Add(item.Group);
        //    //}
        //    //GradesCView.IsSourceGrouped = true;
        //    //GradesCView.ItemsPath = new PropertyPath("GroupItems");
        //    //GradesCView.Source = gradesGrouped;
        //}

        #endregion
    }
}
