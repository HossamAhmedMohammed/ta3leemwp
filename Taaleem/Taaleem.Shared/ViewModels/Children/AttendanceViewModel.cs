﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Collections;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;
using Windows.UI.Xaml.Data;

namespace Taaleem.ViewModels.Children
{
    public class AttendancesViewModel : BindableBase
    {
        #region Fields
        private readonly IChildrenDataService _childrenDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly Resources _resources;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        private readonly INetworkHelper _networkHelper;
        private readonly IDialogManager _dialogManager;

        private const int daysNumber = 5;
        #endregion

        #region Properties
        private List<string> _weekDaysList;
        /// <summary>
        /// List of days, Sunday,Monday....Thursday
        /// </summary>
        public List<string> WeekDaysList
        {
            get { return _weekDaysList; }
            set { SetProperty(ref _weekDaysList, value); }
        }

        private AttendanceWrapper _selectedAttendanceWrapper;
        public AttendanceWrapper SelectedAttendanceWrapper
        {
            get { return _selectedAttendanceWrapper; }
            set { SetProperty(ref _selectedAttendanceWrapper, value); }
        }


        private List<AttendanceStatusWrapper> _statusTypes;
        /// <summary>
        /// List retrieved from server, Present, Absence .....
        /// </summary>
        public List<AttendanceStatusWrapper> StatusTypes
        {
            get { return _statusTypes; }
            set { SetProperty(ref _statusTypes, value); }
        }

        /// <summary>
        /// This represents the Foramtted attendance data that will be presented in UI.
        /// </summary>
        public CollectionViewSource AttendancesCView { get; set; }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private DateTime _startDate;
        /// <summary>
        /// This must be sunday always
        /// </summary>
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetProperty(ref _startDate, value); }
        }

        private DateTime _endDate;
        /// <summary>
        /// This must be Thursday always
        /// </summary>
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetProperty(ref _endDate, value); }
        }

        #endregion

        #region Initialization

        public AttendancesViewModel(INetworkHelper networkHelper,IDialogManager dialogManager, Resources resources, IChildrenDataService childrenDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService, Resources resources1)
        {
            _childrenDataService = childrenDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _resources = resources1;
            _dialogManager = dialogManager;
            _networkHelper = networkHelper;

            AttendancesCView = new CollectionViewSource();

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadAttendancesAsync);
            PreviousWeekCommand = new AsyncExtendedCommand(PreviousWeekAsync);
            NextWeekCommand = new AsyncExtendedCommand(NextWeekAsync);
            ClearSelectionCommand = new ExtendedCommand(ClearSelection);
            InitializeWeekDaysList();
            AssignCurrentWeekDates();
        }

        private void ClearSelection()
        {
            SelectedAttendanceWrapper = null;
        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        public AsyncExtendedCommand NextWeekCommand { get; set; }
        public AsyncExtendedCommand PreviousWeekCommand { get; set; }
        public ExtendedCommand ClearSelectionCommand { get; set; }
        #endregion

        #region Methods

        public async Task LoadAttendancesAsync()
        {
            if (_networkHelper.HasInternetAccess())
            {
                LoadCommand.CanExecute = false;
                NextWeekCommand.CanExecute = false;
                PreviousWeekCommand.CanExecute = false;
                IsBusy = true;
                ErrorMessage = null;
                SelectedAttendanceWrapper = null;
                AttendancesCView.Source = null;
                try
                {

                    //This will load statuses absence,present... if they are not loaded before
                    //Don't await here so it doesn't block GetStudentSchedulesAsync to wait it's complete.
                    //instead await at the end of try
                    var loadStatusTask = LoadStatusTypesAsync();
                    var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
                    var child = loginResponse.Children[_appSettings.SelectedChildIndex];
                    var parameters = new GetAttendanceScheduleParameters()
                    {
                        LanguageId = _appSettings.LanguageId,
                        StudentId = child.UserID,
                        SchoolId = child.School.SchoolID,
                        StartDate = StartDate,
                        EndDate = EndDate
                    };
                    var response = await _childrenDataService.GetStudentSchedulesAsync(parameters);
                    if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                    {
                        if (response.Result == null || response.Result.Count == 0)
                        {
                            ErrorMessage = RequestMessage.GetNoDataMessage();
                        }
                        else
                        {
                            AssignAttendanceListUsingIndix(response.Result);
                        }
                    }
                    else
                    {
                        ErrorMessage = _messageResolver.ResultToMessage(response);
                    }
                    await loadStatusTask;
                }
                finally
                {
                    LoadCommand.CanExecute = true;
                    NextWeekCommand.CanExecute = true;
                    PreviousWeekCommand.CanExecute = true;
                    IsBusy = false;
                }
            }
            else
            {
                await _dialogManager.ShowMessage("", _resources.NoInternet);
            }
        }

        private async Task<bool> LoadStatusTypesAsync()
        {
            //This prevents reloading status types again if they are loaded
            if (StatusTypes != null) return true;

            var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
            var child = loginResponse.Children[_appSettings.SelectedChildIndex];
            var parameters = new AttendanceStatusParameters()
            {
                LanguageId = _appSettings.LanguageId,
                SchoolId = child.School.SchoolID,
            };
            var response = await _childrenDataService.GetAttendanceStatusesAsync(parameters);
            if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
            {
                if (response.Result != null && response.Result.Count > 0)
                {
                    StatusTypes = response.Result.Select(status => AttendanceStatusWrapper.FromAttendanceStatus(status, _appSettings.LanguageId)).ToList();
                    return true;
                }
            }
            return false;
        }

        public void InitializeWeekDaysList()
        {
            var list = new List<string>();
            if (_appSettings.LanguageId == 1)
            {
                list.Add("Sun");
                list.Add("Mon");
                list.Add("Tue");
                list.Add("Wed");
                list.Add("Thu");
                //list.Add("Fri");
                //list.Add("Sat");
               
            }
            else
            {
                //list.Add("سبت");
                list.Add("احد");
                list.Add("اثنين");
                list.Add("ثلاثاء");
                list.Add("اربعاء");
                list.Add("خميس");
                //list.Add("الجمعة");
            }



            //var info = CultureInfo.DefaultThreadCurrentCulture.DateTimeFormat;
            //var list = new List<string>();
            //for (int index = 0; index < daysNumber; index++)
            //{
            //    var dayName = info.DayNames[index];
            //    if(CultureInfo.DefaultThreadCurrentCulture.Name == "en")
            //    {
            //        list.Add(dayName.Substring(0, 3));
            //    }
            //    else
            //    {
            //        list.Add(dayName);
            //    }
              
            //}
            WeekDaysList = list;
        }

        public void AssignCurrentWeekDates()
        {
            //#if DEBUG
            //            StartDate = new DateTime(2015, 3, 29);
            //            EndDate = new DateTime(2015, 4, 2);
            //#else
            StartDate = DateTime.Now.StartOfWeek(DayOfWeek.Sunday).Date;
            EndDate = StartDate.AddDays(4);
            //#endif

        }


        public async Task<bool> ShowThisWeekButtonVisibility()
        {
          await  Task.Delay(2000);
            if (DateTime.Now.Date >= StartDate && DateTime.Now.Date <= EndDate)
            {
                return false;

            }
            else
            {
                return true;
            }
        }
        private void AssignAttendanceList(List<Attendance> attendanceList)
        {
            int neglectedCount = 0;//For debugging,how many attendances or days have been neglected,It should be zero
            int correctedCount = 0;//For debugging, how many wrong periods have been corrected
            int maxPeriod = attendanceList.Max(attendance => attendance.PeriodNumber);
            maxPeriod = Math.Max(8, maxPeriod);//If maximum is lower than 8, set to 8, So view won't appear empty

            //to keep track of last index
            List<int> indices = new List<int>();
            //Build matrix of period as rows and days as columns
            //Intitialize all cells with dummy No Period cell
            var weekAttendances = new List<KeyedList<int, AttendanceWrapper>>();
            for (int index = 0; index < maxPeriod; index++)
            {
                var periodList = new KeyedList<int, AttendanceWrapper>() { Key = index + 1 };
                for (int dayIndex = 0; dayIndex < daysNumber; dayIndex++)
                {
                    periodList.Add(AttendanceWrapper.GetDummyWrapper(_appSettings.LanguageId));
                }
                weekAttendances.Add(periodList);
            }

            //Fill Data
            foreach (var attendance in attendanceList)
            {
                //We will start week by sunday
                //Enum value of sunday is zero, and increased by one for other days,Saturday is 6
                //Note Also that Date parsed from server as Invariant culture
                var date = attendance.AttendanceDate.Date;
                var dateIndex = date.DayOfWeek - DayOfWeek.Sunday;
                if (dateIndex >= daysNumber)
                {
                    neglectedCount++;
                    continue;//ignore friday and saturday
                }
                int periodIndex = attendance.PeriodNumber - 1;

                if (periodIndex < 0)
                {
                    neglectedCount++;
                    continue;
                }

                //Put the attendance in the correct indices
                //But take care there is a bug in server that returns Last 3 periods on Wednesday as perdiod 5
                //To correct this behavior, While loop will check if there is a value in this bucket
                //If wasn't dummy then it will assign it's value, else it will increment period id to correct it.
                //Example two attendances with 5,5, Will be corrected to 5,6, 
                //It will check also that it doesn't exceed boundaries
                while (periodIndex < weekAttendances.Count && !weekAttendances[periodIndex][dateIndex].IsDummy)
                {
                    periodIndex++;
                }

                if (periodIndex >= weekAttendances.Count)
                {
                    neglectedCount++;
                    continue;//Exceeded length
                }
                else if (periodIndex != attendance.PeriodNumber - 1)
                {
                    correctedCount++;
                }

                weekAttendances[periodIndex][dateIndex] = AttendanceWrapper.FromAttendance(attendance, _appSettings.LanguageId);
            }

            FillVacationInDays(weekAttendances);
            Debug.WriteLine(@"Neglected: " + neglectedCount);
            Debug.WriteLine(@"Corrected: " + correctedCount);
            AttendancesCView.Source = weekAttendances;
            AttendancesCView.IsSourceGrouped = true;
        }

        /// <summary>
        /// To be like Android and I Phone
        /// </summary>
        /// <param name="attendanceList"></param>
        private void AssignAttendanceListUsingIndix(List<Attendance> attendanceList)
        {
            int neglectedCount = 0;//For debugging,how many attendances or days have been neglected,It should be zero
            int correctedCount = 0;//For debugging, how many wrong periods have been corrected
            int maxPeriod = attendanceList.Max(attendance => attendance.PeriodNumber);
            maxPeriod = Math.Max(8, maxPeriod);//If maximum is lower than 8, set to 8, So view won't appear empty


            //Build matrix of period as rows and days as columns
            //Intitialize all cells with dummy No Period cell
            var weekAttendances = new List<KeyedList<int, AttendanceWrapper>>();
            for (int index = 0; index < maxPeriod; index++)
            {
                var periodList = new KeyedList<int, AttendanceWrapper>() { Key = index + 1 };
                for (int dayIndex = 0; dayIndex < daysNumber; dayIndex++)
                {
                    periodList.Add(AttendanceWrapper.GetDummyWrapper(_appSettings.LanguageId));
                }
                weekAttendances.Add(periodList);
            }

            //initializeIndices
            var dayIndices = new Dictionary<int, int>();
            for (int dayIndex = 0; dayIndex < daysNumber; dayIndex++)
            {
                dayIndices[dayIndex] = 0;
            }

            //Fill Data
            foreach (var attendance in attendanceList)
            {
                //We will start week by sunday
                //Enum value of sunday is zero, and increased by one for other days,Saturday is 6
                //Note Also that Date parsed from server as Invariant culture
                var date = attendance.AttendanceDate.Date;
                var dateIndex = date.DayOfWeek - DayOfWeek.Sunday;
                if (dateIndex >= daysNumber)
                {
                    neglectedCount++;
                    continue;//ignore friday and saturday
                }
                int periodIndex = dayIndices[dateIndex];

                if (periodIndex < 0)
                {
                    neglectedCount++;
                    continue;
                }

                if (periodIndex >= weekAttendances.Count)
                {
                    neglectedCount++;
                    continue;//Exceeded length
                }
                else if (periodIndex != attendance.PeriodNumber - 1)
                {
                    correctedCount++;
                }

                weekAttendances[periodIndex][dateIndex] = AttendanceWrapper.FromAttendance(attendance, _appSettings.LanguageId);
                dayIndices[dateIndex] = dayIndices[dateIndex] + 1;
            }

            FillVacationInDays(weekAttendances);
            Debug.WriteLine(@"Neglected: " + neglectedCount);
            Debug.WriteLine(@"Corrected: " + correctedCount);
            AttendancesCView.Source = weekAttendances;
            AttendancesCView.IsSourceGrouped = true;
        }

        /// <summary>
        /// This will responsible for filling all periods of a day that have only one period as vacation
        /// </summary>
        /// <param name="weekAttendances"></param>
        private void FillVacationInDays(List<KeyedList<int, AttendanceWrapper>> weekAttendances)
        {
            for (int dayIndex = 0; dayIndex < weekAttendances[0].Count; dayIndex++)
            {
                //Check if first period is vacation
                if (weekAttendances[0][dayIndex] != null &&
                    weekAttendances[0][dayIndex].AttendanceStatusCode == AttendanceStatusType.Vacation)
                {
                    //then fill all period of this day with vacation
                    for (int periodIndex = 1; periodIndex < weekAttendances.Count; periodIndex++)
                    {
                        weekAttendances[periodIndex][dayIndex] = weekAttendances[0][dayIndex];
                    }
                }
            }
        }


        private async Task NextWeekAsync()
        {
            StartDate = StartDate.AddDays(7);
            EndDate = EndDate.AddDays(7);
            await LoadAttendancesAsync();
        }

        private async Task PreviousWeekAsync()
        {
            StartDate = StartDate.AddDays(-7);
            EndDate = EndDate.AddDays(-7);
            await LoadAttendancesAsync();
        }

        #endregion
    }
}
