﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Common;
using Taaleem.Helpers;
using Taaleem.Models.Children.Response;

namespace Taaleem.ViewModels.Children
{
    public class BehaviorDetailsViewModel : BindableBase
    {
        private readonly Resources _resources;
        #region Properties
        private Behavior _behavior;

        public BehaviorDetailsViewModel(Resources resources)
        {
            _resources = resources;
        }

        public Behavior Behavior
        {
            get { return _behavior; }
            set
            {
                if (SetProperty(ref _behavior, value))
                {
                }
            }
        }

        public string BehaviorTypeName
        {
            get
            {
                return Behavior == null || String.IsNullOrWhiteSpace(Behavior.BehaviorTypeName) ? _resources.NoData : Behavior.BehaviorTypeName;
            }
        }
        public string Description
        {
            get
            {
                return Behavior == null || String.IsNullOrWhiteSpace(Behavior.Description) ? _resources.NoDescription : Behavior.Description;
            }
        }
        public string Consequences
        {
            get
            {
                return Behavior == null || String.IsNullOrWhiteSpace(Behavior.Consequences) ? _resources.NoConsequences : Behavior.Consequences;
            }
        }
        public string Comments
        {
            get
            {
                return Behavior == null || String.IsNullOrWhiteSpace(Behavior.Comments) ? _resources.NoComments : Behavior.Comments;
            }
        }
        #endregion

    }
}
