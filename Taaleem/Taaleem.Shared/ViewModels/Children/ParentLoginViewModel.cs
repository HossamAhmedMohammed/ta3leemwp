﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children;
using Taaleem.Models.Children.Request;
using Taaleem.Views.Children;
using Windows.UI.Xaml.Controls;

namespace Taaleem.ViewModels.Children
{
   public class ParentLoginViewModel : BindableBase
    {
        #region Fields
        private readonly Resources _resources;
        private readonly IChildrenDataService _childrenDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly IAppSettings _appSettings;
        private readonly INavigationService _navigationService;
        private readonly INetworkHelper _networkHelper;
        private readonly IDialogManager _dialogManager;
        #endregion

        #region Properties
       


        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value);
                OnPropertyChanged();
            }
        }

        private String _userName;
        public String UserName
        {
            get { return _userName; }
            set
            {
                if (SetProperty(ref _userName, value))
                {
                    LoginCommand.CanExecute = !String.IsNullOrWhiteSpace(UserName) && !String.IsNullOrWhiteSpace(Password);
                }
            }
        }
        private String parentName;
        public String ParentName
        {
            get { return parentName; }
            set { parentName = value;
                OnPropertyChanged();
            }
        }
        private String _password;
        public String Password
        {
            get { return _password; }
            set
            {
                if (SetProperty(ref _password, value))
                {
                    LoginCommand.CanExecute = !String.IsNullOrWhiteSpace(UserName) && !String.IsNullOrWhiteSpace(Password);
                }
            }
        }


        private RequestMessage _errorMessage;

        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private List<ParentMenuItem> parentMenu;

        public List<ParentMenuItem> ParentMenu
        {
            get { return parentMenu; }
            set { SetProperty(ref parentMenu, value); }
        }
        private string pageTitle;

        public string PageTitle
        {
            get { return pageTitle; }
            set
            {
                pageTitle = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Initialization

        public ParentLoginViewModel(Resources resources, IChildrenDataService childrenDataService,
                                   IRequestMessageResolver messageResolver, IAppSettings appSettings, 
                                   INavigationService navigationService, INetworkHelper networkHelper, IDialogManager dialogManager)
        {
            _resources = resources;
            _childrenDataService = childrenDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _networkHelper = networkHelper;
            _dialogManager = dialogManager;
            parentMenu = new List<ParentMenuItem>();
            //Initialize Commands
            LoginCommand = new AsyncExtendedCommand(LoginAsync, false);
            ItemClickedCommand = new ExtendedCommand<ItemClickEventArgs>(NavigateToPage);
            //var credentials = _appSettings.ParentCredential;
            LoadParentMenuItems();
            loadParentData();
            Logout = new ExtendedCommand(logoutMethod);
#if DEBUG

            if (_appSettings.AmIParent)
            {
                UserName = "a.alsayed";
                Password = "A123456a";
            }
            else
            {
                UserName = "M.abdelmagid0807";
                Password = "M@12345";
            }
            //UserName = "D.parent20";
            //Password = "Sec.12345";
            //  callLoginPage();
#endif
           // callLoginPage();
        }

      

        //private async void callLoginPage()
        //{
        //    await LoginAsync();
        //}

        #endregion

        #region Commands
        public AsyncExtendedCommand LoginCommand { get; set; }
       
       
        public ExtendedCommand<ItemClickEventArgs> ItemClickedCommand { get; set; }
        public ExtendedCommand Logout { get; set; }

        #endregion

        #region Methods
        public void LoadParentMenuItems()
        {
            parentMenu.Clear();
            parentMenu.Add(new ParentMenuItem { Image = "ms-appx:///Assets/Parent/menu/parent_hw_ic@3x.png", Name = _resources.HomeWork });
            parentMenu.Add(new ParentMenuItem { Image = "ms-appx:///Assets/Parent/menu/parent_attendance_ic@3x.png", Name = _resources.Attendance });
            parentMenu.Add(new ParentMenuItem { Image = "ms-appx:///Assets/Parent/menu/parent_grades_ic@3x.png", Name = _resources.Grades});
            if (_appSettings.AmIParent)
            {
                parentMenu.Add(new ParentMenuItem { Image = "ms-appx:///Assets/Parent/menu/parent_behave_ic@3x.png", Name = _resources.Behavior });
            }
            parentMenu.Add(new ParentMenuItem { Image = "ms-appx:///Assets/Parent/menu/parent_ads_ic@3x.png", Name = _resources.Announcements });
            parentMenu.Add(new ParentMenuItem { Image = "ms-appx:///Assets/Parent/menu/parent_teachers_ic@3x.png", Name = _resources.Teachers });
            //parentMenu.Add(new ParentMenuItem { Image = "ms-appx:///Assets/Parent/menu/parent_replace_ic@3x.png", Name =_resources.StudentTransfer });
        }
        public async Task LoginAsync()
        {
            LoginCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                int roleID = 0;
                if (_appSettings.AmIParent)
                    roleID = 3;
                else
                    roleID = 1;
                var parameters = new ChildLoginParameters { LanguageId = _appSettings.LanguageId, RoleId = roleID };
                var response = await _childrenDataService.LoginAsync(parameters, UserName, Password);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (_appSettings.AmIParent)
                    {
                        _appSettings.SetParentCredential(UserName, Password);
                        var parentResponse = await ParentLoginWrapper.WrapAsync(response.Result);
                       
                        ParentName = parentResponse.Children[0].Lastname;
                        await _appSettings.SetParentLoginResponseAsync(parentResponse);
                        //ViewModelLocator.Locator.MainMenuVM.CurrentMenuItem.Name = "خدمات الاباء";
                        ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = new Models.MainMenuItem { Name = "خدمات الاباء", Image = "" };
                    }
                    else if(!_appSettings.AmIParent)
                    {
                        _appSettings.SetStudentCredential(UserName, Password);
                        var studentResponse = await ParentLoginWrapper.WrapAsync(response.Result);
                        ParentName = studentResponse.Children[0].DisplayName;
                        
                        await _appSettings.SetStudentLoginResponseAsync(studentResponse);
                        await _appSettings.GetStudentLoginResponseAsync();
                        ViewModelLocator.Locator.MainMenuVM.CurrentMenuItem.Name = "خدمات الطلاب";
                        ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = new Models.MainMenuItem { Name = "خدمات الطلاب", Image = "" };
                    }
                    _navigationService.NavigateByPage<ParentMenuPage>();
                    ViewModelLocator.Locator.DialogManager.CloseDialog();
                }
                else if (response.ResponseStatus == ResponseStatus.HttpError && response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    ErrorMessage = new RequestMessage(_resources.FailedToLogin);
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoginCommand.CanExecute = true;
                IsBusy = false;
            }
        }
        private void logoutMethod()
        {
           if(_appSettings.AmIParent)
            {
                _appSettings.SetParentLoginResponseAsync(null);
                _appSettings.RemoveParentCredential();
                _appSettings.IsParentMode = false;
                _appSettings.AmIParent = false;
            }
           else if(!_appSettings.AmIParent)
            {
                _appSettings.SetStudentLoginResponseAsync(null);
                _appSettings.RemoveStudentCredential();
                _appSettings.IsStudentMode = false;
                _appSettings.AmIParent = false;
            }
            UserName = "";
            Password = "";
            _navigationService.NavigateByPage<Home>();
        }

        public async void loadParentData()
        {
            ParentLoginWrapper p = await _appSettings.GetParentLoginResponseAsync();
            ParentLoginWrapper s = await _appSettings.GetStudentLoginResponseAsync();
            if (p != null && _appSettings.AmIParent)
            {
                if (_appSettings.IsParentMode)
                {
                    ParentName = p.Children[0].Lastname;
                     PageTitle = _resources.ParentsService;
                }
            }
            else if (s!=null && !_appSettings.AmIParent)
            { 
             if (_appSettings.IsStudentMode)
            {
                ParentName = s.Children[0].DisplayName;
                PageTitle = _resources.StudentService;
            }
            }
        }
        private async void NavigateToPage(ItemClickEventArgs obji)
        {
            ParentMenuItem obj = obji.ClickedItem as ParentMenuItem;
            if (_networkHelper.HasInternetAccess())
            {
                if (obj.Name == _resources.HomeWork)
                {
                    _navigationService.NavigateByPage<Assignments>();
                }
                if (obj.Name == _resources.Attendance)
                {
                    _navigationService.NavigateByPage<Attendance>();
                }
                if (obj.Name == _resources.Grades)
                {
                    _navigationService.NavigateByPage<Grades>();
                }
                if (obj.Name == _resources.Behavior)
                {
                    _navigationService.NavigateByPage<Behavior>();
                }
                if (obj.Name == _resources.Announcements)
                {
                    _navigationService.NavigateByPage<Adds>();
                }
                if (obj.Name == _resources.Teachers)
                {
                    _navigationService.NavigateByPage<TeachersView>();
                }
            }
            else
            {
                await _dialogManager.ShowMessage("", _resources.NoInternet);
                
            }
        }

        #endregion
    }
}
