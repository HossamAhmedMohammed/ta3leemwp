﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;
using Taaleem.Views.Children;

namespace Taaleem.ViewModels.Children
{
    public class TeachersViewModel : BindableBase
    {
        #region Fields
        private readonly Resources _resources;
        private readonly IChildrenDataService _childrenDataService;
        private readonly INavigationService _navigationService;
        
        private readonly IRequestMessageResolver _messageResolver;
        private readonly IAppSettings _appSettings;
        #endregion

        #region Properties

        private List<ContactTeacher> _teachers;
        public List<ContactTeacher> Teachers
        {
            get { return _teachers; }
            set { SetProperty(ref _teachers, value); }
        }


        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }


        #endregion

        #region Initialization

        public TeachersViewModel(Resources resources, IChildrenDataService childrenDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService)
        {
            _resources = resources;
            _childrenDataService = childrenDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadTeachersAsync);
            ItemClickedCommand = new AsyncExtendedCommand<ContactTeacher>(ItemClickedAsync);
        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        public AsyncExtendedCommand<ContactTeacher> ItemClickedCommand { get; set; }
        #endregion

        #region Methods

        public async Task LoadTeachersAsync()
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
                var child = loginResponse.Children[_appSettings.SelectedChildIndex];
                var parameters = new ContactTeacherParameters()
                {
                    LanguageId = _appSettings.LanguageId,
                    StudentId = child.UserID,
                    SchoolId = child.School.SchoolID
                };

                var response = await _childrenDataService.GetStudentTeachersAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    Teachers = response.Result;
                }

                else
                {
                    Teachers = null;
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }

        private async Task ItemClickedAsync(ContactTeacher contactTeacher)
        {
            // await _dialogManager.ShowContactTeacherDialog(contactTeacher);
            _navigationService.NavigateByPage<SpeakToTeacher>(contactTeacher);
        }
        #endregion
    }
}
