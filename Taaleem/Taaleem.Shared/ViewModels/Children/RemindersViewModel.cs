﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Collections.Extended;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;

namespace Taaleem.ViewModels.Children
{
    public class RemindersViewModel : BindableBase
    {
        #region Fields
        private readonly IChildrenDataService _childrenDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly Resources _resources;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        #endregion

        #region Properties

        private List<Reminder> _reminders;
        public List<Reminder> Reminders
        {
            get { return _reminders; }
            set { SetProperty(ref _reminders, value); }
        }

        public ExtendedCollectionView RemindersCView { get; set; }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }


        #endregion

        #region Initialization

        public RemindersViewModel(Resources resources, IChildrenDataService childrenDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService, Resources resources1)
        {
            _childrenDataService = childrenDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _resources = resources1;

            RemindersCView = new ExtendedCollectionView();
            //  RemindersCView.SortDescriptions.Add(new SortDescription(@"ReminderType", ListSortDirection.Ascending));
            RemindersCView.GroupDescriptions.Add(new PropertyGroupDescription(@"ReminderType"));
            RemindersCView.Filter = item =>
            {
                var reminder = item as Reminder;
                if (reminder != null && reminder.ReminderType.Id == ReminderType.PreviouslyCategoryType.Id)
                    return false;
                return true;
            };
            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadRemindersAsync);
        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        #endregion

        #region Methods

        public async Task LoadRemindersAsync()
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
                var child = loginResponse.Children[_appSettings.SelectedChildIndex];
                var parameters = new ReminderParameters()
                {
                    LanguageId = _appSettings.LanguageId,
                    StudentId = child.UserID,
                    SchoolId = child.School.SchoolID
                };

                var response = await _childrenDataService.GetStudentRemindersAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    var sortedList = response.Result
                        .OrderBy(item => item.ReminderType).ToList();
                    RemindersCView.Source = sortedList;
                }
                else
                {
                    RemindersCView.Source = null;
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }

        #endregion
    }
}
