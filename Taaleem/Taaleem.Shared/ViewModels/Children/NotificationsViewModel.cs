﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.Dao.Children;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children;
using Taaleem.Models.Children.Database;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;

namespace Taaleem.ViewModels.Children
{
    public class NotificationsViewModel : BindableBase
    {
        #region Fields
        private readonly Resources _resources;
        private readonly IChildrenDataService _childrenDataService;
       
        private readonly IRequestMessageResolver _messageResolver;
        private readonly IAppSettings _appSettings;
        private readonly FeedsDao _feedsDao;
        /// <summary>
        /// This will be used for fast lookup O(1) on the status of any feed
        /// </summary>
        private readonly HashSet<int> _favouriteIdDictionary = new HashSet<int>();
        #endregion

        #region Properties

        private readonly ObservableCollection<Feed> _feeds = new ObservableCollection<Feed>();
        /// <summary>
        /// This collection represents feeds without any search criteria.
        /// </summary>
        public ObservableCollection<Feed> Feeds
        {
            get { return _feeds; }
        }

        private readonly ObservableCollection<Feed> _favouriteFeeds = new ObservableCollection<Feed>();
        /// <summary>
        /// This collection represents favourited feeds list.
        /// </summary>
        public ObservableCollection<Feed> FavouriteFeeds
        {
            get { return _favouriteFeeds; }
        }

        private RequestMessage _favuritesFeedsMessage;

        public RequestMessage FavuritesFeedsMessage
        {
            get { return _favuritesFeedsMessage; }
            set { SetProperty(ref _favuritesFeedsMessage, value); }
        }
        private bool isBusy;

        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

      
        private readonly ObservableCollection<Feed> _matchedFeeds = new ObservableCollection<Feed>();
        /// <summary>
        /// This collection represents matched results returned from server.
        /// </summary>
        public ObservableCollection<Feed> MatchedFeeds
        {
            get { return _matchedFeeds; }
        }

        //For Search
        private List<FeedCategoryWrapper> _allCategories;
        /// <summary>
        /// This collection represents all search categories, User can select some or all of this categories.
        /// </summary>
        public List<FeedCategoryWrapper> AllCategories
        {
            get { return _allCategories; }
            set { SetProperty(ref _allCategories, value); }
        }

        private readonly List<FeedCategoryWrapper> _selectedFeedCategories = new List<FeedCategoryWrapper>();
        /// <summary>
        /// This collection represents selected search categories, Selected by user.
        /// </summary>
        public List<FeedCategoryWrapper> SelectedFeedCategories
        {
            get { return _selectedFeedCategories; }
        }

        private bool _isLoadingAllFeeds;
        /// <summary>
        /// This boolean is used with loading all feeds indicator
        /// </summary>
        public bool IsLoadingAllFeeds
        {
            get { return _isLoadingAllFeeds; }
            set { SetProperty(ref _isLoadingAllFeeds, value); }
        }

        private RequestMessage _feedsErrorMessage;
        /// <summary>
        /// This string used to represent error if exists with Feeds error.
        /// </summary>
        public RequestMessage FeedsErrorMessage
        {
            get { return _feedsErrorMessage; }
            set { SetProperty(ref _feedsErrorMessage, value); }
        }

        //Searching related Progress and error properties

        private bool _isLoadingCategories;

        public bool IsLoadingCategories
        {
            get { return _isLoadingCategories; }
            set { SetProperty(ref _isLoadingCategories, value); }
        }

        private RequestMessage _loadingCategoriesMessage;

        public RequestMessage LoadingCategoriesMessage
        {
            get { return _loadingCategoriesMessage; }
            set { SetProperty(ref _loadingCategoriesMessage, value); }
        }

        private bool _isSearchingFeeds;
        /// <summary>
        /// This boolean is used with loading all feeds indicator
        /// </summary>
        public bool IsSearchingFeeds
        {
            get { return _isSearchingFeeds; }
            set { SetProperty(ref _isSearchingFeeds, value); }
        }

        private RequestMessage _searchErrorMessage;
        /// <summary>
        /// This string used to represent error if exists with Feeds error.
        /// </summary>
        public RequestMessage SearchErrorMessage
        {
            get { return _searchErrorMessage; }
            set { SetProperty(ref _searchErrorMessage, value); }
        }

        #endregion

        #region Initialization

        public NotificationsViewModel(Resources resources, IChildrenDataService childrenDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, FeedsDao feedsDao)
        {
            _resources = resources;
            _childrenDataService = childrenDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
          
            _feedsDao = feedsDao;

            FavuritesFeedsMessage = RequestMessage.GetNoDataMessage();//initially before any loading
            _favouriteFeeds.CollectionChanged += (s, args) =>
            {
                //when favourite collection change
                FavuritesFeedsMessage = _favouriteFeeds.Count == 0 ? RequestMessage.GetNoDataMessage() : null;
            };

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadInitialFeedsAsync);
            LoadCategoriesCommand = new AsyncExtendedCommand(LoadCategoriesAsync);
            SearchCommand = new AsyncExtendedCommand(SearchFeedsAsync);
            ToggleFavouriteCommand = new ExtendedCommand<Feed>(ToggleFavourite);
        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        public AsyncExtendedCommand LoadCategoriesCommand { get; set; }
        public AsyncExtendedCommand SearchCommand { get; set; }
        public ExtendedCommand<Feed> ToggleFavouriteCommand { get; set; }
      
        #endregion

        #region Methods

        #region loading all Feeds methods
        public async Task LoadInitialFeedsAsync()
        {
            IsBusy = true;
            LoadCommand.CanExecute = false;
            IsLoadingAllFeeds = true;
            FeedsErrorMessage = null;
            Feeds.Clear();
            try
            {
                var response = await GetFeedsByMethodAsync(GetFeedsMethod.Feeds, 0);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    foreach (var feed in response.Result)
                    {
                        AssignIsFavouriteValue(feed);
                        Feeds.Add(feed);
                    }
                }
                else
                {
                    Feeds.Clear();
                    FeedsErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            catch
            {
                FeedsErrorMessage = new RequestMessage(_resources.ClientSideError);
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsLoadingAllFeeds = false;
                IsBusy = false;
            }
        }

        public async Task LoadMoreFeedsAsync()
        {
            if (IsLoadingAllFeeds) return;
            try
            {
                LoadCommand.CanExecute = false;
                var response =
                    await
                        GetFeedsByMethodAsync(GetFeedsMethod.MoreFeeds, Feeds.Count == 0 ? 0 : Feeds[Feeds.Count - 1].Id);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    foreach (var feed in response.Result)
                    {
                        AssignIsFavouriteValue(feed);
                        Feeds.Add(feed);
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                LoadCommand.CanExecute = true;
            }
        }
        #endregion

        #region Search methods

        public async Task<bool> LoadCategoriesAsync()
        {
            //This prevents reloading AllCategories again if they are loaded
            if (AllCategories != null) return true;
            try
            {
                IsBusy = true;
                LoadCategoriesCommand.CanExecute = false;
                IsLoadingCategories = true;
                LoadingCategoriesMessage = null;
                var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
                var child = loginResponse.Children[_appSettings.SelectedChildIndex];
                var parameters = new FeedCategoryParameters()
                {
                    LanguageId = _appSettings.LanguageId,
                    SchoolId = child.School.SchoolID,
                };
                var response = await _childrenDataService.GetFeedCategoriesAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result != null && response.Result.Count > 0)
                    {
                        AllCategories =
                            response.Result.Select(x => FeedCategoryWrapper.Wrap(x, _appSettings.LanguageId)).ToList();
                        IsLoadingCategories = false;
                        return true;
                    }
                }
                else
                {
                    LoadingCategoriesMessage = _messageResolver.ResultToMessage(response);
                }
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                IsLoadingCategories = false;
                LoadCategoriesCommand.CanExecute = true;
                IsBusy = false;
            }

            return false;
        }

        public async Task SearchFeedsAsync()
        {
            LoadCommand.CanExecute = false;
            IsSearchingFeeds = true;
            SearchErrorMessage = null;
            MatchedFeeds.Clear();
            try
            {
                var response = await GetFeedsByMethodAsync(GetFeedsMethod.Feeds, 0, true);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    foreach (var feed in response.Result)
                    {
                        AssignIsFavouriteValue(feed);
                        MatchedFeeds.Add(feed);
                    }
                }
                else
                {
                    MatchedFeeds.Clear();
                    SearchErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            catch
            {
                // ignored
            }
            finally
            {
                SearchCommand.CanExecute = true;
                IsSearchingFeeds = false;

            }
        }

        public async Task SearchMoreFeedsAsync()
        {
            if (IsSearchingFeeds) return;
            SearchCommand.CanExecute = false;
            var response = await GetFeedsByMethodAsync(GetFeedsMethod.MoreFeeds, Feeds.Count == 0 ? 0 : Feeds[Feeds.Count - 1].Id, true);
            if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
            {
                foreach (var feed in response.Result)
                {
                    AssignIsFavouriteValue(feed);
                    MatchedFeeds.Add(feed);
                }
            }
            SearchCommand.CanExecute = true;
        }
        #endregion

        #region Get feeds by 'FeedMethod' helpers
        private async Task<RequestResponse<List<Feed>>> GetFeedsByMethodAsync(GetFeedsMethod feedsMethod, int feedId, bool includeFilter = false)
        {
            var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
            var child = loginResponse.Children[_appSettings.SelectedChildIndex];
            var parameters = new FeedParameters
            {
                LanguageType = _appSettings.LanguageId,
                ChildId = child.UserID,
                SchoolId = child.School.SchoolID,
                FeedId = feedId,
                FeedCount = 20
            };
            if (includeFilter && SelectedFeedCategories != null && SelectedFeedCategories.Count > 0) parameters.FilterCategoryIds = SelectedFeedCategories.Select(x => x.Id).ToList();
            var response = await _childrenDataService.GetFeedsAsync(feedsMethod, parameters);
            if (response.Result != null && response.Result.Count > 0)
            {
                foreach (var feed in response.Result)
                {
                    feed.StudentId = child.UserID;
                }
            }
            return response;
        }
        #endregion

        #region Favourite methods

        public async Task LoadFavouritesAsync()
        {
            try
            {
                if (_favouriteIdDictionary.Count == 0)
                {
                    var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
                    var child = loginResponse.Children[_appSettings.SelectedChildIndex];
                    var feedsDataList = await _feedsDao.GetFavouritedFeedsDataAsync(child.UserID);
                    foreach (FeedData feedData in feedsDataList)
                    {
                        if (!_favouriteIdDictionary.Contains(feedData.ServerId))
                        {
                            _favouriteIdDictionary.Add(feedData.ServerId);
                            FavouriteFeeds.Add(new Feed(feedData));
                        }
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private void ToggleFavourite(Feed feed)
        {
            if (feed == null)
            {
                Debug.WriteLine("feed is null");
                return;
            }
            try
            {

                if (feed.IsFavourite)
                {
                    //Search for index in favourites to remove it
                    feed.IsFavourite = false;
                    var foundIndex = FindFavouriteIndex(feed);
                    if (foundIndex != -1) FavouriteFeeds.RemoveAt(foundIndex);
                    _favouriteIdDictionary.Remove(feed.Id);
                    _feedsDao.DeleteFeedServerId(feed.Id);

                    //Update first matched feed in All Feeds Section
                    var foundFeed = Feeds.FirstOrDefault(f => f.Id == feed.Id);
                    if (foundFeed != null) foundFeed.IsFavourite = false;
                    //Update first matched feed in Search Feeds Section
                    var searchFeed = MatchedFeeds.FirstOrDefault(f => f.Id == feed.Id);
                    if (searchFeed != null) searchFeed.IsFavourite = false;
                }
                else
                {
                    if (!_favouriteIdDictionary.Contains(feed.Id))
                    {
                        //Update first matched feed in All Feeds Section
                        var foundFeed = Feeds.FirstOrDefault(f => f.Id == feed.Id);
                        if (foundFeed != null) foundFeed.IsFavourite = true;
                        //Update first matched feed in Search Feeds Section
                        var searchFeed = MatchedFeeds.FirstOrDefault(f => f.Id == feed.Id);
                        if (searchFeed != null) searchFeed.IsFavourite = true;

                        FavouriteFeeds.Add(feed);
                        _favouriteIdDictionary.Add(feed.Id);
                        _feedsDao.AddFeed(new FeedData(feed));
                    }
                }
            }
            catch(Exception e)
            {
                // ignored
            }

        }

        private void AssignIsFavouriteValue(Feed feed)
        {
            if (_favouriteIdDictionary.Contains(feed.Id))
                feed.IsFavourite = true;
        }

        private int FindFavouriteIndex(Feed feed)
        {
            int foundIndex = -1;
            for (int index = 0; index < FavouriteFeeds.Count; index++)
            {
                if (feed.Id == FavouriteFeeds[index].Id)
                {
                    foundIndex = index;
                    break;
                }
            }
            return foundIndex;
        }

        #endregion

        #endregion
    }
}
