﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;
using Taaleem.Views.Children;

namespace Taaleem.ViewModels.Children
{
    public class AnnouncementsViewModel : BindableBase
    {
        #region Fields
        private readonly IChildrenDataService _childrenDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        #endregion

        #region Properties

        private List<Announcement> _announcements;
        public List<Announcement> Announcements
        {
            get { return _announcements; }
            set { SetProperty(ref _announcements, value); }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }


        #endregion

        #region Initialization

        public AnnouncementsViewModel(Resources resources, IChildrenDataService childrenDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _childrenDataService = childrenDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadAnnouncementsAsync);
            ItemClickedCommand = new ExtendedCommand<Announcement>(ItemClicked);
        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        public ExtendedCommand<Announcement> ItemClickedCommand { get; set; }
        #endregion

        #region Methods

        public async Task LoadAnnouncementsAsync()
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var loginResponse =  _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync(): await _appSettings.GetStudentLoginResponseAsync();
                var child = loginResponse.Children[_appSettings.SelectedChildIndex];
                var parameters = new AnnouncementParameters()
                {
                    LanguageId = _appSettings.LanguageId,
                    StudentId = child.UserID,
                    SchoolId = child.School.SchoolID
                };

                var response = await _childrenDataService.GetAnnouncementsAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    Announcements = response.Result;
                }
                else
                {
                    Announcements = null;
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }

        private void ItemClicked(Announcement announcement)
        {
          _navigationService.NavigateByPage<AddsDetails>(announcement);
        }
        #endregion
    }
}

