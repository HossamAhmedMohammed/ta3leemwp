﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;

namespace Taaleem.ViewModels.Children
{
    public class AssignmentsViewModel : BindableBase
    {
        #region Fields
        private readonly IChildrenDataService _childrenDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        private readonly INetworkHelper _networkHelper;
        private readonly IDialogManager _dialogManager;
        private readonly Resources _resources;
        #endregion

        #region Properties
        private readonly IReadOnlyList<AssignmentCategory> _assignmentCategories;
        public IReadOnlyList<AssignmentCategory> AssignmentCategories
        {
            get { return _assignmentCategories; }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;

        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }
        #endregion

        #region Initialization

        public AssignmentsViewModel(Resources resources, IChildrenDataService childrenDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService, INetworkHelper networkService,IDialogManager dialogManager)
        {
            _childrenDataService = childrenDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _networkHelper = networkService;
            _dialogManager = dialogManager;
            _resources = resources;

            var categoryTypes =  AssignmentCategoryType.LoadCategories();
            var categories = new List<AssignmentCategory>(categoryTypes.Count);
            foreach (var categoryType in categoryTypes)
            {
                categories.Add(new AssignmentCategory(categoryType));
            }
            _assignmentCategories = new ReadOnlyCollection<AssignmentCategory>(categories);

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadAssignmentsAsync);
            ItemClickedCommand = new ExtendedCommand<Assignment>(ItemClicked);

           // callLoadAssigments();
        }

        private async void callLoadAssigments()
        {
         await   LoadAssignmentsAsync();
        }


        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        public ExtendedCommand<Assignment> ItemClickedCommand { get; set; }
        #endregion

        #region Methods

        public async Task LoadAssignmentsAsync()
        {
            if (_networkHelper.HasInternetAccess())
            {
                LoadCommand.CanExecute = false;
                IsBusy = true;
                ErrorMessage = null;
                try
                {
                    var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
                    var child = loginResponse.Children[_appSettings.SelectedChildIndex];
                    var parameters = new AssignmentParameters()
                    {
                        LanguageId = _appSettings.LanguageId,
                        StudentId = child.UserID,
                        SchoolId = child.School.SchoolID
                    };

                    var response = await _childrenDataService.GetAssignmentsAsync(parameters);
                    if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                    {
                        SetAssignments(response.Result);
                    }
                    else
                    {
                        ClearAssignments();
                        ErrorMessage = _messageResolver.ResultToMessage(response);
                    }
                }
                finally
                {
                    LoadCommand.CanExecute = true;
                    IsBusy = false;
                }
            }
            else
            {
              await  _dialogManager.ShowMessage("", _resources.NoInternet);
            }
        }

        private void ClearAssignments()
        {
            foreach (var assignmentCategory in AssignmentCategories)
            {
                assignmentCategory.Assignments.Clear();
            }
        }

        public void SetAssignments(List<Assignment> assignments)
        {
            var dictionary = new Dictionary<int, List<Assignment>>();
            if (assignments != null)
            {
                var query = (from assignment in assignments group assignment by assignment.Category);
                foreach (var group in query)
                {
                    dictionary[group.Key] = group.ToList();
                }
            }
            foreach (var assignmentCategory in AssignmentCategories)
            {
                int key = assignmentCategory.CategoryType.Id;
                assignmentCategory.Assignments.Clear();
               
                
                if (dictionary.ContainsKey(key))
                {
                    var list = dictionary[key];
                    for (int index = 0; index < list.Count; index++)
                    {
                        var assignment = list[index];
                        assignment.CategoryType = assignmentCategory.CategoryType;
                        assignmentCategory.Assignments.Add(assignment);
                    }
                }
            }
        }

        private void ItemClicked(Assignment assignment)
        {
            //_navigationService.NavigateByPage<AssignmentDetailsView>(assignment);
        }
        #endregion
    }
}
