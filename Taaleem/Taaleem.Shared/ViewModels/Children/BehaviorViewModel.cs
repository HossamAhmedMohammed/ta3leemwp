﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.Children.Request;
using Taaleem.Models.Children.Response;
using Taaleem.Views.Children;

namespace Taaleem.ViewModels
{
    public class BehaviorViewModel : BindableBase
    {
        #region Fields
        private readonly IChildrenDataService _childrenDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        #endregion

        #region Properties

        private List<Taaleem.Models.Children.Response.Behavior> _behaviors;
        public List<Taaleem.Models.Children.Response.Behavior> Behaviors
        {
            get { return _behaviors; }
            set { SetProperty(ref _behaviors, value); }
        }


        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }


        #endregion

        #region Initialization

        public BehaviorViewModel(Resources resources, IChildrenDataService childrenDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _childrenDataService = childrenDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadBehaviorsAsync);
            ItemClickedCommand = new ExtendedCommand<Models.Children.Response.Behavior>(ItemClicked);
        }

        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        public ExtendedCommand<Taaleem.Models.Children.Response.Behavior> ItemClickedCommand { get; set; }
        #endregion

        #region Methods

        public async Task LoadBehaviorsAsync()
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var loginResponse = _appSettings.AmIParent ? await _appSettings.GetParentLoginResponseAsync() : await _appSettings.GetStudentLoginResponseAsync();
                var child = loginResponse.Children[_appSettings.SelectedChildIndex];
                var parameters = new BehaviorParameters()
                {
                    LanguageId = _appSettings.LanguageId,
                    StudentId = child.UserID,
                    SchoolId = child.School.SchoolID
                };

                var response = await _childrenDataService.GetBehaviorsAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    Behaviors = response.Result;
                }
                else
                {
                    Behaviors = null;
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            catch
            {
                // ignored
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }

        private void ItemClicked(Taaleem.Models.Children.Response.Behavior behavior)
        {
           _navigationService.NavigateByPage<BehaviorDetails>(behavior);
        }
        #endregion
    }
}

