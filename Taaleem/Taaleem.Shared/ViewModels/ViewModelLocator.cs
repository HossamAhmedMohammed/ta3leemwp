﻿using Autofac;
using Taaleem.Helpers;
using Taaleem.DataServices;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;

using Taaleem.Helpers.Interfaces;
using Taaleem.Converters;
using Taaleem.ViewModels.Children;
using Taaleem.Dao.Children;
using Taaleem.DataServices.Mix;
using Taaleem.DataServices.Mock;
using Taaleem.ViewModels.Employees;
using Taaleem.ViewModels.PublicServices;
using Taaleem.ViewModels.PublicServices.PrivateSchoolsViewModels;
using Taaleem.ViewModels.Exceptions;
using Taaleem.ViewModels.Login;
using Taaleem.ViewModels.PreRegistration;
using Taaleem.ViewModels.Employees.MyRequests;
//using Taaleem.Viewmodels.Children;

namespace Taaleem.ViewModels
{
    public  class ViewModelLocator
    { /// <summary>
      /// This container will be used internaly to resolve registered intances
      /// </summary>
        private readonly static IContainer Container;

        /// <summary>
        /// Register all required types in this static constructor,
        /// This will be executed the first time ViewModelLocator is accessed in code
        /// </summary>
        static ViewModelLocator()
        {
            var builder = new ContainerBuilder();

            #region DataServices and Helpers

            builder.RegisterType<ViewModelLocator>().SingleInstance();
            builder.RegisterType<NetworkHelper>().SingleInstance();
            builder.Register<INetworkHelper>(c => c.Resolve<NetworkHelper>());
            builder.RegisterType<AppSettings>().SingleInstance();
            builder.Register<IAppSettings>(c => c.Resolve<AppSettings>());
            builder.RegisterType<ToastService>().SingleInstance();
            builder.Register<IToastService>(c => c.Resolve<ToastService>());
            builder.RegisterType<Resources>().SingleInstance();
            builder.RegisterType<FilePicker>();
            builder.RegisterType<DateFormatConverter>();
            builder.RegisterType<ImageHelper>();
           builder.RegisterType<DialogManager>().SingleInstance();
            builder.Register<IDialogManager>(c => c.Resolve<DialogManager>());
            builder.RegisterType<DatabaseManager>();
            builder.Register<IDatabaseManager>(c => c.Resolve<DatabaseManager>());
            //It will be single Instance to keep the memory as low as possible while encrypting/decrypting.
            builder.RegisterType<CryptoHelper>().SingleInstance();
            builder.RegisterType<FeedsDao>();

            builder.RegisterType<BaseRequest>().SingleInstance();
            builder.RegisterType<RequestMessageResolver>();
            builder.Register<IRequestMessageResolver>(c => c.Resolve<RequestMessageResolver>());

            ////map
            //builder.RegisterType<MapControl>().SingleInstance().OnActivating(service =>
            //{
            //    service.Instance.MapServiceToken = "Tga0x1zkBrcF4AoGtCO9ZQ";
            //});


            //Data Services Registeration
            builder.RegisterType<ChildrenDataService>();
           builder.RegisterType<MockChildrenDataService>();
            builder.RegisterType<MixChildrenDataService>();
#if Mock
            builder.RegisterType<MockMyRequestsDataService>().SingleInstance();
#else
            builder.RegisterType<MyRequestsDataService>().SingleInstance();
#endif
            builder.RegisterType<MockMyRequestsDataService>().SingleInstance();
            builder.RegisterType<EmployeesDataService>().SingleInstance();
            builder.RegisterType<MockEmployeesDataService>().SingleInstance();
            builder.RegisterType<MixEmployeesDataService>().SingleInstance();
            builder.RegisterType<PublicSectorDataService>();
            builder.RegisterType<MockPublicSectorDataService>();
            builder.RegisterType<MixPublicSectorDataService>();
            builder.RegisterType<SchoolRegisterationService>().SingleInstance();
            builder.RegisterType<MockExceptionDataService>();
            builder.RegisterType<ExceptionDataService>().SingleInstance();
            builder.RegisterType<LoginDataService>().SingleInstance();
            builder.RegisterType<PrivateSchoolsDataService>().SingleInstance();
            builder.RegisterType<PreRegisttrationDataService>().SingleInstance();

            builder.Register<IChildrenDataService>(c => c.Resolve<MixChildrenDataService>());
#if Mock
            builder.Register<IMyRequestsDataService>(c => c.Resolve<MockMyRequestsDataService>());
#else
            builder.Register<IMyRequestsDataService>(c => c.Resolve<MyRequestsDataService>());
#endif
            builder.Register<IEmployeesDataService>(c => c.Resolve<MixEmployeesDataService>());
            builder.Register<IPublicSectorDataService>(c => c.Resolve<MixPublicSectorDataService>());
            builder.Register<IExceptionDataService>(c => c.Resolve<ExceptionDataService>());
            builder.Register<ILoginDataService>(c => c.Resolve<LoginDataService>());
            builder.Register<IPrivateSchoolsDataService>(c => c.Resolve<PrivateSchoolsDataService>());
            builder.Register<IPreRegistrationDataService>(c => c.Resolve<PreRegisttrationDataService>());

            //Register navigation Service
            builder.RegisterType<NavigationService>().SingleInstance().
                OnActivating(service =>
                {
                    //Here before the first request to Navigation Service is completed
                    //We will register the mapping between viewmodels and views
                    //This mapping will be used in navigation

                });
            builder.Register<INavigationService>(c => c.Resolve<NavigationService>());

            //Database Registerations
            #endregion

            #region ViewModels registeration
            builder.RegisterType<ParentLoginViewModel>().SingleInstance();
            builder.RegisterType<ChildrenSelectionViewModel>().SingleInstance();
            builder.RegisterType<MainMenuViewModel>().SingleInstance();

             builder.RegisterType<StagesSelectionViewModel>().SingleInstance();
            builder.RegisterType<ApplicantViewModel>().SingleInstance();
            builder.RegisterType<ChildrenViewModel>();
            builder.RegisterType<UserAccountViewModel>();
            builder.RegisterType<OtherViewModel>();
            builder.RegisterType<ParentViewModel>().SingleInstance();
            builder.RegisterType<StudentViewModel>();
            builder.RegisterType<TeachersViewModel>();
            builder.RegisterType<MyRequestsViewModel>();
            builder.RegisterType<DocumentRequestViewModel>();
            builder.RegisterType<PersonalLoanRequestViewModel>();
            builder.RegisterType<LeaveRequestViewModel>();
            builder.RegisterType<LeaveReturnRequestViewModel>();

            builder.RegisterType<MyExceptionRequestsViewModel>();
            builder.RegisterType<SpeakToTeacherViewModel>();
            builder.RegisterType<BehaviorViewModel>();
            builder.RegisterType<BehaviorDetailsViewModel>();
            builder.RegisterType<AnnouncementsViewModel>();
            builder.RegisterType<AttendancesViewModel>();
            builder.RegisterType<AssignmentsViewModel>();
            builder.RegisterType<RemindersViewModel>();
            builder.RegisterType<GradesViewModel>().SingleInstance();
            builder.RegisterType<EmployeeLoginViewModel>().SingleInstance();
            builder.RegisterType<PublicServicesMenuViewModel>();
            builder.RegisterType<ExitPermitRequestViewModel>();
            
            //builder.RegisterType<ChooseSalaryViewModel>();
            builder.RegisterType<SalariesViewModel>();
            //builder.RegisterType<ChooseEvaluationViewModel>();
            builder.RegisterType<EvaluationsViewModel>();
            //builder.RegisterType<ChooseLoanViewModel>();
            builder.RegisterType<LoansViewModel>();
            builder.RegisterType<VacationsViewModel>();
            builder.RegisterType<SearchSchoolsViewModel>().SingleInstance();
            builder.RegisterType<SchoolsViewModel>().SingleInstance();
            builder.RegisterType<SchoolDetailsViewModel>();
            //builder.RegisterType<SecondaryAvailabilityViewModel>();
            builder.RegisterType<SecondaryResultsViewModel>();
            //builder.RegisterType<ScholarRegisterationViewModel>();
            builder.RegisterType<AboutMaglesViewModel>();
            builder.RegisterType<UniversitiesViewModel>();
            //builder.RegisterType<ScholarsViewModel>();
            builder.RegisterType<MaglesNewsViewModel>();
            builder.RegisterType<SearchPrivateSchoolsViewModel>() ;
             builder.RegisterType<PrivateSchoolDetailsViewModel>().SingleInstance();
            builder.RegisterType<PreRegistrationViewModel>().SingleInstance();

            //School Registeration Service
            //builder.RegisterType<LoginParentViewModel>();
            //builder.RegisterType<RegisterParentViewModel>();
            //builder.RegisterType<RegisterationsStatusViewModel>();
            //builder.RegisterType<StatusDetailsViewModel>();
            //builder.RegisterType<AddRegisterationViewModel>();
            //builder.RegisterType<SchoolsRegisterationViewmodel>();
            //builder.RegisterType<UploadDocumentsViewModel>();
            //builder.RegisterType<ApprovingRegisterationViewModel>();

            #endregion

            Container = builder.Build();
           // ValidationAttribute.ErrorMessageAccessor = literal => Resources.GetString(literal);
        }

        #region data Services and Helpers Properties

        public BaseRequest BaseReq
        {
            get { return Container.Resolve<BaseRequest>(); }
        }
        public IAppSettings AppSettings
        {
            get { return Container.Resolve<IAppSettings>(); }
        }

        public IDatabaseManager DatabaseManager
        {
            get { return Container.Resolve<IDatabaseManager>(); }
        }

        public IDialogManager DialogManager
        {
            get { return Container.Resolve<IDialogManager>(); }
        }

        public IToastService ToastService
        {
            get { return Container.Resolve<IToastService>(); }
        }

        public CryptoHelper CryptoHelper
        {
            get { return Container.Resolve<CryptoHelper>(); }
        }

        public INavigationService NavigationService
        {
            get
            {
                return Container.Resolve<INavigationService>();
            }
        }

        public static Resources Resources
        {
            get { return Container.Resolve<Resources>(); }
        }

        public PublicSectorDataService PublicSectorDataService
        {
            get { return Container.Resolve<PublicSectorDataService>(); }
        }

        public ISchoolRegisterationService SchoolRegisterationService
        {
            get { return Container.Resolve<ISchoolRegisterationService>(); }
        }
        public ChildrenDataService ChildrenDataServ
        {
            get { return Container.Resolve<ChildrenDataService>(); }
        }
        #endregion

        #region View Models Properties
        public ParentLoginViewModel ParentLoginVM
        {
            get
            {
                return Container.Resolve<ParentLoginViewModel>();
            }
        }
        public ChildrenSelectionViewModel ChildrenSelectionVM
        {
            get
            {
                return Container.Resolve<ChildrenSelectionViewModel>();
            }
        }
        public MainMenuViewModel MainMenuVM
        {
            get
            {
                return Container.Resolve<MainMenuViewModel>();
            }
        }
        public PublicServicesMenuViewModel PSMenuVM
        {
            get
            {
                return Container.Resolve<PublicServicesMenuViewModel>();
            }
        }
        public StagesSelectionViewModel StagesSelectionVM
        {
            get
            {
                return Container.Resolve<StagesSelectionViewModel>();
            }
        }

        public ApplicantViewModel applicantVM
        {
            get
            {
                return Container.Resolve<ApplicantViewModel>();
            }
        }

        public ChildrenViewModel childrenVM
        {
            get
            {
                return Container.Resolve<ChildrenViewModel>();
            }
        }
        public OtherViewModel otherVM
        {
            get
            {
                return Container.Resolve<OtherViewModel>();
            }
        }
        public ParentViewModel parentsVM
        {
            get
            {
                return Container.Resolve<ParentViewModel>();
            }
        }

        public StudentViewModel studentVM
        {
            get
            {
                return Container.Resolve<StudentViewModel>();
            }
        }
        public NotificationsViewModel NotificationsVM
        {
            get
            {
                return Container.Resolve<NotificationsViewModel>();
            }
        }

        public AssignmentsViewModel AssignmentsViewModel
        {
            get
            {
                return Container.Resolve<AssignmentsViewModel>();
            }
        }

        public AttendancesViewModel AttendancesVM
        {
            get
            {
                return Container.Resolve<AttendancesViewModel>();
            }
        }

        public BehaviorViewModel BehaviorVM
        {
            get
            {
                return Container.Resolve<BehaviorViewModel>();
            }
        }

        public BehaviorDetailsViewModel BehaviorDetailsViewModel
        {
            get
            {
                return Container.Resolve<BehaviorDetailsViewModel>();
            }
        }

        public AnnouncementsViewModel AnnouncementsViewModel
        {
            get
            {
                return Container.Resolve<AnnouncementsViewModel>();
            }
        }

        public TeachersViewModel TeachersViewModel
        {
            get
            {
                return Container.Resolve<TeachersViewModel>();
            }
        }

        public MyRequestsViewModel MyRequestsVM
        {
            get
            {
                return Container.Resolve<MyRequestsViewModel>();
            }
        }
        public DocumentRequestViewModel DocumentRequestViewModel
        {
            get
            {
                return Container.Resolve<DocumentRequestViewModel>();
            }
        }

        public PersonalLoanRequestViewModel PersonalLoanRequestViewModel
        {
            get
            {
                return Container.Resolve<PersonalLoanRequestViewModel>();
            }
        }

        public ExitPermitRequestViewModel ExitPermitRequestViewModel
        {
            get
            {
                return Container.Resolve<ExitPermitRequestViewModel>();
            }
        }

        public LeaveRequestViewModel LeaveRequestViewModel
        {
            get
            {
                return Container.Resolve<LeaveRequestViewModel>();
            }
        }

        public LeaveReturnRequestViewModel LeaveReturnRequestViewModel
        {
            get
            {
                return Container.Resolve<LeaveReturnRequestViewModel>();
            }
        }

        public SpeakToTeacherViewModel SpeakToTeacherViewModel
        {
            get
            {
                return Container.Resolve<SpeakToTeacherViewModel>();
            }
        }

        public RemindersViewModel RemindersViewModel
        {
            get
            {
                return Container.Resolve<RemindersViewModel>();
            }
        }

        public GradesViewModel GradesViewModel
        {
            get
            {
                return Container.Resolve<GradesViewModel>();
            }
        }

        //public MainViewModel MainViewModel
        //{
        //    get
        //    {
        //        return Container.Resolve<MainViewModel>();
        //    }
        //}

        public EmployeeLoginViewModel EmployeeLoginViewModel
        {
            get
            {
                return Container.Resolve<EmployeeLoginViewModel>();
            }
        }

        public UserAccountViewModel UserAccountVM
        {
            get
            {
                return Container.Resolve<UserAccountViewModel>();
            }
        }

        public SalariesViewModel SalariesViewModel
        {
            get
            {
                return Container.Resolve<SalariesViewModel>();
            }
        }

        public PrivateSchoolDetailsViewModel PrivateSchoolDetailsVM
        {
            get
            {
                return Container.Resolve<PrivateSchoolDetailsViewModel>();
            }
        }

        public EvaluationsViewModel EvaluationsViewModel
        {
            get
            {
                return Container.Resolve<EvaluationsViewModel>();
            }
        }

        public MyRequestsViewModel MyRequestsViewModel
        {
            get
            {
                return Container.Resolve<MyRequestsViewModel>();
            }
        }

        public LoansViewModel LoansViewModel
        {
            get
            {
                return Container.Resolve<LoansViewModel>();
            }
        }

        public PreRegistrationViewModel PreRegistrationVM
        {
            get
            {
                return Container.Resolve<PreRegistrationViewModel>();
            }
        }

        public VacationsViewModel VacationsViewModel
        {
            get
            {
                return Container.Resolve<VacationsViewModel>();
            }
        }

        public SearchSchoolsViewModel SearchSchoolsViewModel
        {
            get
            {
                return Container.Resolve<SearchSchoolsViewModel>();
            }
        }

        public SchoolsViewModel SchoolsViewModel
        {
            get
            {
                return Container.Resolve<SchoolsViewModel>();
            }
        }

        public SchoolDetailsViewModel SchoolDetailsViewModel
        {
            get
            {
                return Container.Resolve<SchoolDetailsViewModel>();
            }
        }
        public SearchPrivateSchoolsViewModel PrivateSchoolsvViewModel
        {
            get
            {
                return Container.Resolve<SearchPrivateSchoolsViewModel>();
            }
        }

        //public SecondaryAvailabilityViewModel SecondaryAvailabilityViewModel
        //{
        //    get
        //    {
        //        return Container.Resolve<SecondaryAvailabilityViewModel>();
        //    }
        //}

        public SecondaryResultsViewModel SecondaryResultsViewModel
        {
            get
            {
                return Container.Resolve<SecondaryResultsViewModel>();
            }
        }

        //public ScholarRegisterationViewModel ScholarRegisterationViewModel
        //{
        //    get
        //    {
        //        return Container.Resolve<ScholarRegisterationViewModel>();
        //    }
        //}

        public AboutMaglesViewModel AboutMaglesViewModel
        {
            get
            {
                return Container.Resolve<AboutMaglesViewModel>();
            }
        }

        public UniversitiesViewModel UniversitiesViewModel
        {
            get
            {
                return Container.Resolve<UniversitiesViewModel>();
            }
        }

        //public ScholarsViewModel ScholarsViewModel
        //{
        //    get
        //    {
        //        return Container.Resolve<ScholarsViewModel>();
        //    }
        //}

        public MaglesNewsViewModel MaglesNewsViewModel
        {
            get
            {
                return Container.Resolve<MaglesNewsViewModel>();
            }
        }

        ////School Registeration viewmodels
        //public LoginParentViewModel LoginParentViewModel
        //{
        //    get
        //    {
        //        return Container.Resolve<LoginParentViewModel>();
        //    }
        //}
        //public RegisterParentViewModel RegisterParentViewModel
        //{
        //    get
        //    {
        //        return Container.Resolve<RegisterParentViewModel>();
        //    }
        //}

        //public RegisterationsStatusViewModel RegisterationsStatusViewModel
        //{
        //    get
        //    {
        //        return Container.Resolve<RegisterationsStatusViewModel>();
        //    }
        //}

        //public StatusDetailsViewModel StatusDetailsViewModel
        //{
        //    get
        //    {
        //        return Container.Resolve<StatusDetailsViewModel>();
        //    }
        //}

        //public AddRegisterationViewModel AddRegisterationViewModel
        //{
        //    get
        //    {
        //        return Container.Resolve<AddRegisterationViewModel>();
        //    }
        //}

        //public UploadDocumentsViewModel UploadDocumentsViewModel
        //{
        //    get
        //    {
        //        return Container.Resolve<UploadDocumentsViewModel>();
        //    }
        //}

        //public ApprovingRegisterationViewModel ApprovingRegisterationViewModel
        //{
        //    get
        //    {
        //        return Container.Resolve<ApprovingRegisterationViewModel>();
        //    }
        //}

        //private Owned<SchoolsRegisterationViewmodel> _ownedSchoolsRegisterationViewmodel;
        //public SchoolsRegisterationViewmodel SchoolsRegisterationViewmodel
        //{
        //    get
        //    {
        //        if (_ownedSchoolsRegisterationViewmodel == null)
        //        {
        //            _ownedSchoolsRegisterationViewmodel = Container.Resolve<Owned<SchoolsRegisterationViewmodel>>();
        //        }
        //        return _ownedSchoolsRegisterationViewmodel.Value;
        //    }
        //    set
        //    {
        //        if (value == null && _ownedSchoolsRegisterationViewmodel != null)
        //        {
        //            _ownedSchoolsRegisterationViewmodel.Dispose();
        //            _ownedSchoolsRegisterationViewmodel = null;
        //        }
        //    }
        //}


        #endregion

        /// <summary>
        ///Create instance in App.xaml resources so you can use it in data binding in all xaml pages
        ///And don't forget to set this instance from App.xaml.cs, So you can access it from code behind in pages(if required)
        /// </summary>
        public static ViewModelLocator Locator { set; get; }

        

    }
}

