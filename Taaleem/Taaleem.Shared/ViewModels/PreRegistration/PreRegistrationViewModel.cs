﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.PreRegistration.Request;
using Taaleem.Models.PreRegistration.Response;
using Taaleem.Views.PublicServices.PreRegistration;

namespace Taaleem.ViewModels.PreRegistration
{
  public  class PreRegistrationViewModel:BindableBase
    {
        #region Fields
        private readonly IPreRegistrationDataService _PreRegistrationDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        private readonly IDialogManager _dialogManager;

        #endregion

        #region Properties
       
        public string PageHeader
        {
            get {
                if (IsTransfer == true)
                    return ViewModelLocator.Resources.StudentTransfer;
                else
                    return ViewModelLocator.Resources.Enrollment;
            }
          
        }
        private List<Rootresult> _enrollment;
        public List<Rootresult> Enrollment
        {
            get { return _enrollment; }
            set { _enrollment = value;
                OnPropertyChanged();
            }
        }
        private List<Rootresult> _transfer;
        public List<Rootresult> Transfer
        {
            get { return _transfer; }
            set
            {
                _transfer = value;
                OnPropertyChanged();
            }
        }

        private List<Rootresult> _preEnrollment;
        public List<Rootresult> PreEnrollment
        {
            get { return _preEnrollment; }
            set
            {
                _preEnrollment = value;
                OnPropertyChanged();
            }
        }
        private List<Rootresult> _preTransfer;
        public List<Rootresult> PreTransfer
        {
            get { return _preTransfer; }
            set
            {
                _preTransfer = value;
                OnPropertyChanged();
            }
        }
        private Rootresultt _enrollmentDate;
        public Rootresultt EnrollmentDate
        {
            get { return _enrollmentDate; }
            set
            {
                _enrollmentDate = value;
                OnPropertyChanged();
            }
        }
        private Rootresultt _transferDate;
        public Rootresultt TransferDate
        {
            get { return _transferDate; }
            set
            {
                _transferDate = value;
                OnPropertyChanged();
            }
        }
        private List<Getrelationshipsresult> _relationships;
        public List<Getrelationshipsresult> Relationships
        {
            get { return _relationships; }
            set
            {
                _relationships = value;
                OnPropertyChanged();
            }
        }
        private List<Getgradelevelsresult> _gradeLevels;
        public List<Getgradelevelsresult> GradeLevels
        {
            get { return _gradeLevels; }
            set
            {
                _gradeLevels = value;
                OnPropertyChanged();
            }
        }
        private Getrelationshipsresult _selectedRelationship;
        public Getrelationshipsresult SelectedRelationship
        {
            get { return _selectedRelationship; }
            set
            {
                _selectedRelationship = value;
                OnPropertyChanged();
            }
        }
        private Getgradelevelsresult _selectedGradeLevel;
        public Getgradelevelsresult SelectedGradeLevel
        {
            get { return _selectedGradeLevel; }
            set
            {
                _selectedGradeLevel = value;
                OnPropertyChanged();
            }
        }

        private string _studentQID;
        public string StudentQID
        {
            get { return _studentQID; }
            set
            {
                _studentQID = value;
                OnPropertyChanged();
            }
        }
        private string _studentElecNum;
        public string StudentElecnum
        {
            get { return _studentElecNum; }
            set
            {
                _studentElecNum = value;
                OnPropertyChanged();
            }
        }
       
        private Searchstudentresult _studentData;
        public Searchstudentresult StudentData
        {
            get { return _studentData; }
            set
            {
                _studentData = value;
                OnPropertyChanged();
            }
        }
        private List<Rootresulttt> _nearestSchools;
        public List<Rootresulttt> NearestSchools
        {
            get { return _nearestSchools; }
            set
            {
                _nearestSchools = value;
                OnPropertyChanged();
            }
        }
        private List<Rootresultttt> _nearestSchoolsDetails;
        public List<Rootresultttt> NearestSchoolsDetails
        {
            get { return _nearestSchoolsDetails; }
            set
            {
                _nearestSchoolsDetails = value;
                OnPropertyChanged();
            }
        }
        private Rootresultttt _selectedSchool;
        public Rootresultttt SelectedSchool
        {
            get { return _selectedSchool; }
            set
            {
                _selectedSchool = value;
                OnPropertyChanged();
            }
        }
        private ReceiveFileParameters _fileParameters;
        public ReceiveFileParameters FileParameters
        {
            get { return _fileParameters; }
            set
            {
                _fileParameters = value;
                OnPropertyChanged();
            }
        }
        private ReceiveFileResponse _fileWorkParentResponse;
        public ReceiveFileResponse FileWorkParentResponse
        {
            get { return _fileWorkParentResponse; }
            set
            {
                _fileWorkParentResponse = value;
                OnPropertyChanged();
            }
        }
        private ReceiveFileResponse _fileElecResponse;
        public ReceiveFileResponse FileElecResponse
        {
            get { return _fileElecResponse; }
            set
            {
                _fileElecResponse = value;
                OnPropertyChanged();
            }
        }
        private Getparentresult _parentData;
        public Getparentresult ParentData
        {
            get { return _parentData; }
            set
            {
                _parentData = value;
                OnPropertyChanged();
            }
        }
        private List<Getschoolenrollmentsresult> _schoolEnrollments;
        public List<Getschoolenrollmentsresult> SchoolEnrollments
        {
            get { return _schoolEnrollments; }
            set
            {
                _schoolEnrollments = value;
                OnPropertyChanged();
            }
        }
        private Getschoolenrollmentsresult _selectedschoolEnrollments;
        public Getschoolenrollmentsresult SelectedschoolEnrollments
        {
            get { return _selectedschoolEnrollments; }
            set
            {
                _selectedschoolEnrollments = value;
                OnPropertyChanged();
            }
        }
        private List<Getstatesresult> _states;
        public List<Getstatesresult> States
        {
            get { return _states; }
            set
            {
                _states = value;
                OnPropertyChanged();
            }
        }
        private Getstatesresult _selectedState;
        public Getstatesresult SelectedState
        {
            get { return _selectedState; }
            set
            {
                _selectedState = value;
                OnPropertyChanged();
            }
        }
        private bool _isTransfer;
        public bool IsTransfer
        {
            get { return _isTransfer; }
            set { _isTransfer = value;
                OnPropertyChanged();
            }
        }
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        #endregion

        #region Initialization

        public PreRegistrationViewModel(Resources resources, IPreRegistrationDataService preRegistrationDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _PreRegistrationDataService = preRegistrationDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _dialogManager = dialogManager;

            Enrollment = new List<Rootresult>();
            Transfer = new List<Rootresult>();
            Relationships = new List<Getrelationshipsresult>();
            GradeLevels = new List<Getgradelevelsresult>();
            States = new List<Getstatesresult>();
            NearestSchools = new List<Rootresulttt>();
            NearestSchoolsDetails = new List<Rootresultttt>();
            FileParameters = new ReceiveFileParameters();
            FileWorkParentResponse = new ReceiveFileResponse();
            FileElecResponse = new ReceiveFileResponse();
            SchoolEnrollments = new List<Getschoolenrollmentsresult>();
            //Initialize Commands
          //  loadData();
           

         
        }

        //private async void loadData()
        //{
        //  await  GetAllRequestsAcync();
        //    await GetSettingsAcync();
        //}



        #endregion

        #region Commands


        #endregion

        #region Methods
        public async Task GetAllRequestsAcync()
        {
            Enrollment.Clear();
            Transfer.Clear();
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var response = await _PreRegistrationDataService.GetALLRequests();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result != null && response.Result.GetRequestsResult.TotalCount != 0)
                    {
                        Enrollment = response.Result.GetRequestsResult.RootResults.ToList().Where(x => x.ProcessID == "e8cf9faa-e750-40d0-ba43-3d0083b3e285").ToList();
                        Transfer = response.Result.GetRequestsResult.RootResults.ToList().Where(x => x.ProcessID == "5ade5ae2-5ce2-41a1-826a-7237b7649019").ToList();
                        PreEnrollment = response.Result.GetRequestsResult.RootResults.ToList().Where(x => x.ProcessID == "aa0d13c9-3229-48B9-a347-3c7ce9131f5b").ToList();
                        PreTransfer = response.Result.GetRequestsResult.RootResults.ToList().Where(x => x.ProcessID == "090cfe50-268b-443a-9650-4fab4916c564").ToList();
                    }
                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);
                }

            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task GetSettingsAcync()
        {

            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var response = await _PreRegistrationDataService.GetSettings();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result != null && response.Result.GetSettingsResult.TotalCount != 0)
                    {
                        EnrollmentDate = response.Result.GetSettingsResult.RootResults.Where(x => x.ID == "62a0ae29-5316-40db-9f77-683bf25c8880").SingleOrDefault();
                        TransferDate = response.Result.GetSettingsResult.RootResults.Where(x => x.ID == "5cc9e330-7a91-4ffc-90c4-f3a02e3fd954").SingleOrDefault();
                    }
                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);
                }

            }
            finally
            {

                IsBusy = false;
            }
        }
        public async Task GetRelationshipsAcync()
        {
            Relationships.Clear();
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var response = await _PreRegistrationDataService.GetRelationships();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result != null && response.Result.GetRelationshipsResult != null)
                    {
                        Relationships = response.Result.GetRelationshipsResult.ToList(); 
                    }
                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);
                }

            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task GetGradeLevelsAcync()
        {
            GradeLevels.Clear();
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var response = await _PreRegistrationDataService.GetGradeLevels();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result != null && response.Result.GetGradeLevelsResult != null)
                    {
                        GradeLevels = response.Result.GetGradeLevelsResult.ToList();
                    }
                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);
                }

            }
            finally
            {

                IsBusy = false;
            }
        }
        public async Task SearchStudentAcync()
        {
            StudentData = new Searchstudentresult();
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var param = new SearchStudentParameters { gradeLevelID = SelectedGradeLevel.ID, QID = StudentQID };
                var response = await _PreRegistrationDataService.SearchStudent(param);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if(response.Result.SearchStudentResult != null)
                    {
                        StudentData = response.Result.SearchStudentResult;
                        _navigationService.NavigateByPage(typeof(NearestSchool));
                    }
                   
                }
                else
                {
                    if(response.InternalServerErrorMessage != null)
                  await  _dialogManager.ShowMessage("", response.InternalServerErrorMessage);
                    //ErrorMessage = _messageResolver.ResultToMessage(response);
                }

            }
            finally
            {

                IsBusy = false;
            }
        }
        public async Task GetNearestSchools()
        {
            NearestSchools = new List<Rootresulttt>();
            NearestSchoolsDetails = new List<Rootresultttt>();
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var param = new GetNearestSchoolsParameters { electNo = StudentElecnum , genderID= StudentData.GenderID, gradeLevelID=SelectedGradeLevel.ID, QID = StudentQID };
                var response = await _PreRegistrationDataService.GetNearestSchools(param);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result.GetNearestSchoolsResult.TotalCount != 0)
                    {
                        NearestSchools = response.Result.GetNearestSchoolsResult.RootResults.ToList();
                       await GetNearestSchoolsDeatils();
                    }

                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);
                }

            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task GetNearestSchoolsDeatils()
        {
            NearestSchoolsDetails.Clear();
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                List<string> temp = new List<string>();
                foreach (var item in NearestSchools)
                {
                    temp.Add(item.ID);
                }
                var param = new GetNearestSchoolsByQarsParameters {  schoolIDList = temp.ToArray() };
                var response = await _PreRegistrationDataService.GetNearestSchoolsByQars(param);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (response.Result.GetSchoolContactsResult.TotalCount != 0)
                    {
                        NearestSchoolsDetails = response.Result.GetSchoolContactsResult.RootResults.ToList();
                        foreach(var item in NearestSchools)
                        {
                            NearestSchoolsDetails.Where(x => x.ID == item.ID).SingleOrDefault().Name = item.Name;
                            NearestSchoolsDetails.Where(x => x.ID == item.ID).SingleOrDefault().IsFull = item.ISFull;
                            NearestSchoolsDetails.Where(x => x.ID == item.ID).SingleOrDefault().Code = item.LocalId;
                        }
                    }

                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);
                }

            }
            finally
            {

                IsBusy = false;
            }
        }

        public async Task<ReceiveFileResponse> ReceiveFileAsync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {
               
                var param = new ReceiveFileParameters { data = FileParameters.data, filetype= FileParameters.filetype, locationId = FileParameters.locationId };
                var response = await _PreRegistrationDataService.ReceiveFile(param);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    if (response.Result != null && response.Result.ReceiveFileResult != null)
                    {
                        return response.Result;
                    }
                    return null;
                }
                else
                {
                    if(response.ResponseStatus == ResponseStatus.NoInternet)
                    {
                        await _dialogManager.ShowMessage("", ViewModelLocator.Resources.NoInternet);
                    }
                  else  if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);
                    return null;
                }

            }
            finally
            {

                IsBusy = false;
            }
        }
        public async Task GetParentData()
        {
            ParentData = new Getparentresult();
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var response = await _PreRegistrationDataService.GetParent();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    if (response.Result != null && response.Result.GetParentResult != null)
                    {
                        ParentData = response.Result.GetParentResult;
                    }
                   
                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);

                }

            }
            finally
            {

                IsBusy = false;
            }
        }
        public async Task EnrollAsync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                var paramss = new EnrollStudentParameters { addressVerificationType = "Electricity", electFilename = FileElecResponse.ReceiveFileResult, electricityNumber = StudentElecnum, gradeLevelID = SelectedGradeLevel.ID, relationshipID = SelectedRelationship.ID, schoolID = SelectedSchool.ID, studentQID = StudentQID, workCertificateFilename = FileWorkParentResponse.ReceiveFileResult };
                var response = await _PreRegistrationDataService.EnrollStudent(paramss);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    if (response.Result.EnrollResult == true )
                    {
                     await   _dialogManager.ShowMessage("", "تمت عملية التسجيل الالكتروني بنجاح");
                        _navigationService.NavigateByPage(typeof(RegisterAndTransfer));
                    }

                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);

                }

            }
            finally
            {

                IsBusy = false;
            }
        }
        public async Task GetSchoolEnrollmentsAsync()
        {
            SchoolEnrollments.Clear();
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                
                var response = await _PreRegistrationDataService.GetRegistrations();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    if (response.Result.GetSchoolEnrollmentsResult != null)
                    {
                        SchoolEnrollments = response.Result.GetSchoolEnrollmentsResult.ToList();
                    }

                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);

                }

            }
            finally
            {

                IsBusy = false;
            }
        }
        public async Task GetStatesAsync()
        {
            States.Clear();
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var response = await _PreRegistrationDataService.GetStates();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    if (response.Result.GetStatesResult != null)
                    {
                        States = response.Result.GetStatesResult.ToList();
                    }

                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);

                }

            }
            finally
            {

                IsBusy = false;
            }
        }
        public async Task TransferAsync()
        {
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                TransferParameters paramseters = new TransferParameters { enrollmentID = SelectedschoolEnrollments.ID, schoolID = SelectedSchool.ID };
                var response = await _PreRegistrationDataService.TransferStudent(paramseters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    if (response.Result != null)
                    {
                        
                    }

                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);

                }

            }
            finally
            {

                IsBusy = false;
            }
        }
        public async Task GetSchoolsByStateAsync()
        {
            NearestSchools = new List<Rootresulttt>();
            NearestSchoolsDetails = new List<Rootresultttt>();
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                GetSchoolsByStateParameters paramseters = new GetSchoolsByStateParameters { enrollmentID = SelectedschoolEnrollments.ID, stateID = SelectedState.ID };
                var response = await _PreRegistrationDataService.GetSchoolsByState(paramseters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    if (response.Result != null)
                    {
                        if (response.Result.GetSchoolsByStateResult.TotalCount > 0)
                        {
                            NearestSchools = response.Result.GetSchoolsByStateResult.RootResults.ToList();
                            await GetNearestSchoolsDeatils();
                        }else
                        {
                            response.ResponseStatus = ResponseStatus.SuccessWithNoData;
                            ErrorMessage = _messageResolver.ResultToMessage(response);
                        }
                    }
                }
                else
                {

                    if (response.InternalServerErrorMessage != null)
                        await _dialogManager.ShowMessage("", response.InternalServerErrorMessage);

                }

            }
            finally
            {

                IsBusy = false;
            }
        }
        #endregion
    }
}
