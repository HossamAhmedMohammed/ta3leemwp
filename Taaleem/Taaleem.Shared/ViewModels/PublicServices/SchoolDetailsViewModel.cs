﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Taaleem.Common;
using Taaleem.Models.PublicServices;
using Windows.Devices.Geolocation;

namespace Taaleem.ViewModels.PublicServices
{
    public class SchoolDetailsViewModel : BindableBase
    {
        #region Fields
        #endregion

        #region Properties

        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetProperty(ref _isLoading, value); }
        }


        private SchoolInfoWrapper _schoolInfo;
        public SchoolInfoWrapper SchoolInfo
        {
            get { return _schoolInfo; }
            set { SetProperty(ref _schoolInfo, value); }
        }


        private ObservableCollection<SchoolInfoWrapper> _schoolInfoList;
        public ObservableCollection<SchoolInfoWrapper> SchoolInfoList
        {
            get { return _schoolInfoList; }
            set { SetProperty(ref _schoolInfoList, value); }
        }

        private Geopoint _yourLocation;
        public Geopoint YourLocation
        {
            get { return _yourLocation; }
            set { SetProperty(ref _yourLocation, value); }
        }

        private Geopoint _schoolLocation;
        public Geopoint SchoolLocation
        {
            get { return _schoolLocation; }
            set { SetProperty(ref _schoolLocation, value); }
        }

        private List<Geopoint> _schoolLocations;
        public List<Geopoint> SchoolLocations
        {
            get { return _schoolLocations; }
            set { SetProperty(ref _schoolLocations, value); }
        }

        #endregion

        #region Initialization

        public SchoolDetailsViewModel()
        {
            SchoolInfoList = new ObservableCollection<SchoolInfoWrapper>();
            SchoolLocations = new List<Geopoint>();
            //Initialize Commands
        }

        #endregion

        #region Commands

        #endregion

        #region Methods

        public void InitializeLocations()
        {
            if (SchoolInfo.SchoolX.HasValue && SchoolInfo.SchoolY.HasValue)
            {
                SchoolLocation = new Geopoint(new BasicGeoposition
                {
                    Latitude = SchoolInfo.SchoolY.Value,
                    Longitude = SchoolInfo.SchoolX.Value
                });
            }

            if (SchoolInfo.ElecX.HasValue && SchoolInfo.ElecY.HasValue)
            {
                YourLocation = new Geopoint(new BasicGeoposition
                {
                    Latitude = SchoolInfo.ElecY.Value,
                    Longitude = SchoolInfo.ElecX.Value
                });
            }
        }

        internal void InitializeLocationsList()
        {


            foreach (SchoolInfoWrapper item in SchoolInfoList)
            {
                if (item.SchoolX.HasValue && item.SchoolY.HasValue)
                {
                    SchoolLocations.Add ( new Geopoint(new BasicGeoposition
                    {
                        Latitude = item.SchoolY.Value,
                        Longitude = item.SchoolX.Value
                    }));
                }
            }

        }

        #endregion
    }
}
