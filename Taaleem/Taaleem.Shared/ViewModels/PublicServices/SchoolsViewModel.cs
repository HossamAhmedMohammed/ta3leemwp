﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Collections.Extended;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.PublicServices;
using Taaleem.Models.PublicServices.Request;
using Taaleem.Models.PublicServices.Response;
using Taaleem.Views.PublicServices;

namespace Taaleem.ViewModels.PublicServices
{
    public class SchoolsViewModel : BindableBase
    {
        #region Fields
        private readonly IPublicSectorDataService _publicSectorDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;

        #endregion

        #region Properties

        public SearchSchoolNavParameters Parameters { get; set; }

        private ObservableCollection<SchoolInfoWrapper> _schools;
        public ObservableCollection<SchoolInfoWrapper> Schools
        {
            get { return _schools; }
            set { SetProperty(ref _schools, value); }
        }


        private ExtendedCollectionView _schoolsCView;
        public ExtendedCollectionView SchoolsCView
        {
            get { return _schoolsCView; }
            set { SetProperty(ref _schoolsCView, value); }
        }


        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }


        #endregion

        #region Initialization

        public SchoolsViewModel(Resources resources, IPublicSectorDataService publicSectorDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService)
        {
            _publicSectorDataService = publicSectorDataService;
            _messageResolver = messageResolver;
            _navigationService = navigationService;
            SchoolsCView = new ExtendedCollectionView();

            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand(LoadSchoolsAsync);
            SchoolClickedCommand = new ExtendedCommand<SchoolInfoWrapper>(SchoolClicked);
        }


        #endregion

        #region Commands
        public AsyncExtendedCommand LoadCommand { get; set; }
        public ExtendedCommand<SchoolInfoWrapper> SchoolClickedCommand { get; set; }
        #endregion

        #region Methods

        public async Task LoadSchoolsAsync()
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                RequestResponse<List<SchoolInfo>> response;
                if (String.IsNullOrWhiteSpace(Parameters.Elect))
                {
                    response = await _publicSectorDataService.GetAllSchoolsAsync(Parameters.Type);
                }
                else
                {
                    var parameters = new SchoolsByElecParameters()
                    {
                        Type = Parameters.Type,
                        Elec = Parameters.Elect
                    };
                    response = await _publicSectorDataService.GetSchoolsByElectricAsync(parameters);
                }

                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {

                    var list = response.Result.Select(item => new SchoolInfoWrapper(item)).ToList();

                    if (!String.IsNullOrWhiteSpace(Parameters.Elect))
                    {
                        list = list.OrderBy(item => item.DistanceInMeters).ToList();
                    }
                    SchoolsCView.Source = list;
                  //  Schools = list;
                  foreach(var item in list)
                    {
                        Schools.Add(item);
                    }
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                    SchoolsCView.Source = null;
                }
            }
            catch
            {
                // ignored
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }

        private void SchoolClicked(SchoolInfoWrapper school)
        {
            if (school.SchoolX.HasValue && school.SchoolY.HasValue)
            {
                _navigationService.NavigateByPage<SchoolDetails>(school);
            }
        }

        #endregion
    }

   
}