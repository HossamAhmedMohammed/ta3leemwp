﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Common;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.PublicServices;
using Taaleem.Views.PublicServices;

namespace Taaleem.ViewModels.PublicServices
{
    public class SearchSchoolsViewModel : BindableBase
    {
        #region Fields

        private readonly Resources _resources;
        private readonly INavigationService _navigationService;
        #endregion

        #region Properties

        private SchoolStage _schoolStage;
        public SchoolStage SchoolStage
        {
            get { return _schoolStage; }
            set
            {
                if (value != null)
                {
                    // Set and notify if not null
                    if (SetProperty(ref _schoolStage, value))
                    {
                        ChooseResultsCommand.CanExecute = _schoolStage != null;
                    }
                    OnPropertyChanged("SchoolStage");
                }
                else // just notify when trying to set to null
                    OnPropertyChanged("SchoolStage");

               
                
            }
        }

        private List<SchoolStage> _schoolStagesList;
        public List<SchoolStage> SchoolStagesList
        {
            get { return _schoolStagesList; }
            set { SetProperty(ref _schoolStagesList, value); }
        }

        private GenderType _genderType;
        public GenderType GenderType
        {
            get { return _genderType; }
            set { SetProperty(ref _genderType, value); }
        }

        private List<GenderType> _genderTypesList;
        public List<GenderType> GenderTypesList
        {
            get { return _genderTypesList; }
            set { SetProperty(ref _genderTypesList, value); }
        }

        private string _elecNumber;
        public string ElecNumber
        {
            get { return _elecNumber; }
            set { SetProperty(ref _elecNumber, value); }
        }


        #endregion

        #region Initialization

        public SearchSchoolsViewModel(INavigationService navigationService, Resources resources)
        {
            _navigationService = navigationService;
            _resources = resources;
            SchoolStagesList = SchoolStage.Stages;
            GenderTypesList = GenderType.All;
            GenderType = GenderTypesList[0];
            //Initialize Commands
            ChooseResultsCommand = new ExtendedCommand(ChooseResults, false);
           // SchoolStage = SchoolStagesList[1];

        }

        #endregion

        #region Commands

        public ExtendedCommand ChooseResultsCommand { get; set; }
        #endregion

        #region Methods
        private void ChooseResults()
        {
            _navigationService.NavigateByPage<Schools>(new SearchSchoolNavParameters() { Type = GetDataTypeId(), Elect = ElecNumber });
        }

        private int GetDataTypeId()
        {
            int genderId = GenderType.Id;
            int stageId = SchoolStage.Id;
            int data = 0;
            if (stageId == 1 && genderId == 1)
            {
                data = 7;
            }
            else if (stageId == 1 && genderId == 2)
            {
                data = 8;
            }
            else if (stageId == 2 && genderId == 1)
            {
                data = 1;
            }
            else if (stageId == 2 && genderId == 2)
            {
                data = 2;
            }
            else if (stageId == 3 && genderId == 1)
            {
                data = 3;
            }
            else if (stageId == 3 && genderId == 2)
            {
                data = 4;
            }
            else if (stageId == 4 && genderId == 1)
            {
                data = 5;
            }
            else if (stageId == 4 && genderId == 2)
            {
                data = 6;
            }
            return data;
        }

        #endregion
    }


}
