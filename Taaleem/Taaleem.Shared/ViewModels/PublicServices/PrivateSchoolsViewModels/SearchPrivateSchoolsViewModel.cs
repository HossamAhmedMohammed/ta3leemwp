﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Collections.Extended;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.PrivateSchools.Request;
using Taaleem.Models.PrivateSchools.Response;
using Taaleem.Models.PublicServices;
using Taaleem.Views.PublicServices.PrivateSchoolsViews;
using Windows.Devices.Geolocation;

namespace Taaleem.ViewModels.PublicServices.PrivateSchoolsViewModels
{
   public class SearchPrivateSchoolsViewModel : BindableBase
    {
        #region Fields
        private readonly IPrivateSchoolsDataService _privateSchoolsDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;

        #endregion

        #region Properties

    

        private List<PrivateSchool> _schools;
        public List<PrivateSchool> Schools
        {
            get { return _schools; }
            set {
                _schools = value;
                OnPropertyChanged();
            }
        }

        private List<Getregionsresult> _regions;

        public List<Getregionsresult> Regions
        {
            get { return _regions; }
            set { _regions = value;
                OnPropertyChanged();
            }
        }
        private Getregionsresult _selectedRegion;

        public Getregionsresult SelectedRegion
        {
            get { return _selectedRegion; }
            set
            {
                _selectedRegion = value;
                OnPropertyChanged();
            }
        }

        private List<Geteducationalcurriculumsresult> _curriculums;

        public List<Geteducationalcurriculumsresult> Curriculums
        {
            get { return _curriculums; }
            set
            {
                _curriculums = value;
                OnPropertyChanged();
            }
        }

        private Geteducationalcurriculumsresult _selectedCurriculum;

        public Geteducationalcurriculumsresult SelectedCurriculum
        {
            get { return _selectedCurriculum; }
            set { _selectedCurriculum = value;
                OnPropertyChanged();
            }
        }


        private List<Geteducationallevelsresult> _levels;

        public List<Geteducationallevelsresult> Levels
        {
            get { return _levels; }
            set
            {
                _levels = value;
                OnPropertyChanged();
            }
        }

        private Geteducationallevelsresult _selectedLevel;

        public Geteducationallevelsresult SelectedLevel
        {
            get { return _selectedLevel; }
            set
            {
                _selectedLevel = value;
                OnPropertyChanged();
            }
        }

        private List<Getschooltypesresult> _schoolTypes;

        public List<Getschooltypesresult> SchoolTypes
        {
            get { return _schoolTypes; }
            set
            {
                _schoolTypes = value;
                OnPropertyChanged();
            }
        }

        private Getschooltypesresult _selectedSchoolType;

        public Getschooltypesresult SelectedSchoolType
        {
            get { return _selectedSchoolType; }
            set
            {
                _selectedSchoolType = value;
                OnPropertyChanged();
            }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }
            #region locations
        private List<Geopoint> _schoolLocations;
        public List<Geopoint> SchoolLocations
        {
            get { return _schoolLocations; }
            set { SetProperty(ref _schoolLocations, value); }
        }
        private Geopoint _yourLocation;
        public Geopoint YourLocation
        {
            get { return _yourLocation; }
            set { SetProperty(ref _yourLocation, value); }
        }
        #endregion
        #endregion

        #region Initialization

        public SearchPrivateSchoolsViewModel(Resources resources, IPrivateSchoolsDataService privateSchoolsDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService)
        {
            _privateSchoolsDataService = privateSchoolsDataService;
            _messageResolver = messageResolver;
            _navigationService = navigationService;
            //SchoolsCView = new ExtendedCollectionView();
            Schools = new List<PrivateSchool>();
            Curriculums = new List<Geteducationalcurriculumsresult>();
            Levels = new List<Geteducationallevelsresult>();
            SchoolTypes = new List<Getschooltypesresult>();
            Regions = new List<Getregionsresult>();
            SchoolLocations = new List<Geopoint>();
            //Initialize Commands
         //   LoadCommand = new AsyncExtendedCommand(SearchSchoolsAsync);
            SchoolClickedCommand = new ExtendedCommand<PrivateSchool>(SchoolClicked);

            LoadData();
            
        }

        private async void LoadData()
        {
           await GetAllEducationalCurriculumsAsync();
           await GetAllEducationalLevelsAsync();
            await GetAllSchoolTypesAsync();
            await GetAllRegionsAsync();
          
        }


        #endregion

        #region Commands
      //  public AsyncExtendedCommand LoadCommand { get; set; }
        public ExtendedCommand<PrivateSchool> SchoolClickedCommand { get; set; }
        #endregion

        #region Methods
        public void InitializeLocationsList()
        {


            foreach (var item in Schools)
            {
                if (item.Latitude.HasValue && item.Longitude.HasValue)
                {
                    SchoolLocations.Add(new Geopoint(new BasicGeoposition
                    {
                        Latitude = item.Latitude.Value,
                        Longitude = item.Longitude.Value
                    }));
                }
            }

        }
        public async Task SearchSchoolsAsync()
        {
           
            IsBusy = true;
            ErrorMessage = null;
         //   LoadCommand.CanExecute = false;
            try
            {
                //if (SelectedCurriculum != null && SelectedLevel != null && SelectedRegion != null && SelectedSchoolType != null)
                //{
                    var parameters = new SearchSchoolParameters()
                    {
                        educationalCurriculumIDs = SelectedCurriculum != null ? new List<string> {  SelectedCurriculum.ID   }:null,
                        educationalLevelIDs = SelectedLevel != null ? new List<string> { SelectedLevel.ID } : null,
                        regionIDs = SelectedRegion != null ? new List<string> { SelectedRegion.ID } : null,
                        schoolTypeIDs = SelectedSchoolType != null ? new List<string> { SelectedSchoolType.ID } : null
                    };
                    var response = await _privateSchoolsDataService.SearchSchools(parameters);
                    if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                    {
                        Schools = response.Result;
                        _navigationService.NavigateByPage(typeof(PrivateSchoolsView),Schools);
                    }
                    else
                    {
                        ErrorMessage = _messageResolver.ResultToMessage(response);
                    }
               // }
            }
            catch(Exception e)
            {
                // ignored
            }
            finally
            {
              //  LoadCommand.CanExecute = true; 
                IsBusy = false;
            }
        }

        public async Task GetAllRegionsAsync()
        {
          
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                
                var response = await _privateSchoolsDataService.GetAllRegions();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    Regions = response.Result.GetRegionsResult.ToList();
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                    
                }
            }
            catch
            {
                // ignored
            }
            finally
            {
               
                IsBusy = false;
            }
        }
        public async Task GetAllEducationalCurriculumsAsync()
        {
          
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var response = await _privateSchoolsDataService.GetEducationalCurriculums();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    Curriculums = response.Result.GetEducationalCurriculumsResult.ToList()  ;
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                   
                }
            }
            catch
            {
                // ignored
            }
            finally
            {
               
                IsBusy = false;
            }
        }

        public async Task GetAllEducationalLevelsAsync()
        {
          
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var response = await _privateSchoolsDataService.GetEducationalLevels();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    Levels = response.Result.GetEducationalLevelsResult.ToList();
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);

                }
            }
            catch
            {
                // ignored
            }
            finally
            {
               
                IsBusy = false;
            }
        }

        public async Task GetAllSchoolTypesAsync()
        {
          
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var response = await _privateSchoolsDataService.GetSchoolTypes();
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    SchoolTypes = response.Result.GetSchoolTypesResult.ToList();
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);

                }
            }
            catch
            {
                // ignored
            }
            finally
            {
               
                IsBusy = false;
            }
        }
        
        

        private void SchoolClicked(PrivateSchool school)
        {
            if(school != null)
            _navigationService.NavigateByPage(typeof(SchoolDetailsView), school);
        }

        #endregion
    }


}