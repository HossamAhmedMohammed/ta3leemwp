﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.PrivateSchools.Response;
using Windows.Devices.Geolocation;

namespace Taaleem.ViewModels.PublicServices.PrivateSchoolsViewModels
{
  public  class PrivateSchoolDetailsViewModel : BindableBase
    {
        #region Fields
        private readonly IPrivateSchoolsDataService _privateSchoolsDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;

        #endregion

        #region Properties



        private PrivateSchool _school;
        public PrivateSchool School
        {
            get { return _school; }
            set
            {
                _school = value;
                OnPropertyChanged();

             
            }
        }

        private Geopoint _schoolLocation;
        public Geopoint SchoolLocation
        {
            get { return _schoolLocation; }
            set { SetProperty(ref _schoolLocation, value); }
        }

      
        private Getschooldetailsjsonresult _schoolDetails;
        public Getschooldetailsjsonresult SchoolDetails
        {
            get { return _schoolDetails; }
            set
            {
                _schoolDetails = value;
                OnPropertyChanged();
            }
        }
        private Getschoolinfodetailjsonresult _schoolInfoDetails;
        public Getschoolinfodetailjsonresult SchoolInfoDetails
        {
            get { return _schoolInfoDetails; }
            set
            {
                _schoolInfoDetails = value;
                OnPropertyChanged();
            }
        }


        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }
        #region locations
        
        private Geopoint _yourLocation;
        public Geopoint YourLocation
        {
            get { return _yourLocation; }
            set { SetProperty(ref _yourLocation, value); }
        }
        #endregion
        #endregion

        #region Initialization

        public PrivateSchoolDetailsViewModel(Resources resources, IPrivateSchoolsDataService privateSchoolsDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService)
        {
            _privateSchoolsDataService = privateSchoolsDataService;
            _messageResolver = messageResolver;
            _navigationService = navigationService;
            //SchoolsCView = new ExtendedCollectionView();

            //Initialize Commands

           

        }

        public async void LoadData()
        {
            await GetSchoolDetails();
            await GetSchoolInfoDetails();
        }




        #endregion

        #region Commands

        #endregion

        #region Methods
        public void InitializeLocations()
        {
            if (School.Longitude.HasValue && School.Latitude.HasValue)
            {
                SchoolLocation = new Geopoint(new BasicGeoposition
                {
                    Latitude = School.Latitude.Value,
                    Longitude = School.Longitude.Value
                });
            }

            if (School.Longitude.HasValue && School.Latitude.HasValue)
            {
                YourLocation = new Geopoint(new BasicGeoposition
                {
                    Latitude = School.Latitude.Value,
                    Longitude = School.Longitude.Value
                });
            }
        }
        public async Task GetSchoolDetails()
        {
          
            IsBusy = true;
            ErrorMessage = null;
            try
            {
                
                var response = await _privateSchoolsDataService.GetSchoolDetailsByID(School.SchoolID);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    SchoolDetails = response.Result.GetSchoolDetailsJsonResult;
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                    // SchoolsCView.Source = null;
                }
            }
            catch (Exception e)
            {
                // ignored
            }
            finally
            {
               
                IsBusy = false;
            }
        }
        public async Task GetSchoolInfoDetails()
        {

            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var response = await _privateSchoolsDataService.GetSchoolInfoDetailsByID(School.SchoolID);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    SchoolInfoDetails = response.Result.GetSchoolInfoDetailJsonResult;
                }
                else
                {
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                    // SchoolsCView.Source = null;
                }
            }
            catch (Exception e)
            {
                // ignored
            }
            finally
            {

                IsBusy = false;
            }
        }

        #endregion
    }


}