﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Common;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Windows.ApplicationModel.Contacts;
using Windows.ApplicationModel.Email;
using Windows.Devices.Geolocation;
using Windows.System;

namespace Taaleem.ViewModels.PublicServices
{
    public class AboutMaglesViewModel : BindableBase
    {
        #region Fields
        private readonly Resources _resources;
        private readonly INavigationService _navigationService;
        #endregion

        #region Properties

        private List<ContactPhone> _contacts;
        public List<ContactPhone> Contacts
        {
            get { return _contacts; }
            set { SetProperty(ref _contacts, value); }
        }

        private Geopoint _maglesLocation;
        public Geopoint MaglesLocation
        {
            get { return _maglesLocation; }
            set { SetProperty(ref _maglesLocation, value); }
        }

        #endregion

        #region Initialization

        public AboutMaglesViewModel(Resources resources, INavigationService navigationService)
        {
            _resources = resources;
            _navigationService = navigationService;
            Contacts = new List<ContactPhone>
        {
            new ContactPhone(){Description =_resources.HotLine,Number = "155"},
            new ContactPhone(){Description =_resources.OfficeLine,Number = "44044502"},
            new ContactPhone(){Description =_resources.OfficeLine,Number = "44044528"},

        };
            MaglesLocation = new Geopoint(new BasicGeoposition()
            {
                Latitude = 25.320734,
                Longitude = 51.525318
            });
            //Initialize Commands
            OpenYoutubeCommand = new ExtendedCommand(OpenYoutube);
            OpenFacebookCommand = new ExtendedCommand(OpenFacebook);
            OpenTwitterCommand = new ExtendedCommand(OpenTwitter);
            ShowEmailCommand = new ExtendedCommand(ShowEmail);
            RequestDirectionsCommand = new ExtendedCommand(ShowMaglesMap);
            CallPhoneCommand = new ExtendedCommand<ContactPhone>(MakePhoneCall);
        }

        #endregion

        #region Commands

        public ExtendedCommand OpenYoutubeCommand { get; set; }
        public ExtendedCommand OpenFacebookCommand { get; set; }
        public ExtendedCommand OpenTwitterCommand { get; set; }
        public ExtendedCommand ShowEmailCommand { get; set; }
        public ExtendedCommand RequestDirectionsCommand { get; set; }
        public ExtendedCommand<ContactPhone> CallPhoneCommand { get; set; }
        #endregion

        #region Methods

        private void OpenYoutube()
        {
            OpenLink("https://www.youtube.com/user/secqatar123/");
        }

        private void OpenFacebook()
        {
            OpenLink("https://www.facebook.com/pages/SECQatar/434366096645296");
        }

        private void OpenTwitter()
        {
            OpenLink("https://twitter.com/SEC_QATAR");
        }

        private async void ShowEmail()
        {

            // Define Recipient
            EmailRecipient sendTo = new EmailRecipient()
            {
                Name = _resources.MOEFullName,
                Address = "info@sec.gov.qa"
            };

            // Create email object
            EmailMessage mail = new EmailMessage();

            mail.To.Add(sendTo);
            // Open the share contract with Mail only:
            await EmailManager.ShowComposeNewEmailAsync(mail);
        }

        private void ShowMaglesMap()
        {
           // _navigationService.NavigateByPage<MaglesMapView>();
        }

        private async void RequestDirections()
        {
            // Get the values required to specify the destination.
            //string latitude = "25.320734";
            //string longitude = "51.525318";
            string name = _resources.MOEFullName;


            //Default app
            // Uri uri = new Uri(String.Format(@"bingmaps:?cp={0}~-{1}&lvl=10", latitude, longitude));
            //
            // Uri uri = new Uri(string.Format("{0}collection=point.{1}_-{2}_{4}&lvl={3}&cp={1}%7E{2}", "bingmaps:?", latitude, longitude, 16, "MOE"));
            Uri uri = new Uri("bingmaps:?collection=point.25.320734_-51.525318_" + Uri.EscapeUriString(name) + "&lvl=16");

            // Launch the Uri.
            var success = await Windows.System.Launcher.LaunchUriAsync(uri);

            if (success)
            {
                // Uri launched.
            }
            else
            {
                // Uri failed to launch.
            }
        }

        private void MakePhoneCall(ContactPhone contactPhone)
        {
            Windows.ApplicationModel.Calls.PhoneCallManager.ShowPhoneCallUI(contactPhone.Number, contactPhone.Description);
        }


        private async void OpenLink(string url)
        {
            Uri uri = new Uri(url);
            await Launcher.LaunchUriAsync(uri);
        }
        #endregion
    }

}
