﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models;
using Taaleem.Models.PublicServices;
using Taaleem.Models.PublicServices.Request;
using Taaleem.Models.PublicServices.Response;

namespace Taaleem.ViewModels.PublicServices
{
  public  class UniversitiesViewModel : BindableBase
    {
        #region Fields

        private readonly IPublicSectorDataService _publicDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly Resources _resources;
        #endregion

        #region Properties
        private List<Country> _countries;
        public List<Country> Countries
        {
            get { return _countries; }
            set { SetProperty(ref _countries, value); }
        }

        private Country _selectedCountry;
        public Country SelectedCountry
        {
            get { return _selectedCountry; }
            set { SetProperty(ref _selectedCountry, value); }
        }

        private List<CountryState> _states;
        public List<CountryState> States
        {
            get { return _states; }
            set { SetProperty(ref _states, value); }
        }

        private CountryState _selectedState;
        public CountryState SelectedState
        {
            get { return _selectedState; }
            set { SetProperty(ref _selectedState, value); }
        }

        private List<Major> _majors;
        public List<Major> Majors
        {
            get { return _majors; }
            set { SetProperty(ref _majors, value); }
        }

        private List<University> _universities;
        public List<University> Universities
        {
            get { return _universities; }
            set { SetProperty(ref _universities, value); }
        }

        private University _selectedUniversity;
        public University SelectedUniversity
        {
            get { return _selectedUniversity; }
            set
            {
                if (SetProperty(ref _selectedUniversity, value))
                {
                    SearchScholarshipCommand.CanExecute = value != null;
                }
            }
        }

        private Major _selectedMajor;
        public Major SelectedMajor
        {
            get { return _selectedMajor; }
            set { SetProperty(ref _selectedMajor, value); }
        }

        private bool _isLoading;

        /// <summary>
        /// A value of true indicates that there is at least one ongiong operation.
        /// </summary>
        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetProperty(ref _isLoading, value); }
        }

        //The following Status properties will be used to update UI
        private OperationStatus _loadStatesStatus;
        public OperationStatus LoadStatesStatus
        {
            get { return _loadStatesStatus; }
            set { SetProperty(ref _loadStatesStatus, value); }
        }

        public OperationStatus LoadUniversitiesStatus { get; private set; }
        public OperationStatus LoadMajorsStatus { get; private set; }
        public OperationStatus SearchScholarStatus { get; private set; }

        #endregion

        #region Initialization

        public UniversitiesViewModel(IPublicSectorDataService publicDataService, IRequestMessageResolver messageResolver, INavigationService navigationService, Resources resources)
        {
            _publicDataService = publicDataService;
            _messageResolver = messageResolver;
            _navigationService = navigationService;
            _resources = resources;
            //Initialize operation status for each request
            LoadStatesStatus = new OperationStatus();
            LoadUniversitiesStatus = new OperationStatus();
            LoadMajorsStatus = new OperationStatus();
            SearchScholarStatus = new OperationStatus();

            //Initialize Commands
            CountryChangedCommand = new AsyncExtendedCommand<Country>(CountryChangedAsync);
            StateChangedCommand = new AsyncExtendedCommand<CountryState>(StateChangedAsync);
            UniversityChangedCommand = new AsyncExtendedCommand<University>(UniversityChangedAsync);
            SearchScholarshipCommand = new AsyncExtendedCommand(SearchScholarshipAsync, false);
        }
        #endregion

        #region Commands

        public AsyncExtendedCommand<Country> CountryChangedCommand { get; set; }
        public AsyncExtendedCommand<CountryState> StateChangedCommand { get; set; }
        public AsyncExtendedCommand<University> UniversityChangedCommand { get; set; }
        public AsyncExtendedCommand SearchScholarshipCommand { get; set; }
        #endregion

        #region Methods

        private async Task CountryChangedAsync(Country newCountry)
        {
            SelectedCountry = newCountry;
            if (IsLoading) return;
            try
            {
                CountryChangedCommand.CanExecute = false;
                IsLoading = true;

                //Clear previous status
                ClearStates();
                ClearUniversities();
                ClearMajors();

                //Load dependeant tasks in parallel
                var tasks = new List<Task>
                {
                    LoadStates(),
                    LoadUniversitiesAsync(),
                    LoadMajorsAsync()
                };
                await Task.WhenAll(tasks);

            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                IsLoading = false;
                CountryChangedCommand.CanExecute = true;
            }
        }

        private async Task StateChangedAsync(CountryState state)
        {
            SelectedState = state;
            if (IsLoading) return;

            try
            {
                StateChangedCommand.CanExecute = false;
                IsLoading = true;

                //Clear previous status
                ClearUniversities();
                ClearMajors();

                //Load dependeant tasks in parallel
                var tasks = new List<Task>
                {
                    LoadUniversitiesAsync(),
                    LoadMajorsAsync()
                };
                await Task.WhenAll(tasks);

            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                IsLoading = false;
                StateChangedCommand.CanExecute = true;
            }
        }

        private async Task UniversityChangedAsync(University university)
        {
            SelectedUniversity = university;
            if (IsLoading) return;
            try
            {
                UniversityChangedCommand.CanExecute = false;
                IsLoading = true;

                //Clear previous status
                ClearMajors();
                //Load dependeant tasks in parallel
                var tasks = new List<Task>
                {
                    LoadMajorsAsync()
                };
                await Task.WhenAll(tasks);

            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                IsLoading = false;
                UniversityChangedCommand.CanExecute = true;
            }
        }

        private async Task LoadStates()
        {
            Debug.WriteLine("Load States");
            try
            {
                LoadStatesStatus.ResetStatus();
                LoadStatesStatus.IsBusy = true;
                var parameters = new GetStatesParameters()
                {
                    Country = SelectedCountry.Value
                };

                var response = await _publicDataService.GetCountryStatesAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (IsDummySelect(response.Result[0].Text))
                    {
                        response.Result[0].Text = _resources.All;
                        response.Result[0].Value = "";
                    }
                    States = response.Result;

                    if (States.Count == 1) SelectedState = States[0];
                }
                else if (response.ResponseStatus == ResponseStatus.SuccessWithNoData)
                {
                    //Don't display error
                }
                else
                {
                    LoadStatesStatus.ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadStatesStatus.IsBusy = false;
            }
        }

        private bool IsDummySelect(string text)
        {
            return String.Equals(text, @"select", StringComparison.OrdinalIgnoreCase)
                   || String.Equals(text, @"اختر", StringComparison.OrdinalIgnoreCase)
                   || String.Equals(text, @"أختر", StringComparison.OrdinalIgnoreCase);
        }

        private async Task LoadUniversitiesAsync()
        {
            Debug.WriteLine("Load Universities");
            try
            {
                LoadUniversitiesStatus.ResetStatus();
                LoadUniversitiesStatus.IsBusy = true;
                var parameters = new GetUniversityParameters()
                {
                    Country = SelectedCountry != null ? SelectedCountry.Value : null,
                    State = SelectedState != null ? SelectedState.Value : null
                };

                var response = await _publicDataService.GetUniversitiesAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (IsDummySelect(response.Result[0].Text))
                    {
                        response.Result[0].Text = _resources.All;
                        response.Result[0].Value = "";
                        if (response.Result.Count == 1)
                        {
                            LoadUniversitiesStatus.ErrorMessage = RequestMessage.GetNoDataMessage();
                            return;//Don't set university to all
                        }
                        response.Result.RemoveAt(0);
                    }
                    Universities = response.Result;
                }
                else
                {
                    LoadUniversitiesStatus.ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadUniversitiesStatus.IsBusy = false;
            }
        }

        private async Task LoadMajorsAsync()
        {
            Debug.WriteLine("Load Majors");

            try
            {
                LoadMajorsStatus.ResetStatus();
                LoadMajorsStatus.IsBusy = true;

                var parameters = new GetMajorParameters()
                {
                    University = SelectedUniversity != null ? SelectedUniversity.Value : null,
                };

                var response = await _publicDataService.GetMajorsAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    if (IsDummySelect(response.Result[0].Text))
                    {
                        response.Result[0].Text = _resources.All;
                        response.Result[0].Value = "";
                    }
                    Majors = response.Result;

                    if (Majors.Count == 1) SelectedMajor = Majors[0];
                }
                else if (response.ResponseStatus == ResponseStatus.SuccessWithNoData)
                {
                    //Don't display error
                }
                else
                {
                    LoadMajorsStatus.ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadMajorsStatus.IsBusy = false;
            }
        }

        private async Task SearchScholarshipAsync()
        {
            try
            {
                if (IsLoading) return;
                IsLoading = true;
                SearchScholarshipCommand.CanExecute = false;
                SearchScholarStatus.ResetStatus();
                SearchScholarStatus.IsBusy = true;
                var parameters = new SearchForScholarsParameters()
                {
                    Country = SelectedCountry != null ? SelectedCountry.Text : null,
                    State = SelectedState != null ? SelectedState.Value : null,
                    Major = SelectedMajor != null ? SelectedMajor.Value : null,
                    Univer = SelectedUniversity != null ? SelectedUniversity.Value : null,
                };

                var response = await _publicDataService.SearchForScholarshipAsync(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    List<Scholar> scholars = ParseScholars(response.Result);
                    var navParameters = new ScholarsNavigationParameters()
                    {
                        Country = SelectedCountry != null ? SelectedCountry.Value : null,
                        University = SelectedUniversity != null && !String.IsNullOrEmpty(SelectedUniversity.Value) ? SelectedUniversity.Text : null,
                        Scholars = scholars
                    };
                  //  _navigationService.NavigateByPage<ScholarsView>(navParameters);
                }
                else
                {
                    SearchScholarStatus.ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                IsLoading = false;
                SearchScholarshipCommand.CanExecute = true;
                SearchScholarStatus.IsBusy = false;
            }
        }

        private List<Scholar> ParseScholars(string html)
        {
            var list = new List<Scholar>();
            if (String.IsNullOrEmpty(html)) return list;
            try
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(html);
                var rows = doc.DocumentNode.Descendants("tr").ToList();
                if (rows != null)
                {
                    //Skip First Row (index 0), It contains table header
                    for (int rowIndex = 1; rowIndex < rows.Count; rowIndex++)
                    {
                        //Td elements
                        var tdElements = rows[rowIndex].Descendants("td").ToList();
                        if (tdElements.Count >= 3)
                        {
                            var scholar = new Scholar();
                            scholar.Country = GetInnerText(tdElements[0]);
                            scholar.University = GetInnerText(tdElements[1]);
                            scholar.Major = GetInnerText(tdElements[2]);
                            list.Add(scholar);
                        }
                    }
                }
                return list;
            }
            catch (Exception)
            {

                return list;
            }
        }

        private string GetInnerText(HtmlNode td)
        {
            string text = td.InnerText;
            return text != null ? text.Trim() : null;
        }

        private void ClearMajors()
        {
            SelectedMajor = null;
            Majors = null;
        }

        private void ClearUniversities()
        {
            SelectedUniversity = null;
            Universities = null;
        }

        private void ClearStates()
        {
            SelectedState = null;
            States = null;
        }
        #endregion
    }
}
