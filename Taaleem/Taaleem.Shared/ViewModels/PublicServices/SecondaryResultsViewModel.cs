﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.PublicServices.Request;
using Taaleem.Models.PublicServices.Response;
using Taaleem.Views.PublicServices;

namespace Taaleem.ViewModels.PublicServices
{
    public class SecondaryResultsViewModel : BindableBase
    {
        #region Fields
        private readonly IPublicSectorDataService _publicSectorDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly INavigationService _navigationService;
        private readonly IAppSettings _appSettings;
        #endregion

        #region Properties

        private ObservableCollection<Studentgrade> _results;
        public ObservableCollection<Studentgrade> Results
        {
            get { return _results; }
            set { SetProperty(ref _results, value);
              
            }
        }
        private NatejaResult _nateja;
        public NatejaResult Nateja
        {
            get { return _nateja; }
            set { SetProperty(ref _nateja, value); }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }
        private float _totalResult;
        public float TotalResult
        {
            get { return _totalResult; }
            set { SetProperty(ref _totalResult, value); }
        }
        private float _totalGrade;
        public float TotalGrade
        {
            get { return _totalGrade; }
            set { SetProperty(ref _totalGrade, value); }
        }
        private string _seatNumber;
        public string SeatNumber
        {
            get { return _seatNumber; }
            set { SetProperty(ref _seatNumber, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }


        #endregion

        #region Initialization

        public SecondaryResultsViewModel(Resources resources, IPublicSectorDataService publicSectorDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, IDialogManager dialogManager, INavigationService navigationService)
        {
            _publicSectorDataService = publicSectorDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;

            _results = new ObservableCollection<Studentgrade>();
            //Initialize Commands
            LoadCommand = new AsyncExtendedCommand<string>(LoadResultsAsync);
            ShowResultsCommand = new ExtendedCommand<string>(showResults);
        }

       

        #endregion

        #region Commands
        public AsyncExtendedCommand<string> LoadCommand { get; set; }
        public ExtendedCommand<string> ShowResultsCommand { get; set; }
        #endregion

        #region Methods
        private   void showResults(string arg)
        {
            if(arg != null)
            _navigationService.NavigateByPage<ResultDetails>(arg);
        }
        public async Task LoadResultsAsync(string seat)
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var parameters = new SecondaryResultParameters()
                {
                    ID = seat
                };

                var response = await _publicSectorDataService.GetSecondaryNatigaResult(parameters);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    Nateja = response.Result;
                    foreach (Studentgrade item in response.Result.GetEnrollmentResult.StudentGrades)
                    {
                        Results.Add(item);
                    }
                    TotalResult = response.Result.GetEnrollmentResult.YearGradePercentage ;
                    TotalGrade = response.Result.GetEnrollmentResult.YearGradeNumeric;
                }
                else
                {
                    Results = null;
                    ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }

     
        #endregion
    }
}

