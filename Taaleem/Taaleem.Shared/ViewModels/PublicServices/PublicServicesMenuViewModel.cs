﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.PublicServices;
using Taaleem.Views.Exceptions;
using Taaleem.Views.PublicServices;
using Taaleem.Views.PublicServices.PreRegistration;
using Taaleem.Views.PublicServices.PrivateSchoolsViews;
using Windows.System;
using Windows.UI.Xaml.Controls;

namespace Taaleem.ViewModels.PublicServices
{
    public class PublicServicesMenuViewModel : BindableBase
    {
        #region Fields
        private readonly Resources _resources;
        private readonly IEmployeesDataService _employeesDataService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly IAppSettings _appSettings;
        private readonly INavigationService _navigationService;
        private readonly IPreRegistrationDataService _preRegistrationDataService;
        #endregion

        #region Properties
        private bool _isBusy;
      



        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private List<PublicServicesMenuItem> pSMenu;

        public List<PublicServicesMenuItem> PSMenu
        {
            get { return pSMenu; }
            set { SetProperty(ref pSMenu, value); }
        }

        private List<PublicServicesMenuItem> scholarshipsMenu;

        public List<PublicServicesMenuItem> ScholarshipsMenu
        {
            get { return scholarshipsMenu; }
            set { SetProperty(ref scholarshipsMenu, value);
                OnPropertyChanged();
            }
        }
        private List<PublicServicesMenuItem> privateSchoolsMenu;

        public List<PublicServicesMenuItem> PrivateSchoolsMenu
        {
            get { return privateSchoolsMenu; }
            set
            {
                SetProperty(ref privateSchoolsMenu, value);
                OnPropertyChanged();
            }
        }
        #endregion

        #region Initialization

        public PublicServicesMenuViewModel(IPreRegistrationDataService preRegistrationDataService, Resources resources, IEmployeesDataService employeesDataService, IRequestMessageResolver messageResolver, IAppSettings appSettings, INavigationService navigationService)
        {
            _resources = resources;
            _employeesDataService = employeesDataService;
            _messageResolver = messageResolver;
            _appSettings = appSettings;
            _navigationService = navigationService;
            _preRegistrationDataService = preRegistrationDataService;
            pSMenu = new List<PublicServicesMenuItem>();
            scholarshipsMenu = new List<PublicServicesMenuItem>();
            PrivateSchoolsMenu = new List<PublicServicesMenuItem>();
            //Initialize Commands
         
            ItemClicked = new ExtendedCommand<ItemClickEventArgs>(NavigateTo);
            UniItemClicked = new ExtendedCommand<ItemClickEventArgs>(UniNavigateto);
            PrivateSchoolsMenuItemClicked = new ExtendedCommand<ItemClickEventArgs>(PriSchoolsMenItemClicked);
            LoadPublicServicesMenuItems();
          //  LoadScholarshipsMenuItems();
        }

      


        #endregion

        #region Commands

        public ExtendedCommand<ItemClickEventArgs> ItemClicked { get; set; }
        public ExtendedCommand<ItemClickEventArgs> UniItemClicked { get; set; }
        public ExtendedCommand<ItemClickEventArgs> PrivateSchoolsMenuItemClicked { get; set; }
        #endregion

        #region Methods
        public void LoadPublicServicesMenuItems()
        {
            pSMenu.Clear();
            pSMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/school_search_ic@3x.png", Name = _resources.WhereMySchool });
            pSMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/school_university_ic@3x.png", Name = _resources.UniversitiesGuide });
            pSMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/school_graduation_ic@3x.png", Name = _resources.Secondary });
            pSMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/generalservice_center_ic@3x.png", Name = _resources.EducationCenters });
            pSMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/generalservice_certficate_ic@3x.png", Name = _resources.ExceptionRequests });
            pSMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/PrivateSchools/generalservice_privateschoole_ic@3x.png", Name = _resources.PrivateSchools });
            pSMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/school_about_ic@3x.png", Name = _resources.PreRegistration });

        }

        public void LoadScholarshipsMenuItems()
        {
            ScholarshipsMenu.Clear();
            ScholarshipsMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/school_search_ic@3x.png", Name = _resources.SearchUniversity });
            ScholarshipsMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/schoolerships_university_list@3x.png", Name = _resources.ListOfAccreditedUniversites });
            ScholarshipsMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/schoolerships_doc_ic@3x.png", Name = _resources.StudentGuideScholarship });
            ScholarshipsMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/school_graduation_ic@3x.png", Name = _resources.OtherScholarships });
            ScholarshipsMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/schoolerships_gift_ic@3x.png", Name = _resources.ScholarshipRequest });
          //  ScholarshipsMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/school_about_ic@3x.png", Name = _resources.News });
            ScholarshipsMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/schoolerships_mail_ic@3x.png", Name = _resources.NeedHelp });
        }

        public void LoadPrivateSchoolsMenuItems()
        {
            PrivateSchoolsMenu.Clear();
            PrivateSchoolsMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/PrivateSchools/privateschool_guide_ic@3x.png", Name = _resources.SchoolsGuide });
            PrivateSchoolsMenu.Add(new PublicServicesMenuItem { Image = "ms-appx:///Assets/PublicServices/Menu/PrivateSchools/privateschool_-licenses_ic@3x.png", Name = _resources.SchoolLicenses });
        }

        private async void UniNavigateto(ItemClickEventArgs obji)
        {
            PublicServicesMenuItem obj = obji.ClickedItem as PublicServicesMenuItem;
            if (obj.Name == _resources.SearchUniversity)
            {
                Uri uri = new Uri("https://scholarship.sec.gov.qa/Home/SearchUniversity");
                await Launcher.LaunchUriAsync(uri);
              
            }
            else if (obj.Name == _resources.ListOfAccreditedUniversites)
            {
                Uri uri = new Uri("http://www.edu.gov.qa/Ar/structure/HigherEducation/HigherEducationInstitute/Offices/Pages/universities_list.aspx");
                await Launcher.LaunchUriAsync(uri);
            }
            if (obj.Name == _resources.StudentGuideScholarship)
            {
                Uri uri = new Uri("https://scholarship.sec.gov.qa/Docs/StudentGuid2016-2017.pdf");
                await Launcher.LaunchUriAsync(uri);
            }
            else if (obj.Name == _resources.OtherScholarships)
            {
                Uri uri = new Uri("https://scholarship.sec.gov.qa/Home/OtherScholarship");
                await Launcher.LaunchUriAsync(uri);
            }
            if (obj.Name == _resources.ScholarshipRequest)
            {
                Uri uri = new Uri("https://scholarship.sec.gov.qa/account/student_login");
                await Launcher.LaunchUriAsync(uri);
            }
            if (obj.Name == _resources.News)
            {
                Uri uri = new Uri("https://scholarship.sec.gov.qa/Public/News");
                await Launcher.LaunchUriAsync(uri);
            }
     
            else if (obj.Name == _resources.NeedHelp)
            {
                Uri uri = new Uri("https://scholarship.sec.gov.qa/Home/Assistance");
                await Launcher.LaunchUriAsync(uri);
            }
        }

        private async void NavigateTo(ItemClickEventArgs obji)
        {
            PublicServicesMenuItem obj = obji.ClickedItem as PublicServicesMenuItem;
            if (obj.Name == _resources.WhereMySchool)
            {
                _navigationService.NavigateByPage<MySchool>();
            }
            else if (obj.Name == _resources.UniversitiesGuide)
            {
                _navigationService.NavigateByPage<Universities>();
            }
            else if (obj.Name == _resources.Secondary)
            {
                _navigationService.NavigateByPage<Secondary>();
            }
            else if (obj.Name == _resources.EducationCenters)
            {
                Uri uri = new Uri("https://ecenters.sec.gov.qa/");
                await Launcher.LaunchUriAsync(uri);
            }
            else if (obj.Name == _resources.NationalCertificates)
            {
                Uri uri = new Uri("https://nce.sec.gov.qa");
                await Launcher.LaunchUriAsync(uri);
            }
            else if (obj.Name == _resources.PrivateSchools)
            {
                _navigationService.NavigateByPage<PrivateSchools>();
            }
            else if (obj.Name == _resources.ExceptionRequests)
            {

                _appSettings.CurrentModule = 1;
                if (_appSettings.LoginUserName == null && _appSettings.LoginPassword == null)
                    _navigationService.NavigateByPage<Views.PublicServices.Login.Login>();
                else if (_appSettings.LoginUserName != null && _appSettings.LoginPassword != null)
                {
                    ViewModelLocator.Locator.BaseReq.UserName = _appSettings.LoginUserName;
                    ViewModelLocator.Locator.BaseReq.Password = _appSettings.LoginPassword;
                    _navigationService.NavigateByPage<MyRequests>();
                }

            }
            else if (obj.Name ==_resources.PreRegistration)
            {
                _appSettings.CurrentModule = 2;
                if (_appSettings.LoginUserName == null && _appSettings.LoginPassword == null)
                    _navigationService.NavigateByPage<Views.PublicServices.Login.Login>();
                else if (_appSettings.LoginUserName != null && _appSettings.LoginPassword != null)
                {
                    ViewModelLocator.Locator.BaseReq.UserName = _appSettings.LoginUserName;
                    ViewModelLocator.Locator.BaseReq.Password = _appSettings.LoginPassword;
                    var res = await _preRegistrationDataService.Login(new Taaleem.Models.PreRegistration.Request.LoginParameters { createPersistentCookie = "false", userName = _appSettings.LoginUserName, password = _appSettings.LoginPassword });
                    if (res.ResponseStatus == ResponseStatus.SuccessWithResult)
                    {
                        if (res.Result.d == true)
                        {
                            _navigationService.NavigateByPage(typeof(RegisterAndTransfer));
                        }
                        else
                        {
                            // TO DO
                        }
                    }
                }
            }
        }

        private async void PriSchoolsMenItemClicked(ItemClickEventArgs obji)
        {
            PublicServicesMenuItem obj = obji.ClickedItem as PublicServicesMenuItem;

            if (obj.Name == _resources.SchoolsGuide)
            {

                _navigationService.NavigateByPage(typeof(Search));

            }
            else if (obj.Name == _resources.SchoolLicenses)
            {

                if (_appSettings.LanguageId == 2)
                {
                    Uri uri = new Uri("https://elr.sec.gov.qa/apps/elr/arabic/Pages/default.aspx");
                    await Launcher.LaunchUriAsync(uri);
                }
                else if (_appSettings.LanguageId == 1)
                {

                    Uri uri = new Uri("https://elr.sec.gov.qa/apps/elr/arabic/Pages/default.aspx");
                    await Launcher.LaunchUriAsync(uri);
                }
                
            }
        }
        #endregion
    }
}