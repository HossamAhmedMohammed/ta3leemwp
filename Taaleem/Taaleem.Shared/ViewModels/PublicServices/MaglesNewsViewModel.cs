﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Helpers;
using Taaleem.Helpers.Interfaces;
using Taaleem.Models.PublicServices;
using Taaleem.Views.PublicServices;

namespace Taaleem.ViewModels.PublicServices
{
    public class MaglesNewsViewModel : BindableBase
    {
        #region Fields

        private readonly INavigationService _navigationService;
        private readonly IRequestMessageResolver _messageResolver;
        private readonly IPublicSectorDataService _publicService;
        private readonly IDialogManager _dialogManager;
        private readonly Resources _resources;
        #endregion

        #region Properties

        private List<MaglesNewsItem> _newsList;
        public List<MaglesNewsItem> NewsList
        {
            get { return _newsList; }
            set { SetProperty(ref _newsList, value); }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        #endregion

        #region Initialization

        public MaglesNewsViewModel(Resources resources, IRequestMessageResolver messageResolver, IPublicSectorDataService publicService, INavigationService navigationService , IDialogManager dialogManager)
        {
            _messageResolver = messageResolver;
            _publicService = publicService;
            _navigationService = navigationService;
            _dialogManager = dialogManager;
            _resources = resources;
            //Initialize Commands

            LoadCommand = new AsyncExtendedCommand(LoadFeedsAsync);
            ItemClickedCommand = new ExtendedCommand<MaglesNewsItem>(ItemClicked);
        }

        #endregion

        #region Commands

        public AsyncExtendedCommand LoadCommand { get; set; }
        public ExtendedCommand<MaglesNewsItem> ItemClickedCommand { get; set; }
        #endregion

        #region Methods
        private async Task LoadFeedsAsync()
        {
            LoadCommand.CanExecute = false;
            IsBusy = true;
            ErrorMessage = null;
            try
            {

                var response = await _publicService.GetNewsAsync();

                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    NewsList = response.Result.Select(item => MaglesNewsItem.Wrap(item, _publicService)).ToList();
                }
                else
                {
                   await _dialogManager.ShowMessage("", _resources.NoInternet);
                   // ErrorMessage = _messageResolver.ResultToMessage(response);
                }
            }
            finally
            {
                LoadCommand.CanExecute = true;
                IsBusy = false;
            }
        }

        private void ItemClicked(MaglesNewsItem item)
        {
            _navigationService.NavigateByPage<NewsDetails>(item);
        }

        #endregion
    }
}
