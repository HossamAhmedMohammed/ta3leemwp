﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Common;

namespace Taaleem.ViewModels
{
    public class EmptyViewModel : BindableBase
    {
        #region Fields
        #endregion

        #region Properties


        #endregion

        #region Initialization

        public EmptyViewModel()
        {
            //Initialize Commands
        }

        #endregion

        #region Commands

        #endregion

        #region Methods


        #endregion
    }
}