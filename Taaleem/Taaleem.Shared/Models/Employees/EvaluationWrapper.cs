﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.Employees.Response;

namespace Taaleem.Models.Employees
{
    public class EvaluationWrapper
    {
        public string Description { get; set; }
        public string Value { get; set; }
        public string Year { get; set; }

        public static EvaluationWrapper Wrap(Evaluation evaluation, int languageId)
        {
            EvaluationWrapper wrapper = new EvaluationWrapper
            {
                Value = evaluation.Value,
                Year = evaluation.Year,
            };
            switch (languageId)
            {
                case Lanuage.English:
                    wrapper.Description = evaluation.DescriptionEn;
                    break;
                case Lanuage.Arabic:
                    wrapper.Description = evaluation.DescriptionAr;
                    break;
                default:
                    break;
            }
            return wrapper;
        }
    }
}
