﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Employees.Request
{
    public class SalaryParameters
    {
        [QueryParameter("id")]
        public string Id { get; set; }

        //yyyy
        [QueryParameter("year")]
        public string Year { get; set; }

        //m, 1-12
        [QueryParameter("month")]
        public string Month { get; set; }


    }
}
