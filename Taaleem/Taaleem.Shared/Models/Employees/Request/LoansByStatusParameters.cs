﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Employees.Request
{
    public class LoansByStatusParameters
    {
        [QueryParameter("id")]
        public string Id { get; set; }

        [QueryParameter("status")]
        public string Status { get; set; }
    }
}

