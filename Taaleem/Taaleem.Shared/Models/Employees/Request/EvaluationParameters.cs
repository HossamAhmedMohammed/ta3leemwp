﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Employees.Request
{
    public class EvaluationParameters
    {
        [QueryParameter("id")]
        public string Id { get; set; }

        /// <summary>
        /// year is yyyy or All   e.g 2014 or All
        /// </summary>
        [QueryParameter("year")]
        public string Year { get; set; }
    }
}
