﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.Request
{
    public class EmpolyeeLoginBody
    {
        [JsonProperty("password")]
        public string Password { get; set; }
        [JsonProperty("userName")]
        public string UserName { get; set; }
    }
}
