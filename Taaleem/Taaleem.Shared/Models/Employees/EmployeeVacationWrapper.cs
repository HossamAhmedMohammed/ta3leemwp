﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Taaleem.Models.Employees.Response;
using Taaleem.ViewModels;

namespace Taaleem.Models.Employees
{
    public class EmployeeVacationWrapper
    {
        public string Name { get; set; }
        public string NumberOfDays { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public string LocalizedStatus { get; set; }

        public static EmployeeVacationWrapper Wrap(EmployeeVacation vacation, int languageId)
        {
            EmployeeVacationWrapper wrapper = new EmployeeVacationWrapper
            {
                NumberOfDays = vacation.NumberOfDays,
                Status = vacation.Status,
                StartDate = ParseDate(vacation.StartDate),
                EndDate = ParseDate(vacation.EndDate),
            };
            switch (languageId)
            {
                case Lanuage.English:
                    wrapper.Name = vacation.NameEn;
                    break;
                case Lanuage.Arabic:
                    wrapper.Name = vacation.NameAr;
                    break;
                default:
                    break;
            }

            var resources = ViewModelLocator.Resources;

            if (String.IsNullOrEmpty(vacation.Status)) wrapper.LocalizedStatus = "-";
            else if (String.Equals(vacation.Status, @"NOTCALCULATED", StringComparison.OrdinalIgnoreCase))
                wrapper.LocalizedStatus = resources.No;
            //Todo:replace with the real result that the server will return
            else if (String.Equals(vacation.Status, @"CALCULATED", StringComparison.OrdinalIgnoreCase))
                wrapper.LocalizedStatus = resources.Yes;
            else wrapper.LocalizedStatus = "-";
            return wrapper;
        }

        private static DateTime ParseDate(string date)
        {
            if (String.IsNullOrWhiteSpace(date)) return new DateTime();
            try
            {
                return DateTime.ParseExact(date, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return new DateTime();
            }
        }
    }
}
