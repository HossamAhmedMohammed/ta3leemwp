﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.ViewModels;

namespace Taaleem.Models.Employees
{
    public class VacationGroupType
    {
        public string DisplayName { get; set; }
        public string PropertyName { get; set; }
        public int Type { get; set; }

        public VacationGroupType(string displayName, string propertyName, int type)
        {
            DisplayName = displayName;
            PropertyName = propertyName;
            Type = type;
        }
        public static readonly VacationGroupType Year = new VacationGroupType(ViewModelLocator.Resources.Year, @"StartDate.Year", 1);

        public static readonly VacationGroupType Vacation = new VacationGroupType(ViewModelLocator.Resources.VacationType, @"Name", 2);

        public static readonly List<VacationGroupType> GroupTypes = new List<VacationGroupType>
        {
           Vacation,Year
        };
    }
}
