﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.Employees.Response;
using Taaleem.ViewModels;

namespace Taaleem.Models.Employees
{
    public class LoanWrapper
    {
        public string Balance { get; set; }
        public string Name { get; set; }
        public string Paid { get; set; }
        public string Status { get; set; }
        public string LocalizedStatus { get; set; }
        public string Value { get; set; }

        public static LoanWrapper Wrap(EmployeeLoan loan, int languageId)
        {
            if (!String.IsNullOrEmpty(loan.Status)) loan.Status = loan.Status.Trim();
            LoanWrapper wrapper = new LoanWrapper
            {
                Value = String.IsNullOrEmpty(loan.Value) || loan.Value == "0" ? ViewModelLocator.Resources.DoesnotExist : loan.Value,
                Status = String.IsNullOrEmpty(loan.Status) || loan.Status == "0" ? ViewModelLocator.Resources.DoesnotExist : loan.Status,
                Paid = String.IsNullOrEmpty(loan.Paid) || loan.Paid == "0" ? ViewModelLocator.Resources.DoesnotExist : loan.Paid,
                Balance = String.IsNullOrEmpty(loan.Balance) || loan.Balance == "0" ? ViewModelLocator.Resources.DoesnotExist : loan.Balance
            };
            switch (languageId)
            {
                case Lanuage.English:
                    wrapper.Name = loan.NameEn;
                    break;
                case Lanuage.Arabic:
                    wrapper.Name = loan.NameAr;
                    break;
                default:
                    break;
            }

            var resources = ViewModelLocator.Resources;

            if (loan.Status == null) wrapper.LocalizedStatus = resources.Loans;
            else if (loan.Status == LoanType.Active.Status || String.Equals(loan.Status, "active", StringComparison.OrdinalIgnoreCase))
                wrapper.LocalizedStatus = resources.ActiveLoans;
            else if (loan.Status == LoanType.Completed.Status || String.Equals(loan.Status, "completed", StringComparison.OrdinalIgnoreCase))
                wrapper.LocalizedStatus = resources.CompletedLoans;
            else wrapper.LocalizedStatus = resources.Loans;
            return wrapper;
        }
    }
}
