﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Helpers;
using Taaleem.Models.Employees.MyRequests.Response;
using System.Linq;
using Taaleem.ViewModels.Employees;

namespace Taaleem.Models.Employees.MyRequests
{
    public class SectionItem
    {
        public List<SectionGroup> Groups { get; set; }
    }

    public class SectionGroup
    {
        public List<SectionSubItem> Items { get; set; }
    }

    public class SectionSubItem
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class SectionsListWrapper //: SectionItem
    {
        //public List<SectionSubItem> Items { get; set; }
        public string Applicant { get; set; }
        public string LeaveType { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public float RequiredDays { get; set; }
        public string DepartureDate { get; set; }
        public string JobNo { get; set; }
        public string JobTitle { get; set; }
        public string Status { get; set; }
        public string InstituteDepartmentUnit { get; set; }
        public string LoanType { get; set; }
        public string LoanAmount { get; set; }
        public string Installment { get; set; }
        public string PaymentPeriod { get; set; }
        public string RequestedDocuments { get; set; }

        public MyRequestTypeEnum Type { get; set; }

        public static SectionsListWrapper Wrap(LeaveRequestResponse response, int languageId, MyRequestTypeEnum type)
        {
            switch (type)
            {
                case MyRequestTypeEnum.PersonalLoan:
                    return new SectionsListWrapper
                    {
                        LoanType = languageId == 1 ? response.RequiredLoanType : response.RequiredLoanTypeAr,

                        Applicant = languageId == 1 ? response.EmpName : response.EmpNameAr,
                        JobNo = response.JobNo,
                        JobTitle = languageId == 1 ? response.JobTitle : response.JobTitleAr,
                        LoanAmount = response.LoanAmount.HasValue? response.LoanAmount.Value.ToString(): "null",
                        Installment = response.MonthlyInstallment.HasValue ? response.MonthlyInstallment.Value.ToString() : "null",
                        PaymentPeriod = response.PaymentPeriod.HasValue ? response.PaymentPeriod.Value.ToString() : "null",
                        Status = languageId == 1 ? response.StatusEn : response.StatusAr,
                        Type = type
                    };
                default:
                    return new SectionsListWrapper
                    {
                        Applicant = languageId == 1 ? response.EmpName : response.EmpNameAr,
                        LeaveType = languageId == 1 ? response.LeaveType?.Trim() : response.LeaveTypeAr?.Trim(),
                        StartDate = GetDate(response.FirstDayOfLeave, languageId),
                        EndDate = languageId == 1 ? response.LastDayOfLeave.ToString("dd-MM-yyyy") : response.LastDayOfLeave.ToString("yyyy-MM-dd"),
                        RequiredDays = Math.Max(response.NoOfDaysRequired.HasValue? response.NoOfDaysRequired.Value:0, response.NumberofDays.HasValue? response.NumberofDays.Value: 0),
                        DepartureDate = GetDate(response.ActualDepartureDate, languageId),
                        JobNo = response.JobNo,
                        JobTitle = languageId == 1 ? response.JobTitle : response.JobTitleAr,
                        Status = languageId == 1 ? response.StatusEn : response.StatusAr,
                        InstituteDepartmentUnit = languageId == 1 ? response.InstituteDepartmentUnit : response.InstituteDepartmentUnitAr,
                        RequestedDocuments = languageId == 1 ? response.RequestedDocuments: response.RequestedDocumentsAr,
                        Type = type
                    };
            }
        }
        public static SectionsListWrapper Wrap(StupidLeaveRequestResponse response, int languageId, MyRequestTypeEnum type)
        {
            switch (type)
            {
                case MyRequestTypeEnum.PersonalLoan:
                    return new SectionsListWrapper
                    {

                        Applicant = languageId == 1 ? response.EmpName : response.EmpNameAr,
                        JobNo = response.JobNo,
                        JobTitle = languageId == 1 ? response.JobTitle : response.JobTitleAr,
                        Type = type
                    };
                default:
                    return new SectionsListWrapper
                    {
                        Applicant = languageId == 1 ? response.EmpName : response.EmpNameAr,
                        JobNo = response.JobNo,
                        JobTitle = languageId == 1 ? response.JobTitle : response.JobTitleAr,

                        StartDate = GetDate(response.FirstDayOfLeave, languageId),
                        EndDate = languageId == 1 ? response.LastDayOfLeave.ToString("dd-MM-yyyy") : response.LastDayOfLeave.ToString("yyyy-MM-dd"),
                        RequiredDays = Math.Max(response.NoOfDaysRequired.HasValue ? response.NoOfDaysRequired.Value : 0, response.NumberOfDays.HasValue ? response.NumberOfDays.Value : 0),

                        DepartureDate = GetDate(response.ActualDepartureDate, languageId),
                        Status = languageId == 1 ? response.StatusEn : response.StatusAr,
                        InstituteDepartmentUnit = languageId == 1 ? response.InstituteDepartmentUnit : response.InstituteDepartmentUnitAr,
                        RequestedDocuments = languageId == 1 ? response.RequestedDocuments : response.RequestedDocumentsAr,
                        LeaveType = languageId == 1 ? response.LeaveType: response.LeaveTypeAr,
                        Type = type
                    };
            }
        }

        public static string GetDate(DateTime? date, int languageId)
        {
            if (!date.HasValue)
                return null;
            return languageId == 1 ? date.Value.ToString("dd-MM-yyyy") : date.Value.ToString("yyyy-MM-dd");
        }
    }
}
