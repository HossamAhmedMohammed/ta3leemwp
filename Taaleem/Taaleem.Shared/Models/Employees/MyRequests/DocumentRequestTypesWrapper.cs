﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.Employees.MyRequests.Response;
using Taaleem.ViewModels;

namespace Taaleem.Models.Employees.MyRequests
{
    public class ItemTypeWrapper
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public static ItemTypeWrapper Wrap(DocumentRequestType response, int languageId)
        {
            return new ItemTypeWrapper
            {
                Id = response.DocID.ToString(),
                Name = languageId == 1? response.DocTitle: response.DocTitleAr
            };
        }

        public static ItemTypeWrapper Wrap(LoanRequestType response, int languageId)
        {
            return new ItemTypeWrapper
            {
                Id = response.code,
                Name = languageId == 1 ? response.nameEn : response.nameAr
            };
        }
    }
}
