﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.Employees.MyRequests.Response;
using Taaleem.ViewModels;

namespace Taaleem.Models.Employees.MyRequests
{
    public class RequestTypesWrapper
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static RequestTypesWrapper Wrap(RequestTypesResponse response, int languageId)
        {
            RequestTypesWrapper wrapper = new RequestTypesWrapper
            {
                Id = int.Parse(response.ID)
            };
            switch (languageId)
            {
                case Lanuage.English:
                    wrapper.Name = response.RequestNameEn;
                    break;
                case Lanuage.Arabic:
                    wrapper.Name = response.RequestNameAr;
                    break;
                default:
                    break;
            }

            return wrapper;
        }
    }
}
