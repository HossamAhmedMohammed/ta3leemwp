﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.MyRequests.Response
{
    public class BasicResponse
    {
        public string ResponseNameEn { get; set; }
        public string ResponseNameAr { get; set; }
        public string ResponseCode { get; set; }
    }
}
