﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.MyRequests.Response
{
    public class RootResponse<T>
    {
        public List<T> Requests { get; set; }
        public string Error { get; set; }
        public float Count { get; set; }
    }
    public class LeaveRequestResponse
    {
        public string Title { get; set; }
        public string JobNo { get; set; }
        public string QatarID { get; set; }
        public string JobTitle { get; set; }
        public string JobTitleAr { get; set; }
        public string EmpName { get; set; }
        public string EmpNameAr { get; set; }
        public DateTime? DateofJoin { get; set; }
        public string Nationality { get; set; }
        public string NationalityAr { get; set; }
        public string Grade { get; set; }
        public string InstituteDepartmentUnit { get; set; }
        public string InstituteDepartmentUnitAr { get; set; }
        public string DirectManager { get; set; }
        public string LeaveType { get; set; }
        public float? Balance { get; set; }
        public float? NoOfDaysRequired { get; set; }
        public string Comments { get; set; }
        public string AppropriateRequest { get; set; }
        public object ListName { get; set; }
        public float OldID { get; set; }
        public bool? OneMonthBonus { get; set; }
        public string LeaveTypeAr { get; set; }
        public string AppropriateRequestAr { get; set; }
        public object Actions { get; set; }
        public string ReplacementEmployee { get; set; }
        public bool RequiredHRManager { get; set; }
        public bool RequiredMinister { get; set; }
        public bool HRDatabaseStatus { get; set; }
        public string HRDatabaseMessageAr { get; set; }
        public string HRDatabaseMessageEn { get; set; }
        public object Permissions { get; set; }
        public float ID { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public object Attachments { get; set; }
        public string TableName { get; set; }
        public bool HasAttachments { get; set; }
        public string StatusEn { get; set; }
        public string StatusAr { get; set; }

        public string LeaveSequence { get; set; }
        public DateTime FirstDayOfLeave { get; set; }
        public DateTime LastDayOfLeave { get; set; }
        public float? NumberofDays { get; set; }
        public DateTime DutyCommencingDate { get; set; }
        public bool ReturnAsPlannedDate { get; set; }
        public float ActualLeaveDays { get; set; }
        public bool ExtendLeave { get; set; }
        public float ExtendedDays { get; set; }
        public object ExtendLeaveCode { get; set; }
        public bool CutLeave { get; set; }
        public float CutDays { get; set; }
        public bool AddSickLeave { get; set; }
        public DateTime? SickLeaveStartDate { get; set; }
        public DateTime? SickLeaveEndDate { get; set; }
        public float SickLeaveNoOfDays { get; set; }
        public bool AddCompassionateLeave { get; set; }
        public DateTime? CompassionateLeaveStartDate { get; set; }
        public DateTime? CompassionateLeaveEndDate { get; set; }
        public float CompassionateLeaveNoOfDays { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public object ExtendLeaveCodeAr { get; set; }
        public object LeaveSequenceAr { get; set; }

        public string MobileNo { get; set; }
        public object ExitPermitReason { get; set; }
        public DateTime ActualDepartureDate { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public DateTime? RequestDate { get; set; }
        public object HRAdminRemarks { get; set; }
        public object ManagerApprovalDate { get; set; }
        public object DepManagerApprovalDate { get; set; }
        public object HRAdminApprovalDate { get; set; }
        public DateTime? ActorReminderDate { get; set; }


        public string RequiredLoanType { get; set; }
        public string RequiredLoanTypeAr { get; set; }
        public decimal? LoanAmount { get; set; }
        public float? PaymentPeriod { get; set; }
        public decimal? MonthlyInstallment { get; set; }


        public string RequestedDocumentsAr { get; set; }
        public string RequestedDocuments { get; set; }

    }


    public class StupidLeaveRequestResponse
    {
        [JsonProperty("<Title>k__BackingField")]
        public string Title { get; set; }
        [JsonProperty("<JobNo>k__BackingField")]
        public string JobNo { get; set; }
        [JsonProperty("<QatarID>k__BackingField")]
        public string QatarID { get; set; }
        [JsonProperty("<JobTitle>k__BackingField")]
        public string JobTitle { get; set; }
        [JsonProperty("<JobTitleAr>k__BackingField")]
        public string JobTitleAr { get; set; }
        [JsonProperty("<EmpName>k__BackingField")]
        public string EmpName { get; set; }
        [JsonProperty("<EmpNameAr>k__BackingField")]
        public string EmpNameAr { get; set; }
        [JsonProperty("<DateofJoin>k__BackingField")]
        public DateTime? DateofJoin { get; set; }
        [JsonProperty("<Nationality>k__BackingField")]
        public string Nationality { get; set; }
        [JsonProperty("<NationalityAr>k__BackingField")]
        public string NationalityAr { get; set; }
        [JsonProperty("<Grade>k__BackingField")]
        public string Grade { get; set; }
        [JsonProperty("<InstituteDepartmentUnit>k__BackingField")]
        public string InstituteDepartmentUnit { get; set; }
        [JsonProperty("<InstituteDepartmentUnitAr>k__BackingField")]
        public string InstituteDepartmentUnitAr { get; set; }
        [JsonProperty("<DirectManager>k__BackingField")]
        public object DirectManager { get; set; }
        [JsonProperty("<RequestedDocuments>k__BackingField")]
        public string RequestedDocuments { get; set; }
        [JsonProperty("<RequestedDocumentsAr>k__BackingField")]
        public string RequestedDocumentsAr { get; set; }
        [JsonProperty("<Details>k__BackingField")]
        public string Details { get; set; }
        [JsonProperty("<ReasonForRequest>k__BackingField")]
        public string ReasonForRequest { get; set; }
        [JsonProperty("<Comments>k__BackingField")]
        public string Comments { get; set; }
        [JsonProperty("<HeadFile>k__BackingField")]
        public string HeadFile { get; set; }
        [JsonProperty("<FooterFile>k__BackingField")]
        public string FooterFile { get; set; }
        [JsonProperty("<Data>k__BackingField")]
        public string Data { get; set; }
        [JsonProperty("<BarecodeNumber>k__BackingField")]
        public string BarecodeNumber { get; set; }
        [JsonProperty("<DocumentType>k__BackingField")]
        public int DocumentType { get; set; }
        [JsonProperty("<Permissions>k__BackingField")]
        public string Permissions { get; set; }
        [JsonProperty("<ID>k__BackingField")]
        public int ID { get; set; }
        [JsonProperty("<CreationDate>k__BackingField")]
        public DateTime CreationDate { get; set; }
        [JsonProperty("<ModificationDate>k__BackingField")]
        public DateTime ModificationDate { get; set; }
        [JsonProperty("<CreatedBy>k__BackingField")]
        public string CreatedBy { get; set; }
        [JsonProperty("<ModifiedBy>k__BackingField")]
        public string ModifiedBy { get; set; }
        [JsonProperty("<Attachments>k__BackingField")]
        public List<Attachment> Attachments { get; set; }
        [JsonProperty("<TableName>k__BackingField")]
        public string TableName { get; set; }
        [JsonProperty("<HasAttachments>k__BackingField")]
        public bool HasAttachments { get; set; }
        [JsonProperty("<StatusEn>k__BackingField")]
        public string StatusEn { get; set; }
        [JsonProperty("<StatusAr>k__BackingField")]
        public string StatusAr { get; set; }

        [JsonProperty("<ActualDepartureDate>k__BackingField")]
        public DateTime ActualDepartureDate { get; set; }


        [JsonProperty("<LeaveType>k__BackingField")]
        public string LeaveType { get; set; }

        [JsonProperty("<LeaveTypeAr>k__BackingField")]
        public string LeaveTypeAr { get; set; }


        [JsonProperty("<FirstDayofLeave>k__BackingField")]
        public DateTime FirstDayOfLeave { get; set; }

        [JsonProperty("<LastDayofLeave>k__BackingField")]
        public DateTime LastDayOfLeave { get; set; }

        [JsonProperty("<NumberofDays>k__BackingField")]
        public int? NumberOfDays { get; set; }

        [JsonProperty("<NoOfDaysRequired>k__BackingField")]
        public int? NoOfDaysRequired { get; set; }
        //NoOfDaysRequired
    }

    public class BaseMyRequestResponse
    {
        public string EmpName { get; set; }
        public string EmpNameAr { get; set; }
        public string JobNo { get; set; }
        public string JobTitle { get; set; }
        public string JobTitleAr { get; set; }
        public string StatusEn { get; set; }
        public string StatusAr { get; set; }
    }

    //public class LeaveRequestResponse : BaseMyRequestResponse
    //{
    //    public string LeaveType { get; set; }
    //    public string LeaveTypeAr { get; set; }
    //    public string FirstDayofLeave { get; set; }
    //    public string LastDayofLeave { get; set; }
    //    public float NumberofDays { get; set; }
    //    public float NoOfDaysRequired { get; set; }
    //}

    public class ExitPermitResponse : BaseMyRequestResponse
    {
        public string Departure { get; set; }
    }

    public class DocumentRequestResponse : BaseMyRequestResponse
    {
        public string Document { get; set; }
    }


}
