﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.MyRequests.Response
{
    public class DocumentRequestType
    {
        public string DocTitle { get; set; }
        public string DocTitleAr { get; set; }
        public int DocID { get; set; }
    }

    public class LoanRequestType
    {
        public string nameEn { get; set; }
        public string nameAr { get; set; }
        public string code { get; set; }
    }

}
