﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.MyRequests.Response
{
    public class DirectManager
    {
        public string nameAr { get; set; }
        public string nameEn { get; set; }
        public string emailAddress { get; set; }
        public string jobTitleEn { get; set; }
        public string jobTitleAr { get; set; }
        public string departmentEn { get; set; }
        public string departmentAr { get; set; }
        public string empID { get; set; }
    }
    public class EmployeeByNameResponse
    {
        [JsonProperty("<jobID>k__BackingField")]
        public string jobID { get; set; }
        [JsonProperty("<userName>k__BackingField")]
        public string userName { get; set; }
        [JsonProperty("<nameAr>k__BackingField")]
        public string nameAr { get; set; }
        [JsonProperty("<nameEn>k__BackingField")]
        public string nameEn { get; set; }
        [JsonProperty("<emailAddress>k__BackingField")]
        public string emailAddress { get; set; }
        [JsonProperty("<jobTitleEn>k__BackingField")]
        public string jobTitleEn { get; set; }
        [JsonProperty("<jobTitleAr>k__BackingField")]
        public string jobTitleAr { get; set; }
        [JsonProperty("<departmentEn>k__BackingField")]
        public string departmentEn { get; set; }
        [JsonProperty("<departmentAr>k__BackingField")]
        public string departmentAr { get; set; }
        [JsonProperty("<unitEn>k__BackingField")]
        public string unitEn { get; set; }
        [JsonProperty("<unitAr>k__BackingField")]
        public string unitAr { get; set; }
        [JsonProperty("<joiningDate>k__BackingField")]
        public string joiningDate { get; set; }
        [JsonProperty("<qatariID>k__BackingField")]
        public string qatariID { get; set; }
        [JsonProperty("<nationalityAr>k__BackingField")]
        public string nationalityAr { get; set; }
        [JsonProperty("<nationalityEn>k__BackingField")]
        public string nationalityEn { get; set; }
        [JsonProperty("<financialGrade>k__BackingField")]
        public string financialGrade { get; set; }
        [JsonProperty("<financialGradeDate>k__BackingField")]
        public string financialGradeDate { get; set; }
        [JsonProperty("<educationalMajor>k__BackingField")]
        public string educationalMajor { get; set; }
        [JsonProperty("<educationSpecialization>k__BackingField")]
        public string educationSpecialization { get; set; }
        [JsonProperty("<graduationYear>k__BackingField")]
        public string graduationYear { get; set; }
        [JsonProperty("<directManager>k__BackingField")]
        public DirectManager directManager { get; set; }
        [JsonProperty("<workingStatus>k__BackingField")]
        public bool workingStatus { get; set; }
        [JsonProperty("<appraisalType>k__BackingField")]
        public string appraisalType { get; set; }
        [JsonProperty("<isQatari>k__BackingField")]
        public bool isQatari { get; set; }
        [JsonProperty("<birthDate>k__BackingField")]
        public string birthDate { get; set; }
        [JsonProperty("<mobileNumber>k__BackingField")]
        public string mobileNumber { get; set; }
        [JsonProperty("<qatariIDExpiryDate>k__BackingField")]
        public string qatariIDExpiryDate { get; set; }
        [JsonProperty("<passportNumber>k__BackingField")]
        public string passportNumber { get; set; }
        [JsonProperty("<passportExpiryDate>k__BackingField")]
        public string passportExpiryDate { get; set; }
        [JsonProperty("<basicSalary>k__BackingField")]
        public string basicSalary { get; set; }
        [JsonProperty("<contractType>k__BackingField")]
        public string contractType { get; set; }
        [JsonProperty("<gender>k__BackingField")]
        public string gender { get; set; }
        [JsonProperty("<grossSalary>k__BackingField")]
        public string grossSalary { get; set; }
        [JsonProperty("<bankCode>k__BackingField")]
        public string bankCode { get; set; }
        [JsonProperty("<sectorManagerUserName>k__BackingField")]
        public string sectorManagerUserName { get; set; }
        [JsonProperty("<houseNumber>k__BackingField")]
        public string houseNumber { get; set; }
        [JsonProperty("<grossSalaryInWordsAr>k__BackingField")]
        public string grossSalaryInWordsAr { get; set; }
        [JsonProperty("<grossSalaryInWordsEn>k__BackingField")]
        public string grossSalaryInWordsEn { get; set; }
        [JsonProperty("<isWorkingInSchool>k__BackingField")]
        public bool isWorkingInSchool { get; set; }
        [JsonProperty("<bankName>k__BackingField")]
        public string bankName { get; set; }
        [JsonProperty("<bankAccountNumber>k__BackingField")]
        public string bankAccountNumber { get; set; }
        [JsonProperty("<isHeAWorker>k__BackingField")]
        public bool isHeAWorker { get; set; }
        [JsonProperty("<marriedStatusAr>k__BackingField")]
        public string marriedStatusAr { get; set; }
        [JsonProperty("<marriedStatusEn>k__BackingField")]
        public string marriedStatusEn { get; set; }
    }
}
