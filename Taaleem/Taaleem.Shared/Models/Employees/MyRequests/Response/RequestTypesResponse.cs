﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.MyRequests.Response
{
    public class RequestTypesResponse
    {
        public string RequestNameEn { get; set; }
        public string RequestNameAr { get; set; }
        public string ID { get; set; }
    }
}
