﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.MyRequests.Response
{
    public class EmployeeApprovedLeave
    {
        public string qatariID { get; set; }
        public string nameAr { get; set; }
        public string nameEn { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string location { get; set; }
        public string typeEn { get; set; }
        public string typeAR { get; set; }
        public string leaveSequence { get; set; }
        public string depDate { get; set; }
        public string numberOfDays { get; set; }
        public string status { get; set; }
        public string leaveCode { get; set; }
    }
}
