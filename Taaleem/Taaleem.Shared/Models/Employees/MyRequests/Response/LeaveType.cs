﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.MyRequests.Response
{
    public class LeaveType
    {
        public string nameEn { get; set; }
        public string nameAR { get; set; }
        public string balance { get; set; }
        public bool requireEndDate { get; set; }
        public string leaveCode { get; set; }
        public bool requiredAttachment { get; set; }
    }
}
