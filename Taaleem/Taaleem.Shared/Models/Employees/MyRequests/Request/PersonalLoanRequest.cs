﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.MyRequests.Request
{
    public class PersonalLoanRequest
    {
        public string Title { get; set; }
        public string JobNo { get; set; }
        public string QatarID { get; set; }
        public string JobTitle { get; set; }
        public string JobTitleAr { get; set; }
        public string EmpName { get; set; }
        public string EmpNameAr { get; set; }
        public string DateofJoin { get; set; }
        public string Nationality { get; set; }
        public string NationalityAr { get; set; }
        public string Grade { get; set; }
        public string InstituteDepartmentUnit { get; set; }
        public string InstituteDepartmentUnitAr { get; set; }
        public string DirectManager { get; set; }
        public decimal LoanAmount { get; set; }
        public int PaymentPeriod { get; set; }
        public decimal MonthlyInstallment { get; set; }
        public string RequiredLoanType { get; set; }
        public string RequiredLoanID { get; set; }
        public string Comments { get; set; }
        public string RequiredLoanTypeAr { get; set; }
        public bool HRDatabaseStatus { get; set; }
        public int Salary { get; set; }

        public List<Attachment> Attachments { get; set; }
        public bool HasAttachments
        {
            get
            {
                return Attachments != null && Attachments.Count > 0 ? true : false;
            }
            set
            {

            }
        }
    }
}
