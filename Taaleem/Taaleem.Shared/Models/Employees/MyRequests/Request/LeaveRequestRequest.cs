﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.MyRequests.Request
{
    public class LeaveRequestRequest
    {
        public string Title { get; set; }
        public string Grade { get; set; }
        public string InstituteDepartmentUnit { get; set; }
        public string InstituteDepartmentUnitAr { get; set; }
        public string JobNo { get; set; }
        public string QatarID { get; set; }
        public string JobTitle { get; set; }
        public string JobTitleAr { get; set; }
        public string EmpName { get; set; }
        public string EmpNameAr { get; set; }
        public string DateofJoin { get; set; }
        public string Nationality { get; set; }
        public string NationalityAr { get; set; }
        public string DirectManager { get; set; }
        //public string AppropriateRequest { get; set; }
        //public string AppropriateRequestAr { get; set; }
        //public object Actions { get; set; }
        //public object ListName { get; set; }
        //public object OldID { get; set; }
        //public bool OneMonthBonus { get; set; }
        public string LeaveType { get; set; }
        public string FirstDayOfLeave { get; set; }
        public string LastDayOfLeave { get; set; }
        public string Comments { get; set; }
        public string LeaveTypeAr { get; set; }
        public string ReplacementEmployee { get; set; }
        public string NoOfDaysRequired { get; set; }

        public bool HasAttachments
        {
            get
            {
                return Attachments != null && Attachments.Count > 0;
            }
        }
        public List<Attachment> Attachments { get; set; }
    }
}
