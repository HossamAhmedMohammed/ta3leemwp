﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.MyRequests.Request
{
    public class LeaveReturnRequestRequest : Response.StupidLeaveRequestResponse
    {

        [JsonProperty("<LeaveSequence>k__BackingField")]
        public string LeaveSequence { get; set; }


        /// <summary>
        /// Short date
        /// </summary>

        [JsonProperty("<DutyCommencingDate>k__BackingField")]
        public string DutyCommencingDate { get; set; }

        [JsonProperty("<ReturnAsPlannedDate>k__BackingField")]
        public bool ReturnAsPlannedDate { get; set; }

        [JsonProperty("<ActualLeaveDays>k__BackingField")]
        public int ActualLeaveDays { get; set; }




        [JsonProperty("<ExtendLeave>k__BackingField")]
        public bool ExtendLeave { get; set; }

        [JsonProperty("<ExtendedDays>k__BackingField")]
        public int ExtendedDays { get; set; }

        [JsonProperty("<ExtendLeaveCode>k__BackingField")]
        public string ExtendLeaveCode { get; set; }





        [JsonProperty("<CutLeave>k__BackingField")]
        public bool CutLeave { get; set; }

        [JsonProperty("<CutDays>k__BackingField")]
        public int CutDays { get; set; }





        [JsonProperty("<AddSickLeave>k__BackingField")]
        public bool AddSickLeave { get; set; }

        [JsonProperty("<SickLeaveStartDate>k__BackingField")]
        public string SickLeaveStartDate { get; set; }

        [JsonProperty("<SickLeaveEndDate>k__BackingField")]
        public string SickLeaveEndDate { get; set; }

        [JsonProperty("<SickLeaveNoOfDays>k__BackingField")]
        public int SickLeaveNoOfDays { get; set; }





        [JsonProperty("<AddCompassionateLeave>k__BackingField")]
        public bool AddCompassionateLeave { get; set; }

        [JsonProperty("<CompassionateLeaveStartDate>k__BackingField")]
        public string CompassionateLeaveStartDate { get; set; }

        [JsonProperty("<CompassionateLeaveEndDate>k__BackingField")]
        public string CompassionateLeaveEndDate { get; set; }

        [JsonProperty("<CompassionateLeaveNoOfDays>k__BackingField")]
        public int CompassionateLeaveNoOfDays { get; set; }






        [JsonProperty("<LeaveSequenceAr>k__BackingField")]
        public string LeaveSequenceAr { get; set; }
        [JsonProperty("<ExtendLeaveCodeAr>k__BackingField")]
        public string ExtendLeaveCodeAr { get; set; }
    }
}
