﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.Response
{
    public class EmployeeLoan
    {
        [JsonProperty("balance")]
        public string Balance { get; set; }
        [JsonProperty("nameAr")]
        public string NameAr { get; set; }
        [JsonProperty("nameEn")]
        public string NameEn { get; set; }
        [JsonProperty("paid")]
        public string Paid { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
