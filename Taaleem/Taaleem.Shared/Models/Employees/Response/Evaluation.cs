﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.Response
{
    public class Evaluation
    {
        [JsonProperty("description_Ar")]
        public string DescriptionAr { get; set; }
        [JsonProperty("description_En")]
        public string DescriptionEn { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
        [JsonProperty("year")]
        public string Year { get; set; }
    }

}
