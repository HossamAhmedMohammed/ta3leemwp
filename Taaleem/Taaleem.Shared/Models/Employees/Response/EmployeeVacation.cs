﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.Response
{
    public class EmployeeVacation
    {
        [JsonProperty("nameAr")]
        public string NameAr { get; set; }
        [JsonProperty("nameEn")]
        public string NameEn { get; set; }
        [JsonProperty("numberOfDays")]
        public string NumberOfDays { get; set; }
        [JsonProperty("startDate")]
        public string StartDate { get; set; }
        [JsonProperty("endDate")]
        public string EndDate { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }

}
