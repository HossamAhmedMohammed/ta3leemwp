﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.Response
{
    public class EmployeeLoginResponse
    {
        [JsonProperty("Success")]
        public bool Success { get; set; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("Reason")]
        public string Reason { get; set; }
    }
}
