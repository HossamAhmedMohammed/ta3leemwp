﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Employees.Response
{
    public class SalaryDetail
    {
        [JsonProperty("amount")]
        public string Amount { get; set; }
        [JsonProperty("nameAr")]
        public string NameAr { get; set; }
        [JsonProperty("nameEn")]
        public string NameEn { get; set; }
    }

}
