﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.Employees.Response;

namespace Taaleem.Models.Employees
{
    public class SalaryDetailWrapper
    {

        public string Amount { get; set; }
        public string Name { get; set; }

        public static SalaryDetailWrapper Wrap(SalaryDetail salary, int languageId)
        {
            SalaryDetailWrapper wrapper = new SalaryDetailWrapper
            {
                Amount = salary.Amount,
            };
            switch (languageId)
            {
                case Lanuage.English:
                    wrapper.Name = salary.NameEn;
                    break;
                case Lanuage.Arabic:
                    wrapper.Name = salary.NameAr;
                    break;
                default:
                    break;
            }
            return wrapper;
        }
    }
}
