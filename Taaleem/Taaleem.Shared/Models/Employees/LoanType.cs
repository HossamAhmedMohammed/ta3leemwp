﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.ViewModels;

namespace Taaleem.Models.Employees
{
    public class LoanType
    {
        public string DisplayName { get; set; }
        public string Status { get; set; }

        public LoanType(string displayName, string status)
        {
            DisplayName = displayName;
            Status = status;
        }

        public static readonly LoanType All = new LoanType(ViewModelLocator.Resources.All, @"All");

        public static readonly LoanType Active = new LoanType(ViewModelLocator.Resources.ActiveLoans, "1");

        public static readonly LoanType Completed = new LoanType(ViewModelLocator.Resources.CompletedLoans, "3");


        public static readonly List<LoanType> LoanTypes = new List<LoanType>
        {
           All,Active ,Completed,
        };
    }
}
