﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Request
{
    public class ReceiveFileParameters
    {

            public string data { get; set; }
            public string filetype { get; set; }
            public string locationId { get; set; }
    }
}
