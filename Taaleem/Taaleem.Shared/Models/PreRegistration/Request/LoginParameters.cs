﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Request
{
    public class LoginParameters
    {

            public string userName { get; set; }
            public string password { get; set; }
            public string createPersistentCookie { get; set; }
      

    }
}
