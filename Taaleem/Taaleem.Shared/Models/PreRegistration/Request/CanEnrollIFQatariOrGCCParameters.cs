﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Request
{
   public class CanEnrollIFQatariOrGCCParameters
    {
        public string QID { get; set; }
        public Guid gradeLevelID { get; set; }
        
    }
}
