﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Request
{
    public class EnrollStudentParameters
    {

     
            public string addressVerificationType { get; set; }
            public string electFilename { get; set; }
            public string electricityNumber { get; set; }
            public string gradeLevelID { get; set; }
            public string relationshipID { get; set; }
            public string schoolID { get; set; }
            public string studentQID { get; set; }
            public string workCertificateFilename { get; set; }
        

    }
}
