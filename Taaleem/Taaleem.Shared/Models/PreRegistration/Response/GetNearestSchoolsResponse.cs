﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Response
{
    public class GetNearestSchoolsResponse
    {



        public Getnearestschoolsresult GetNearestSchoolsResult { get; set; }
    }

    public class Getnearestschoolsresult
    {
        public int TotalCount { get; set; }
        public Rootresulttt[] RootResults { get; set; }
    }

    public class Rootresulttt
    {
        public int Capacity { get; set; }
        public int EnrollmentCount { get; set; }
        public string ID { get; set; }
        public bool IsAlternate { get; set; }
        public string LocalId { get; set; }
        public string Name { get; set; }
       

        public bool ISFull
        {
            get
            {
                if (EnrollmentCount >= Capacity)
                    return true;
                else if (EnrollmentCount < Capacity)
                    return false;
                else return false;
            }
            
        }

    }


}
