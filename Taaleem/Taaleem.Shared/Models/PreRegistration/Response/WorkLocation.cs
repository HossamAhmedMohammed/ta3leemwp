﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Response
{
    public class WorkLocation
    {

      
            public string Code { get; set; }
            public string CustomCodesetID { get; set; }
            public string Description { get; set; }
            public string DescriptionResourceID { get; set; }
        

    }
}
