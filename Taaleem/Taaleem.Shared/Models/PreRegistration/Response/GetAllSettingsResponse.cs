﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Response
{
    public class GetAllSettingsResponse
    {

        public Getsettingsresult GetSettingsResult { get; set; }
    }

    public class Getsettingsresult
    {
        public int TotalCount { get; set; }
        public Rootresultt[] RootResults { get; set; }
    }

    public class Rootresultt
    {
        public DateTime EndDate { get; set; }
        public string ID { get; set; }
        public bool IsEnabled { get; set; }
        public DateTime StartDate { get; set; }
    }


}
