﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Response
{
    public class GetSchoolEnrollmentsResponsecs
    {

        public Getschoolenrollmentsresult[] GetSchoolEnrollmentsResult { get; set; }
    }

    public class Getschoolenrollmentsresult
    {
        public object ElectricityFilePath { get; set; }
        public string ElectricityNumber { get; set; }
        
        public string GradeLevel { get; set; }
        public string ID { get; set; }
        public string Relationship { get; set; }
        public string SchoolID { get; set; }
        public string SchoolInfoID { get; set; }
        
        public string SchoolName { get; set; }
        public object Status { get; set; }
        public string StudentID { get; set; }
        public string StudentName { get; set; }
        public int TransactionNumber { get; set; }
        public object WorkCertificateFilePath { get; set; }
    }


}
