﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Response
{
    public class GetNearestSchoolsDetailsResponse
    {


        public Getschoolcontactsresult GetSchoolContactsResult { get; set; }
    }

    public class Getschoolcontactsresult
    {
        public int TotalCount { get; set; }
        public Rootresultttt[] RootResults { get; set; }
    }

    public class Rootresultttt
    {
        public string Area { get; set; }
        public string ID { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public string Municipality { get; set; }
        public string PhoneNumber { get; set; }
        public string Principal { get; set; }
        public string Name { get; set; }
        public bool IsFull { get; set; }
        public string Code { get; set; }

    }


}
