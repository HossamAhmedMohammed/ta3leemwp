﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Response
{
    public class Relationship
    {


        public Getrelationshipsresult[] GetRelationshipsResult { get; set; }
    }

    public class Getrelationshipsresult
    {
        public string Code { get; set; }
        public string CodeResourceID { get; set; }
        public string CodesetID { get; set; }
        public string Description { get; set; }
        public string DescriptionResourceID { get; set; }
        public string ID { get; set; }
    }


}
