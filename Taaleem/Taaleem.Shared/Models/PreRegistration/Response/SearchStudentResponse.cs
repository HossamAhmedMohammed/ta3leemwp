﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Response
{
    public class SearchStudentResponse
    {


        public Searchstudentresult SearchStudentResult { get; set; }
    }

    public class Searchstudentresult
    {
        public DateTime BirthDate { get; set; }
        public string GenderID { get; set; }
        public string ID { get; set; }
        public bool IsQatari { get; set; }
        public bool IsQatariOrGCC { get; set; }
        public string Name { get; set; }
      

        public string Gender
        {
            get {
                if(GenderID == "4603C395-D01C-DD11-8C44-0019D1638957")
                {
                    if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                        return "Male";
                    else
                        return "ذكر";
                }
                else
                {
                    if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                        return "Female";
                    else
                        return "انثي";
                }
            }
           
        }

    }


}
