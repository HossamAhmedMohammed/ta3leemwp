﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Response
{
    public class RegisterAsParentResponse
    {

     
            public int ErrorCode { get; set; }
            public string ErrorMessage { get; set; }
            public bool IsDomainException { get; set; }
            public string StackTrace { get; set; }
        

    }
}
