﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Response
{
    public class ResetPasswordPesponse
    {


        public Resetpasswordresult ResetPasswordResult { get; set; }
    }

        public class Resetpasswordresult
        {
            public bool MobileNumberExists { get; set; }
            public bool PasswordHasBeenSent { get; set; }
            public bool QIDExists { get; set; }
            public bool WorkInfoExists { get; set; }
        }

    
}
