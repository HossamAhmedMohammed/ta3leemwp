﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Response
{
    public class Parent
    {


        public Getparentresult GetParentResult { get; set; }
    }

    public class Getparentresult
    {
        public string EMail { get; set; }
        public string EmployerType { get; set; }
        public string EmployerTypeID { get; set; }
        public string ID { get; set; }
        public bool IsQatariOrGCC { get; set; }
        public string LocalId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string WorkLocation { get; set; }
    }


}
