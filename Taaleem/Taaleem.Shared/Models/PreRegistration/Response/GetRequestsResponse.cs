﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PreRegistration.Response
{
    public class GetRequestsResponse
    {

        public Getrequestsresult GetRequestsResult { get; set; }
    }

    public class Getrequestsresult
    {
        public int TotalCount { get; set; }
        public Rootresult[] RootResults { get; set; }
    }

    public class Rootresult
    {
        public string ID { get; set; }
        public string ProcessID { get; set; }
        public Schoolenrollment SchoolEnrollment { get; set; }
    }

    public class Schoolenrollment
    {
        public string ElectricityFilePath { get; set; }
        public string GradeLevel { get; set; }
        public string ID { get; set; }
        public string Relationship { get; set; }
        public string SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string Status { get; set; }
        public string StudentID { get; set; }
        public string StudentName { get; set; }
        public int TransactionNumber { get; set; }
        public string WorkCertificateFilePath { get; set; }
    }


    
}
