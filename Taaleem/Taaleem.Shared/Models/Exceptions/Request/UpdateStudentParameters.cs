﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Request
{
    public class UpdateStudentParameters
    {


        public Student Student { get; set; }
        public bool HasTwins { get; set; }
        public Qataridattachment qatarIdAttachment { get; set; }
        public Passportattachment PassportAttachment { get; set; }
        public Birthcertificate BirthCertificate { get; set; }
        public Lastschoolcertificate LastSchoolCertificate { get; set; }
    }

        public class Student
        {
            public int Id { get; set; }
            public int RequestId { get; set; }
            public int NationalityId { get; set; }
            public string PrivateSchoolCode { get; set; }
            public string Name { get; set; }
            public string QatarId { get; set; }
            public string Passport { get; set; }
            public int Gender { get; set; }
            public string DOB { get; set; }
            public int Grade { get; set; }
            public int SchoolType { get; set; }
            public bool IsTwins { get; set; }
        }

      

      

        

    
}
