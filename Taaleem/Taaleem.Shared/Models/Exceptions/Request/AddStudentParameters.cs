﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Request
{
    public class AddStudentParameters
    {


        public Student Student { get; set; }
        public bool HasTwins { get; set; }
        public Qataridattachment qatarIdAttachment { get; set; }
        public Passportattachment PassportAttachment { get; set; }
        public Birthcertificate BirthCertificate { get; set; }
        public Lastschoolcertificate LastSchoolCertificate { get; set; }
    } 

      

      

        public class Birthcertificate
        {
            public string FileName { get; set; }
            public string FileContent { get; set; }
        }

        public class Lastschoolcertificate
        {
            public string FileName { get; set; }
            public string FileContent { get; set; }
        }

    
}
