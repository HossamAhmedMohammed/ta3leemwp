﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Exceptions.Request
{
 public   class GetIndependentSchoolByIDParameters
    {
        [QueryParameter("independentSchoolID")]
        public int independentSchoolID { get; set; }
    }
}
