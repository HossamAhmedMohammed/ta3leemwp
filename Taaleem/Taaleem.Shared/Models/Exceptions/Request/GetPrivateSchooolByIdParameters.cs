﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Exceptions.Request
{
   public class GetPrivateSchooolByIdParameters
    {
        [QueryParameter("PrivateSchoolID")]
        public int PrivateSchoolID { get; set; }
    }
}
