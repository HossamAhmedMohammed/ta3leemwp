﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Request
{
    public class UpdateApplicantParameters
    {

       
            public Applicant Applicant { get; set; }
            public Qataridattachment QatarIdAttachment { get; set; }
      

      

    }
    public class Applicant
    {
        public int Id { get; set; }
        public string ApplicantName { get; set; }
        public string QatarId { get; set; }
        public string DOB { get; set; }
        public string Mobile { get; set; }
    }

   
}
