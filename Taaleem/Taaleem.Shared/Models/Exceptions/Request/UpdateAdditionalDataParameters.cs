﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Request
{
    public class UpdateAdditionalDataParameters
    {


        public Additionaldata AdditionalData { get; set; }
        public Houserentcontract HouseRentContract { get; set; }
    }

    public class Additionaldata
    {
        public int RequestId { get; set; }
        public bool HasException { get; set; }
        public string ElectricityNumber { get; set; }
        public string Area { get; set; }
        public string Street { get; set; }
        public string BuildingNumber { get; set; }
        public bool IsPayingRent { get; set; }
        public int RentAmount { get; set; }
    }

    public class Houserentcontract
    {
        public string FileName { get; set; }
        public string FileContent { get; set; }
    }


}
