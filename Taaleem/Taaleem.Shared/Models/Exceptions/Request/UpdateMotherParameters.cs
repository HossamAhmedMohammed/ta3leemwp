﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Request
{
    public class UpdateMotherParameters
    {

       
            public ParentM Parent { get; set; }
            public Qataridattachment qatarIdAttachment { get; set; }
            public Passportattachment PassportAttachment { get; set; }
            public Salarycertificate SalaryCertificate { get; set; }
            public Bankcertificate BankCertificate { get; set; }
            public Deathcertificate DeathCertificate { get; set; }
            public Incubatorcertificate IncubatorCertificate { get; set; }
      

      

      

    }
    public class ParentM
    {
        public int Id { get; set; }
        public int RequestId { get; set; }
        public int? NationalityId { get; set; }
        public string Name { get; set; }
        public string QatarId { get; set; }
        public string Passport { get; set; }
        public string Mobile { get; set; }
        public int ParentType { get; set; }
        public int ParentStatus { get; set; }
        public bool? IsEmployed { get; set; }
        public string Employer { get; set; }
        public string Job { get; set; }
        public object Salary { get; set; }
        public bool? IsDivorced { get; set; }
        public object IsIncubator { get; set; }
      

        public bool IsIncubatorMother
        {
            get { if (IsIncubator == null) return false;
                else if (IsIncubator.ToString() == "true") return true;
                else return false;
            }
            set { IsIncubator = value; }
        }


    }
}
