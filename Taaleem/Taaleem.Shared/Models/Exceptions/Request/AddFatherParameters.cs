﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Request
{
    public class AddFatherParameters
    {

       
            public Parent Parent { get; set; }
            public Qataridattachment qatarIdAttachment { get; set; }
            public Passportattachment PassportAttachment { get; set; }
            public Salarycertificate SalaryCertificate { get; set; }
            public Bankcertificate BankCertificate { get; set; }
            public Deathcertificate DeathCertificate { get; set; }
       

       
    }

   
    public class Qataridattachment
    {
        public string FileName { get; set; }
        public string FileContent { get; set; }
    }

    public class Passportattachment
    {
        public string FileName { get; set; }
        public string FileContent { get; set; }
    }

    public class Salarycertificate
    {
        public string FileName { get; set; }
        public string FileContent { get; set; }
    }

    public class Bankcertificate
    {
        public string FileName { get; set; }
        public string FileContent { get; set; }
    }

    public class Deathcertificate
    {
        public string FileName { get; set; }
        public string FileContent { get; set; }
    }

}
