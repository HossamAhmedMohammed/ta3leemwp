﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Exceptions.Request
{
   public class GetReqestByIDParameters
    {
        [QueryParameter("requestID")]
        public int requestID { get; set; }
    }
}
