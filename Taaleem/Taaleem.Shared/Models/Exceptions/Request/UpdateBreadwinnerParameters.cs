﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Request
{
    public class UpdateBreadwinnerParameters
    {

    
            public Breadwinner Breadwinner { get; set; }
            public Breadwinnercertificate BreadwinnerCertificate { get; set; }
        

       

    }
    public class Breadwinner
    {
        public int Id { get; set; }
        public int RequestId { get; set; }
        public int NationalityId { get; set; }
        public string Name { get; set; }
        public string QatarId { get; set; }
        public string Passport { get; set; }
        public int Relationship { get; set; }
        public string Relation { get; set; }
    }

    public class Breadwinnercertificate
    {
        public string FileName { get; set; }
        public string FileContent { get; set; }
    }
}
