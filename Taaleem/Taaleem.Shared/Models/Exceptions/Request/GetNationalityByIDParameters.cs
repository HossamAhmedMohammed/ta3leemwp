﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Exceptions.Request
{
  public  class GetNationalityByIDParameters
    {
        [QueryParameter("nationalityID")]
        public int nationalityID { get; set; }
    }
}
    