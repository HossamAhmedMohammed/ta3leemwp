﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Request
{
    public class UpdateFatherParameters
    {

      
            public Parent Parent { get; set; }
            public Qataridattachment qatarIdAttachment { get; set; }
            public Passportattachment PassportAttachment { get; set; }
            public Salarycertificate SalaryCertificate { get; set; }
            public Bankcertificate BankCertificate { get; set; }
            public Deathcertificate DeathCertificate { get; set; }
        

       

    }
    public class Parent
    {
        public int Id { get; set; }
        public int RequestId { get; set; }
        public int? NationalityId { get; set; }
        public string Name { get; set; }
        public string QatarId { get; set; }
        public string Passport { get; set; }
        public string Mobile { get; set; }
        public int ParentType { get; set; }
        public int ParentStatus { get; set; }
        public bool? IsEmployed { get; set; }
        public string Employer { get; set; }
        public string Job { get; set; }
        public object Salary { get; set; }
    }
   // {"Id":11128,"RequestId":11105,"NationalityId":null,"Name":null,"QatarId":null,"Passport":null,"Mobile":null,"ParentType":1,"ParentStatus":3,"IsEmployed":null,"Employer":null,"Job":null,"Salary":null,"IsDivorced":null,"IsIncubator":null}
    
}
