﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Request
{
    public class GetAttachmentParameters
    {

            public int OwnerId { get; set; }
            public int AttachmentType { get; set; }
        
    }
}
