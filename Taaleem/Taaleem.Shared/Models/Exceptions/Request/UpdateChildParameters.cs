﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Request
{
    public class UpdateChildParameters
    {

       
            public Child Child { get; set; }
            public Lastschoolcertificate LastSchoolCertificate { get; set; }
        }

        public class Child
        {
            public int Id { get; set; }
            public int RequestId { get; set; }
            public int NationalityId { get; set; }
            public object IndependentSchoolCode { get; set; }
            public string PrivateSchoolCode { get; set; }
            public string Name { get; set; }
            public string QatarId { get; set; }
            public string Passport { get; set; }
            public string DOB { get; set; }
            public int Gender { get; set; }
            public int EducationType { get; set; }
            public int SchoolType { get; set; }
            public int Grade { get; set; }
        }

        

    
}
