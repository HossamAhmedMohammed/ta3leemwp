﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions
{
   public class Stage
    {
        public int StageID { get; set; }
        public String Name { get; set; }
        public String ImagePath { get; set; }
        private bool _isSelected;
        //internal usage
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }
    }
}
