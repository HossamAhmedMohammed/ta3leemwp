﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Response
{
    public class School
    {

       
            public string Id { get; set; }
            public string NameAr { get; set; }
            public string NameEn { get; set; }
        public string Name
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return NameEn;
                }
                else
                {
                    return NameAr;
                }
            }

        }
        //public bool IsGCC { get; set; }
        //public int OrderId { get; set; }


    }
}
