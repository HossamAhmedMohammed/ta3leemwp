﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Response
{
    public class UpdateResponse
    {

      
            public bool IsValid { get; set; }
            public string ErrorMessageAr { get; set; }
            public string ErrorMessageEn { get; set; }
        

    }
}
