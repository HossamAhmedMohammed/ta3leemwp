﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Exceptions.Response
{
    public class MyExceptionRequest
    {
        
            public int Id { get; set; }
            public string ApplicantName { get; set; }
            public string QatarId { get; set; }
            public DateTime Created { get; set; }
            public object SendingDate { get; set; }
            public int? RequestNextStep { get; set; }
            public bool IsEditable { get; set; }
    
    }
}
