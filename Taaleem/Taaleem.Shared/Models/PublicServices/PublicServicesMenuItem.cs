﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices
{
   public class PublicServicesMenuItem
    {
        private string image;

        public string Image
        {
            get { return image; }
            set { image = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}
