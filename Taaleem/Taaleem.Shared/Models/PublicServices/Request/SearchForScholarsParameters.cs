﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Request
{
    public class SearchForScholarsParameters
    {
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("univer")]
        public string Univer { get; set; }
        [JsonProperty("major")]
        public string Major { get; set; }
    }
}

