﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.PublicServices.Request
{
    //tybe calculated in viewmodel
    //elec is the electricity number    23 and 45 are working
    public class SchoolsByElecParameters
    {
        [QueryParameter("elec")]
        public string Elec { get; set; }

        [QueryParameter("tybe")]
        public int Type { get; set; }


    }
}
