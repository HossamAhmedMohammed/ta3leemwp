﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Request
{
    public class GetStatesParameters
    {
        [JsonProperty("country")]
        public string Country { get; set; }
    }
}
