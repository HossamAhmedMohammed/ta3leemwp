﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Request
{
  public  class SecondaryResultParameters
    {
        [JsonProperty("ID")]
        public string ID { get; set; }
    }
}
