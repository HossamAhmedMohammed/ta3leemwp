﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Request
{
    public class GetMajorParameters
    {
        [JsonProperty("unv")]
        public string University { get; set; }
        //In case of no university {unv: ""}
    }
}
