﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Request
{
    public class ValidateQidParameters
    {
        [JsonProperty("nid")]
        public string Nid { get; set; }
    }
}
