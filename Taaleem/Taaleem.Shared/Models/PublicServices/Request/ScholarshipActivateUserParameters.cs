﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Request
{
    public class ScholarshipActivateUserParameters
    {
        [JsonProperty("nid")]
        public string Nid { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("mobile")]
        public string Mobile { get; set; }
        [JsonProperty("UserType")]
        public string UserType { get; set; }
    }
}
