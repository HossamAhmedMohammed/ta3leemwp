﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Helpers;

namespace Taaleem.Models.PublicServices
{
    public class Country
    {
        //[JsonProperty("key")]
        //public string Key { get; set; }
        //[JsonProperty("country")]
        //public string Name { get; set; }
        [JsonProperty("Disabled")]
        public string Disabled { get; set; }
        [JsonProperty("Group")]
        public string Group { get; set; }
        [JsonProperty("Selected")]
        public string Selected { get; set; }
        [JsonProperty("Text")]
        public string Text { get; set; }
        [JsonProperty("Value")]
        public string Value { get; set; }


        public static async Task<List<Country>> GetCountriesAsync()
        {
            var result = await new JsonHelper().DeserializeJsonFileAsync<List<Country>>(@"ms-appx:///Assets/json/countries.json");
            return result;
        }

    }

}
