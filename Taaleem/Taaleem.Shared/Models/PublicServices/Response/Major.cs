﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Response
{
    public class Major
    {
        [JsonProperty("Selected")]
        public bool Selected { get; set; }
        [JsonProperty("Text")]
        public string Text { get; set; }
        [JsonProperty("Value")]
        public string Value { get; set; }
    }
}
