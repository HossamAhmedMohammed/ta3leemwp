﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Response
{
    
        public class NatejaResult
        {
            [JsonProperty("GetEnrollmentResult")]
            public Getenrollmentresult GetEnrollmentResult { get; set; }
        }

        public class Getenrollmentresult
        {
            [JsonProperty("ID")]
            public string ID { get; set; }
            [JsonProperty("PromotionStatus")]
            public string PromotionStatus { get; set; }
            [JsonProperty("School")]
            public string School { get; set; }
            [JsonProperty("SchoolYear")]
            public string SchoolYear { get; set; }
            [JsonProperty("SeatNumber")]
            public string SeatNumber { get; set; }
            [JsonProperty("StudentGrades")]
            public Studentgrade[] StudentGrades { get; set; }
            [JsonProperty("YearGradeNumeric")]
            public float YearGradeNumeric { get; set; }
            [JsonProperty("YearGradePercentage")]
            public float YearGradePercentage { get; set; }
        }

        public class Studentgrade
        {
            [JsonProperty("CourseTitle")]
            public string CourseTitle { get; set; }
            [JsonProperty("Letter")]
            public object Letter { get; set; }
            [JsonProperty("Numeric")]
            public float Numeric { get; set; }
        }

    }

