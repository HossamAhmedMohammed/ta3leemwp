﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Response
{
    public class CountryState
    {
        [JsonProperty("Selected")]
        public bool Selected { get; set; }
        [JsonProperty("Text")]
        public string Text { get; set; }
        [JsonProperty("Value")]
        public string Value { get; set; }
    }
}
