﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Taaleem.ViewModels;

namespace Taaleem.Models.PublicServices.Response
{
    public class StudentStageResult
    {

        [JsonProperty("QID")]
        public string Qid { get; set; }

        [JsonProperty("Result")]
        public string Result { get; set; }

        [JsonProperty("Subjects")]
        public Subject[] Subjects { get; set; }

        [JsonProperty("grade")]
        public string Grade { get; set; }

        [JsonProperty("nameAr")]
        public string NameAr { get; set; }
        [JsonProperty("nameEn")]
        public string NameEn { get; set; }

        private string _percentage;
        [JsonProperty("percentage")]
        public string Percentage
        {
            get { return String.IsNullOrWhiteSpace(_percentage) ? "--" : _percentage; }
            set { _percentage = value; }
        }

        [JsonProperty("schoolName")]
        public string SchoolName { get; set; }
        [JsonProperty("seatNumber")]
        public string SeatNumber { get; set; }

        /// <summary>
        /// Localized Name based on current language
        /// </summary>
        public string Name
        {
            get
            {
                string name = ViewModelLocator.Locator.AppSettings.LanguageId == Lanuage.Arabic ? NameAr : NameEn;
                if (String.IsNullOrWhiteSpace(name)) return ViewModelLocator.Resources.DoesnotExist;
                return name;
            }
        }

        public string PercentageIndicatorColor
        {
            get
            {
                double value;
                Double.TryParse(Percentage, out value);
                if (String.IsNullOrWhiteSpace(Percentage) || Percentage == "--" || value >= 50)
                {
                    //Green
                    return "#FF54AF62";
                }
                else
                {   //Red
                    return "#FFC40714";
                }
            }
        }
    }

    [DataContract]//To be serialized if needed
    public class Subject
    {
        private string _degree;
        [JsonProperty("Degree")]
        public string Degree
        {
            get { return String.IsNullOrWhiteSpace(_degree) ? "--" : _degree; }
            set { _degree = value; }
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("studentResult")]
        public string StudentResult { get; set; }
    }
}
