﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Response
{
    public class ActivateScholarResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("responseText")]
        public string ResponseText { get; set; }

    }
}
