﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Response
{
    public class NewsDetail
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("imageUrl")]
        public string ImageUrl { get; set; }
        [JsonProperty("newsDetails")]
        public string NewsDetails { get; set; }

    }
}
