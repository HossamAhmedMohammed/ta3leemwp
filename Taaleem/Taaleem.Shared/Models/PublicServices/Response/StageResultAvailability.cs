﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Response
{
    public class StageResultAvailability
    {
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("weburl")]
        public string WebUrl { get; set; }
    }

}
