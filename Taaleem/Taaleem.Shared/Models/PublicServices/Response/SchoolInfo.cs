﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices.Response
{
    public class SchoolInfo
    {
        [JsonProperty("ELEC")]
        public string Elec { get; set; }
        [JsonProperty("ELEC_X")]
        public double? ElecX { get; set; }
        [JsonProperty("ELEC_Y")]
        public double? ElecY { get; set; }
        [JsonProperty("SchoolName")]
        public string SchoolName { get; set; }
        [JsonProperty("School_X")]
        public double? SchoolX { get; set; }
        [JsonProperty("School_Y")]
        public double? SchoolY { get; set; }
    }

}
