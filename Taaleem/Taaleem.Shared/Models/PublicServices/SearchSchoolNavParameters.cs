﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices
{
    public class SearchSchoolNavParameters
    {
        public int Type { get; set; }
        public string Elect { get; set; }
    }
}
