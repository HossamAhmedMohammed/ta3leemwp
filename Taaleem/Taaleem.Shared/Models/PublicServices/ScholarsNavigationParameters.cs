﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices
{
    public class ScholarsNavigationParameters
    {
        public string Country { get; set; }
        public string State { get; set; }
        public string Major { get; set; }
        public string University { get; set; }
        public List<Scholar> Scholars { get; set; }
    }
}
