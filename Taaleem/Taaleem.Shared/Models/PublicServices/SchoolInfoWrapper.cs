﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Helpers;
using Taaleem.Models.PublicServices.Response;
using Taaleem.ViewModels;

namespace Taaleem.Models.PublicServices
{
    public class SchoolInfoWrapper
    {
        public string SchoolName { get; set; }
        public string Elec { get; set; }
        public double? ElecX { get; set; }
        public double? ElecY { get; set; }
        public double? SchoolX { get; set; }
        public double? SchoolY { get; set; }
        public double DistanceInMeters { get; set; }
        public string FormattedDistance { get; set; }
        public SchoolInfoWrapper(SchoolInfo schoolInfo)
        {
            SchoolName = schoolInfo.SchoolName;
            Elec = schoolInfo.Elec;
            ElecX = schoolInfo.ElecX;
            if (ElecX == 0) ElecX = null;
            ElecY = schoolInfo.ElecY;
            if (ElecY == 0) ElecY = null;
            SchoolX = schoolInfo.SchoolX;
            if (SchoolX == 0) SchoolX = null;
            SchoolY = schoolInfo.SchoolY;
            if (SchoolY == 0) SchoolY = null;
            if (ElecX.HasValue && ElecY.HasValue && SchoolX.HasValue && SchoolY.HasValue)
            {
                try
                {
                    DistanceInMeters = GetDistance(ElecY.Value, ElecX.Value, SchoolY.Value, SchoolX.Value);
                    var resources = ViewModelLocator.Resources;
                    //if(DistanceInMeters < 1000)
                    //    FormattedDistance = String.Format(resources.DistanceFromYou, DistanceInMeters.SingleFractionOrIntCurrentCulture(), resources.MeterAbbrevation);
                    //else
                    FormattedDistance = string.Format(resources.DistanceFromYou, (DistanceInMeters / 1000).SingleFractionOrIntCurrentCulture(), resources.KiloMeterAbbrevation);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lat1"></param>
        /// <param name="long1"></param>
        /// <param name="lat2"></param>
        /// <param name="long2"></param>
        /// <returns></returns>
        public double GetDistance(double lat1, double long1, double lat2, double long2)
        {
            var R = 6378137; // Earth’s mean radius in meter
            var dLat = Radius(lat2 - lat1);
            var dLong = Radius(long2 - long1);
            var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Cos(Radius(lat1)) * Math.Cos(Radius(lat2)) *
                    Math.Sin(dLong / 2) * Math.Sin(dLong / 2);

            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R * c;
            return d; // returns the distance in meter
        }

        public double Radius(double x)
        {
            return x * Math.PI / 180;
        }
    }
}
