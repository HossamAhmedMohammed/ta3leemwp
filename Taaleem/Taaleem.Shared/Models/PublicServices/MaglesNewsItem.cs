﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.ViewModels;
using Windows.Data.Html;
using Windows.Foundation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Web.Syndication;

namespace Taaleem.Models.PublicServices
{
    public class MaglesNewsItem : BindableBase
    {
        #region Fields

        private readonly IPublicSectorDataService _publicService;
        #endregion

        #region Properties
        public string Title { get; set; }
        public DateTime PublishedDate { get; set; }

        /// <summary>
        /// This is is parsed from rss and will be used to get details from extra service.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// This represent the short description returned with rss, 
        /// This will be used as a fall back when getting full description fails.
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// This is will be pulled from the extra service
        /// </summary>
        public string FullDescription { get; set; }

        /// <summary>
        /// Link of article on website.
        /// </summary>
        public string Link { get; set; }

        private BitmapImage _imageUrl;
        /// <summary>
        /// Image url extracted from extra service using id.
        /// </summary>
        public BitmapImage ImageUrl
        {
            get { return _imageUrl; }
            set { SetProperty(ref _imageUrl, value); }
        }

        /// <summary>
        /// This will prefer full description, when it's empty it will switch back to short description.
        /// </summary>
        public string Description
        {
            get { return String.IsNullOrEmpty(FullDescription) ? ShortDescription : FullDescription; }
        }

        private bool _isBusy;
        /// <summary>
        /// This will be used to indicate that there is ongoing request to get details
        /// </summary>
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        #endregion

        #region Initialization
        private MaglesNewsItem(IPublicSectorDataService publicService)
        {
            _publicService = publicService;
        }

        /// <summary>
        /// For serialization purposes only
        /// </summary>
        public MaglesNewsItem()
        {
            _publicService = ViewModelLocator.Locator.PublicSectorDataService;
        }

        public static MaglesNewsItem Wrap(SyndicationItem item, IPublicSectorDataService publicService)
        {
            var wrapper = new MaglesNewsItem(publicService);

            //Title
            wrapper.Title = item.Title != null ? item.Title.Text : "";

            if (item.Links.Count > 0)
            {
                //Link, Get First link
                wrapper.Link = item.Links[0].Uri.AbsoluteUri;

                //Parse Id
                try
                {
                    WwwFormUrlDecoder decoder = new WwwFormUrlDecoder(new Uri(wrapper.Link).Query);
                    wrapper.Id = decoder.GetFirstValueByName(@"NewsID");
                }
                catch (Exception)
                {
                    // ignored
                }
            }


            //parse Description
            string content = "";
            if (item.Content != null)
            {
                content = item.Content.Text;
            }
            else if (item.Summary != null)
            {
                content = item.Summary.Text;
            }

            if (!String.IsNullOrEmpty(content))
            {
                try
                {
                    //Remove html tags from short description
                    content = HtmlUtilities.ConvertToText(content);

                    //Get only Description after Brief("Feeds will almost have structure of date then Brief.")
                    const string briefLiteral = "Brief:";
                    const string ThumbnailLiteral = "Thumbnail:";
                    int briefIndex = content.IndexOf(briefLiteral, StringComparison.Ordinal);
                  
                   // int x = content.Length;
                    if (briefIndex > -1) content = content.Substring(briefIndex + briefLiteral.Length);
                   // int y = content.Length;
                    int ThumbnailIndex = content.IndexOf(ThumbnailLiteral, StringComparison.Ordinal);
                    if (ThumbnailIndex > -1) content = content.Remove(ThumbnailIndex, ThumbnailLiteral.Length);
                }
                catch (Exception e)
                {
                    // ignored
                }
            }
            //Set short description
            wrapper.ShortDescription = content;
            wrapper.ImageUrl = new BitmapImage(new Uri("http://www.sec.gov.qa" + wrapper.FetchLinksFromSource(item.NodeValue)));
            wrapper.PublishedDate = item.PublishedDate.DateTime;

            //Call FullDescription and image request async and don't await
            wrapper.GetDetails();

            return wrapper;
        }
        #endregion

        #region Commands
        #endregion

        #region Methods

        private async void GetDetails()
        {
            if (IsBusy) return;
            try
            {
                IsBusy = true;
                var response = await _publicService.GetArticleDetailAsync(Id);
                if (response.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    FullDescription = response.Result.NewsDetails;
                    OnPropertyChanged(@"Description");
                    //if (!String.IsNullOrEmpty(response.Result.ImageUrl))
                       // ImageUrl = response.Result.ImageUrl;
                }
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                IsBusy = false;
            }
        }

        public string FetchLinksFromSource(string htmlSource)
        {
            string href="";
            List<Uri> links = new List<Uri>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            //foreach (Match m in matchesImgSrc)
            //{
            if (matchesImgSrc.Count > 0)
            {
                 href = matchesImgSrc[0].Groups[1].Value;
            }
               // links.Add(new Uri(href));
           // }
            return href;
        }

        #endregion
    }
}
