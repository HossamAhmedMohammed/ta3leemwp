﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.ViewModels;

namespace Taaleem.Models.PublicServices
{
    public class GenderType
    {
        public string Name { get; set; }
        public int Id { get; set; }

        public GenderType(string name, int id)
        {
           

                Name = name;
            Id = id;
        }
        public static readonly GenderType Male = new GenderType(ViewModelLocator.Resources.Male, 1);//(ViewModelLocator.Resources.Male, 1);
        public static readonly GenderType Female = new GenderType(ViewModelLocator.Resources.Female, 2);
        public static readonly List<GenderType> All = new List<GenderType> { Male, Female };
    }
}
