﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PublicServices
{
    public class Scholar
    {
        public string University { get; set; }
        public string Major { get; set; }
        public string Country { get; set; }
    }
}

