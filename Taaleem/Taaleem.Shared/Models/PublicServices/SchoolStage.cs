﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.ViewModels;

namespace Taaleem.Models.PublicServices
{
    public class SchoolStage
    {
        public String Name { get; set; }
        public int Id { get; set; }
        public SchoolStage(String name, int id)
        {
            Name = name;
            Id = id;
        }
        public static readonly SchoolStage Garden = new SchoolStage(ViewModelLocator.Resources.Garden, 1);
        public static readonly SchoolStage Eelementary = new SchoolStage(ViewModelLocator.Resources.Eelementary, 2);
        public static readonly SchoolStage Preparatory = new SchoolStage(ViewModelLocator.Resources.Preparatory, 3);
        public static readonly SchoolStage Secondary = new SchoolStage(ViewModelLocator.Resources.Secondary, 4);

        public static readonly List<SchoolStage> Stages = new List<SchoolStage>()
        {
            Garden,Eelementary,Preparatory,Secondary
        };

        public static void refreshData()
        {
            Stages.Clear();
            Stages.Add(new SchoolStage(ViewModelLocator.Resources.Garden, 1));
            Stages.Add(new SchoolStage(ViewModelLocator.Resources.Eelementary, 2));
            Stages.Add(new SchoolStage(ViewModelLocator.Resources.Preparatory, 3));
            Stages.Add(new SchoolStage(ViewModelLocator.Resources.Secondary, 4));
        }
    }


}
