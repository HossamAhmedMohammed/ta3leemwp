﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.SchoolRegisteration.Response;

namespace Taaleem.Models.SchoolRegisteration
{
    public class GradeLevelWrapper
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public static GradeLevelWrapper Wrap(GradeLevel gradeLevel, int languageId)
        {
            var wrapper = new GradeLevelWrapper
            {
                Id = gradeLevel.Id,
                Description = gradeLevel.Description,
                Code = gradeLevel.Code
            };

            //Try to localize string
            string localized = Descriptionresource.GetDescription(gradeLevel.DescriptionResource,
                Culture.LanguageToCulture(languageId));
            if (!String.IsNullOrEmpty(localized)) wrapper.Description = localized;
            return wrapper;
        }
    }
}
