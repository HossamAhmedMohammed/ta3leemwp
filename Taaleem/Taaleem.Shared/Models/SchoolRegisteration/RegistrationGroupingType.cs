﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.ViewModels;

namespace Taaleem.Models.SchoolRegisteration
{
    public class RegisterationGroupingType
    {
        public string DisplayName { get; set; }
        public string PropertyName { get; set; }
        public int Type { get; set; }

        public RegisterationGroupingType(string displayName, string propertyName, int type)
        {
            DisplayName = displayName;
            PropertyName = propertyName;
            Type = type;
        }
        public static readonly RegisterationGroupingType StudentName = new RegisterationGroupingType(ViewModelLocator.Resources.StudentName, @"StudentName", 1);

        public static readonly RegisterationGroupingType Status = new RegisterationGroupingType(ViewModelLocator.Resources.Status, @"Status", 2);

        public static readonly List<RegisterationGroupingType> GroupTypes = new List<RegisterationGroupingType>
        {
           StudentName ,Status
        };
    }
}
