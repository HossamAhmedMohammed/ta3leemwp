﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.DataServices.Interfaces;
using Taaleem.DataServices.Requests;
using Taaleem.Models.SchoolRegisteration.Request;
using Taaleem.Models.SchoolRegisteration.Response;

namespace Taaleem.Models.SchoolRegisteration
{
    public class SchoolRegisterationWrapper : BindableBase
    {
        #region Fields

        #endregion
        #region properties
        public int Capacity { get; set; }
        public int EnrollmentCount { get; set; }
        public string Id { get; set; }
        public bool IsAlternate { get; set; }
        public string LocalId { get; set; }
        public string Name { get; set; }

        private SchoolContact _contact;
        public SchoolContact Contact
        {
            get { return _contact; }
            set
            {
                if (SetProperty(ref _contact, value))
                {
                    OnPropertyChanged(@"HasLocation");
                }
            }
        }

        public bool HasLocation
        {
            get { return Contact != null && Contact.Longitude.HasValue && Contact.Latitude.HasValue; }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        #endregion

        #region Initialization

        public SchoolRegisterationWrapper()
        {
        }
        public SchoolRegisterationWrapper(RegisterationSchool school)
        {
            ;
            Capacity = school.Capacity;
            EnrollmentCount = school.EnrollmentCount;
            Id = school.Id;
            IsAlternate = school.IsAlternate;
            LocalId = school.LocalId;
            Name = school.Name;
        }
        #endregion

        #region Methods

        public async Task<bool> GetContactAsync(ISchoolRegisterationService schoolservice, CancellationToken? token)
        {
            try
            {
                var parameters = new GetSchoolContactQuery() { SchoolId = Id };
                var requestResponse = await schoolservice.GetSchoolContactAsync(parameters, token);
                if (requestResponse.ResponseStatus == ResponseStatus.SuccessWithResult)
                {
                    var contacts = requestResponse.Result.SchoolContacts.Items;
                    Contact = contacts.FirstOrDefault();
                    return Contact != null;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        #endregion
    }
}
