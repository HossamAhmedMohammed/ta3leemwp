﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class EmployerTypesResponse
    {
        [JsonProperty("GetEmployerTypesResult")]
        public List<EmployerType> EmployerTypes { get; set; }
    }

    public class EmployerType
    {
        [JsonProperty("Code")]
        public string Code { get; set; }
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("DescriptionResource")]
        public Descriptionresource DescriptionResource { get; set; }
        [JsonProperty("ID")]
        public string Id { get; set; }
    }

}
