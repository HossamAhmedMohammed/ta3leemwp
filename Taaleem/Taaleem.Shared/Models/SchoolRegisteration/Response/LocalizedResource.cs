﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class Descriptionresource
    {
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("LocalizedStringResources")]
        public Localizedstringresource[] LocalizedStringResources { get; set; }

        public static string GetDescription(Descriptionresource descriptionresource, string cultureName)
        {
            if (descriptionresource == null) return String.Empty;
            if (descriptionresource.LocalizedStringResources != null)
            {
                foreach (var localized in descriptionresource.LocalizedStringResources)
                {
                    if (String.Equals(localized.CultureName, cultureName, StringComparison.OrdinalIgnoreCase))
                    {
                        return localized.Value;
                    }
                }
            }
            return descriptionresource.Description;
        }
    }

    public class Localizedstringresource
    {
        [JsonProperty("CultureName")]
        public string CultureName { get; set; }
        [JsonProperty("Value")]
        public string Value { get; set; }
    }


}
