﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.ViewModels;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class GetRegistrationsResponse : ServerError
    {
        [JsonProperty("GetRegistrationsResult")]
        public List<RegisterationStatus> GetRegistrationsResult { get; set; }
    }

    public class RegisterationStatus
    {
        [JsonProperty("ElectricityFilePath")]
        public string ElectricityFilePath { get; set; }
        [JsonProperty("GradeLevel")]
        public string GradeLevel { get; set; }
        [JsonProperty("ID")]
        public string Id { get; set; }
        [JsonProperty("Relationship")]
        public string Relationship { get; set; }
        [JsonProperty("SchoolID")]
        public string SchoolId { get; set; }
        [JsonProperty("SchoolName")]
        public string SchoolName { get; set; }
        [JsonProperty("Status")]
        public string Status { get; set; }
        [JsonProperty("StudentID")]
        public string StudentId { get; set; }
        [JsonProperty("StudentName")]
        public string StudentName { get; set; }
        [JsonProperty("TransactionNumber")]
        public int TransactionNumber { get; set; }
        [JsonProperty("WorkCertificateFilePath")]
        public string WorkCertificateFilePath { get; set; }

        public string TransactionFormatted
        {
            get { return String.Format(ViewModelLocator.Resources.StudentTransactionNo, TransactionNumber); }
        }

        public string StatusColor
        {
            get
            {
                if (String.IsNullOrEmpty(Status))
                {
                    return GrayColor;
                }
                if (String.Equals(Status, "Approved ", StringComparison.OrdinalIgnoreCase) ||
                    String.Equals(Status, "معتمد", StringComparison.OrdinalIgnoreCase))
                    return GreenColor;
                if (String.Equals(Status, "Pending", StringComparison.OrdinalIgnoreCase) ||
                   String.Equals(Status, "قيد الإجراء", StringComparison.OrdinalIgnoreCase))
                    return OrangleColor;
                if (String.Equals(Status, "Rejected", StringComparison.OrdinalIgnoreCase) ||
                   String.Equals(Status, "مرفوض", StringComparison.OrdinalIgnoreCase))
                    return RedColor;
                return GrayColor;
            }
        }

        private const string RedColor = "#c9282d";
        private const string OrangleColor = "#f2af32 ";
        private const string GreenColor = "#00ae42";
        private const string GrayColor = "#b5b5b5";
    }

}
