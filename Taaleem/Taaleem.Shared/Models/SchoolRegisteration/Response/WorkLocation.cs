﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class WorkLocationsResponse
    {
        [JsonProperty("GetWorkLocationsResult")]
        public List<Worklocation> Worklocations { get; set; }
    }

    public class Worklocation
    {
        [JsonProperty("Description")]
        public string Description { get; set; }
    }

}
