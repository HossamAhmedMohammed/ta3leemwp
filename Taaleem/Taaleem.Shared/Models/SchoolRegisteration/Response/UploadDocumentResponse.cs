﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class UploadDocumentResponse
    {
        [JsonProperty("ReceiveFileResult")]
        public string ReceiveFileResult { get; set; }
    }
}
