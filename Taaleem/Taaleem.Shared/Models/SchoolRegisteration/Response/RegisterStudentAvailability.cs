﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class RegisterStudentAvailability
    {
        [JsonProperty("IsWebRegistrationEnabledResult")]
        public bool IsEnabled { get; set; }
    }

}
