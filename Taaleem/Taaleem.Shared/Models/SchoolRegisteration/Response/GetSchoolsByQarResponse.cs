﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class GetSchoolsByQarResponse : ServerError
    {
        [JsonProperty("GetNearestSchoolsByQarsResult")]
        public NearestSchoolQar NearestSchools { get; set; }
    }

    public class NearestSchoolQar
    {
        [JsonProperty("TotalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("RootResults")]
        public List<RegisterationSchool> Items { get; set; }
    }


}
