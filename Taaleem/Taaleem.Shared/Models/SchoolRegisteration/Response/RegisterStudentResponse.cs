﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class RegisterStudentResponse
    {
        [JsonProperty("RegisterStudentResult")]
        public bool Success { get; set; }
    }
}
