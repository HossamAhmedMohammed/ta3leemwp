﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class ParentInfoResponse : ServerError
    {
        [JsonProperty("GetParentResult")]
        public ParentInfo ParentInfo { get; set; }
    }

    public class ParentInfo
    {
        [JsonProperty("EMail")]
        public string EMail { get; set; }
        [JsonProperty("EmployerType")]
        public string EmployerType { get; set; }
        [JsonProperty("ID")]
        public string Id { get; set; }
        [JsonProperty("LocalId")]
        public string LocalId { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("PhoneNumber")]
        public string PhoneNumber { get; set; }
        [JsonProperty("WorkLocation")]
        public string WorkLocation { get; set; }
    }
}
