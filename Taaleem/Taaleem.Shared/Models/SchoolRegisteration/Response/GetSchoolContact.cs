﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{

    public class GetSchoolContactResponse
    {
        [JsonProperty("GetSchoolContactResult")]
        public SchoolContacts SchoolContacts { get; set; }
    }

    public class SchoolContacts
    {
        [JsonProperty("TotalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("RootResults")]
        public SchoolContact[] Items { get; set; }
    }

    public class SchoolContact
    {

        [JsonProperty("Area")]
        public string Area { get; set; }
        [JsonProperty("ID")]
        public string Id { get; set; }
        [JsonProperty("Latitude")]
        public double? Latitude { get; set; }
        [JsonProperty("Longitude")]
        public double? Longitude { get; set; }
        [JsonProperty("Municipality")]
        public string Municipality { get; set; }

        private string _phoneNumber;
        [JsonProperty("PhoneNumber")]
        public string PhoneNumber
        {
            get { return String.IsNullOrEmpty(_phoneNumber) ? "--" : _phoneNumber; }
            set { _phoneNumber = value; }
        }

        private string _principal;

        [JsonProperty("Principal")]
        public string Principal
        {
            get { return String.IsNullOrEmpty(_principal) ? "--" : _principal; }
            set { _principal = value; }
        }
    }

}
