﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class RegisterationSchool
    {
        [JsonProperty("Capacity")]
        public int Capacity { get; set; }
        [JsonProperty("EnrollmentCount")]
        public int EnrollmentCount { get; set; }
        [JsonProperty("ID")]
        public string Id { get; set; }
        [JsonProperty("IsAlternate")]
        public bool IsAlternate { get; set; }
        [JsonProperty("LocalId")]
        public string LocalId { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
    }

}
