﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class RegisterParentResponse : ServerError
    {
        //in case of success
        [JsonProperty("RegisterAsParentResult")]
        public bool RegisterAsParentResult { get; set; }
    }
}
