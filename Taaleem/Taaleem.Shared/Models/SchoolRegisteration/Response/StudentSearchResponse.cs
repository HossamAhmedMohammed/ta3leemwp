﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class StudentSearchResponse : ServerError
    {
        [JsonProperty("SearchStudentResult")]
        public StudentSearchInfo StudentInfo { get; set; }
    }

    public class StudentSearchInfo
    {
        [JsonProperty("BirthDate")]
        public DateTime BirthDate { get; set; }
        [JsonProperty("GenderID")]
        public string GenderId { get; set; }
        [JsonProperty("ID")]
        public string Id { get; set; }
        [JsonProperty("IsQatariOrGCC")]
        public bool IsQatariOrGcc { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
    }
}
