﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class ServerError
    {
        [JsonProperty("ErrorCode")]
        public int ErrorCode { get; set; }
        [JsonProperty("ErrorMessage")]
        public string ErrorMessage { get; set; }
        [JsonProperty("IsDomainException")]
        public bool IsDomainException { get; set; }
        [JsonProperty("StackTrace")]
        public string StackTrace { get; set; }

        public bool HasError
        {
            get { return ErrorCode != 0 || !String.IsNullOrWhiteSpace(ErrorMessage); }
        }
    }
}

