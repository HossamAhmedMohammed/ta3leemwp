﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class GradeLevelResponse
    {
        [JsonProperty("GetGradeLevelsResult")]
        public List<GradeLevel> GradeLevels { get; set; }
    }

    public class GradeLevel
    {
        [JsonProperty("Code")]
        public string Code { get; set; }
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("DescriptionResource")]
        public Descriptionresource DescriptionResource { get; set; }
        [JsonProperty("ID")]
        public string Id { get; set; }
    }
}
