﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Response
{
    public class GetSchoolsByElecResponse : ServerError
    {
        [JsonProperty("GetNearestSchoolsResult")]
        public NearestSchoolsElec NearestSchools { get; set; }
    }

    public class NearestSchoolsElec
    {
        [JsonProperty("TotalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("RootResults")]
        public List<RegisterationSchool> Items { get; set; }
    }

}
