﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.SchoolRegisteration.Response;

namespace Taaleem.Models.SchoolRegisteration
{
    public class EmployerTypeWrapper
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public static EmployerTypeWrapper Wrap(EmployerType employerType, int languageId)
        {
            var wrapper = new EmployerTypeWrapper
            {
                Id = employerType.Id,
                Description = employerType.Description
            };

            //Try to localize string
            string localized = Descriptionresource.GetDescription(employerType.DescriptionResource,
                Culture.LanguageToCulture(languageId));
            if (!String.IsNullOrEmpty(localized)) wrapper.Description = localized;
            return wrapper;
        }
    }
}
