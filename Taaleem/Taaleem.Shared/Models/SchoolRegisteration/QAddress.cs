﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Common;

namespace Taaleem.Models.SchoolRegisteration
{
    public class QAddress : BindableBase
    {
        private string _area;
        public string Area
        {
            get { return _area; }
            set { SetProperty(ref _area, value); }
        }

        private string _street;
        public string Street
        {
            get { return _street; }
            set { SetProperty(ref _street, value); }
        }

        private string _plot;
        public string Plot
        {
            get { return _plot; }
            set { SetProperty(ref _plot, value); }
        }

        public bool HasEmptyValues()
        {
            return String.IsNullOrWhiteSpace(Area) || String.IsNullOrWhiteSpace(Street) ||
                   String.IsNullOrWhiteSpace(Plot);
        }

        public void ResetValues()
        {
            Area = null;
            Street = null;
            Plot = null;
        }

    }
}
