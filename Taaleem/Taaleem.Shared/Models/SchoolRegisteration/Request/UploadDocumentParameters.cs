﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Request
{
    public class UploadDocumentParamters
    {
        [JsonProperty("filetype")]
        public string Filetype { get; set; }
        [JsonProperty("locationId")]
        public string LocationId { get; set; }
        [JsonProperty("data")]
        public string Data { get; set; }
    }
}
