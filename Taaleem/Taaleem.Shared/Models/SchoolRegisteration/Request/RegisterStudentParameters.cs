﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Request
{
    public class RegisterStudentParameters
    {
        [JsonProperty("studentQID")]
        public string StudentQID { get; set; }
        [JsonProperty("gradeLevelID")]
        public string GradeLevelID { get; set; }
        [JsonProperty("schoolID")]
        public string SchoolID { get; set; }
        [JsonProperty("relationshipID")]
        public string RelationshipID { get; set; }
        [JsonProperty("addressVerificationType", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string AddressVerificationType { get; set; }
        [JsonProperty("electricityNumber")]
        public string ElectricityNumber { get; set; }
        [JsonProperty("workCertificateFilename")]
        public string WorkCertificateFilename { get; set; }
        [JsonProperty("electFilename")]
        public string ElectFilename { get; set; }
        [JsonProperty("birthCertificateFileName", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string BirthCertificateFileName { get; set; }
        [JsonProperty("isMotherQatari", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? IsMotherQatari { get; set; }
        [JsonProperty("area", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Area { get; set; }
        [JsonProperty("street", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Street { get; set; }
        [JsonProperty("plot", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Plot { get; set; }
    }

}
