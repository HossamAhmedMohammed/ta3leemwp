﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Request
{
    public class WorkLocationsParameters
    {
        [JsonProperty("employerTypeID")]
        public string EmployerTypeId { get; set; }
    }
}
