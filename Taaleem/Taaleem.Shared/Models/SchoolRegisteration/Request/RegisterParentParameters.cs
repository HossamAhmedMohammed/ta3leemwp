﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Request
{
    public class RegisterParentParameters
    {
        [JsonProperty("QID")]
        public string Qid { get; set; }
        [JsonProperty("mobileNumber")]
        public string MobileNumber { get; set; }
        [JsonProperty("employerType")]
        public string EmployerType { get; set; }
        [JsonProperty("workLocation")]
        public string WorkLocation { get; set; }
        [JsonProperty("eMail")]
        public string EMail { get; set; }
    }

}
