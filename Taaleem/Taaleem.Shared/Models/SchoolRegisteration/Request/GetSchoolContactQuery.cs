﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.SchoolRegisteration.Request
{
    public class GetSchoolContactQuery
    {
        [QueryParameter("schoolID")]
        public string SchoolId { get; set; }
    }
}
