﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration.Request
{
    public class GetSchoolsByQarParameters
    {
        [JsonProperty("area")]
        public string Area { get; set; }
        [JsonProperty("street")]
        public string Street { get; set; }
        [JsonProperty("plot")]
        public string Plot { get; set; }
        [JsonProperty("gradeLevelID")]
        public string GradeLevelId { get; set; }
        [JsonProperty("genderID")]
        public string GenderId { get; set; }
        [JsonProperty("QID")]
        public string StudentQid { get; set; }
        [JsonProperty("isMotherQatari", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsMotherQatari { get; set; }
        [JsonProperty("motherQID", NullValueHandling = NullValueHandling.Ignore)]
        public string MotherQid { get; set; }
    }

}
