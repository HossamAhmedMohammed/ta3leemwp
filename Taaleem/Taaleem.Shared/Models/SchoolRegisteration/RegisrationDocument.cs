﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Common;
using Taaleem.DataServices.Requests;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;

namespace Taaleem.Models.SchoolRegisteration
{
    public class RegisterationDocument : BindableBase
    {
        public IStorageFile StorageFile { get; set; }

        private string _documentTitle;
        public string DocumentTitle
        {
            get { return _documentTitle; }
            set { SetProperty(ref _documentTitle, value); }
        }

        private BitmapImage _thumbnailBitmap;
        public BitmapImage ThumbnailBitmap
        {
            get { return _thumbnailBitmap; }
            set { SetProperty(ref _thumbnailBitmap, value); }
        }

        private RegisterationDocumentType _documentType;
        public RegisterationDocumentType DocumentType
        {
            get { return _documentType; }
            set { SetProperty(ref _documentType, value); }
        }

        private RequestMessage _errorMessage;
        public RequestMessage ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        public RegisterationDocument()
        {

        }

        public RegisterationDocument(string documentTitle, RegisterationDocumentType documentType)
        {
            _documentTitle = documentTitle;
            _documentType = documentType;
        }
    }

    public enum RegisterationDocumentType
    {
        ParentWork,
        Eelecticity,
        MotherBirth
    }
}
