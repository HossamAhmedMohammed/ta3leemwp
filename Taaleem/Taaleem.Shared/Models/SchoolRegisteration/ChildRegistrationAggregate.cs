﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration
{
    public class ChildRegisterationAggregate
    {
        public string StudentName { get; set; }
        public string StudentQId { get; set; }
        public string StudentId { get; set; }
        public DateTime StudentBirthDate { get; set; }
        public string StudentGenderId { get; set; }
        public string GradelevelId { get; set; }
        public string GradelevelDescription { get; set; }
        public string RelationshipId { get; set; }
        public string RelationshipDescription { get; set; }
        public bool IsStudentQatariOrGcc { get; set; }
        public bool IsMotherQatari { get; set; }
        public string MotherQId { get; set; }
        public string ElectNo { get; set; }
        public QAddress QAddress { get; set; }//QAR
        public string SchoolId { get; set; }
        public string SchoolLocalId { get; set; }
        public string SchoolName { get; set; }
        public string WorkCertificateFileName { get; set; }
        public string ElectFileName { get; set; }
        public string MotherBirthFileName { get; set; }

    }
}
