﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.SchoolRegisteration
{
    /// <summary>
    /// This will be used to hold pairs of property name and value, Which will be presented later in list.
    /// </summary>
    public class PropertyPair
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public PropertyPair()
        {
        }

        public PropertyPair(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}
