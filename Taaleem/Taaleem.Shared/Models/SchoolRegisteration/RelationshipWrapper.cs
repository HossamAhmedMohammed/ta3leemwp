﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.SchoolRegisteration.Response;

namespace Taaleem.Models.SchoolRegisteration
{
    public class RelationshipWrapper
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public static RelationshipWrapper Wrap(Relationship relationship, int languageId)
        {
            var wrapper = new RelationshipWrapper
            {
                Id = relationship.Id,
                Code = relationship.Code,
                Description = relationship.Description
            };

            //Try to localize string
            string localized = Descriptionresource.GetDescription(relationship.DescriptionResource,
                Culture.LanguageToCulture(languageId));
            if (!String.IsNullOrEmpty(localized)) wrapper.Description = localized;
            return wrapper;
        }


    }
}
