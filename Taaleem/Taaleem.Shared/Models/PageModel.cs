﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models
{
    public class PageModel
    {
        public String Title { get; set; }
        public String ImagePath { get; set; }

        /// <summary>
        /// If page type value is null, PageTypeReslover will be used internally to return page type value.
        /// </summary>
        private Func<Type> _pageTypeReslover;

        private Type _pageType;
        //Page Type,Can be replaced with enum or viewmodel that will be mapped to page type
        public Type PageType
        {
            get { return _pageType ?? (_pageTypeReslover == null ? null : _pageTypeReslover.Invoke()); }
            set { _pageType = value; }
        }

        public PageModel()
        {

        }
        public PageModel(string title, Func<Type> pageTypeReslover, string imagePath = null)
        {
            Title = title;
            ImagePath = imagePath;
            _pageTypeReslover = pageTypeReslover;
        }

        public PageModel(string title, Type pageType, string imagePath = null)
        {
            Title = title;
            ImagePath = imagePath;
            PageType = pageType;
        }
    }
}
