﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PrivateSchools.Response
{
    public class PrivateSchool
    {

      
            public object Accredations { get; set; }
            public object AddressArabic { get; set; }
            public object AddressEnglish { get; set; }
            public string AreaArabic { get; set; }
            public string AreaEnglish { get; set; }
            public string CarriculumArabic { get; set; }
            public string CarriculumEnglish { get; set; }
            public object Email { get; set; }
            public object Facebook { get; set; }
            public string GenderArabic { get; set; }
            public string GenderEnglish { get; set; }
            public string[] GradeLevels { get; set; }
            public object Instagram { get; set; }
            public float? Latitude { get; set; }
            public object Logo { get; set; }
            public object LogoBinary { get; set; }
            public float? Longitude { get; set; }
            public object Mission { get; set; }
            public string NameArabic { get; set; }
            public string NameEnglish { get; set; }
            public string ParentID { get; set; }
            public object Phone { get; set; }
            public object PrincipleNameArabic { get; set; }
            public object PrincipleNameEnglish { get; set; }
            public object PrincipleTitleArabic { get; set; }
            public object PrincipleTitleEnglish { get; set; }
            public int RatingCount { get; set; }
            public object RatingScore { get; set; }
            public object SchoolFees { get; set; }
            public string SchoolID { get; set; }
            public bool StudentsAvailability { get; set; }
            public int StudentsCapacity { get; set; }
            public object Twitter { get; set; }
            public object Vision { get; set; }
            public bool VoucherSupport { get; set; }
            public object Website { get; set; }
            public object YouTube { get; set; }
        public string Name
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return NameEnglish;
                }
                else
                {
                    return NameArabic;
                }
            }

        }
        public string Gender
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return GenderEnglish;
                }
                else
                {
                    return GenderArabic;
                }
            }

        }
        public string Area
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return AreaEnglish;
                }
                else
                {
                    return AreaArabic;
                }
            }

        }
        public string Carriculum
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return CarriculumEnglish;
                }
                else
                {
                    return CarriculumArabic;
                }
            }

        }
        public object Address
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return AddressEnglish;
                }
                else
                {
                    return AddressArabic;
                }
            }

        }

        public object PrincipleName
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return PrincipleNameEnglish;
                }
                else
                {
                    return PrincipleNameArabic;
                }
            }

        }

        public object PrincipleTitle
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return PrincipleTitleEnglish;
                }
                else
                {
                    return PrincipleTitleArabic;
                }
            }

        }
    }
}
