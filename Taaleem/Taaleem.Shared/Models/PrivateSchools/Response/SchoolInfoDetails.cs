﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PrivateSchools.Response
{
    public class SchoolInfoDetails
    {


        public Getschoolinfodetailjsonresult GetSchoolInfoDetailJsonResult { get; set; }
    }
    public class Getschoolinfodetailjsonresult
    {
        public string Accredation { get; set; }
        public string Mission { get; set; }
        public int RatingCount { get; set; }
        public int RatingScore { get; set; }
        public string SchoolInfoID { get; set; }
        public string Vision { get; set; }
    }




}
