﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PrivateSchools.Response
{
    public class GetSchoolDetailsResponse
    {

        public Getschooldetailsjsonresult GetSchoolDetailsJsonResult { get; set; }
    }

    public class Getschooldetailsjsonresult
    {
        public string[] Accredations { get; set; }
        public string AddressArabic { get; set; }
        public string AddressEnglish { get; set; }
        public string AreaArabic { get; set; }
        public string AreaEnglish { get; set; }
        public string CarriculumArabic { get; set; }
        public string CarriculumEnglish { get; set; }
        public string Email { get; set; }
        public string Facebook { get; set; }
        public string GenderArabic { get; set; }
        public string GenderEnglish { get; set; }
        public string[] GradeLevels { get; set; }
        public string Instagram { get; set; }
        public float Latitude { get; set; }
        public object Logo { get; set; }
        public object LogoBinary { get; set; }
        public float Longitude { get; set; }
        public string Mission { get; set; }
        public string NameArabic { get; set; }
        public string NameEnglish { get; set; }
        public string ParentID { get; set; }
        public string Phone { get; set; }
        public string PrincipleNameArabic { get; set; }
        public string PrincipleNameEnglish { get; set; }
        public string PrincipleTitleArabic { get; set; }
        public string PrincipleTitleEnglish { get; set; }
        public int RatingCount { get; set; }
        public int RatingScore { get; set; }
        public List<SchoolFee> SchoolFees { get; set; }
        public string SchoolID { get; set; }
        public bool StudentsAvailability { get; set; }
        public int StudentsCapacity { get; set; }
        public string Twitter { get; set; }
        public string Vision { get; set; }
        public bool VoucherSupport { get; set; }
        public object Website { get; set; }
        public object YouTube { get; set; }


        public string GradeLvls
        {
            get
            {
                string s = "";
                foreach (string str in GradeLevels)
                {
                    s = s + str + "  ";
                }
                return s;
            }

        }
        public object Address
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return AddressEnglish;
                }
                else
                {
                    return AddressArabic;
                }
            }

        }

        public object PrincipleName
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return PrincipleNameEnglish;
                }
                else
                {
                    return PrincipleNameArabic;
                }
            }

        }

        public object PrincipleTitle
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return PrincipleTitleEnglish;
                }
                else
                {
                    return PrincipleTitleArabic;
                }
            }

        }

    }


    public class SchoolFee
    {
        public float Fee { get; set; }
        public string GradeLevelArabic { get; set; }
        public string GradeLevelEnglish { get; set; }
        public string SchoolID { get; set; }
        public object GradeLevell
        {
            get
            {
                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return GradeLevelEnglish;
                }
                else
                {
                    return GradeLevelArabic;
                }
            }

        }
    }


}

