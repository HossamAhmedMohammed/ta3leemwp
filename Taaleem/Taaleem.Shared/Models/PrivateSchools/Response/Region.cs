﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PrivateSchools.Response
{
    public class Region
    {

        public Getregionsresult[] GetRegionsResult { get; set; }
    }

    public class Getregionsresult
    {
        public string CodeArabic { get; set; }
        public string CodeEnglish { get; set; }
        public string DescriptionArabic { get; set; }
        public string DescriptionEnglish { get; set; }
        public string ID { get; set; }
       

        public string Description
        {
            get {
                if(ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    return DescriptionEnglish;
                }
                else
                {
                    return DescriptionArabic;
                }
            }
           
        }

    }


}
