﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.PrivateSchools.Request
{
   public class SearchSchoolParameters
    {
       public List<string> schoolTypeIDs { get; set; }
        public List<string> educationalCurriculumIDs { get; set; }
        public List<string> educationalLevelIDs { get; set; }
        public List<string> regionIDs { get; set; }
    }
}
