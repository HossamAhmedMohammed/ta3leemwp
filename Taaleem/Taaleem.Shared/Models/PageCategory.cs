﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Taaleem.Models
{
    public class PageCategory
    {
        public String Title { get; set; }

        public String ImagePath { get; set; }

        public PageCategoryType CategoryType { get; set; }

        public LoginInfo LoginInfo { get; private set; }

        public bool IsSinglePage
        {
            get { return Pages.Count == 1; }
        }

        public PageCategory()
        {
            Pages = new List<PageModel>();
            LoginInfo = new LoginInfo();
        }
        public List<PageModel> Pages { get; private set; }
    }

    public enum PageCategoryType
    {
        None,
        Children,
        Employees,
        News,
        Public,
    }

    public class LoginInfo
    {
        public bool RequiresCredential { get; set; }
        public Type LoginPageType { get; set; }
        public Func<Task<bool>> IsLoggedInAction { set; get; }
    }
}
