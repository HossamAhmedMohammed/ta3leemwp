﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models
{
   public class MainMenuItem
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string image;

        public string Image
        {
            get { return image; }
            set { image = value; }
        }

        private string uRl;

        public string URL
        {
            get { return uRl; }
            set { uRl = value; }
        }

    }
}
