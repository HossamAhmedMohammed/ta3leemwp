﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.Children.Response;

namespace Taaleem.Models.Children
{
    public class PrivateNoteWrapper
    {
        public int ConversationPostId { get; set; }
        public DateTime PostDate { get; set; }
        public string PostText { get; set; }
        public long PrivateConversationId { get; set; }
        public int UserId { get; set; }
        public bool IsTeacher { set; get; }

        public static PrivateNoteWrapper FromPrivateNote(PrivateNote note, int teacherId)
        {
            PrivateNoteWrapper wrapper = new PrivateNoteWrapper();
            wrapper.ConversationPostId = note.ConversationPostId;
            wrapper.PostDate = note.PostDate;
            wrapper.PostText = note.PostText;
            wrapper.PrivateConversationId = note.PrivateConversationId;
            wrapper.UserId = note.UserId;
            wrapper.IsTeacher = note.UserId == teacherId;
            return wrapper;
        }
    }
}
