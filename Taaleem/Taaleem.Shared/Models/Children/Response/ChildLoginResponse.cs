﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Common;

namespace Taaleem.Models.Children.Response
{
    public class ChildLoginResponse
    {
        public Parent Parent { get; set; }
        public bool IsValid { get; set; }
        public int ChildrenNo { get; set; }
        public Child[] Children { get; set; }
    }

    public class Parent
    {
        public string Username { get; set; }
    }

    public class Child : BindableBase
    {
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string Grades { get; set; }
        public string ImageUrl { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Lastname { get; set; }
        //  public string LearningGroups { get; set; }
        public string MiddleName { get; set; }
        public string ProfilePictureURL { get; set; }
        //public string RoleIds { get; set; }
        public School School { get; set; }
        public string SecondaryEmail { get; set; }
        public string Sid { get; set; }
        public string SiteUrl { get; set; }
        public int UserID { get; set; }
        public string UserLogonName { get; set; }

        #region
        private bool _isSelected;
        //internal usage
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value); }
        }
        #endregion
    }

    public class School
    {
        public string Address { get; set; }
        public string Email { get; set; }
        public string ExternalFullUrl { get; set; }
        public string Fax { get; set; }
        public string FullUrl { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string RelativeUrl { get; set; }
        public string SchoolID { get; set; }
        public string ShortName { get; set; }
        public string WebSite { get; set; }
    }

}
