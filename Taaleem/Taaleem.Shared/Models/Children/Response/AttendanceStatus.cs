﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Children.Response
{
    public class AttendanceStatus
    {
        public int AttendanceStatusId { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public string Description3 { get; set; }
        public string ExternalId { get; set; }
        public string StatusColor { get; set; }
    }

}
