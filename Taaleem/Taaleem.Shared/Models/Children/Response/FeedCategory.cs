﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Children.Response
{
    public class FeedCategory
    {
        public int Id { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
    }

}
