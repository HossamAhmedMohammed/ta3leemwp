﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Taaleem.Common;

namespace Taaleem.Models.Children.Response
{
    public class AssignmentCategory : BindableBase
    {
        public AssignmentCategoryType CategoryType { get; private set; }

        private readonly ObservableCollection<Assignment> _assignments = new ObservableCollection<Assignment>();

        public ObservableCollection<Assignment> Assignments
        {
            get { return _assignments; }
        }

        public AssignmentCategory(AssignmentCategoryType categoryType)
        {
            CategoryType = categoryType;
        }
    }
}
