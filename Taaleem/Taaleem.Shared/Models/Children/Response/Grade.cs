﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Helpers;

namespace Taaleem.Models.Children.Response
{
    public class Grade
    {

        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public object FinalScore { get; set; }
        private string _gradeName;
        public string GradeName
        {
            get { return _gradeName; }
            set
            {
                _gradeName = value;
                SplitGrade(GradeName);
            }
        }
        public bool IsPublished { get; set; }
        public int LearningGroupId { get; set; }
        public int PeriodId { get; set; }
        public string PeriodName { get; set; }
        public int PeriodOrder { get; set; }
        public int UserId { get; set; }

        /// <summary>
        /// This value isn't returned from server, It's calculated using SplitGrade method
        /// </summary>
        public string StudentGrade { get; set; }

        /// <summary>
        /// This value isn't returned from server, It's calculated using SplitGrade method
        /// </summary>
        public string TotalGrade { get; set; }

        private void SplitGrade(string gradeName)
        {
            string[] parts = gradeName.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (parts == null || parts.Length != 2)
            {
                StudentGrade = "-";
                TotalGrade = "-";
            }
            else
            {
                double gradeDouble;
                if (Double.TryParse(parts[0], out gradeDouble))
                {
                    StudentGrade = gradeDouble.SingleFractionOrIntCurrentCulture();
                }
                else StudentGrade = "-";

                double finalDouble;
                if (Double.TryParse(parts[1], out finalDouble))
                {
                    TotalGrade = finalDouble.SingleFractionOrIntCurrentCulture();
                }
                else TotalGrade = "-";
            }
        }
    }

}
