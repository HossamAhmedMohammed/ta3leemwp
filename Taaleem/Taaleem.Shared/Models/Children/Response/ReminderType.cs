﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.ViewModels;

namespace Taaleem.Models.Children.Response
{
    public class ReminderType : IComparable
    {
        #region Fields
        public readonly static ReminderType PreviouslyCategoryType = new ReminderType()
        {
            Id = 0,
            Title = ViewModelLocator.Resources.Previously
        };

        public readonly static ReminderType TodayCategoryType = new ReminderType()
        {
            Id = 1,
            Title = ViewModelLocator.Resources.Today
        };

        public readonly static ReminderType TomorrowCategoryType = new ReminderType()
        {
            Id = 2,
            Title = ViewModelLocator.Resources.Tomorrow
        };

        public readonly static ReminderType LaterCategoryType = new ReminderType()
        {
            Id = 3,
            Title = ViewModelLocator.Resources.Later
        };

        public static readonly List<ReminderType> Categories = new List<ReminderType>
        {
          PreviouslyCategoryType, TodayCategoryType ,TomorrowCategoryType,LaterCategoryType
        };
        #endregion

        #region Properties
        public int Id { get; set; }
        public string Title { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as ReminderType;
            if (other == null) return true;
            return Id.Equals(other.Id);
        }

        public override string ToString()
        {
            return Id.ToString();
        }

        public int CompareTo(object obj)
        {
            var other = obj as ReminderType;
            if (other == null) return 1;//return 1 as null is less valuable
            return Id.CompareTo(other.Id);

        }
        #endregion
    }
}
