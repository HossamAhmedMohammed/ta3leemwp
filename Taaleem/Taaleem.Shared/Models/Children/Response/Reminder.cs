﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Children.Response
{
    public class Reminder
    {
        public DateTime Begin { get; set; }
        public int Category { get; set; }
        public string CustomProperties { get; set; }
        public string Description { get; set; }
        public DateTime End { get; set; }
        public string GUID { get; set; }
        public bool IsAllDay { get; set; }
        public bool IsRecurrent { get; set; }
        public string Location { get; set; }
        public string ObjectId { get; set; }
        public string Role { get; set; }
        public string School { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }

        private ReminderType _reminderType;
        public ReminderType ReminderType
        {
            get
            {
                if (_reminderType == null)
                {
                    var today = DateTime.Today;
                    if (End.Date < today.Date)
                        _reminderType = ReminderType.PreviouslyCategoryType;
                    else if (Begin.Date == today.Date.AddDays(1))
                        _reminderType = ReminderType.TomorrowCategoryType;
                    else if (Begin.Date <= today.Date && today.Date <= End.Date)
                        _reminderType = ReminderType.TodayCategoryType;
                    else _reminderType = ReminderType.LaterCategoryType;
                }
                return _reminderType;
            }
        }


    }

}
