﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Common;
using Taaleem.Models.Children.Database;
using Windows.Data.Html;

namespace Taaleem.Models.Children.Response
{
    public class Feed : BindableBase
    {
        public int CategoryId { get; set; }
        public DateTime Date { get; set; }
        public int Id { get; set; }
        /// <summary>
        /// Locally Assigned
        /// </summary>
        public int StudentId { get; set; }
        public string Text { get; set; }
        public string FormattedText
        {
            get
            {
                try
                {
                    return HtmlUtilities.ConvertToText(Text);
                }
                catch (Exception)
                {
                    return Text;
                }
            }
        }
        private bool _isFavourite;
        /// <summary>
        /// This is cached value and not returned from server.
        /// </summary>
        public bool IsFavourite
        {
            get { return _isFavourite; }
            set { SetProperty(ref _isFavourite, value); }
        }

        #region Constructrs
        public Feed()
        {
        }
        public Feed(FeedData feedData)
        {
            CategoryId = feedData.CategoryId;
            Date = feedData.Date;
            Id = feedData.ServerId;
            Text = feedData.Text;
            IsFavourite = feedData.IsFavourite;
            StudentId = feedData.StudentId;
        }
        #endregion
    }

}
