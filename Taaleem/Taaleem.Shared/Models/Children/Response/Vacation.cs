﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Children.Response
{
    public class Vacation
    {
        public int AcademicYearId { get; set; }
        public DateTime Date { get; set; }
        public string ExternalId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public int VacationId { get; set; }
        public string VacationName1 { get; set; }
        public string VacationName2 { get; set; }
        public string VacationName3 { get; set; }
    }

}
