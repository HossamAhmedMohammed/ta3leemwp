﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Children.Response
{
    public class Behavior
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string BehaviorTypeName { get; set; }
        public string Description { get; set; }
        public string Consequences { get; set; }
        public string Comments { get; set; }

        /// <summary>
        /// if category greater than 0 good
        /// else if  category lower than 0 bad
        /// if = 0 neutral
        /// </summary>
        public int Category { set; get; }

        /// <summary>
        /// Computed localy, It contains Path geometry Data
        /// </summary>
        public string ImagePath
        {
            get
            {
                if (Category > 0) return GemoetryPaths.BehaviorGood;
                if (Category == 0) return GemoetryPaths.BehaviorOk;
                return GemoetryPaths.BehaviorBad;
            }
        }

        /// <summary>
        /// Computed localy, It contains Path Fill Color
        /// </summary>
        public string ImageColor
        {
            get
            {
                if (Category > 0) return @"#FF00AE42";
                if (Category == 0) return @"#FFF2AF32";
                return @"#FFC9282D";
            }
        }
    }

}
