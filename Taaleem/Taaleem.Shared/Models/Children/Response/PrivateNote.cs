﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Children.Response
{
    public class PrivateNote
    {
        public int ConversationPostId { get; set; }
        public DateTime PostDate { get; set; }
        public string PostText { get; set; }
        public long PrivateConversationId { get; set; }
        public int UserId { get; set; }
    }
}
