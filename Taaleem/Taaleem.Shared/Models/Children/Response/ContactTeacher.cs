﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Children.Response
{
    public class ContactTeacher
    {
        public Teacher Teacher { get; set; }
        public Subject[] Subjects { get; set; }
        public string LearningGroupName { get; set; }

        public string FirstSubject
        {
            get
            {
                if (Subjects == null || Subjects.Length == 0)
                    return LearningGroupName;
                var subject = Subjects[0];
                return subject.SubjectName;
                //var language = ViewModelLocator.Locator.AppSettings.LanguageId;
                //switch (language)
                //{
                // case  Lanuage.English:
                //       return subject.SubjectName2;

                //}
            }
        }

        public int StudentId { get; set; }
    }

    public class Teacher
    {
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string EncryptedUserID { get; set; }
        public string FirstName { get; set; }
        public string ImageUrl { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Lastname { get; set; }
        public string MiddleName { get; set; }
        public string PersonalSecondaryMail { get; set; }
        public string ProfilePictureURL { get; set; }
        public string SecondaryEmail { get; set; }
        public string Sid { get; set; }
        public string SiteUrl { get; set; }
        public int UserID { get; set; }
        public string UserLogonName { get; set; }
    }

    public class Subject
    {
        public object ExternalId { get; set; }
        public bool IsActive { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string SchoolId { get; set; }
        public string SpWebGuid { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string SubjectName1 { get; set; }
        public string SubjectName2 { get; set; }
        public string SubjectName3 { get; set; }
        public string SubjectSiteUrl { get; set; }
    }

}
