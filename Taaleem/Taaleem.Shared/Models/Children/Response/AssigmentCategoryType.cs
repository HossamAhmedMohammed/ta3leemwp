﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.ViewModels;

namespace Taaleem.Models.Children.Response
{
    public class AssignmentCategoryType
    {
      
        public int Id { get; set; }
        public string Title { get; set; }

        ////public readonly static AssignmentCategoryType LateCategoryType = new AssignmentCategoryType()
        ////{
        ////    Id = 1,
        ////    Title = ViewModelLocator.Resources.Late
        ////};

        ////public readonly static AssignmentCategoryType DueCategoryType = new AssignmentCategoryType()
        ////{
        ////    Id = 0,
        ////    Title = ViewModelLocator.Resources.Due
        ////};

        ////public readonly static AssignmentCategoryType SubmittedCategoryType = new AssignmentCategoryType()
        ////{
        ////    Id = 2,
        ////    Title = ViewModelLocator.Resources.Submitted
        ////};

        //public readonly static AssignmentCategoryType ReturnedCategoryType = new AssignmentCategoryType()
        //{
        //    Id = 3,
        //    Title = ViewModelLocator.Resources.Returned
        //};

        ////public readonly static AssignmentCategoryType FutureCategoryType = new AssignmentCategoryType()
        ////{
        ////    Id = 3,
        ////    Title = ViewModelLocator.Resources.Future
        ////};


        public static List<AssignmentCategoryType> LoadCategories ()
        {
            Categories.Clear();
            Categories.Add(new AssignmentCategoryType()
            {
                Id = 0,
                Title = ViewModelLocator.Resources.Late
            });
            Categories.Add(new AssignmentCategoryType()
            {
                Id = 1,
                Title = ViewModelLocator.Resources.Due
            });
            Categories.Add(new AssignmentCategoryType()
            {
                Id = 3,
                Title = ViewModelLocator.Resources.Submitted
            });
            Categories.Add(new AssignmentCategoryType()
            {
                Id = 2,
                Title = ViewModelLocator.Resources.Delivered
            });

            return Categories;
        }

        public static readonly List<AssignmentCategoryType> Categories = new List<AssignmentCategoryType>();
        //{
        //   ,   ,,
        //};
    }
}
