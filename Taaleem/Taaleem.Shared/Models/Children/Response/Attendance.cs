﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Children.Response
{
    //TODO:Wrap this data in Response that choose literals based on language
    public class Attendance
    {
        public DateTime AttendanceDate { get; set; }
        public int AttendanceLogID { get; set; }
        public int AttendanceStatusCode { get; set; }
        public string AttendanceStatusName1 { get; set; }
        public string AttendanceStatusName2 { get; set; }
        public string AttendanceStatusName3 { get; set; }
        public DateTime EndTime { get; set; }
        public string LearningGroupName1 { get; set; }
        public string LearningGroupName2 { get; set; }
        public string LearningGroupName3 { get; set; }
        public string LearningGroupUrl { get; set; }
        public int LocationId { get; set; }
        public string LocationName1 { get; set; }
        public string LocationName2 { get; set; }
        public string LocationName3 { get; set; }
        public int PeriodNumber { get; set; }
        public DateTime StartTime { get; set; }
        public int VacationID { get; set; }
        public string VacationName1 { get; set; }
        public string VacationName2 { get; set; }
        public string VacationName3 { get; set; }

    }
}
