﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Data.Html;

namespace Taaleem.Models.Children.Response
{
    public class Announcement
    {
        public string Body { get; set; }
        public string FilteredBody
        {
            get { return String.IsNullOrWhiteSpace(Body) ? "" : HtmlUtilities.ConvertToText(Body); }
        }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Title { get; set; }
    }
}
