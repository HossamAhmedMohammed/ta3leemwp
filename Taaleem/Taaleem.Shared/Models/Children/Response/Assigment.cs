﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Children.Response
{
    public class Assignment
    {
        /// <summary>
        /// This value isn't returned from server but assigned locally,
        /// This value contains localized name of category.
        /// </summary>
        public AssignmentCategoryType CategoryType { get; set; }
        public int Category { get; set; }
        public DateTime EndDate { get; set; }
        public int ExamConfigurationId { get; set; }
        public int ExamScheduleId { get; set; }
        public float Grade { get; set; }
        public int? GradingStatus { get; set; }
        public string Instructions { get; set; }
        public string LearningGroupIconUrl { get; set; }
        public int LearningGroupId { get; set; }
        public string LearningGroupName { get; set; }
        public int Mode { get; set; }
        public string Name { get; set; }
        public int? PaperId { get; set; }
        public int? PaperStatus { get; set; }
        public DateTime ReturnDate { get; set; }
        public float? Score { get; set; }
        public DateTime StartDate { get ; set; }
        public string StudentComment { get; set; }
        public int StudentId { get; set; }
        public DateTime? SubmissionTime { get; set; }
        public object TeacherComment { get; set; }
        public int TrialTypeId { get; set; }
        public string Url { get; set; }

      

        public String Start
        {
            get { return String.Format("{0:d/M}", StartDate.Date); }


        }

        public String End
        {
            get { return SubmissionTime == null ?null: String.Format("{0:d/M}", SubmissionTime.Value.Date); }


        }
    }

}
