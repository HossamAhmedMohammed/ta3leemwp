﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.Children.Response;

namespace Taaleem.Models.Children.Database
{
    [Table("Feed")]
    public class FeedData
    {
        [PrimaryKey, AutoIncrement]
        public int LocalId { get; set; }
        public int CategoryId { get; set; }
        public int StudentId { get; set; }
        public DateTime Date { get; set; }
        [Indexed]
        public int ServerId { get; set; }
        public string Text { get; set; }
        public bool IsFavourite { get; set; }

        #region Constructrs
        public FeedData()
        {
        }

        public FeedData(Feed feed)
        {
            CategoryId = feed.CategoryId;
            Date = feed.Date;
            ServerId = feed.Id;
            Text = feed.Text;
            IsFavourite = feed.IsFavourite;
            StudentId = feed.StudentId;
        }
        #endregion
    }
}
