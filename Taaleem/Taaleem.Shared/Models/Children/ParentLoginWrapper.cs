﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.Models.Children.Response;

namespace Taaleem.Models.Children
{
    public class ParentLoginWrapper : BindableBase
    {
        public Parent Parent { get; set; }
        public bool IsValid { get; set; }
        public int ChildrenNo { get; set; }
        public ChildWrapper[] Children { get; set; }

        public ParentLoginWrapper()
        {
        }

        public static async Task<ParentLoginWrapper> WrapAsync(ChildLoginResponse response)
        {
            var loginWrapper = new ParentLoginWrapper()
            {
                Parent = response.Parent,
                IsValid = response.IsValid,
                ChildrenNo = response.ChildrenNo,
            };
            try
            {
                if (response.Children != null)
                {
                    var childWrapperList = new ChildWrapper[response.Children.Length];
                    for (int i = 0; i < response.Children.Length; i++)
                    {
                        var child = response.Children[i];
                        var childWrapper = await ChildWrapper.WrapAsync(child);
                        childWrapperList[i] = childWrapper;
                    }
                    loginWrapper.Children = childWrapperList;
                }
            }
            catch (Exception)
            {
                //ignored
            }
            return loginWrapper;
        }
    }
}
