﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Children.Request
{
    //used for register and un register device
    public class PushNotificationParameters
    {
        //if SupportMultiSchool == true which is always  SupportMultiSchool: true
        //params will be ParentUserName instead of ParentId and value will be parent username
        [QueryParameter("parentId")]
        public int parentId { get; set; }

        [QueryParameter("DeviceId")]
        public string DeviceId { get; set; }

        //1 for android
        //2 for ios
        //No value for windows 

        [QueryParameter("Environment")]
        public string Environment { get; set; }
    }
}
