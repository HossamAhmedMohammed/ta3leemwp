﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Children.Request
{
    public class BehaviorParameters
    {
        [QueryParameter("LanguageId")]
        public int LanguageId { get; set; }

        [QueryParameter("SchoolId")]
        public string SchoolId { get; set; }

        [QueryParameter("StudentId")]
        public int StudentId { get; set; }
    }
}
