﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Children.Request
{
    public class ChildLoginParameters
    {
        [QueryParameter("LanguageId")]
        public int LanguageId { get; set; }
        [QueryParameter("RoleId")]
        public int RoleId { get; set; }
    }
}
