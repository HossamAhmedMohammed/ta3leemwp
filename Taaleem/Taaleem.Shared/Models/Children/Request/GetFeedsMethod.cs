﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Children.Request
{
    public class GetFeedsMethod
    {
        public string MethodName { get; private set; }

        public static readonly GetFeedsMethod Feeds = new GetFeedsMethod { MethodName = "getparentfeeds" };
        public static readonly GetFeedsMethod NewerFeeds = new GetFeedsMethod { MethodName = "getnewerparentfeeds" };
        public static readonly GetFeedsMethod MoreFeeds = new GetFeedsMethod { MethodName = "getmoreparentfeeds" };
    }

}
