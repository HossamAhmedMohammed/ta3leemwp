﻿
using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Children.Request
{
    public class FeedParameters
    {
        [QueryParameter("LanguageType")]
        public int LanguageType { get; set; }

        [QueryParameter("SchoolId")]
        public string SchoolId { get; set; }

        [QueryParameter("ChildId")]
        public int ChildId { get; set; }

        [QueryParameter("FeedCount")]
        public int FeedCount { get; set; }

        [QueryArray("Filter[CategoryIds]", DontEscapeKey = true)]
        public List<int> FilterCategoryIds { get; set; }

        [QueryParameter("FeedId")]
        public int FeedId { get; set; }

    }
}
