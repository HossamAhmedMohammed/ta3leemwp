﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Children.Request
{
    public class GetAttendanceScheduleParameters
    {
        [QueryParameter("LanguageId")]
        public int LanguageId { get; set; }

        [QueryParameter("SchoolId")]
        public string SchoolId { get; set; }

        [QueryParameter("StudentId")]
        public int StudentId { get; set; }

        //TODO:Add format and culture(default Invariant) to Date Time the backend is expecting
        [QueryParameter("StartDate", Format = "M/d/yyyy")]
        public DateTime StartDate { get; set; }

        //TODO:Add format and culture(default Invariant) to Date Time the backend is expecting
        [QueryParameter("EndDate", Format = "M/d/yyyy")]
        public DateTime EndDate { get; set; }

        [QueryParameter("AcademicYearId")]
        public int AcademicYearId { get; set; }

    }
}
