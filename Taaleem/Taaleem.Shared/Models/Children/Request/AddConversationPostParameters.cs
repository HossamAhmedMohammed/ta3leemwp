﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.DataServices.QueryParameters;

namespace Taaleem.Models.Children.Request
{
    public class AddConversationPostParameters
    {
        [QueryParameter("LanguageId")]
        public int LanguageId { get; set; }

        [QueryParameter("SchoolId")]
        public string SchoolId { get; set; }

        [QueryParameter("StudentId")]
        public int StudentId { get; set; }

        [QueryParameter("TeacherId")]
        public int TeacherId { get; set; }

        [QueryParameter("Message")]
        public string Message { get; set; }

        [QueryParameter("PrivateConversationId")]
        public long PrivateConversationId { get; set; }

    }
}
