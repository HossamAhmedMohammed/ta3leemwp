﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.Children.Response;
using Taaleem.ViewModels;

namespace Taaleem.Models.Children
{
    public class AttendanceStatusWrapper
    {
        public int AttendanceStatusId { get; set; }
        public string Description { get; set; }
        public string ExternalId { get; set; }
        public string StatusColor { get; set; }

        public static AttendanceStatusWrapper FromAttendanceStatus(AttendanceStatus status, int languageId)
        {
            AttendanceStatusWrapper wrapper = new AttendanceStatusWrapper
            {
                AttendanceStatusId = status.AttendanceStatusId,
                ExternalId = status.ExternalId,
                StatusColor = status.StatusColor,
            };
            switch (languageId)
            {
                case Lanuage.English:
                    wrapper.Description = status.Description1;
                    break;
                case Lanuage.Arabic:
                    wrapper.Description = status.Description2;
                    if (String.Equals(wrapper.Description, "vacation", StringComparison.OrdinalIgnoreCase))
                    {
                        wrapper.Description = ViewModelLocator.Resources.Vacation;
                    }
                    break;
                case Lanuage.Third:
                    wrapper.Description = status.Description3;
                    break;
                default:
                    break;
            }
            return wrapper;
        }
    }
}
