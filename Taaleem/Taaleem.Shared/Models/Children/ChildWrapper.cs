﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.Helpers;
using Taaleem.Models.Children.Response;
using Windows.Storage.Streams;

namespace Taaleem.Models.Children
{
    public class ChildWrapper : BindableBase
    {
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        /// <summary>
        /// Image path on local storage
        /// </summary>
        public string ImagePath { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Lastname { get; set; }
        public string MiddleName { get; set; }
        public School School { get; set; }
        public string Sid { get; set; }
        public int UserID { get; set; }
        public string UserLogonName { get; set; }

        private bool _isSelected;
        //internal usage
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value); }
        }

        public static async Task<ChildWrapper> WrapAsync(Child child)
        {
            var wrapper = new ChildWrapper()
            {
                DisplayName = child.DisplayName,
                FirstName = child.FirstName,
                Lastname = child.Lastname,
                MiddleName = child.MiddleName,
                School = child.School,
                Sid = child.Sid,
                UserID = child.UserID,
                UserLogonName = child.UserLogonName,
                LastModifiedDate = child.LastModifiedDate,
            };

            var source = child.ImageUrl;

            //Save image on storage
            //Avoid Dummy Data
            if (!source.StartsWith("<"))
            {
                try
                {
                    string path = String.Format(@"ParentService\{0}.png", child.UserID);
                    var file = await FileSystemUtils.CreateLocalFile(path);
                    using (var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.ReadWrite))
                    {
                        var bytes = System.Convert.FromBase64String(source);
                        using (var dataWriter = new DataWriter(stream))
                        {
                            dataWriter.WriteBytes(bytes);
                            dataWriter.StoreAsync();
                        }
                    }
                    wrapper.ImagePath = file.Path;
                }
                catch (Exception)
                {
                    //ignored
                }

            }
            return wrapper;
        }
    }
}
