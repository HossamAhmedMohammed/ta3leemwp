﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.Children.Response;
using Taaleem.ViewModels;

namespace Taaleem.Models.Children
{
    public class AttendanceWrapper
    {
        public DateTime AttendanceDate { get; set; }
        public int AttendanceLogId { get; set; }
        public int AttendanceStatusCode { get; set; }
        public string AttendanceStatusName { get; set; }
        public DateTime EndTime { get; set; }
        public string LearningGroupName { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int PeriodNumber { get; set; }
        public DateTime StartTime { get; set; }
        public int VacationId { get; set; }
        public string VacationName { get; set; }
        public bool IsDummy { get; set; }
        public static AttendanceWrapper FromAttendance(Attendance attendance, int languageId)
        {
            AttendanceWrapper wrapper = new AttendanceWrapper
            {
                AttendanceDate = attendance.AttendanceDate,
                AttendanceLogId = attendance.AttendanceLogID,
                AttendanceStatusCode = attendance.AttendanceStatusCode,
                LocationId = attendance.LocationId,
                VacationId = attendance.VacationID,
                StartTime = attendance.StartTime,
                PeriodNumber = attendance.PeriodNumber,
                EndTime = attendance.EndTime,
                IsDummy = false
            };
            switch (languageId)
            {
                case Lanuage.English:
                    wrapper.AttendanceStatusName = attendance.AttendanceStatusName1;
                    wrapper.LearningGroupName = attendance.LearningGroupName1;
                    wrapper.VacationName = attendance.VacationName1;
                    wrapper.LocationName = attendance.LocationName1;
                    break;
                case Lanuage.Arabic:
                    wrapper.AttendanceStatusName = attendance.AttendanceStatusName2;
                    wrapper.LearningGroupName = attendance.LearningGroupName2;
                    wrapper.VacationName = attendance.VacationName2;
                    wrapper.LocationName = attendance.LocationName2;
                    break;
                case Lanuage.Third:
                    wrapper.AttendanceStatusName = attendance.AttendanceStatusName3;
                    wrapper.LearningGroupName = attendance.LearningGroupName3;
                    wrapper.VacationName = attendance.VacationName3;
                    wrapper.LocationName = attendance.LocationName3;
                    break;
                default:
                    break;
            }
            return wrapper;
        }

        public static AttendanceWrapper GetDummyWrapper(int languageId)
        {
            return new AttendanceWrapper()
            {
                AttendanceStatusName = ViewModelLocator.Resources.NoPeriod,
                LearningGroupName = "-",
                IsDummy = true
            };
        }


    }


    public class AttendanceStatusType
    {
        public const int NoPeriod = 0;
        public const int Present = 1;
        public const int Absence = 2;
        public const int AuthorizedAbsence = 4;
        public const int Unrecorded = 100;
        public const int Vacation = 1000;
    }
}
