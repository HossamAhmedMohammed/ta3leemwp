﻿using System;
using System.Collections.Generic;
using System.Text;
using Taaleem.Models.Children.Response;

namespace Taaleem.Models.Children
{
    public class FeedCategoryWrapper
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static FeedCategoryWrapper Wrap(FeedCategory category, int languageId)
        {
            FeedCategoryWrapper wrapper = new FeedCategoryWrapper
            {
                Id = category.Id,
            };
            switch (languageId)
            {
                case Lanuage.English:
                    wrapper.Name = category.Name1;
                    break;
                case Lanuage.Arabic:
                    wrapper.Name = ConvertToArabic(category.Name2);
                    break;
                case Lanuage.Third:
                    wrapper.Name = category.Name3;
                    break;
                default:
                    break;
            }
            return wrapper;
        }

        private static string ConvertToArabic(string name)
        {
            switch (name)
            {
                case "AttendanceExpert":
                    return "الحضور";
                case "Assignments":
                    return "الواجبات";
                case "SchoolAnnouncements":
                    return "الاعلانات";
                case "Virtual Classrooms":
                    return "الفصول الافتراضيه";
                case "CoursePlanning":
                    return "تخطيط المناهج";
                case "Latest Updates":
                    return "اخر التحديثات";
                case "User Communication":
                    return "المحادثات";
                case "Behavior":
                    return "السلوك";
                case "Exams":
                    return "الاختبارات";
                case "ILP":
                    return "خطط التعلم الفردية";
                default:
                    return name;
            }

        }
    }
}
