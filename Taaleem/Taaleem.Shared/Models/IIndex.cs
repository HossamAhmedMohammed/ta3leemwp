﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models
{
    /// <summary>
    /// This will be used primarily to color rows as different colors, even and odd.
    /// </summary>
    public interface IIndex
    {
        int IndexInList { get; set; }
    }
}
