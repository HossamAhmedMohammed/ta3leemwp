﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Login.Request
{
    public class CreateUserParameters
    {

     
            public string QatarId { get; set; }
            public string Email { get; set; }
            public string Mobile { get; set; }
            public string Password { get; set; }
            public string OU { get; set; }
            public string GroupName { get; set; }
        

    }
}
