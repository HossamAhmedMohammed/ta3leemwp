﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models.Login.Response
{
    public class ResendActivateUserResponse
    {

      
            public bool IsValid { get; set; }
            public string MessageAr { get; set; }
            public string MessageEn { get; set; }
        

    }
}
