﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Taaleem.Models
{
    public class Attachment
    {
        public string DisplayName { get; set; }
        public string FileName { get; set; }
        public string Data { get; set; }
    }
}
