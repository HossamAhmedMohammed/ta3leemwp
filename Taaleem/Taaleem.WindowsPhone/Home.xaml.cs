﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.ViewModels.Children;
using Taaleem.ViewModels.PublicServices;
using Taaleem.Views.Children;
using Taaleem.Views.Employee;
using Taaleem.Views.PublicServices;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Home : Page
    {
        DispatcherTimer timer;
        public Home()
        {
            this.InitializeComponent();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0,0,15);
            timer.Tick += Timer_Tick;
        }

        private void Timer_Tick(object sender, object e)
        {
            if ((NewsSlider.DataContext as MaglesNewsViewModel).NewsList != null)
            {
                if (NewsSlider.Items.Count > 0)
                {
                    if (NewsSlider.SelectedIndex < NewsSlider.Items.Count - 1)
                        NewsSlider.SelectedIndex = NewsSlider.SelectedIndex + 1;
                    else
                        NewsSlider.SelectedIndex = 0;
                }
                else
                {
                    this.Timer_Tick(this, null);
                }
            }
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
           

            timer.Start();
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = new Models.MainMenuItem { Name = "الرئيسيه", Image = "" };
          
        }

        private  void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
           // await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                Frame.Navigate(typeof(Splash));
        }

        protected override  void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            timer.Stop();
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        private void FlipView_Loaded(object sender, RoutedEventArgs e)
        {
            FlipView tempfv = sender as FlipView;
            MaglesNewsViewModel tempVm = (MaglesNewsViewModel)tempfv.DataContext;
            tempVm.LoadCommand.Execute(null);
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            Button thisBtn = sender as Button;
            switch (thisBtn.Content.ToString())
            {
                //case "الرئيسيه":
                //    {
                //        _navigationService.NavigateByPage<Home>();
                //        if (_appSettings.LanguageId == 2)
                //        {
                //            items.Where(i => i.Name == "الرئيسيه").Single().Image = "ms-appx:///Assets/Menu/parent_home_selected_ic@3x.png";
                //        }
                //        else
                //        {
                //            items.Where(i => i.Name == "الرئيسيه").Single().Image = "ms-appx:///Assets/Menu/parent_home_selected_ic_en@3x.png";
                //        }
                //        currentMenuItem.Name = "الرئيسيه";
                //    }
                //    break;
                case "خدمات الاباء":

                    {
                        ViewModels.ViewModelLocator.Locator.AppSettings.IsParentMode = true;
                        ViewModels.ViewModelLocator.Locator.AppSettings.AmIParent = true;
                        if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_parents_btn_presed@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush; // =  "ms-appx:///Assets/Menu/parent_services_selected_ic@2x.png ";
                        }
                        else
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_parents_btn_presed_en@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }
                        if (await ViewModels.ViewModelLocator.Locator.AppSettings.GetParentLoginResponseAsync() == null)
                            await ViewModels.ViewModelLocator.Locator.DialogManager.ShowParentLoginDialog();
                        else
                        {
                           

                            //Frame.Navigate(typeof(ParentMenuPage));
                            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem= new Models.MainMenuItem { Name = "خدمات الاباء",Image=""};
                            }
                        break;
                    }
                case "خدمات الموظفين":
                    {
                        

                        if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_employee_btn_presed@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }
                        else
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_employee_btn_pressed_en@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }
                        if (await ViewModels.ViewModelLocator.Locator.AppSettings.GetEmployeeLoginResponseAsync() == null)
                            await ViewModels.ViewModelLocator.Locator.DialogManager.ShowEmployeeLoginDialog();
                        else
                        {
                           // Frame.Navigate(typeof(EmployeeMenuPage));
                            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = new Models.MainMenuItem { Name = "خدمات الموظف", Image = "" };
                        }
                      
                        break;
                    }
                case "الطلاب":
                    {
                        ViewModels.ViewModelLocator.Locator.AppSettings.IsStudentMode = true;
                        ViewModels.ViewModelLocator.Locator.AppSettings.AmIParent = false;
                        if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                        {
                            BitmapImage bmp1 = new BitmapImage();
                            bmp1.UriSource = new Uri("ms-appx:///Assets/Home/home_students_btn_presed@3x.png", UriKind.Absolute);

                            var imageBrush1 = new ImageBrush
                            {
                                ImageSource = bmp1
                            };

                            thisBtn.Background = imageBrush1;
                        }
                        else
                        {
                            BitmapImage bmp1 = new BitmapImage();
                            bmp1.UriSource = new Uri("ms-appx:///Assets/Home/home_students_btn_presed_en@3x.png", UriKind.Absolute);

                            var imageBrush1 = new ImageBrush
                            {
                                ImageSource = bmp1
                            };

                            thisBtn.Background = imageBrush1;
                        }
                        if (await ViewModels.ViewModelLocator.Locator.AppSettings.GetStudentLoginResponseAsync() == null)
                            await ViewModels.ViewModelLocator.Locator.DialogManager.ShowStudentLoginDialog();
                        else
                        {


                         //   Frame.Navigate(typeof(ParentMenuPage));
                            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = new Models.MainMenuItem { Name = "خدمات الطلاب", Image = "" };
                        }
                        break;
                    }
                case "خدمات عامه":
                    {
                        if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_general_btn_pressed@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }
                        else
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_general_btn_pressed_en@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }

                      //  Frame.Navigate(typeof(PublicServicesMenuPage));
                        ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = new Models.MainMenuItem { Name = "خدمات عامه", Image = "" };


                        break;
                      
                    }
            
                case "عن المجلس":
                    {
                       // Frame.Navigate(typeof(About));
                        ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = new Models.MainMenuItem { Name = "عن المجلس", Image = "" };
                        //if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                        //{
                        //   // items.Where(i => i.Name == "عن المجلس").Single().Image = "ms-appx:///Assets/Menu/parent_about_selected_ic-@3x.png";
                        //}
                        //else
                        //{ 
                        //    //items.Where(i => i.Name == "عن المجلس").Single().Image = "ms-appx:///Assets/Menu/parent_about_selected_ic-_en@3x.png";
                        //}

                        break;
                    }
            }
        }

        private void Button_Loaded(object sender, RoutedEventArgs e)
        {
           Button thisBtn = sender as Button;
            switch (thisBtn.Content.ToString())
            {
               
                case "خدمات الاباء":

                    {
                        if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_parents_btn@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush; // =  "ms-appx:///Assets/Menu/parent_services_selected_ic@2x.png ";
                        }
                        else
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_parents_btn_en@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }
                        break;
                    }
                case "خدمات الموظفين":
                    {
                        

                        if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_employee_btn@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }
                        else
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_employee_btn_en@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }
                       
                        break;
                    }
                case "الطلاب":
                    if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                    {
                        BitmapImage bmp1 = new BitmapImage();
                        bmp1.UriSource = new Uri("ms-appx:///Assets/Home/home_students_btn@3x.png", UriKind.Absolute);

                        var imageBrush1 = new ImageBrush
                        {
                            ImageSource = bmp1
                        };

                        thisBtn.Background = imageBrush1;
                    }
                    else
                    {
                        BitmapImage bmp1 = new BitmapImage();
                        bmp1.UriSource = new Uri("ms-appx:///Assets/Home/home_students_btn_en@3x.png", UriKind.Absolute);

                        var imageBrush1 = new ImageBrush
                        {
                            ImageSource = bmp1
                        };

                        thisBtn.Background = imageBrush1;
                    }
                    break;
                case "خدمات عامه":
                    {
                        if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_general_btn@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }
                        else
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_general_btn_en@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }

                            break;
                      
                    }
                case "عن المجلس":
                    {
                        if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_about_btn@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }
                        else
                        {
                            BitmapImage bmp = new BitmapImage();
                            bmp.UriSource = new Uri("ms-appx:///Assets/Home/home_about_btn_en@3x.png", UriKind.Absolute);

                            var imageBrush = new ImageBrush
                            {
                                ImageSource = bmp
                            };

                            thisBtn.Background = imageBrush;
                        }

                        break;

                    }


            }
        }


        private void mImage_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }


        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
    }
}
