﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Student
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class StudentLogin : UserControl
    {
        public StudentLogin()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
          
            ViewModels.ViewModelLocator.Locator.ParentLoginVM.ErrorMessage = null; // disable showing the message after close 
            ViewModels.ViewModelLocator.Locator.DialogManager.CloseDialog();
            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = ViewModels.ViewModelLocator.Locator.MainMenuVM.CurrentMenuItem;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            usertxt.Text = "";
            passtxt.Password = "";
        }
    }
}
