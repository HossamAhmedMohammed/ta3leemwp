﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Taaleem.Models.Exceptions.Response;
using Taaleem.ViewModels.Exceptions;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Exceptions
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Stage4StudentData : Page
    {
        StudentViewModel vm;
        CoreApplicationView view1;
        public Stage4StudentData()
        {
            this.InitializeComponent();
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            vm = (StudentViewModel)DataContext;
            view1 =  CoreApplication.GetCurrentView();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.Navigate(typeof(MyRequests));
        }
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = ViewModels.ViewModelLocator.Locator.MainMenuVM.Items.Where(x => x.Name == "خدمات عامه").SingleOrDefault();
            ViewModels.ViewModelLocator.Locator.StagesSelectionVM.LoadStagesAsync();
            StagesControl.SelectedStage = 3;
            vm.Request = e.Parameter as MyExceptionRequestResponse;
            if (vm.Request.RequestNextStep == 6) // Student
            { }

            else if (vm.Request.RequestNextStep == 7) // Twins
            {
                if (Frame.BackStack.Last().SourcePageType == typeof(Stage3Parents))
                {
                    await vm.GetStudentTwinsByRequestID();
                    if (vm.Student.Id != 0)
                    {
                        vm.HasTwins = true;
                        HasTwinsCheckBox.IsChecked = true;
                    }
                    else
                    {
                        HasTwinsCheckBox.IsChecked = false;
                    }

                        await vm.GetStudentByRequestID();
                   
                    HasTwinsCheckBox.Visibility = Visibility.Visible;
                  
                }
                else {
                    vm.Student = new Models.Exceptions.Request.Student();
                    vm.Student.IsTwins = true;
                    HasTwinsCheckBox.Visibility = Visibility.Collapsed;
                }



            }
            else if (vm.Request.RequestNextStep == 8) // children
            {

                if (Frame.BackStack.Last().SourcePageType == typeof(Stage4StudentData))
                {
                    await vm.GetStudentTwinsByRequestID();
                    HasTwinsCheckBox.Visibility = Visibility.Collapsed;
                }
                else if(Frame.BackStack.Last().SourcePageType == typeof(Stage5Children))
                {
                    await vm.GetStudentTwinsByRequestID();
                    HasTwinsCheckBox.Visibility = Visibility.Collapsed;
                }
                else {
                    await vm.GetStudentTwinsByRequestID();
                    if (vm.Student.Id != 0)
                    {
                        vm.HasTwins = true;
                        HasTwinsCheckBox.IsChecked = true;
                    }
                    else
                    {
                        HasTwinsCheckBox.IsChecked = false;
                    }
                    await vm.GetStudentByRequestID();
                    HasTwinsCheckBox.Visibility = Visibility.Visible;

                }
            }
            if(vm.Student.Id != 0)
            {
                if(vm.Student.SchoolType == 1)
                {
                    privateSchool.IsChecked = true;
                }
                else if (vm.Student.SchoolType == 2)
                {
                    outCountrySchool.IsChecked = true;
                }
                else if (vm.Student.SchoolType == 3)
                {
                    newStudent.IsChecked = true;
                }

                //gender
                if(vm.Student.Gender == 1)
                {
                    male.IsChecked = true;
                }
                else if (vm.Student.Gender == 2)
                {
                    female.IsChecked = true;
                }
              
                    nationalities.SelectedItem = vm.Nationalities.Where(x => x.Id == vm.Student.NationalityId).SingleOrDefault();
                    schools.SelectedItem = vm.PrivateSchools.Where(c => c.Id == vm.Student.PrivateSchoolCode).SingleOrDefault();
                if(vm.LastSchoolCer.FileName != null)
                {
                    LastSchoolCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    LastSchoolCerImagetxt.Text = vm.LastSchoolCer.FileName;
                }
                if (vm.StudentBirthCer.FileName != null)
                {
                    BirthCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    BirthCerImagetxt.Text = vm.StudentBirthCer.FileName;
                }
                if (vm.QIDAttach.FileName != null)
                {
                    QIDImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    QIDImagetxt.Text = vm.QIDAttach.FileName;
                }               
            }
        }
        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (HasTwinsCheckBox.Visibility == Visibility.Visible)
            {
                Frame.Navigate(typeof(Stage3Parents), vm.Request);
            }
            else
            {
                HasTwinsCheckBox.Visibility = Visibility.Visible;
                vm.HasTwins = true;
                HasTwinsCheckBox.IsChecked = true;
               
                await vm.GetStudentByRequestID();
                if (vm.Student.Id != 0)
                {
                    if (vm.Student.SchoolType == 1)
                    {
                        privateSchool.IsChecked = true;
                    }
                    else if (vm.Student.SchoolType == 2)
                    {
                        outCountrySchool.IsChecked = true;
                    }
                    else if (vm.Student.SchoolType == 3)
                    {
                        newStudent.IsChecked = true;
                    }

                    //gender
                    if (vm.Student.Gender == 1)
                    {
                        male.IsChecked = true;
                    }
                    else if (vm.Student.Gender == 2)
                    {
                        female.IsChecked = true;
                    }

                    nationalities.SelectedItem = vm.Nationalities.Where(x => x.Id == vm.Student.NationalityId).SingleOrDefault();
                    schools.SelectedItem = vm.PrivateSchools.Where(c => c.Id == vm.Student.PrivateSchoolCode).SingleOrDefault();
                    if (vm.LastSchoolCer.FileName != null)
                    {
                        LastSchoolCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        LastSchoolCerImagetxt.Text = vm.LastSchoolCer.FileName;
                    }
                    if (vm.StudentBirthCer.FileName != null)
                    {
                        BirthCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        BirthCerImagetxt.Text = vm.StudentBirthCer.FileName;
                    }
                    if (vm.QIDAttach.FileName != null)
                    {
                        QIDImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        QIDImagetxt.Text = vm.QIDAttach.FileName;
                    }
                }
            }
        }

        private void nextbutton_Click(object sender, RoutedEventArgs e)
        {
            Grid1.Visibility = Visibility.Collapsed;
            Grid2.Visibility = Visibility.Visible;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Grid1.Visibility = Visibility.Visible;
            Grid2.Visibility = Visibility.Collapsed;
        }

        private async void nextbutton1_Click(object sender, RoutedEventArgs e)
        {
            bool x;
            if (vm.Student.Id == 0)
             x=   await vm.AddStudentAcync();
            else
             x=  await  vm.UpdateStudentAsync();
            if (x == true)
            {
                if (vm.HasTwins == true)
                {
                    StagesControl.SelectedStage = 3;
                    Frame.Navigate(typeof(Stage4StudentData), vm.Request);
                   
                }
                else
                {
                    StagesControl.SelectedStage = 4;
                    Frame.Navigate(typeof(Stage5Children), vm.Request);
                }
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton gender = sender as RadioButton;
            if(gender.Content.ToString() == ViewModels.ViewModelLocator.Resources.Male)
            {
                vm.Student.Gender = 1;
            }
            else if (gender.Content.ToString() == ViewModels.ViewModelLocator.Resources.Female)
            {
                vm.Student.Gender = 2;
            }
        }

        private void privateSchool_Checked(object sender, RoutedEventArgs e)
        {
           

           
            RadioButton schoolType = sender as RadioButton;
            if (schoolType.Content != null)
            {
                detailsgrid.Visibility = Visibility.Visible;
                if (schoolType.Content.ToString() == ViewModels.ViewModelLocator.Resources.PrivateSchool)
                {
                    vm.Student.SchoolType = 1;
                    schools.ItemsSource = vm.PrivateSchools;
                }
                else if (schoolType.Content.ToString() == ViewModels.ViewModelLocator.Resources.Abroad)
                {
                    vm.Student.SchoolType = 2;
                   // schools.ItemsSource = vm.IndependentSchools;
                }
                else if (schoolType.Content.ToString() == ViewModels.ViewModelLocator.Resources.NewStudent)
                {
                    vm.Student.SchoolType = 5;
                }


                if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
                {
                    schools.DisplayMemberPath = "NameEn";
                }
                else if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
                {
                    schools.DisplayMemberPath = "NameAr";
                }
            }
        }

        private void ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            ComboBox schools = sender as ComboBox;
            if(vm.Student.SchoolType == 1)
            {
                schools.ItemsSource = vm.PrivateSchools;
              
            }
            else if (vm.Student.SchoolType == 2 )
            {
                schools.ItemsSource = vm.IndependentSchools;
                
            }

            if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 1)
            {
                schools.DisplayMemberPath = "NameEn";
            }
            else if (ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId == 2)
            {
                schools.DisplayMemberPath = "NameAr";
            }
        }

        string currentUpload = "";
        bool setFlag = false;
        private void Image_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            Image image = sender as Image;
            if (setFlag == false)
            {
                FileOpenPicker filePicker = new FileOpenPicker();
                filePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                filePicker.ViewMode = PickerViewMode.Thumbnail;

                //// Filter to include a sample subset of file types
                filePicker.FileTypeFilter.Clear();
                filePicker.FileTypeFilter.Add(".pdf");
                filePicker.FileTypeFilter.Add(".png");
                filePicker.FileTypeFilter.Add(".jpeg");
                filePicker.FileTypeFilter.Add(".jpg");

                filePicker.PickSingleFileAndContinue();

                currentUpload = image.Name;
                this.view1.Activated += this.viewActivated;
            }
            else
            {
                image.Source = new BitmapImage(new Uri(
   "ms-appx:///Assets/Exceptions/ic_ab_upload.png", UriKind.Absolute));
                string text = image.Name + "txt";
                var x = layoutroot.FindName(text);
                ((TextBox)x).Text = "";
                setFlag = false;
            }
        }



        private async void viewActivated(CoreApplicationView sender, IActivatedEventArgs args1)
        {
            FileOpenPickerContinuationEventArgs args = args1 as FileOpenPickerContinuationEventArgs;
            if (args != null)
            {
                if (args.Files.Count == 0) return;
                view1.Activated -= viewActivated;
                StorageFile storageFile = args.Files[0];
                byte[] array = await GetBytesAsync(storageFile);
                string str = Convert.ToBase64String(array);
                if (currentUpload == "QIDImage")
                {
                    vm.QIDAttach.FileContent = str;
                    vm.QIDAttach.FileName = args.Files[0].DisplayName + args.Files[0].DisplayType;
                    QIDImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    QIDImagetxt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "BirthCerImage")
                {
                    vm.StudentBirthCer.FileContent = str;
                    vm.StudentBirthCer.FileName = args.Files[0].DisplayName + args.Files[0].DisplayType;
                    BirthCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    BirthCerImagetxt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "LastSchoolCerImage")
                {
                    vm.LastSchoolCer.FileContent = str;
                    vm.LastSchoolCer.FileName = args.Files[0].DisplayName + args.Files[0].DisplayType;
                    LastSchoolCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    LastSchoolCerImagetxt.Text = args.Files[0].Name;
                }

                setFlag = true;
                currentUpload = "";
            }
        }

        public static async Task<byte[]> GetBytesAsync(StorageFile file)
        {
            byte[] fileBytes = null;
            if (file == null) return null;
            using (var stream = await file.OpenReadAsync())
            {
                fileBytes = new byte[stream.Size];
                using (var reader = new DataReader(stream))
                {
                    await reader.LoadAsync((uint)stream.Size);
                    reader.ReadBytes(fileBytes);
                }
            }
            return fileBytes;
        }

       

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox nationalities = sender as ComboBox;
            if (nationalities.SelectedItem != null)
            {
               
                vm.Student.NationalityId = (nationalities.SelectedItem as Nationality).Id;
            }
        }

        private void schools_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox schoolss = sender as ComboBox;
            if (schoolss.SelectedItem != null)
            {

                vm.Student.PrivateSchoolCode = (schoolss.SelectedItem as School).Id.ToString();
            }
            if(vm.Student.Id!= 0)
            {
                schoolss.SelectedItem = vm.PrivateSchools.Where(x => x.Id == vm.Student.PrivateSchoolCode).SingleOrDefault();
            }
        }
    }
}
