﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Models.Exceptions;
using Taaleem.ViewModels.Exceptions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Taaleem.Views.Exceptions
{
    public sealed partial class StagesUserControl : UserControl
    {
        public StagesUserControl()
        {
            this.InitializeComponent();
        }

        private void Stage_Tapped(object sender, TappedRoutedEventArgs e)
        {

        }

        public int SelectedStage
        {
            get { return (int)GetValue(SelectedStageProperty); }
            set
            {
                SetValueDp(SelectedStageProperty, value);
                Stage selectedStage = ((StagesSelectionViewModel)DataContext).Stages.Where(x => x.IsSelected == true).SingleOrDefault();
               selectedStage .IsSelected = false;//[value - 1].IsSelected = false;
               selectedStage.ImagePath = "ms-appx:///Assets/Exceptions/Stages/" + (selectedStage.StageID).ToString() + ".png";//[value - 1].ImagePath = 
                ((StagesSelectionViewModel)DataContext).Stages[value].IsSelected = true;
                ((StagesSelectionViewModel)DataContext).Stages[value].ImagePath = "ms-appx:///Assets/Exceptions/Stages/" + value.ToString() + "_active.png";
            }
        }

        public static readonly DependencyProperty SelectedStageProperty = DependencyProperty.Register("SelectedStage", typeof(int), typeof(StagesUserControl), null);

        //reuse
        public event PropertyChangedEventHandler PropertyChanged;

        void SetValueDp(DependencyProperty property, object value, [System.Runtime.CompilerServices.CallerMemberName] String p = null)
        {
            SetValue(property, value);
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(p));
        }

        
    }
}
