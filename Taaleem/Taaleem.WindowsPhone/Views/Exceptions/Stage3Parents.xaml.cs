﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Taaleem.Models.Exceptions.Response;
using Taaleem.ViewModels.Exceptions;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Exceptions
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Stage3Parents : Page
    {
        CoreApplicationView view1;
        ParentViewModel vm;
        public Stage3Parents()
        {
            this.InitializeComponent();
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            vm = (ParentViewModel)DataContext;
            view1 = CoreApplication.GetCurrentView();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.Navigate(typeof(MyRequests));
        }
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = ViewModels.ViewModelLocator.Locator.MainMenuVM.Items.Where(x => x.Name == "خدمات عامه").SingleOrDefault();
            ViewModels.ViewModelLocator.Locator.StagesSelectionVM.LoadStagesAsync();

            StagesControl.SelectedStage = 2;
            if (e.Parameter != null)
            {
                vm.Request = e.Parameter as MyExceptionRequestResponse;
                if (vm.Request.RequestNextStep == 3) // father
                {
                 //   vm.Father = new Models.Exceptions.Request.Parent();
                }

                else if (vm.Request.RequestNextStep == 4) // mother
                {
                    await vm.GetFatherByRequestID();
                }
                else if (vm.Request.RequestNextStep == 5) // breadwinner
                {
                    await vm.GetFatherByRequestID();
                    await vm.GetMotherByRequestID();
                    if (vm.Mother.Id != 0)
                    {
                        FatherData1.Visibility = Visibility.Collapsed;
                        MotherGrid1.Visibility = Visibility.Visible;
                    }
                }
                else if (vm.Request.RequestNextStep >= 6) // Exceptional student
                {
                    await vm.GetFatherByRequestID();
                    await vm.GetMotherByRequestID();
                    await vm.getBreadwinnnerByRequestID();
                    if(vm.BreadWinner.Id != 0)
                    {
                        FatherData1.Visibility = Visibility.Collapsed;
                        aelGrid1.Visibility = Visibility.Visible;
                    }
                    else if(vm.Mother.Id != 0)
                    {
                        FatherData1.Visibility = Visibility.Collapsed;
                        MotherGrid1.Visibility = Visibility.Visible;
                    }
                }

                if (vm.Father.Id != 0)
                {
                    if(vm.Father.NationalityId == 166)
                    {
                        if(vm.Father.Passport!= null)
                        {
                            radioButton.IsChecked = true;
                        }
                        else
                        {
                            QatarIDtype.IsChecked = true;
                        }
                    }
                   
                    // Status
                    if (vm.Father.ParentStatus == 1)
                    {
                        inCountry.IsChecked = true;
                    }
                    else if (vm.Father.ParentStatus == 2)
                    {
                        outCountry.IsChecked = true;
                    }
                    else if (vm.Father.ParentStatus == 3)
                    {
                        Dead.IsChecked = true;
                    }
                    //work
                    if (vm.Father.IsEmployed == true)
                    {
                        Work.IsChecked = true;
                    }
                    else if (vm.Father.IsEmployed == false)
                    {
                        noWork.IsChecked = true;
                    }
                    // nationality
                    FatherNationality.SelectedItem = vm.Nationalities.Where(x => x.Id == vm.Father.NationalityId).SingleOrDefault();
                    if(vm.FatherBankCertificate.FileName != null)
                    {
                        BankCerFather.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        BankCerFathertxt.Text = vm.FatherBankCertificate.FileName;
                    }
                    if (vm.FatherDeathCertificate.FileName != null)
                    {
                        DeathCerFather.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        DeathCerFathertxt.Text = vm.FatherDeathCertificate.FileName;
                    }
                    if (vm.FatherQIDAttachment.FileName != null)
                    {
                        IDCFather.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        IDCFathertxt.Text = vm.FatherQIDAttachment.FileName;
                        IDCFather2.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        IDCFather2txt.Text = vm.FatherQIDAttachment.FileName;
                    }
                    if (vm.FatherSalaryCer.FileName != null)
                    {
                        SalaryCerFather.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        SalaryCerFathertxt.Text = vm.FatherSalaryCer.FileName;
                    }

                }

                if (vm.Mother.Id != 0)
                {
                    // Status
                    if (vm.Mother.ParentStatus == 1)
                    {
                        inCountrymother.IsChecked = true;
                    }
                    else if (vm.Mother.ParentStatus == 2)
                    {
                        outCountrymother.IsChecked = true;
                    }
                    else if (vm.Mother.ParentStatus == 3)
                    {
                        Deadmother.IsChecked = true;
                    }
                    //work
                    if (vm.Mother.IsEmployed == true)
                    {
                        Workmother.IsChecked = true;
                    }
                    else if (vm.Mother.IsEmployed == false)
                    {
                        noWorkmother.IsChecked = true;
                    }
                    // nationality
                    var t =vm.Nationalities.Where(x => x.Id == vm.Mother.NationalityId).SingleOrDefault();
                    MotherNationality.ItemsSource = vm.Nationalities;
                    MotherNationality.SelectedItem = t;
                    // Divorsed
                    if (vm.Mother.IsDivorced == true)
                    {
                        yes.IsChecked = true;
                        no.IsChecked = false;
                    }
                    else if (vm.Mother.IsDivorced == false)
                    {
                        no.IsChecked = true;
                        yes.IsChecked = false;
                    }
                    // incubator
                    if (vm.Mother.IsIncubatorMother == true)
                    {
                        yes1.IsChecked = true;
                    }
                    else if (vm.Mother.IsIncubatorMother == false)
                    {
                        no1.IsChecked = true;
                    }
                    if (vm.MotherBankCertificate.FileName != null)
                    {
                        BankCerMotherImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        BankCerMotherImagetxt.Text = vm.MotherBankCertificate.FileName;
                    }
                    if (vm.MotherDeathCertificate.FileName != null)
                    {
                        DeadCerMother.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        DeadCerMothertxt.Text = vm.MotherDeathCertificate.FileName;
                    }
                    if (vm.MotherQIDAttachment.FileName != null)
                    {
                        QIDMImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        QIDMImagetxt.Text = vm.MotherQIDAttachment.FileName;
                        QIDMImage2.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        QIDMImage2txt.Text = vm.MotherQIDAttachment.FileName;
                    }
                    if (vm.motherSalaryCer.FileName != null)
                    {
                        SalaryCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        SalaryCerImagetxt.Text = vm.motherSalaryCer.FileName;
                    }
                }

                if(vm.BreadWinner.Id != 0)
                {
                    if(vm.BreadWinner.Relationship == 1)
                    {
                        comboBoxItem.IsSelected = true;
                        rel.SelectedIndex = 0;
                    }
                    else if(vm.BreadWinner.Relationship == 2 )
                    {
                        comboBoxItem1.IsSelected = true;
                        rel.SelectedIndex = 1;
                    }
                    else if (vm.BreadWinner.Relationship == 3)
                    {
                        comboBoxItem2.IsSelected = true;
                        rel.SelectedIndex = 2;
                    }
                    else if (vm.BreadWinner.Relationship ==4)
                    {
                        comboBoxItem3.IsSelected = true;
                        rel.SelectedIndex = 3;
                    }
                    else if (vm.BreadWinner.Relationship == 5)
                    {
                        comboBoxItem4.IsSelected = true;
                        rel.SelectedIndex = 4;
                    }
                    //ComboBox_SelectionChanged_2(rel, null);
                    var xt = vm.Nationalities.Where(x => x.Id == vm.BreadWinner.NationalityId).Single();
                    BreadwinnerNationality.SelectedItem = xt;
                    BreadwinnerNationality1.SelectedItem = xt;
                    if(vm.BreadWinnerCer.FileName != null)
                    {
                        BWCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        BWCerImagetxt.Text = vm.BreadWinnerCer.FileName;
                        BWCerIImagge.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        BWCerIImaggetxt.Text = vm.BreadWinnerCer.FileName;
                        BWCERImageeee.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        BWCERImageeeetxt.Text = vm.BreadWinnerCer.FileName;
                    }
                }
            }
        }
        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StagesControl.SelectedStage = 2;
            Frame.Navigate(typeof(Stage3Parents), vm.Request);
        }

        private void scrollViewer_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            var verticalOffsetValue = scrollViewer.VerticalOffset;
            var maxVerticalOffsetValue = scrollViewer.ExtentHeight - scrollViewer.ViewportHeight;

            if (maxVerticalOffsetValue < 0 || verticalOffsetValue == maxVerticalOffsetValue || outCountry.IsChecked == true)
            {
                // Scrolled to bottom
                nextbutton.Visibility = Visibility.Visible;
                next.Visibility = Visibility.Collapsed;
            }
            else
            {
                // Scrolled to top
                next.Visibility = Visibility.Visible;
                nextbutton.Visibility = Visibility.Collapsed;
            }
        }

        private async void nextbutton_Click(object sender, RoutedEventArgs e)
        {
           
                if (inCountry.IsChecked == true)
                {
                if (string.IsNullOrEmpty(vm.Father.QatarId) && string.IsNullOrEmpty(vm.Father.Passport))
                {
                    await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.InsertAllDataMessage);
                }
                else
                {
                    if (Telephonef.Text.StartsWith("55"))
                    {
                        // vm.FatherDeathCertificate = new Models.Exceptions.Request.Deathcertificate();
                        FatherData1.Visibility = Visibility.Collapsed;
                        FatherGrid2.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.MobilePhoneShouldStartWith55);

                    }
                   
                }

            }
                else if (outCountry.IsChecked == true)
                {
                //vm.Father.Employer = null;
                //vm.Father.IsEmployed = false;
                //vm.Father.Job = null;
                //vm.Father.Mobile = null;
                //vm.Father.Name = null;
                //vm.Father.NationalityId = 0;
                //vm.Father.QatarId = null;
                //vm.Father.Salary = null;
                
                //vm.FatherBankCertificate = new Models.Exceptions.Request.Bankcertificate();
                //vm.FatherSalaryCer = new Models.Exceptions.Request.Salarycertificate();
                //vm.FatherQIDAttachment = new Models.Exceptions.Request.Qataridattachment();
                //vm.FatherDeathCertificate = new Models.Exceptions.Request.Deathcertificate();
                bool x;
                if (vm.Father.Id == 0)
                    x = await vm.AddFather();
                else
                    x = await vm.UpdateFather();
                if (x == true)
                {

                    FatherData1.Visibility = Visibility.Collapsed;
                    MotherGrid1.Visibility = Visibility.Visible;
                }
                }
                else if (Dead.IsChecked == true)
                {
                //vm.Father.Employer = null;
                vm.Father.IsEmployed = false;
                //vm.Father.Job = null;
                //vm.Father.Mobile = null;
                //vm.Father.Name = null;
                vm.Father.NationalityId = null;
                //vm.Father.QatarId = null;
                //vm.Father.Salary = null;

                //vm.FatherBankCertificate = new Models.Exceptions.Request.Bankcertificate();
                //vm.FatherSalaryCer = new Models.Exceptions.Request.Salarycertificate();
                //vm.FatherQIDAttachment = new Models.Exceptions.Request.Qataridattachment();
                    bool x;
                    if (vm.Father.Id == 0)
                        x = await vm.AddFather();
                    else
                        x = await vm.UpdateFather();
                    if (x == true)
                    {

                        FatherData1.Visibility = Visibility.Collapsed;
                        MotherGrid1.Visibility = Visibility.Visible;
                    }
                }
          
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (Work.IsChecked == true)
            {
                FatherGrid2.Visibility = Visibility.Collapsed;
                fatherGrid3.Visibility = Visibility.Visible;
            }
            else if (noWork.IsChecked == true)
            {
                //vm.Father.IsEmployed = false;
                //vm.Father.Employer = "";
                //vm.Father.Job = "";
                //vm.Father.Salary = null;
                //vm.FatherBankCertificate = new Models.Exceptions.Request.Bankcertificate();
                //vm.FatherDeathCertificate = new Models.Exceptions.Request.Deathcertificate();
                //vm.FatherSalaryCer = new Models.Exceptions.Request.Salarycertificate();
                bool x;
                if (vm.Father.Id == 0)
                    x = await vm.AddFather();
                else
                    x = await vm.UpdateFather();
                if (x == true)
                {
                    FatherGrid2.Visibility = Visibility.Collapsed;
                    MotherGrid1.Visibility = Visibility.Visible;
                }
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            StagesControl.SelectedStage = 1;
            Frame.Navigate(typeof(Stage2RequestOwner), vm.Request);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            FatherGrid2.Visibility = Visibility.Collapsed;
            FatherData1.Visibility = Visibility.Visible;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            FatherGrid2.Visibility = Visibility.Visible;
            fatherGrid3.Visibility = Visibility.Collapsed;
        }

        private async void Button_Click_5(object sender, RoutedEventArgs e)
        {
            bool x;
            if (vm.Father.Id == 0)
                x = await vm.AddFather();
            else
                x = await vm.UpdateFather();
            if (x == true)
            {
                fatherGrid3.Visibility = Visibility.Collapsed;
                MotherGrid1.Visibility = Visibility.Visible;
            }
        }

        private async void Button_Click_6(object sender, RoutedEventArgs e)
        {
            MotherGrid1.Visibility = Visibility.Collapsed;
          await  vm.GetFatherByRequestID();
            if (Dead.IsChecked == true  || outCountry.IsChecked == true)//|| outCountrymother.IsChecked == true
            {
                FatherData1.Visibility = Visibility.Visible;
            }
            else if (Work.IsChecked == true)
            {
                fatherGrid3.Visibility = Visibility.Visible;
            }
            else if (noWork.IsChecked == true)
            {
                FatherGrid2.Visibility = Visibility.Visible;
            }
          
        }

        private async void nextbutton1_Click(object sender, RoutedEventArgs e)
        {
          
                if (Deadmother.IsChecked == true)
                {
                vm.MotherBankCertificate = new Models.Exceptions.Request.Bankcertificate();
                vm.motherSalaryCer = new Models.Exceptions.Request.Salarycertificate();
                vm.MotherQIDAttachment = new Models.Exceptions.Request.Qataridattachment();
                bool x;
                    if (vm.Mother.Id == 0)
                        x = await vm.AddMother();
                    else
                        x = await vm.UpdateMother();
                    if (x == true)
                    {
                        if (vm.Father.ParentStatus == 2 || vm.Father.ParentStatus == 3)
                        {
                            MotherGrid1.Visibility = Visibility.Collapsed;
                            aelGrid1.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            StagesControl.SelectedStage = 3;
                            Frame.Navigate(typeof(Stage4StudentData), vm.Request);
                        }
                    }

                }
                else if (inCountrymother.IsChecked == true)
                {
                if (telephonem.Text.StartsWith("55"))
                {
                    MotherGrid1.Visibility = Visibility.Collapsed;
                    motherGrid2.Visibility = Visibility.Visible;
                }
                else
                {
                    await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.MobilePhoneShouldStartWith55);

                }
            }
                else if (outCountrymother.IsChecked == true)
                {
                vm.MotherBankCertificate = new Models.Exceptions.Request.Bankcertificate();
                vm.motherSalaryCer = new Models.Exceptions.Request.Salarycertificate();
                vm.MotherQIDAttachment = new Models.Exceptions.Request.Qataridattachment();
                bool x;
                if (vm.Mother.Id == 0)
                    x = await vm.AddMother();
                else
                    x = await vm.UpdateMother();
                if (x == true)
                {
                    if (vm.Father.ParentStatus == 3 || vm.Father.ParentStatus == 2)
                    {
                        MotherGrid1.Visibility = Visibility.Collapsed;
                        aelGrid1.Visibility = Visibility.Visible;
                    }
                    else {
                        StagesControl.SelectedStage = 3;
                        Frame.Navigate(typeof(Stage4StudentData), vm.Request);
                      
                    }
                }
            }
           
        }

        private void scrollViewer1_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            //var verticalOffsetValue = scrollViewer1.VerticalOffset;
            //var maxVerticalOffsetValue = scrollViewer1.ExtentHeight - scrollViewer1.ViewportHeight;

            //if (maxVerticalOffsetValue < 0 || verticalOffsetValue == maxVerticalOffsetValue || Deadmother.IsChecked == true || outCountrymother.IsChecked == true)
            //{
            //    // Scrolled to bottom
            //    nextbutton1.Visibility = Visibility.Visible;
            //    next1.Visibility = Visibility.Collapsed;
            //}
            //else {    
            //    // Scrolled to top
            //    next1.Visibility = Visibility.Visible;
            //    nextbutton1.Visibility = Visibility.Collapsed;
            //}
        }
        private void scrllviwr_Loaded(object sender, RoutedEventArgs e)
        {
            if(allGrid.ActualHeight <= scrllviwr.ActualHeight)
            {
                // Scrolled to bottom
                nextbutton2.Visibility = Visibility.Visible;
                next2.Visibility = Visibility.Collapsed;
            }
            else
            {
                // Scrolled to top
                next2.Visibility = Visibility.Visible;
                nextbutton2.Visibility = Visibility.Collapsed;
            }

        }
        private void ScrollViewer_ViewChanged_1(object sender, ScrollViewerViewChangedEventArgs e)
        {
            var verticalOffsetValue = scrllviwr.VerticalOffset;
            var maxVerticalOffsetValue = scrllviwr.ExtentHeight - scrllviwr.ViewportHeight;

            if (maxVerticalOffsetValue < 0 || verticalOffsetValue == maxVerticalOffsetValue  ||  allGrid.ActualHeight<= scrllviwr.ActualHeight) // father is dead or mother not working
            {
                // Scrolled to bottom
                nextbutton2.Visibility = Visibility.Visible;
                next2.Visibility = Visibility.Collapsed;


            }
            else
            {
                // Scrolled to top
                next2.Visibility = Visibility.Visible;
                nextbutton2.Visibility = Visibility.Collapsed;
            }
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            motherGrid2.Visibility = Visibility.Collapsed;
            MotherGrid1.Visibility = Visibility.Visible;
        }

        private async void nextbutton2_Click(object sender, RoutedEventArgs e)
        {
           
            if(Workmother.IsChecked == true)
            {
                motherGrid2.Visibility = Visibility.Collapsed;
                motherGrid3.Visibility = Visibility.Visible;
            }
            else if(noWorkmother.IsChecked == true)

            {
                bool x;
                if (vm.Mother.Id == 0)
                 x=   await vm.AddMother();
                else
                  x=  await vm.UpdateMother();
                if (x == true)
                {
                    if (vm.Father.ParentStatus == 2 || vm.Father.ParentStatus == 3)
                    {
                        motherGrid2.Visibility = Visibility.Collapsed;
                        aelGrid1.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        StagesControl.SelectedStage = 3;
                        Frame.Navigate(typeof(Stage4StudentData), vm.Request);
                    }
                }
               
            }
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            motherGrid3.Visibility = Visibility.Collapsed;
            motherGrid2.Visibility = Visibility.Visible;
        }

        private async void Button_Click_9(object sender, RoutedEventArgs e)
        {
           
            bool x;
            if (vm.Mother.Id == 0)
            x=    await vm.AddMother();
            else
              x=  await vm.UpdateMother();
            if (x == true)
            {
                if (vm.Father.ParentStatus == 2 || vm.Father.ParentStatus == 3)
                {
                    motherGrid3.Visibility = Visibility.Collapsed;
                    aelGrid1.Visibility = Visibility.Visible;
                }
                else
                {
                    StagesControl.SelectedStage = 3;
                    Frame.Navigate(typeof(Stage4StudentData), vm.Request);
                }
            }
        }

        private void ScrollViewer_ViewChanged_2(object sender, ScrollViewerViewChangedEventArgs e)
        {
            //var verticalOffsetValue = scrollViewer2.VerticalOffset;
            //var maxVerticalOffsetValue = scrollViewer2.ExtentHeight - scrollViewer2.ViewportHeight;

            //if (maxVerticalOffsetValue < 0 || verticalOffsetValue == maxVerticalOffsetValue || comboBoxItem.IsSelected == true)
            //{
            //    // Scrolled to bottom
            //    nextbutton3.Visibility = Visibility.Visible;
            //    next3.Visibility = Visibility.Collapsed;


            //}
            //else
            //{
            //    // Scrolled to top
            //    next3.Visibility = Visibility.Visible;
            //    nextbutton3.Visibility = Visibility.Collapsed;
            //}
        }

        private async void Button_Click_10(object sender, RoutedEventArgs e)
        {
            await vm.GetMotherByRequestID();
            aelGrid1.Visibility = Visibility.Collapsed;
            if(vm.Mother.IsEmployed == true)//workmother.ischecked
            {
                motherGrid3.Visibility = Visibility.Visible;
            }
            else if(vm.Mother.IsEmployed == false)
            {
                motherGrid2.Visibility = Visibility.Visible;
            }
            else if(vm.Mother.ParentStatus == 3 || vm.Mother.ParentStatus == 2 )//dead
            {
                MotherGrid1.Visibility = Visibility.Visible;
            }
        }

        private async void nextbutton3_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(vm.BreadWinner.QatarId) && vm.BreadWinner.Relationship != 1) // not mother
            {
                await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.InsertAllDataMessage);
              
            }
            else
            {
                bool x;
                if (vm.BreadWinner.Id == 0)
                    x = await vm.AddBreadwinner();
                else
                    x = await vm.UpdateBreadwinner();
                if (x == true)
                {
                    StagesControl.SelectedStage = 3;
                    Frame.Navigate(typeof(Stage4StudentData), vm.Request);
                }
            }
        }

        private void inCountry_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton status = sender as RadioButton;
            if (status.Content != null)
            {
                if (status.Content.ToString() == ViewModels.ViewModelLocator.Resources.InCountry)
                {
                    vm.Father.ParentStatus = 1;
                }
                else if (status.Content.ToString() == ViewModels.ViewModelLocator.Resources.Abroad)
                {
                    vm.Father.ParentStatus = 2;
                }
                else if (status.Content.ToString() == ViewModels.ViewModelLocator.Resources.Dead)
                {
                    vm.Father.ParentStatus = 3;
                }
            }
           
        }

        private void Work_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton work = sender as RadioButton;
            if (work.Content != null)
            {
                if (work.Content.ToString() == ViewModels.ViewModelLocator.Resources.IsWorking)
                {
                    vm.Father.IsEmployed = true;
                }
                else if (work.Content.ToString() == ViewModels.ViewModelLocator.Resources.NoWork)
                {
                    vm.Father.IsEmployed =false;
                }
               
            }
          
        }
        string currentUpload = "";
        bool setFlag = false;
        private void Image_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            Image image = sender as Image;
            if (setFlag == false)
            {
               
                FileOpenPicker filePicker = new FileOpenPicker();
                filePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                filePicker.ViewMode = PickerViewMode.Thumbnail;

                //// Filter to include a sample subset of file types
                filePicker.FileTypeFilter.Clear();
                filePicker.FileTypeFilter.Add(".pdf");
                filePicker.FileTypeFilter.Add(".png");
                filePicker.FileTypeFilter.Add(".jpeg");
                filePicker.FileTypeFilter.Add(".jpg");

                filePicker.PickSingleFileAndContinue();

                currentUpload = image.Name;
                this.view1.Activated += this.viewActivated;
            }
            else
            {
                //vm.QIDAttachment.FileContent = null;
                //vm.QIDAttachment.FileName = null;
                image.Source = new BitmapImage(new Uri(
   "ms-appx:///Assets/Exceptions/ic_ab_upload.png", UriKind.Absolute));
                string text = image.Name + "txt";
             var x =   layoutroot.FindName(text);
                ((TextBox)x).Text = "";
                setFlag = false;
            }
        }

      

        private async void viewActivated(CoreApplicationView sender, IActivatedEventArgs args1)
        {
            FileOpenPickerContinuationEventArgs args = args1 as FileOpenPickerContinuationEventArgs;
            if (args != null)
            { 
                if (args.Files.Count == 0) return;
                view1.Activated -= viewActivated;
                StorageFile storageFile = args.Files[0];
                byte[] array = await GetBytesAsync(storageFile);
                string str = Convert.ToBase64String(array);
                if (currentUpload == "DeathCerFather")
                {
                    vm.FatherDeathCertificate.FileContent = str;
                    vm.FatherDeathCertificate.FileName = args.Files[0].DisplayName;
                    DeathCerFather.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    DeathCerFathertxt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "IDCFather")
                {
                    vm.FatherQIDAttachment.FileContent = str;
                    vm.FatherQIDAttachment.FileName = args.Files[0].DisplayName;
                    IDCFather.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    IDCFathertxt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "SalaryCerFather")
                {
                    vm.FatherSalaryCer.FileContent = str;
                    vm.FatherSalaryCer.FileName = args.Files[0].DisplayName;
                    SalaryCerFather.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    SalaryCerFathertxt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "BankCerFather")
                {
                    vm.FatherBankCertificate.FileContent = str;
                    vm.FatherBankCertificate.FileName = args.Files[0].DisplayName;
                    BankCerFather.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    BankCerFathertxt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "IDCFather2")
                {
                    vm.FatherQIDAttachment.FileContent = str;
                    vm.FatherQIDAttachment.FileName = args.Files[0].DisplayName;
                    IDCFather2.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    IDCFather2txt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "DeadCerMother")
                {
                    vm.MotherDeathCertificate.FileContent = str;
                    vm.MotherDeathCertificate.FileName = args.Files[0].DisplayName;
                    DeadCerMother.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    DeadCerMothertxt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "IsIncubator")
                {
                    vm.IncubatorCertificate.FileContent = str;
                    vm.IncubatorCertificate.FileName = args.Files[0].DisplayName;
                    IsIncubator.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    IsIncubatortxt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "QIDMImage")
                {
                    vm.MotherQIDAttachment.FileContent = str;
                    vm.MotherQIDAttachment.FileName = args.Files[0].DisplayName;
                    QIDMImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    QIDMImagetxt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "SalaryCerImage")
                {
                    vm.motherSalaryCer.FileContent = str;
                    vm.motherSalaryCer.FileName = args.Files[0].DisplayName;
                    SalaryCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    SalaryCerImagetxt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "BankCerMotherImage")
                {
                    vm.MotherBankCertificate.FileContent = str;
                    vm.MotherBankCertificate.FileName = args.Files[0].DisplayName;
                    BankCerMotherImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    BankCerMotherImagetxt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "QIDMImage2")
                {
                    vm.MotherQIDAttachment.FileContent = str;
                    vm.MotherQIDAttachment.FileName = args.Files[0].DisplayName;
                    QIDMImage2.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    QIDMImage2txt.Text = args.Files[0].Name;
                }
                else if (currentUpload == "BWCerImage" || currentUpload == "BWCerIImagge" || currentUpload == "BWCERImageeee")
                {
                    vm.BreadWinnerCer.FileContent = str;
                    vm.BreadWinnerCer.FileName = args.Files[0].DisplayName;
                    BWCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    BWCerImagetxt.Text = args.Files[0].Name;
                    BWCerIImagge.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    BWCerIImaggetxt.Text = args.Files[0].Name;
                    BWCERImageeee.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    BWCERImageeeetxt.Text = args.Files[0].Name;
                }
                setFlag = true;
                currentUpload = "";       
            }
        }

        public static async Task<byte[]> GetBytesAsync(StorageFile file)
        {
            byte[] fileBytes = null;
            if (file == null) return null;
            using (var stream = await file.OpenReadAsync())
            {
                fileBytes = new byte[stream.Size];
                using (var reader = new DataReader(stream))
                {
                    await reader.LoadAsync((uint)stream.Size);
                    reader.ReadBytes(fileBytes);
                }
            }
            return fileBytes;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox nationalities = sender as ComboBox;
            if (nationalities.SelectedItem != null)
            {
                if (nationalities.SelectedIndex == 166)
                {
                    SeryianGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    SeryianGrid.Visibility = Visibility.Collapsed;
                }
                vm.Father.NationalityId = (nationalities.SelectedItem as Nationality).Id;
            }
        }

       

        private void inCountrymother_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton status = sender as RadioButton;
            if (status.Content != null)
            {
                if ( status.Content.ToString() == ViewModels.ViewModelLocator.Resources.InCountry)
                {
                    vm.Mother.ParentStatus = 1;
                }
                else if (status.Content.ToString() == ViewModels.ViewModelLocator.Resources.Abroad)
                {
                    vm.Mother.ParentStatus = 2;
                }
                else if (status.Content.ToString() == ViewModels.ViewModelLocator.Resources.Dead)
                {
                    vm.Mother.ParentStatus = 3;
                }
            }
        }

        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ComboBox nationalities = sender as ComboBox;
            if (nationalities.SelectedItem != null)
            {
               
                vm.Mother.NationalityId = (nationalities.SelectedItem as Nationality).Id;
            }
        }

        private void ComboBox_SelectionChanged_2(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxItem.IsSelected == true)
            {
                vm.BreadWinner.Relationship = 1;
                BWother.Visibility = Visibility.Collapsed;
                BWGrandFUBr.Visibility = Visibility.Collapsed;
                BWMother.Visibility = Visibility.Visible;

                //
                //nextbutton3.Visibility = Visibility.Visible;
                //next3.Visibility = Visibility.Collapsed;
            }
            else if (comboBoxItem1.IsSelected == true)
            {
                vm.BreadWinner.Relationship = 2;
                BWother.Visibility = Visibility.Collapsed;
                BWGrandFUBr.Visibility = Visibility.Visible;
                BWMother.Visibility = Visibility.Collapsed;
            }
            else if (comboBoxItem2.IsSelected == true)
            {
                vm.BreadWinner.Relationship = 3;
                BWother.Visibility = Visibility.Collapsed;
                BWGrandFUBr.Visibility = Visibility.Visible;
                BWMother.Visibility = Visibility.Collapsed;
            }
            else if (comboBoxItem3.IsSelected == true)
            {
                vm.BreadWinner.Relationship = 4;
                BWother.Visibility = Visibility.Collapsed;
                BWGrandFUBr.Visibility = Visibility.Visible;
                BWMother.Visibility = Visibility.Collapsed;
            }
            else if (comboBoxItem4.IsSelected == true)
            {
                vm.BreadWinner.Relationship = 5;
                BWother.Visibility = Visibility.Visible;
                BWGrandFUBr.Visibility = Visibility.Collapsed;
                BWMother.Visibility = Visibility.Collapsed;
            }
        }

        private void inCountry_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void scrollViewer2_Loaded(object sender, RoutedEventArgs e)
        {
//if(BWMother.Visibility == Visibility.Visible || BWGrandFUBr.Visibility == Visibility.Visible)
//            {
//                nextbutton3.Visibility = Visibility.Visible;
//              //  next3.Visibility = Visibility.Collapsed;
//            }
        }

        private void BreadwinnerNationality_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox nationalities = sender as ComboBox;
            if (nationalities.SelectedItem != null)
            {

                vm.BreadWinner.NationalityId = (nationalities.SelectedItem as Nationality).Id;
            }
        }

        private void no_Checked(object sender, RoutedEventArgs e)
        {
            no1.IsChecked = true;
        }
    }
}
