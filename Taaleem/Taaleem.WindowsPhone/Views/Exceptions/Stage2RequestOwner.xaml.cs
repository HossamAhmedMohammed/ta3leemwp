﻿using Autofac.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Taaleem.Common;
using Taaleem.Models.Exceptions.Response;
using Taaleem.ViewModels.Exceptions;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Exceptions
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Stage2RequestOwner :Page
    {
        CoreApplicationView view;
        ApplicantViewModel vm;
        public Stage2RequestOwner()
        {
            this.InitializeComponent();
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            view = CoreApplication.GetCurrentView();
            vm = (ApplicantViewModel)DataContext;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.Navigate(typeof(MyRequests));
        }
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }


        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = ViewModels.ViewModelLocator.Locator.MainMenuVM.Items.Where(x => x.Name == "خدمات عامه").SingleOrDefault();
            base.OnNavigatedTo(e);
            ViewModels.ViewModelLocator.Locator.StagesSelectionVM.LoadStagesAsync();
            StagesControl.SelectedStage = 1;
            if (e.Parameter != null)
            {

              
                vm.Request = e.Parameter as MyExceptionRequestResponse;
               await vm.GetApplicantbyIDAcync();
            }
            if(vm.QIDAttachment.FileName != null)
            {
                UploadImage.Source = new BitmapImage(new Uri(
                 "ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                UploadImagetxt.Text = vm.QIDAttachment.FileName;
                setFlag = true;
            }
            if(vm.Applicant.Id == 0)
            {
                vm.Birthdate = new DateTimeOffset();
            }
        }
        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(vm.Applicant.ApplicantName)&& !string.IsNullOrEmpty(vm.Applicant.QatarId) && !string.IsNullOrEmpty(vm.Applicant.DOB) && !string.IsNullOrEmpty(vm.Applicant.Mobile)&& !string.IsNullOrEmpty(vm.QIDAttachment.FileName))
            {
                if (Telephone.Text.StartsWith("55"))
                {
                    if (vm.Request != null) // update
                    {
                        await vm.UpdateApplicantAcync();
                    }
                    else //add
                    {
                        await vm.AddApplicantAcync();
                    }
                }
                else
                {
                    await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.MobilePhoneShouldStartWith55);

                }
            }
            else
            {
                await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.InsertAllDataMessage);
            }
        }
        bool setFlag = false;
        private void Image_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            if (setFlag == false || vm.QIDAttachment.FileName == null  )
            {
                FileOpenPicker filePicker = new FileOpenPicker();
                filePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                filePicker.ViewMode = PickerViewMode.Thumbnail;

                //// Filter to include a sample subset of file types
                filePicker.FileTypeFilter.Clear();
                filePicker.FileTypeFilter.Add(".pdf");
                filePicker.FileTypeFilter.Add(".png");
                filePicker.FileTypeFilter.Add(".jpeg");
                filePicker.FileTypeFilter.Add(".jpg");

                filePicker.PickSingleFileAndContinue();
                view.Activated += viewActivated;
            }
            else
            {
                vm.QIDAttachment.FileContent = null;
                vm.QIDAttachment.FileName = null;
                UploadImage.Source = new BitmapImage(new Uri(
   "ms-appx:///Assets/Exceptions/ic_ab_upload.png", UriKind.Absolute));
                UploadImagetxt.Text = "";
                setFlag = false;
            }
        }

        private async void viewActivated(CoreApplicationView sender, IActivatedEventArgs args1)
        {
            FileOpenPickerContinuationEventArgs args = args1 as FileOpenPickerContinuationEventArgs;

            if (args != null)
            {
                if (args.Files.Count == 0) return;

                view.Activated -= viewActivated;
                StorageFile storageFile = args.Files[0];

                byte[] array = await GetBytesAsync(storageFile);
               

                string str = Convert.ToBase64String(array);
                vm.QIDAttachment.FileContent = str;
                vm.QIDAttachment.FileName = args.Files[0].DisplayName;
                UploadImage.Source = new BitmapImage(new Uri(
   "ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                UploadImagetxt.Text = args.Files[0].DisplayName;
                setFlag = true;
            }
        }

        public static async Task<byte[]> GetBytesAsync(StorageFile file)
        {
            byte[] fileBytes = null;
            if (file == null) return null;
            using (var stream = await file.OpenReadAsync())
            {
                fileBytes = new byte[stream.Size];
                using (var reader = new DataReader(stream))
                {
                    await reader.LoadAsync((uint)stream.Size);
                    reader.ReadBytes(fileBytes);
                }
            }
            return fileBytes;
        }
    }
}
