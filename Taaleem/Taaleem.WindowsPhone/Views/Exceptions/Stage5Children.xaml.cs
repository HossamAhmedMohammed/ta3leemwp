﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Taaleem.Models.Exceptions.Response;
using Taaleem.ViewModels.Exceptions;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Exceptions
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Stage5Children : Page
    {
        ChildrenViewModel vm;
        CoreApplicationView view1;
        public Stage5Children()
        {
            this.InitializeComponent();
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            vm = (ChildrenViewModel)DataContext;
            view1 = CoreApplication.GetCurrentView();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.Navigate(typeof(MyRequests));
        }
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = ViewModels.ViewModelLocator.Locator.MainMenuVM.Items.Where(x => x.Name == "خدمات عامه").SingleOrDefault();
            ViewModels.ViewModelLocator.Locator.StagesSelectionVM.LoadStagesAsync();
            StagesControl.SelectedStage = 4;
            vm.Request = e.Parameter as MyExceptionRequestResponse;
            if (vm.Request.RequestNextStep == 8) // children
            { }

            else if (vm.Request.RequestNextStep == 9) // other
            {
                await vm.GetChildrenByRequestID();
                if(vm.Child != null)
                {
                    if(vm.LastSchoolCer.FileName != null)
                    {
                        LastSchoolCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                        LastSchoolCerImagetxt.Text = vm.LastSchoolCer.FileName;
                    }
                }
            }
           
        }
        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StagesControl.SelectedStage = 3;
            Frame.Navigate(typeof(Stage4StudentData), vm.Request);
        }

        private void nextbutton1_Click(object sender, RoutedEventArgs e)
        {
            StagesControl.SelectedStage = 5;
            Frame.Navigate(typeof(Stage6Other), vm.Request);
        }

        private void ScrollViewer_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            var verticalOffsetValue = scrollViewer2.VerticalOffset;
            var maxVerticalOffsetValue = scrollViewer2.ExtentHeight - scrollViewer2.ViewportHeight;

            if (maxVerticalOffsetValue < 0 || verticalOffsetValue == maxVerticalOffsetValue)
            {
                // Scrolled to bottom
                nextbutton1.Visibility = Visibility.Visible;
                next1.Visibility = Visibility.Collapsed;


            }
            else// if (verticalOffsetValue == 0)
            {
                // Scrolled to top
                next1.Visibility = Visibility.Visible;
                nextbutton1.Visibility = Visibility.Collapsed;
               
            }
        }

        

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton gender = sender as RadioButton;
            if (gender.Content.ToString() == ViewModels.ViewModelLocator.Resources.Male)
            {
                vm.Child.Gender = 1;
            }
            else if (gender.Content.ToString() == ViewModels.ViewModelLocator.Resources.Female)
            {
                vm.Child.Gender = 2;
            }
        }

        private void Student_Checked(object sender, RoutedEventArgs e)
        {
           
            RadioButton educationType = sender as RadioButton;
            if (educationType.Content.ToString() == ViewModels.ViewModelLocator.Resources.Student)
            {
                vm.Child.EducationType = 1;
                IsStudentGrid.Visibility = Visibility.Visible;
            }
            else if (educationType.Content.ToString() == ViewModels.ViewModelLocator.Resources.NonStudent)
            {
                vm.Child.EducationType = 2;
                IsStudentGrid.Visibility = Visibility.Collapsed;
            }
        }

        private void pSchool_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton schoolType = sender as RadioButton;
          
            if (schoolType.Content.ToString() == ViewModels.ViewModelLocator.Resources.PrivateSchool)
            {
                vm.Child.SchoolType = 1;
                SchoolCheckedGrid.Visibility = Visibility.Visible;
                schools.ItemsSource = vm.PrivateSchools;
            }
            else if (schoolType.Content.ToString() == ViewModels.ViewModelLocator.Resources.IndependentSchool)
            {
                vm.Child.SchoolType = 2;
                SchoolCheckedGrid.Visibility = Visibility.Visible;
                schools.ItemsSource = vm.IndependentSchools;
            }
            else if (schoolType.Content.ToString() == ViewModels.ViewModelLocator.Resources.University)
            {
                vm.Child.SchoolType = 3;
                SchoolCheckedGrid.Visibility = Visibility.Collapsed;
            }
        }

        private void ComboBox_Loaded_1(object sender, RoutedEventArgs e)
        {
            ComboBox schools = sender as ComboBox;
            if (vm.Child.SchoolType == 1)
            {
                schools.ItemsSource = vm.PrivateSchools;

            }
            else if (vm.Child.SchoolType == 2)
            {
                schools.ItemsSource = vm.IndependentSchools;

            }

           
        }


        string currentUpload = "";
        bool setFlag = false;
        private void Image_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            Image image = sender as Image;
            if (setFlag == false)
            {
                FileOpenPicker filePicker = new FileOpenPicker();
                filePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                filePicker.ViewMode = PickerViewMode.Thumbnail;

                //// Filter to include a sample subset of file types
                filePicker.FileTypeFilter.Clear();
                filePicker.FileTypeFilter.Add(".pdf");
                filePicker.FileTypeFilter.Add(".png");
                filePicker.FileTypeFilter.Add(".jpeg");
                filePicker.FileTypeFilter.Add(".jpg");

                filePicker.PickSingleFileAndContinue();

                currentUpload = image.Name;
                this.view1.Activated += this.viewActivated;
            }
            else
            {
                image.Source = new BitmapImage(new Uri(
   "ms-appx:///Assets/Exceptions/ic_ab_upload.png", UriKind.Absolute));
                LastSchoolCerImagetxt.Text = "";
                setFlag = false;
            }
        }



        private async void viewActivated(CoreApplicationView sender, IActivatedEventArgs args1)
        {
            FileOpenPickerContinuationEventArgs args = args1 as FileOpenPickerContinuationEventArgs;
            if (args != null)
            {
                if (args.Files.Count == 0) return;
                view1.Activated -= viewActivated;
                StorageFile storageFile = args.Files[0];
                byte[] array = await GetBytesAsync(storageFile);
                string str = Convert.ToBase64String(array);

                if (currentUpload == "LastSchoolCerImage")
                {
                    vm.LastSchoolCer.FileContent = str;
                    vm.LastSchoolCer.FileName = args.Files[0].DisplayName + args.Files[0].DisplayType;
                    LastSchoolCerImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    LastSchoolCerImagetxt.Text = args.Files[0].Name;
                }

                currentUpload = "";
            }
        }

        public static async Task<byte[]> GetBytesAsync(StorageFile file)
        {
            byte[] fileBytes = null;
            if (file == null) return null;
            using (var stream = await file.OpenReadAsync())
            {
                fileBytes = new byte[stream.Size];
                using (var reader = new DataReader(stream))
                {
                    await reader.LoadAsync((uint)stream.Size);
                    reader.ReadBytes(fileBytes);
                }
            }
            return fileBytes;
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
          await  vm.AddChildAcync();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox nationalities = sender as ComboBox;
            if (nationalities.SelectedItem != null)
            {

                vm.Child.NationalityId = (nationalities.SelectedItem as Nationality).Id;
            }
        }

        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ComboBox Schools = sender as ComboBox;
            if (Schools.SelectedItem != null)
            {
                if(govrenSchool.IsChecked == true)
                vm.Child.IndependentSchoolCode = (Schools.SelectedItem as School).Id;
                else if (pSchool.IsChecked == true)
                    vm.Child.PrivateSchoolCode = (Schools.SelectedItem as School).Id;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Stage6Other), vm.Request);
        }
    }
}
