﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Exceptions
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Stage1Commitment :Page
    {
        public Stage1Commitment()
        {
            this.InitializeComponent();
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.Navigate(typeof(MyRequests));
        }
        //protected override void LoadState(object sender, LoadStateEventArgs e)
        //{
        //    base.LoadState(sender, e);
        //}
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ViewModels.ViewModelLocator.Locator.StagesSelectionVM.LoadStagesAsync();
            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = ViewModels.ViewModelLocator.Locator.MainMenuVM.Items.Where(x => x.Name == "خدمات عامه").SingleOrDefault();
        }
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }
        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (checkBox.IsChecked == true)
            {
                StagesControl.SelectedStage = 1;
                Frame.Navigate(typeof(Stage2RequestOwner));
            }
        }
    }
}
