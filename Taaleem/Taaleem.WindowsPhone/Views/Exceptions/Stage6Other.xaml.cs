﻿using Autofac.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Taaleem.Models.Exceptions.Response;
using Taaleem.ViewModels.Exceptions;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Exceptions
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Stage6Other : Page
    {
        private CoreApplicationView view1;
        private OtherViewModel vm;
        public Stage6Other()
        {
            this.InitializeComponent();
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            vm = (OtherViewModel)DataContext;
            view1 = CoreApplication.GetCurrentView();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.Navigate(typeof(MyRequests));
        }
        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = ViewModels.ViewModelLocator.Locator.MainMenuVM.Items.Where(x => x.Name == "خدمات عامه").SingleOrDefault();
            ViewModels.ViewModelLocator.Locator.StagesSelectionVM.LoadStagesAsync();
            StagesControl.SelectedStage = 5;
            vm.Request = e.Parameter as MyExceptionRequestResponse;
           
        }
        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private void scrollViewer_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            //var verticalOffsetValue = scrollViewer.VerticalOffset;
            //var maxVerticalOffsetValue = scrollViewer.ExtentHeight - scrollViewer.ViewportHeight;

            //if (maxVerticalOffsetValue < 0 || verticalOffsetValue == maxVerticalOffsetValue)
            //{
            //    // Scrolled to bottom
            //    nextbutton1.Visibility = Visibility.Visible;
            //    next1.Visibility = Visibility.Collapsed;


            //}
            //else// if (verticalOffsetValue == 0)
            //{
            //    // Scrolled to top
            //    next1.Visibility = Visibility.Visible;
            //    nextbutton1.Visibility = Visibility.Collapsed;

            //}
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StagesControl.SelectedStage = 4;
            Frame.GoBack();
        }
        bool setFlag = false;
        private void Image_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            Image image = sender as Image;
            if (setFlag == false)
            {
                FileOpenPicker filePicker = new FileOpenPicker();
                filePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                filePicker.ViewMode = PickerViewMode.Thumbnail;

                //// Filter to include a sample subset of file types
                filePicker.FileTypeFilter.Clear();
                filePicker.FileTypeFilter.Add(".pdf");
                filePicker.FileTypeFilter.Add(".png");
                filePicker.FileTypeFilter.Add(".jpeg");
                filePicker.FileTypeFilter.Add(".jpg");

                filePicker.PickSingleFileAndContinue();


                this.view1.Activated += this.viewActivated;
            }
            else
            {
                image.Source = new BitmapImage(new Uri(
   "ms-appx:///Assets/Exceptions/ic_ab_upload.png", UriKind.Absolute));
                rentImagetxt.Text = "";
                setFlag = false;
            }
        }



        private async void viewActivated(CoreApplicationView sender, IActivatedEventArgs args1)
        {
            FileOpenPickerContinuationEventArgs args = args1 as FileOpenPickerContinuationEventArgs;
            if (args != null)
            {
                if (args.Files.Count == 0) return;
                view1.Activated -= viewActivated;
                StorageFile storageFile = args.Files[0];
                byte[] array = await GetBytesAsync(storageFile);
                string str = Convert.ToBase64String(array);

              
                    vm.HouseRentContract.FileContent = str;
                vm.HouseRentContract.FileName = args.Files[0].DisplayName + args.Files[0].DisplayType;
                rentImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                rentImagetxt.Text = args.Files[0].Name;


            }
        }

        public static async Task<byte[]> GetBytesAsync(StorageFile file)
        {
            byte[] fileBytes = null;
            if (file == null) return null;
            using (var stream = await file.OpenReadAsync())
            {
                fileBytes = new byte[stream.Size];
                using (var reader = new DataReader(stream))
                {
                    await reader.LoadAsync((uint)stream.Size);
                    reader.ReadBytes(fileBytes);
                }
            }
            return fileBytes;
        }


        private async void nextbutton1_Click(object sender, RoutedEventArgs e)
        {
          await   vm.UpdateAdditionalDataAcync();
        }
    }
}
