﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.ViewModels.Login;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.PublicServices.Login
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RegisterNewAccount : Page
    {
        UserAccountViewModel vm;
        public RegisterNewAccount()
        {
            this.InitializeComponent();
            mainMenu1.IsOpened = false;
            MenuBlackGrid1.Visibility = Visibility.Collapsed;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            vm = (UserAccountViewModel)DataContext;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.GoBack();
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }
        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu1.IsOpened = false;

            MenuBlackGrid1.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu1.IsOpened = true;

            MenuBlackGrid1.Visibility = Visibility.Visible;
        }

        private void ConfPass_LostFocus(object sender, RoutedEventArgs e)
        {
           
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Pass.Password == "" || ConfPass.Password == "" || QID.Text == "" || EmailTXT.Text == "" || MobileTXT.Text == "")
            {
                
                await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.InsertAllDataMessage);
            }
            else
            {
                if (string.Compare(Pass.Password, ConfPass.Password) != 0)
                {
                    await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.PasswordAndConfirmPasswordRntSame);
                }
                else
                {
                    await vm.RegisterAcync();
                }
            }
        }
    }
}
