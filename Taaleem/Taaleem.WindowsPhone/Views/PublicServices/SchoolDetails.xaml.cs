﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Taaleem.Models.PublicServices;
using Taaleem.ViewModels;
using Taaleem.ViewModels.PublicServices;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.PublicServices
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SchoolDetails 
    {
        private readonly SchoolDetailsViewModel _schoolViewModel;
        //private MapControl SMapControl;
        public SchoolDetails()
        {
            this.InitializeComponent();
            _schoolViewModel = (SchoolDetailsViewModel)DataContext;
        }

        protected override async void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            var parameters = (SchoolInfoWrapper)e.NavigationParameter;
            _schoolViewModel.SchoolInfo = parameters;
            _schoolViewModel.SchoolInfoList = ViewModelLocator.Locator.SchoolsViewModel.Schools;

            _schoolViewModel.SchoolInfoList.Remove(_schoolViewModel.SchoolInfo);
            _schoolViewModel.InitializeLocations();

            //SMapControl=ViewModelLocator.Locator.
            //Add pushpins
            AddPushPin(_schoolViewModel.SchoolLocation, _schoolViewModel.SchoolInfo.SchoolName);
            if (_schoolViewModel.YourLocation != null)
            {
                AddPushPin(_schoolViewModel.YourLocation, ViewModelLocator.Resources.MyLocation);
            }

          _schoolViewModel.  InitializeLocationsList();
            foreach(var item in _schoolViewModel.SchoolLocations)
            {
                AddOthersPushPin(item,"");
            }
            try
            {

                //Zoom on school location
                await SMapControl.TrySetViewAsync(_schoolViewModel.SchoolLocation, 14, null, null, MapAnimationKind.Bow);

                //Correct zoom to include both location
                if (_schoolViewModel.YourLocation != null)
                {
                    //Calculate Boundries
                    var locations = new List<BasicGeoposition>();
                    locations.Add(_schoolViewModel.SchoolLocation.Position);
                    locations.Add(_schoolViewModel.YourLocation.Position);
                    var boundries = GeoboundingBox.TryCompute(locations);
                    await SMapControl.TrySetViewBoundsAsync(boundries, new Thickness(100), MapAnimationKind.None);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

       

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);


            if (SMapControl != null)
            {
                try
                {
                    foreach (var child in SMapControl.Children)
                    {
                        DeepRemove(SMapControl.Children, (UIElement)child);
                    }
                    SMapControl.Children.Clear();

                    Grid parentGrid = VisualTreeHelper.GetParent(SMapControl) as Grid;

                    if (parentGrid != null)
                        parentGrid.Children.Remove(SMapControl);

                    GC.Collect(2);
                    GC.WaitForPendingFinalizers();
                    GC.Collect(2);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        public static void DeepRemove(IList<DependencyObject> UIElementLayer, UIElement uiElement)
        {
            ContentPresenter contentPresenter = VisualTreeHelper.GetParent(uiElement) as ContentPresenter;
            contentPresenter.Content = null;
            Canvas canvas = VisualTreeHelper.GetParent(contentPresenter) as Canvas;
            canvas.Children.Remove(contentPresenter);
            UIElementLayer.Remove(uiElement);
        }

        private void AddPushPin(Geopoint location, string title)
        {
            var pin = new CustomPushPin(title);
            SMapControl.Children.Add(pin);
            var point = new Point(0.5, 1);
            MapControl.SetNormalizedAnchorPoint(pin, point);
            MapControl.SetLocation(pin, location);
        }

        private void AddOthersPushPin(Geopoint location, string title)
        {
            var pin = new YellowCustomPushPin(title);
            SMapControl.Children.Add(pin);
            var point = new Point(0.5, 1);
            MapControl.SetNormalizedAnchorPoint(pin, point);
            MapControl.SetLocation(pin, location);
        }

        private void SMapControl_OnLoadingStatusChanged(MapControl sender, object args)
        {
            _schoolViewModel.IsLoading = sender.LoadingStatus == MapLoadingStatus.Loading;
        }


        private void MapStyleBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MapStyleBox != null)
            {
                string selectedStyle = ((ComboBoxItem)MapStyleBox.SelectedItem).Content.ToString();

                switch (selectedStyle)
                {
                    case "None":
                        SMapControl.Style = MapStyle.None;
                        break;
                    case "Road":
                        SMapControl.Style = MapStyle.Road;
                        break;
                    case "Aerial":
                        SMapControl.Style = MapStyle.Aerial;
                        break;
                    case "AerialWithRoads":
                        SMapControl.Style = MapStyle.AerialWithRoads;
                        break;
                    case "Terrain":
                        SMapControl.Style = MapStyle.Terrain;
                        break;
                    case "لاشئ":
                        SMapControl.Style = MapStyle.None;
                        break;
                    case "طريق":
                        SMapControl.Style = MapStyle.Road;
                        break;
                    case "هوائي":
                        SMapControl.Style = MapStyle.Aerial;
                        break;
                    case "هوائي مع طرق":
                        SMapControl.Style = MapStyle.AerialWithRoads;
                        break;
                    case "تضاريس":
                        SMapControl.Style = MapStyle.Terrain;
                        break;
                    default:
                        SMapControl.Style = MapStyle.None;
                        break;
                }
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri("http://maps.google.com/?daddr=lat,long&zoom=14&views=traffic://maps.google.com/?daddr=56.3546,34.322546&zoom=14&views=traff");
            await Launcher.LaunchUriAsync(uri);
        }
    }
}
