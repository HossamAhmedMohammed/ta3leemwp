﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Taaleem.Common;
using Taaleem.ViewModels;
using Taaleem.ViewModels.PublicServices;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.PublicServices
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MaglesNews 
    {
        private MaglesNewsViewModel _maglesNewsViewModel;
        public MaglesNews()
        {
            this.InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Enabled;
            mainMenu.IsOpened = false;
            AppBar.Visibility = Visibility.Visible;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }

        protected override void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            if (NeedsRefresh)
            {
                _maglesNewsViewModel = ViewModelLocator.Locator.MaglesNewsViewModel;
                DataContext = _maglesNewsViewModel;
                ICommand command = _maglesNewsViewModel.LoadCommand;
                command.Execute(null);
                ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = ViewModels.ViewModelLocator.Locator.MainMenuVM.Items.Where(x => x.Name == "الاخبار").SingleOrDefault();
            }
        }

        private void ItemsPanel_SizeChanged(object sender, Windows.UI.Xaml.SizeChangedEventArgs e)
        {
            //if (!double.IsNaN(e.NewSize.Width) && Math.Abs(e.NewSize.Width - e.PreviousSize.Width) > 1e-6)
            //{
                var width = e.NewSize.Width;
                var itemsPanel = (VariableSizedWrapGrid)sender;
                itemsPanel.ItemWidth = width;
               // itemsPanel.ItemHeight = width * 0.6;
            //}
        }
        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;
            AppBar.Visibility = Visibility.Visible;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;
            AppBar.Visibility = Visibility.Collapsed;
            MenuBlackGrid.Visibility = Visibility.Visible;
        }
    }
}