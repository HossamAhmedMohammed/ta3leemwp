﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Taaleem.Common;
using Taaleem.ViewModels.PublicServices.PrivateSchoolsViewModels;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.PublicServices.PrivateSchoolsViews
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PrivateSchoolsView:Page
    {
        private SearchPrivateSchoolsViewModel vm;
        public PrivateSchoolsView()
        {
            this.InitializeComponent();
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            vm =(SearchPrivateSchoolsViewModel)DataContext;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.GoBack();
        }
       


        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>


        private void AddOthersPushPin(Geopoint location,string region,string type, string title, string Image)
        {
            
          //  MapControl SMapControl = FindControl<MapControl>(mapSection, "SMapControl");

            if (SMapControl != null)
            {
                var pin = new FlagCustomPushPin(title,region,type,Image);
                SMapControl.Children.Add(pin);
                var point = new Point(0.5, 1);
                MapControl.SetNormalizedAnchorPoint(pin, point);
                MapControl.SetLocation(pin, location);
            }
        }
        MapControl SMapControl;
        private async void SMapControl_Loaded(object sender, RoutedEventArgs e)
        {
            SMapControl = sender as MapControl;


            vm.InitializeLocationsList();
            foreach (var item in vm.Schools)
            {
                if (item.Latitude.HasValue && item.Longitude.HasValue)
                {
                    Geopoint x = new Geopoint(new BasicGeoposition
                    {
                        Latitude = item.Latitude.Value,
                        Longitude = item.Longitude.Value
                    });

                    AddOthersPushPin(x,item.Area, item.Gender, item.Name, item.CarriculumEnglish);
                }
            }
            try
            {

                //Zoom on school location
              //  await SMapControl.TrySetViewAsync(vm.SchoolLocation, 14, null, null, MapAnimationKind.Bow);

                //Correct zoom to include both location
                if (vm.SchoolLocations.Count != 0)
                {
                    //Calculate Boundries
                    var locations = new List<BasicGeoposition>();
                    foreach (var item in vm.SchoolLocations)
                    {
                        locations.Add(item.Position);
                       // locations.Add(vm.YourLocation.Position);
                    }
                    var boundries = GeoboundingBox.TryCompute(locations);
                    await SMapControl.TrySetViewBoundsAsync(boundries, new Thickness(100), MapAnimationKind.None);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if(e.Parameter != null)
            {
                vm.Schools = e.Parameter as List<Models.PrivateSchools.Response.PrivateSchool>;
            }
            //ICommand command = vm.LoadCommand;
           // command.Execute(null);


            
         
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
          
            base.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
            //  MapControl SMapControl = FindControl<MapControl>( mapSection, "ssMapControl");

            if (SMapControl != null)
            {
                try
                {
                    foreach (var child in SMapControl.Children)
                    {
                        DeepRemove(SMapControl.Children, (UIElement)child);
                    }
                    SMapControl.Children.Clear();

                    Grid parentGrid = VisualTreeHelper.GetParent(SMapControl) as Grid;

                    if (parentGrid != null)
                        parentGrid.Children.Remove(SMapControl);

                    GC.Collect(2);
                    GC.WaitForPendingFinalizers();
                    GC.Collect(2);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        public static void DeepRemove(IList<DependencyObject> UIElementLayer, UIElement uiElement)
        {
            ContentPresenter contentPresenter = VisualTreeHelper.GetParent(uiElement) as ContentPresenter;
            contentPresenter.Content = null;
            Canvas canvas = VisualTreeHelper.GetParent(contentPresenter) as Canvas;
            canvas.Children.Remove(contentPresenter);
            UIElementLayer.Remove(uiElement);
        }

        private List<Control> AllChildren(DependencyObject parent)
        {
            var _List = new List<Control>();
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var _Child = VisualTreeHelper.GetChild(parent, i);
                if (_Child is Control)
                {
                    _List.Add(_Child as Control);
                }
                _List.AddRange(AllChildren(_Child));
            }
            return _List;
        }


        private T FindControl<T>(DependencyObject parentContainer, string controlName)
        {
            var childControls = AllChildren(parentContainer);
            var control = childControls.OfType<Control>().Where(x => x.Name.Equals(controlName)).Cast<T>().First();
            return control;
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
       
    }
}
