﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Taaleem.Views.PublicServices.PrivateSchoolsViews
{
    public sealed partial class FlagCustomPushPin : UserControl
    {
        public FlagCustomPushPin(string title, string region, string type , string Image)
        {
            this.InitializeComponent(); 
            if(title != null)
            TitleTb.Text = title;
            if (region != null)
                RegionTb.Text = region;
            if (type != null)
                TypeTb.Text = type;
            if (Image != null)
                FlagImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/PublicServices/PrivateSchools/Flagss/"+Image+".png"));
        }

        private void Image_Holding(object sender, HoldingRoutedEventArgs e)
        {
            polygon.Visibility = Visibility.Visible;
            border.Visibility = Visibility.Visible;
        }

        private void Image_LostFocus(object sender, RoutedEventArgs e)
        {
            polygon.Visibility = Visibility.Collapsed;
            border.Visibility = Visibility.Collapsed;
        }
    }
}
