﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Taaleem.ViewModels.PublicServices.PrivateSchoolsViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.PublicServices.PrivateSchoolsViews
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Search:Page
    {
        SearchPrivateSchoolsViewModel vm;
        public Search()
        {
            this.InitializeComponent();
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            vm = (SearchPrivateSchoolsViewModel)DataContext;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.GoBack();
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
           
           
            ToggleButton radio = sender as ToggleButton;
            if(radio.Content != null && vm.SchoolTypes.Count != 0)
            {

                if(radio.Content.ToString() == "Male" || radio.Content.ToString()== "ذكر")
                {
                    vm.SelectedSchoolType = vm.SchoolTypes.Where(x => x.ID == "6fcecf41-0d06-e111-84aa-968f1c650188").SingleOrDefault();
                    Female.IsChecked = false;
                    Mix.IsChecked = false;
                }
                else if (radio.Content.ToString() == "Female" || radio.Content.ToString() == "انثي")
                {
                    vm.SelectedSchoolType = vm.SchoolTypes.Where(x => x.ID == "72cecf41-0d06-e111-84aa-968f1c650188").SingleOrDefault();
                    Male.IsChecked = false;
                    Mix.IsChecked = false;
                }
                else if (radio.Content.ToString() == "Mixed" || radio.Content.ToString() == "مختلط")
                {
                    vm.SelectedSchoolType = vm.SchoolTypes.Where(x => x.ID == "75cecf41-0d06-e111-84aa-968f1c650188").SingleOrDefault();
                    Male.IsChecked = false;
                    Female.IsChecked = false;
                }
               
            }
            else
            {
                radio.IsChecked = false;
            }
        }

       

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
           await vm.SearchSchoolsAsync();
        }

        private void TextBlock_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
