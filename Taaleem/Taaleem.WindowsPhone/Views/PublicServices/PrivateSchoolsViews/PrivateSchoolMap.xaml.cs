﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Models.PrivateSchools.Response;
using Taaleem.ViewModels;
using Taaleem.ViewModels.PublicServices.PrivateSchoolsViewModels;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.PublicServices.PrivateSchoolsViews
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PrivateSchoolMap : Page
    {
        PrivateSchoolDetailsViewModel vm;
        public PrivateSchoolMap()
        {
            this.InitializeComponent();
            vm = (PrivateSchoolDetailsViewModel)DataContext;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.GoBack();
        }
       

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            
            //var parameters = (PrivateSchool)e.Parameter;
            //vm.School = parameters;
          

           
            vm.InitializeLocations();

         
            //Add pushpins
            AddPushPin(vm.SchoolLocation, vm.School.Name,vm.School.Area, vm.School.Gender,vm.School.CarriculumEnglish);
            //if (vm.YourLocation != null)
            //{
            //    AddPushPin(vm.YourLocation, ViewModelLocator.Resources.MyLocation);
            //}

        
            try
            {

                //Zoom on school location
                await SMapControl.TrySetViewAsync(vm.SchoolLocation, 14, null, null, MapAnimationKind.Bow);

                //Correct zoom to include both location
                if (vm.YourLocation != null)
                {
                    //Calculate Boundries
                    var locations = new List<BasicGeoposition>();
                    locations.Add(vm.SchoolLocation.Position);
                    locations.Add(vm.YourLocation.Position);
                    var boundries = GeoboundingBox.TryCompute(locations);
                    await SMapControl.TrySetViewBoundsAsync(boundries, new Thickness(100), MapAnimationKind.None);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }



        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;

            if (SMapControl != null)
            {
                try
                {
                    foreach (var child in SMapControl.Children)
                    {
                        DeepRemove(SMapControl.Children, (UIElement)child);
                    }
                    SMapControl.Children.Clear();

                    Grid parentGrid = VisualTreeHelper.GetParent(SMapControl) as Grid;

                    if (parentGrid != null)
                        parentGrid.Children.Remove(SMapControl);

                    GC.Collect(2);
                    GC.WaitForPendingFinalizers();
                    GC.Collect(2);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        public static void DeepRemove(IList<DependencyObject> UIElementLayer, UIElement uiElement)
        {
            ContentPresenter contentPresenter = VisualTreeHelper.GetParent(uiElement) as ContentPresenter;
            contentPresenter.Content = null;
            Canvas canvas = VisualTreeHelper.GetParent(contentPresenter) as Canvas;
            canvas.Children.Remove(contentPresenter);
            UIElementLayer.Remove(uiElement);
        }

        private void AddPushPin(Geopoint location, string title, string region, string type, string Image)
        {
            var pin = new FlagCustomPushPin(title,region,type,Image);
            SMapControl.Children.Add(pin);
            var point = new Point(0.5, 1);
            MapControl.SetNormalizedAnchorPoint(pin, point);
            MapControl.SetLocation(pin, location);
        }

        //private void AddOthersPushPin(Geopoint location, string title)
        //{
        //    var pin = new YellowCustomPushPin(title);
        //    SMapControl.Children.Add(pin);
        //    var point = new Point(0.5, 1);
        //    MapControl.SetNormalizedAnchorPoint(pin, point);
        //    MapControl.SetLocation(pin, location);
        //}


        private void MapStyleBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MapStyleBox != null)
            {
                string selectedStyle = ((ComboBoxItem)MapStyleBox.SelectedItem).Content.ToString();

                switch (selectedStyle)
                {
                    case "None":
                        SMapControl.Style = MapStyle.None;
                        break;
                    case "Road":
                        SMapControl.Style = MapStyle.Road;
                        break;
                    case "Aerial":
                        SMapControl.Style = MapStyle.Aerial;
                        break;
                    case "AerialWithRoads":
                        SMapControl.Style = MapStyle.AerialWithRoads;
                        break;
                    case "Terrain":
                        SMapControl.Style = MapStyle.Terrain;
                        break;
                    case "لاشئ":
                        SMapControl.Style = MapStyle.None;
                        break;
                    case "طريق":
                        SMapControl.Style = MapStyle.Road;
                        break;
                    case "هوائي":
                        SMapControl.Style = MapStyle.Aerial;
                        break;
                    case "هوائي مع طرق":
                        SMapControl.Style = MapStyle.AerialWithRoads;
                        break;
                    case "تضاريس":
                        SMapControl.Style = MapStyle.Terrain;
                        break;
                    default:
                        SMapControl.Style = MapStyle.None;
                        break;
                }
            }
        }
    }
}
