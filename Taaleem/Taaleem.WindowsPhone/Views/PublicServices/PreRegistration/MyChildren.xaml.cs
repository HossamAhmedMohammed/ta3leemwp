﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.ViewModels.PreRegistration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Taaleem.Views.PublicServices.PreRegistration
{
    public sealed partial class MyChildren : UserControl
    {
        PreRegistrationViewModel vm;
        public MyChildren()
        {
            this.InitializeComponent();
            vm = (PreRegistrationViewModel)DataContext;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            vm.SelectedschoolEnrollments = null;
            vm.SelectedState = null;
            ViewModels.ViewModelLocator.Locator.DialogManager.CloseDialog();
        }

        private  void Button_Click_1(object sender, RoutedEventArgs e)
        {

            if (vm.SelectedschoolEnrollments != null && vm.SelectedState != null)
            {
                ViewModels.ViewModelLocator.Locator.DialogManager.CloseDialog();
                ViewModels.ViewModelLocator.Locator.NavigationService.NavigateByPage(typeof(NearestSchool));
            }
            else
            {
                ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.InsertAllDataMessage);
            }
        }

       

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
          await  vm.GetSchoolEnrollmentsAsync();
            await vm.GetStatesAsync();
        }
    }
}
