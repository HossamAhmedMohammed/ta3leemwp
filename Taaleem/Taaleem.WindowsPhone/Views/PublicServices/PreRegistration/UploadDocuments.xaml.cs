﻿using Autofac.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Taaleem.ViewModels.PreRegistration;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.PublicServices.PreRegistration
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UploadDocuments : Page
    {
        CoreApplicationView view1;
        PreRegistrationViewModel vm;
        public UploadDocuments()
        {
            this.InitializeComponent();
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            view1 = CoreApplication.GetCurrentView();
            vm = (PreRegistrationViewModel)DataContext;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.GoBack();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            vm.FileElecResponse = new Models.PreRegistration.Response.ReceiveFileResponse();
            vm.FileWorkParentResponse = new Models.PreRegistration.Response.ReceiveFileResponse();
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        string currentUpload = "";
        bool setFlag = false;
        private void Image_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            Image image = sender as Image;
            if (setFlag == false)
            {
                FileOpenPicker filePicker = new FileOpenPicker();
                filePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                filePicker.ViewMode = PickerViewMode.Thumbnail;

                //// Filter to include a sample subset of file types
                filePicker.FileTypeFilter.Clear();
                filePicker.FileTypeFilter.Add(".pdf");
                filePicker.FileTypeFilter.Add(".png");
                filePicker.FileTypeFilter.Add(".jpeg");
                filePicker.FileTypeFilter.Add(".jpg");

                filePicker.PickSingleFileAndContinue();

                currentUpload = image.Name;
                this.view1.Activated += this.viewActivated;
            }
            else
            {
                image.Source = new BitmapImage(new Uri(
   "ms-appx:///Assets/Exceptions/ic_ab_upload.png", UriKind.Absolute));
                setFlag = false;
                if(image.Name == "ElecImage")
                {
                    vm.FileElecResponse = new Models.PreRegistration.Response.ReceiveFileResponse();
                }
                else if(image.Name == "ParentWorkImg")
                {
                    vm.FileWorkParentResponse = new Models.PreRegistration.Response.ReceiveFileResponse();
                }
            }
        }

        private async void viewActivated(CoreApplicationView sender, IActivatedEventArgs args1)
        {
            FileOpenPickerContinuationEventArgs args = args1 as FileOpenPickerContinuationEventArgs;
            if (args != null)
            {
                if (args.Files.Count == 0) return;
                view1.Activated -= viewActivated;
                StorageFile storageFile = args.Files[0];
                byte[] array = await GetBytesAsync(storageFile);
                string str = Convert.ToBase64String(array);
                if (currentUpload == "ParentWorkImg")
                {
                    vm.FileParameters.data = str;
                    vm.FileParameters.filetype = args.Files[0].DisplayType;
                    vm.FileParameters.locationId = vm.SelectedSchool.ID;
                    bool x = await ValidateImageSize(args.Files[0]);
                    if (x == true)
                    {
                        vm.FileWorkParentResponse = await vm.ReceiveFileAsync();
                        if(vm.FileWorkParentResponse != null)
                        ParentWorkImg.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));
                    }
                    else
                    {
                        await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", "حجم الملف اكبر من 4 ميجا");
                    }
                }
                else if (currentUpload == "ElecImage")
                {
                    vm.FileParameters.data = str;
                    vm.FileParameters.filetype = args.Files[0].DisplayType;
                    vm.FileParameters.locationId = vm.SelectedSchool.ID;
                    bool x = await ValidateImageSize(args.Files[0]);
                    if (x == true)
                    {
                        vm.FileElecResponse = await vm.ReceiveFileAsync();
                        if(vm.FileWorkParentResponse != null)
                        ElecImage.Source = new BitmapImage(new Uri("ms-appx:///Assets/Exceptions/ic_ab_cancel.png", UriKind.Absolute));

                    }
                    else
                    {
                        await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", "حجم الملف اكبر من 4 ميجا");
                    }
                }


                setFlag = true;
                currentUpload = "";
            }
        }

        private async Task<bool> ValidateImageSize(StorageFile storageFile)
        {
            var basicProperties = await storageFile.GetBasicPropertiesAsync();
            var length = basicProperties.Size;
            if (length < 4 * 1048576)
            {
                return true;
            }
            else return false;
        }

        public static async Task<byte[]> GetBytesAsync(StorageFile file)
        {
            byte[] fileBytes = null;
            if (file == null) return null;
            using (var stream = await file.OpenReadAsync())
            {
                fileBytes = new byte[stream.Size];
                using (var reader = new DataReader(stream))
                {
                    await reader.LoadAsync((uint)stream.Size);
                    reader.ReadBytes(fileBytes);
                }
            }
            return fileBytes;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(vm.FileWorkParentResponse.ReceiveFileResult) && !string.IsNullOrEmpty(vm.FileElecResponse.ReceiveFileResult))
            {
                Frame.Navigate(typeof(ConfirmInformation));
            }
            else
            {
                await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.PleaseUploadAllDocuments);
            }
        }
    }
}
