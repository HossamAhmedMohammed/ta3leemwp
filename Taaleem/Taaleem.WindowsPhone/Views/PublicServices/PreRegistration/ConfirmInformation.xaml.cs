﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.ViewModels.PreRegistration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.PublicServices.PreRegistration
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ConfirmInformation : Page
    {
        PreRegistrationViewModel vm;
        public ConfirmInformation()
        {
            this.InitializeComponent();
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            vm = (PreRegistrationViewModel)DataContext;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.GoBack();
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
           
            await vm.GetParentData();
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (vm.IsTransfer == false)
            {
                await vm.EnrollAsync();
            }
            else
            {
                await vm.TransferAsync();
            }
        }
    }
}
