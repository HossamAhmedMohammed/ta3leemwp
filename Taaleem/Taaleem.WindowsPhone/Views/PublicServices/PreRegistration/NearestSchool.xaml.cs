﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Models.PreRegistration.Response;
using Taaleem.ViewModels.PreRegistration;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.PublicServices.PreRegistration
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NearestSchool:Page
    {
        PreRegistrationViewModel vm;
        public NearestSchool()
        {
            this.InitializeComponent();
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            vm = (PreRegistrationViewModel)DataContext;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.GoBack();
        }
       
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
          
            if (vm.IsTransfer == false)
                await vm.GetNearestSchools();
            else
            {
                //if (vm.StudentData == null)
                //{
                    await vm.GetGradeLevelsAcync();
                    vm.StudentQID = vm.SelectedschoolEnrollments.StudentID;
                    vm.SelectedGradeLevel = vm.GradeLevels.Where(x => x.Description == vm.SelectedschoolEnrollments.GradeLevel).SingleOrDefault();
                    

                    await vm.GetSchoolsByStateAsync();
                    vm.StudentElecnum = vm.SelectedschoolEnrollments.ElectricityNumber;
                    vm.StudentData = new Searchstudentresult { ID = vm.SelectedschoolEnrollments.StudentID, Name = vm.SelectedschoolEnrollments.StudentName };
               // }
            }

        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;


            if (SMapControl != null)
            {
                try
                {
                    foreach (var child in SMapControl.Children)
                    {
                        DeepRemove(SMapControl.Children, (UIElement)child);
                    }
                    SMapControl.Children.Clear();

                    Grid parentGrid = VisualTreeHelper.GetParent(SMapControl) as Grid;

                    if (parentGrid != null)
                        parentGrid.Children.Remove(SMapControl);

                    GC.Collect(2);
                    GC.WaitForPendingFinalizers();
                    GC.Collect(2);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        public static void DeepRemove(IList<DependencyObject> UIElementLayer, UIElement uiElement)
        {
            ContentPresenter contentPresenter = VisualTreeHelper.GetParent(uiElement) as ContentPresenter;
            contentPresenter.Content = null;
            Canvas canvas = VisualTreeHelper.GetParent(contentPresenter) as Canvas;
            canvas.Children.Remove(contentPresenter);
            UIElementLayer.Remove(uiElement);
        }

        private void AddPushPin(Geopoint location, string title )
        {
            var pin = new YellowCustomPushPin(title);
            SMapControl.Children.Add(pin);
            var point = new Point(0.5, 1);
            MapControl.SetNormalizedAnchorPoint(pin, point);
            MapControl.SetLocation(pin, location);
        }
        
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            Rootresultttt item = (sender as Button).DataContext as Rootresultttt;
            vm.SelectedSchool = item;
            await ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("", ViewModels.ViewModelLocator.Resources.PleaseContactSchoolToCheckTransformation);
            if (vm.IsTransfer == false)
                Frame.Navigate(typeof(UploadDocuments));
            else
                Frame.Navigate(typeof(ConfirmInformation));
        }
        MapControl SMapControl;
        private async void MapControl_Loaded(object sender, RoutedEventArgs e)
        {
             SMapControl = sender as MapControl;
            Rootresultttt item = (sender as MapControl).DataContext as Rootresultttt;
           
            Geopoint       SchoolLocation = new Geopoint(new BasicGeoposition
            {
                Latitude = item.Latitude==null? 0.0 : item.Latitude.Value,
                Longitude = item.Longitude == null ? 0.0 : item.Longitude.Value
            });
        

            //    if (School.Longitude.HasValue && School.Latitude.HasValue)
            //    {
            //        YourLocation = new Geopoint(new BasicGeoposition
            //        {
            //            Latitude = School.Latitude.Value,
            //            Longitude = School.Longitude.Value
            //        });
            //    }
            //}
            AddPushPin(SchoolLocation, item.Name);
            //if (vm.YourLocation != null)
            //{
            //    AddPushPin(vm.YourLocation, ViewModelLocator.Resources.MyLocation);
            //}


            try
            {

                //Zoom on school location
                await SMapControl.TrySetViewAsync(SchoolLocation, 14, null, null, MapAnimationKind.Bow);

                ////Correct zoom to include both location
                //if (vm.YourLocation != null)
                //{
                //    //Calculate Boundries
                //    var locations = new List<BasicGeoposition>();
                //    locations.Add(vm.SchoolLocation.Position);
                //    locations.Add(vm.YourLocation.Position);
                //    var boundries = GeoboundingBox.TryCompute(locations);
                //    await SMapControl.TrySetViewBoundsAsync(boundries, new Thickness(100), MapAnimationKind.None);
                //}
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}
