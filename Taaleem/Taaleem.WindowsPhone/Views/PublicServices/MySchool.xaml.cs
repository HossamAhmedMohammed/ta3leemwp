﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Taaleem.Models.PublicServices;
using Taaleem.ViewModels;
using Taaleem.ViewModels.PublicServices;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.PublicServices
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MySchool 
    {
       // private SearchSchoolsViewModel _searchSchoolsViewModel;
        public MySchool()
        {
            this.InitializeComponent();
            //NavigationCacheMode = NavigationCacheMode.Enabled;
            mainMenu.IsOpened = false;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            NavigationCacheMode = NavigationCacheMode.Enabled;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if(e.NavigationMode == NavigationMode.Back && (Window.Current.Content as Frame).ForwardStack.Last().SourcePageType == typeof(SchoolDetails))
            {
                SchoolStage.refreshData();
                ((SearchSchoolsViewModel)DataContext).SchoolStagesList = SchoolStage.Stages;
                if (((SearchSchoolsViewModel)DataContext).SchoolStage != null)
                {
                    stages.ItemsSource = ((SearchSchoolsViewModel)DataContext).SchoolStagesList;
                    stages.SelectedIndex = ((SearchSchoolsViewModel)DataContext).SchoolStage.Id - 1;
                }
                if (((SearchSchoolsViewModel)DataContext).GenderType == GenderType.Female)
                {
                    ((SearchSchoolsViewModel)DataContext).GenderType = GenderType.Female;
                    Female.IsChecked = true;
                }
                if (((SearchSchoolsViewModel)DataContext).GenderType == GenderType.Male)
                {
                    ((SearchSchoolsViewModel)DataContext).GenderType = GenderType.Male;
                    Male.IsChecked = true;
                }
            }
            else
            {
                //if (((SchoolsViewModel)schoolsListView.DataContext).Schools != null)
                ///   ((SchoolsViewModel)schoolsListView.DataContext).SchoolsCView .Clear();
                //if (((SchoolsViewModel)schoolsListView.DataContext).Schools != null)
                //{
                // schoolsListView.ItemsSource = null;
                //((SchoolsViewModel)schoolsListView.DataContext).Schools.Clear();
                SchoolStage.refreshData();
                ((SchoolsViewModel)schoolsListView.DataContext).SchoolsCView.Source = null;
                   // schoolsListView.ItemsSource = ((SchoolsViewModel)schoolsListView.DataContext).SchoolsCView;
              //  }


            }
        }

        protected override void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            //if (NeedsRefresh)
            //{
            //    _searchSchoolsViewModel = ViewModelLocator.Locator.SearchSchoolsViewModel;
            //    DataContext = _searchSchoolsViewModel;
            //}
            //Type x = (Window.Current.Content as Frame).ForwardStack.Last().SourcePageType;
            //if (Frame.BackStack[Frame.BackStackDepth - 1].SourcePageType == typeof(SchoolDetails))
            //{
               
            //}
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            ToggleButton thisButton = sender as ToggleButton;
            if (thisButton.Name == "Male" || thisButton.Name == "ذكر")
                Female.IsChecked = false;
            else
                Male.IsChecked = false;

            var element = (FrameworkElement)sender;
            if (element.DataContext != null && element.DataContext is GenderType)
            {
                var type = (GenderType)element.DataContext;
                ((SearchSchoolsViewModel)DataContext).GenderType = type;
            }
        }
        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (((SearchSchoolsViewModel)DataContext).GenderType != null && ((SearchSchoolsViewModel)DataContext).SchoolStage != null)
            {
                SchoolsList.Visibility = Visibility.Visible;
                SchoolsViewModel tempViewModel = (SchoolsViewModel)SchoolsList.DataContext;
                tempViewModel.Parameters = new SearchSchoolNavParameters() { Type = GetDataTypeId(), Elect = ((SearchSchoolsViewModel)DataContext).ElecNumber };
                await tempViewModel.LoadSchoolsAsync();
            }
        }
        private int GetDataTypeId()
        {
            int genderId = ((SearchSchoolsViewModel)DataContext).GenderType.Id;
            int stageId = ((SearchSchoolsViewModel)DataContext).SchoolStage.Id;
            int data = 0;
            if (stageId == 1 && genderId == 1)
            {
                data = 7;
            }
            else if (stageId == 1 && genderId == 2)
            {
                data = 8;
            }
            else if (stageId == 2 && genderId == 1)
            {
                data = 1;
            }
            else if (stageId == 2 && genderId == 2)
            {
                data = 2;
            }
            else if (stageId == 3 && genderId == 1)
            {
                data = 3;
            }
            else if (stageId == 3 && genderId == 2)
            {
                data = 4;
            }
            else if (stageId == 4 && genderId == 1)
            {
                data = 5;
            }
            else if (stageId == 4 && genderId == 2)
            {
                data = 6;
            }
            return data;
        }
    }
}
