﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Globalization;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Settings 
    {
        public Settings()
        {
            this.InitializeComponent();
        //   HardwareButtons.BackPressed += HardwareButtons_BackPressed1;
        }

        private void HardwareButtons_BackPressed1(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.GoBack();
         
           
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            mainMenu.IsOpened = false;
            whiteGrid.FlowDirection = FlowDirection.LeftToRight;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            checkForLanguage();
            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = ViewModels.ViewModelLocator.Locator.MainMenuVM.Items.Where(x => x.Name == "الاعدادات").SingleOrDefault();
        }

        private void checkForLanguage()
        {
            if (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride=="en-US")
            {
                Eng.IsChecked = true;
                Ara.IsChecked = false;
                header.Text = "Change Language";
                pageTitle.Text = "Settings";
            }
            else if (Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride =="ar")
            {
                Ara.IsChecked = true;
                Eng.IsChecked = false;
                header.Text = "تغيير اللغة";
                pageTitle.Text = "الاعدادات";
            }
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;
            //  appBar.Visibility = Visibility.Visible;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;
            //appBar.Visibility = Visibility.Collapsed;
            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton temp = sender as ToggleButton;
            if(temp.Content.ToString() == "English")
            {
                //if(Ara.IsChecked == false)
                //{
              //  temp.IsChecked = true;
               // }
               
                //Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
                //var culture = new CultureInfo("en");
                //Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = culture.Name;
                ////CultureInfo.DefaultThreadCurrentCulture = culture;
                ////CultureInfo.DefaultThreadCurrentUICulture = culture;
                ////Windows.ApplicationModel.Resources.Core.ResourceContext.GetForViewIndependentUse().Reset();
                //Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();


                //(Window.Current.Content as Frame).FlowDirection = Windows.UI.Xaml.FlowDirection.LeftToRight;


                Frame rootFrame = Window.Current.Content as Frame;
                ((App)Application.Current).SetLanguage(rootFrame, "en-US");
                ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId = 1;
                ViewModels.ViewModelLocator.Locator.MainMenuVM.loadItems();

                Frame.Navigate(this.GetType());
                Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);
                Ara.IsChecked = false;
                Eng.IsChecked = true;
                header.Text = "Change Language";
                pageTitle.Text = "Settings";

            }
            else if (temp.Content.ToString() == "العربية")
            {
                //if (Eng.IsChecked == false)
                //{
                  //  Ara.IsChecked = true;
               // }
                Eng.IsChecked = false;
                header.Text = "تغيير اللغة";
                pageTitle.Text = "الاعدادات";
                //Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
                //var culture = new CultureInfo("ar");
                //Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = culture.Name;
                ////CultureInfo.DefaultThreadCurrentCulture = culture;
                ////CultureInfo.DefaultThreadCurrentUICulture = culture;

                ////Windows.ApplicationModel.Resources.Core.ResourceContext.GetForViewIndependentUse().Reset();
                //Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();

                ////Windows.ApplicationModel.Resources.Core.ResourceManager.Current.DefaultContext.Reset();
                //(Window.Current.Content as Frame).FlowDirection = Windows.UI.Xaml.FlowDirection.RightToLeft;
                Frame rootFrame = Window.Current.Content as Frame;
                ((App)Application.Current).SetLanguage(rootFrame, "ar");
                whiteGrid.FlowDirection = FlowDirection.LeftToRight;
                ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId = 2;
                ViewModels.ViewModelLocator.Locator.MainMenuVM.loadItems();
                Frame.Navigate(this.GetType());
                Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);

            }
        }

        private void SetLanguage(Frame rootFrame, string language)
        {
            //Set Frame Language
            rootFrame.Language = language;
            ResourceManager.Current.DefaultContext.QualifierValues.MapChanged += QualifierValues_MapChanged1;
            ResourceContext.GetForCurrentView().QualifierValues.MapChanged += QualifierValues_MapChanged;

            //Be Aware that this value is persisted between sessions
            ApplicationLanguages.PrimaryLanguageOverride = language;

            //On Windows 8.1 
            // ResourceContext.ResetGlobalQualifierValues();// need to be call to reset the language.
            //Check this link for more info https://timheuer.com/blog/archive/2013/03/26/howto-refresh-languages-winrt-xaml-windows-store.aspxC:\Users\Al-Shymaa\Desktop\new\Taaleem\Taaleem.WindowsPhone\Home.xaml


            var manager = Windows.ApplicationModel.Resources.Core.ResourceManager.Current;
            manager.DefaultContext.QualifierValues.MapChanged += QualifierValues_MapChanged;

            ////reset the context of the resource manager.
            //var resourceContext = ResourceContext.GetForCurrentView();
            //resourceContext.Reset();

            var resourceContext = ResourceContext.GetForCurrentView();
            resourceContext.QualifierValues.MapChanged += QualifierValues_MapChanged;


            //ResourceContext.ResetGlobalQualifierValues();// need to be call to reset the language.

            //Set Flow Direction
            FlowDirection flow = FlowDirection.LeftToRight;
            try
            {
                var loader = ResourceLoader.GetForCurrentView();
                var direction = loader.GetString("FlowDirection");
                Enum.TryParse<FlowDirection>(direction, out flow);
               
            }
            catch (Exception)
            {
                // ignored
            }
            rootFrame.FlowDirection = flow;
        }

        private void QualifierValues_MapChanged1(IObservableMap<string, string> sender, IMapChangedEventArgs<string> @event)
        {
            ResourceContext.GetForCurrentView().QualifierValues.MapChanged -= QualifierValues_MapChanged;
            // ResourceContext.ResetGlobalQualifierValues();
            ResourceManager.Current.DefaultContext.Reset();
        }

        private void QualifierValues_MapChanged(IObservableMap<string, string> sender, IMapChangedEventArgs<string> e)
        {
            ResourceContext.GetForCurrentView().QualifierValues.MapChanged -= QualifierValues_MapChanged;
             ResourceContext.ResetGlobalQualifierValues();
           // ResourceManager.Current.DefaultContext.Reset();
        }
    }
}
