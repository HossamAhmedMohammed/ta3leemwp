﻿using Taaleem.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Taaleem.ViewModels.Employees;
using Taaleem.Controls;
using Windows.Phone.UI.Input;
using Taaleem.Models.Employees.MyRequests;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Employee
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MyRequests
    {
        private MyRequestsViewModel _myRequestsViewModel;
        public MyRequests()
        {
            this.InitializeComponent();
            _myRequestsViewModel = (MyRequestsViewModel)DataContext;
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await ShowStatusBarAsync();
            base.OnNavigatedTo(e);
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }

        protected override async void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            await _myRequestsViewModel.LoadRequestTypesAsync();
            //}
        }
        protected override void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            base.HardwareButtons_BackPressed(sender, e);
            e.Handled = true;
            Frame.Navigate(typeof(EmployeeMenuPage));
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private async void RequesType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox requestTypes = sender as ComboBox;

            RequestTypesWrapper selectedType = requestTypes.SelectedValue as RequestTypesWrapper;
            if (selectedType != null)
            {
                _myRequestsViewModel.Data = null;
                _myRequestsViewModel.PageIndex = 0;
                switch ((MyRequestTypeEnum)selectedType.Id)
                {
                    case MyRequestTypeEnum.MyRequests:
                        break;
                    case MyRequestTypeEnum.LeaveRequest:
                        await _myRequestsViewModel.LeaveRequestAsync();
                        break;
                    case MyRequestTypeEnum.LeaveReturn:
                        await _myRequestsViewModel.LeaveReturnRequestAsync();
                        break;
                    case MyRequestTypeEnum.ExitPermit:
                        await _myRequestsViewModel.ExitPermitRequestAsync();
                        break;
                    case MyRequestTypeEnum.PersonalLoan:
                        await _myRequestsViewModel.PersonalLoanRequestAsync();
                        break;
                    case MyRequestTypeEnum.DocumentRequest:
                        await _myRequestsViewModel.DocumentRequestAsync();
                        break;
                    case MyRequestTypeEnum.OverTime:
                        await _myRequestsViewModel.OverTimeRequestAsync();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
