﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Taaleem.Models.Employees.MyRequests.Response;
using Taaleem.ViewModels.Employees;
using Taaleem.ViewModels.Employees.MyRequests;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Employee
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LeaveReturnRequest
    {
        private LeaveReturnRequestViewModel _vm;
        public LeaveReturnRequest()
        {
            this.InitializeComponent();
            _vm = (LeaveReturnRequestViewModel)DataContext;
        }
        protected override void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
        }
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await ShowStatusBarAsync();
            base.OnNavigatedTo(e);
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
            HardwareButtons.BackPressed += this.HardwareButtons_BackPressed;

            await _vm.LoadDataAsync();
            DataContext = _vm;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= this.HardwareButtons_BackPressed;
        }

        new void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.Navigate(typeof(Employee.EmployeeMenuPage));
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        private async void Next_Click(object sender, RoutedEventArgs e)
        {
            await _vm.NextAsync();
        }

        private async void Back_Click(object sender, RoutedEventArgs e)
        {
            await _vm.BackAsync();
        }

        private async void Submit_Click(object sender, RoutedEventArgs e)
        {
            var result = await _vm.SubmitAsync();
            if (result != null)
            {
                if (result.ResponseCode == "1")
                    this.Frame.Navigate(typeof(Views.Employee.EmployeeMenuPage));
            }
        }

        private void EmployeeApprovedLeavesSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            EmployeeApprovedLeave type = ((ComboBox)sender).SelectedItem as EmployeeApprovedLeave;
            _vm.SelectedEmployeeApprovedLeave = type;

        }

        private void RequestTypeSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LeaveType type = ((ComboBox)sender).SelectedItem as LeaveType;
            _vm.SelectedDocumentRequestType = type;
        }


        private async void SelectAttachment_Click(object sender, RoutedEventArgs e)
        {
            await _vm.SelectAttachmentAsync();
        }

        private void DeleteAttachment_Click(object sender, RoutedEventArgs e)
        {
            _vm.ClearAttachments();
        }

        private async void SearchEmployeeByJobId(object sender, RoutedEventArgs e)
        {
            var jobId = _vm.SearchOnBehalfQuery;
            if(jobId.Length == 5)
            {
                await _vm.SearchEmployee(jobId);
            }
        }
    }
}


