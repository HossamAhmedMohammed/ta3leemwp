﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Taaleem.Models.Employees;
using Taaleem.ViewModels.Employees;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Employee
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Vacations 
    {
        VacationsViewModel _vacationsViewModel;
        public Vacations()
        {
            this.InitializeComponent();
            _vacationsViewModel = (VacationsViewModel)DataContext;
        }
        #region Template Names
        private const string VacationItemTemplateName = "VacationTypeTemplate";
        private const string YearGroupingItemTemplateName = "YearGroupingItemTemplate";
        private const string VacationTypeHeaderTemplateName = "VacationTypeHeaderTemplate";
        private const string YearGroupingHeaderTemplateName = "YearGroupingHeaderTemplate";
        #endregion
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await ShowStatusBarAsync();
            base.OnNavigatedTo(e);
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }

        protected override void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            base.HardwareButtons_BackPressed(sender, e);
            e.Handled = true;
            Frame.Navigate(typeof(EmployeeMenuPage));
        }
        protected override async void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            await _vacationsViewModel.LoadVacationsAsync();
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;

            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;

            MenuBlackGrid.Visibility = Visibility.Visible;
        }
        private void RadioButton_Checked(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var element = (FrameworkElement)sender;
            if (element.DataContext != null && element.DataContext is VacationGroupType)
            {
                var type = (VacationGroupType)element.DataContext;
                ChangeTemplates(type);
                _vacationsViewModel.ChangeGouping(type);
            }
        }

        private void ChangeTemplates(VacationGroupType type)
        {
            if (type.Type == VacationGroupType.Vacation.Type)
            {
                this.yearBtn.IsChecked = false;
                VacationsListView.ItemTemplate = (DataTemplate)Resources[VacationItemTemplateName];
                VacationsListView.GroupStyle[0].HeaderTemplate = (DataTemplate)Resources[VacationTypeHeaderTemplateName];
            }
            else
            {
                this.typeBtn.IsChecked = false;
                VacationsListView.ItemTemplate = (DataTemplate)Resources[YearGroupingItemTemplateName];
                VacationsListView.GroupStyle[0].HeaderTemplate = (DataTemplate)Resources[YearGroupingHeaderTemplateName];
            }
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri("http://portal.sec.gov.qa/Services/Pages/LeaveRequest.aspx");
            await Launcher.LaunchUriAsync(uri);
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri("http://portal.sec.gov.qa/Services/Pages/LeaveReturn.aspx");
            await Launcher.LaunchUriAsync(uri);
        }
    }
}
