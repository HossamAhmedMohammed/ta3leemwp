﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Taaleem.ViewModels.Employees;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Employee
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Salaries 
    {
        SalariesViewModel _salarieViewModel;
        public Salaries()
        {
            this.InitializeComponent();
            _salarieViewModel = (SalariesViewModel)DataContext;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed1;
        }

        private void HardwareButtons_BackPressed1(object sender, BackPressedEventArgs e)
        {
            
            e.Handled = true;
            Frame.Navigate(typeof(EmployeeMenuPage));
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await ShowStatusBarAsync();
            base.OnNavigatedTo(e);
            mainMenu.IsOpened = false;
          
            MenuBlackGrid.Visibility = Visibility.Collapsed;
           
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed1;
        }
        protected override async void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            if (NeedsRefresh || sender == this /*Child Changed*/)
            {
            }
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;
          
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;
           
            MenuBlackGrid.Visibility = Visibility.Visible;
        }

        

        private void ShowThisMonthButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
          _salarieViewModel.AssignCurrentMonthDates();
           // ShowThisMonthButton.Visibility = Visibility.Collapsed;
        }
    }
}
