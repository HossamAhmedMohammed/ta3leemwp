﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Taaleem.Views.Employee
{
    public sealed partial class EllipseUserControl : UserControl
    {


        public double ProgressValue
        {
            get { return (double)GetValue(ProgressValueProperty); }
            set { SetValue(ProgressValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ProgressValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ProgressValueProperty =
            DependencyProperty.Register("ProgressValue", typeof(double), typeof(EllipseUserControl), new PropertyMetadata(0.0, OnProgressValueChanged));

        private static void OnProgressValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //throw new NotImplementedException();
            EllipseUserControl circularProgressBar = d as EllipseUserControl;
            if (circularProgressBar != null)
            {
                double r = 50;
                double x0 = 0;
                double y0 = 50;
                circularProgressBar.myArc.Size = new Size(50, 50);
                double angle = 1800 - (double)e.NewValue / 100 * 360;
                double radAngle = angle * (Math.PI / 180);
                double x = x0 + r * Math.Cos(radAngle);
                double y = y0 - r * Math.Sin(radAngle);

                if (circularProgressBar.myArc != null)
                {
                    circularProgressBar.myArc.IsLargeArc = ((double)e.NewValue >= 50);
                    circularProgressBar.myArc.Point = new Point(x, y);
                }
            }
        }

        public EllipseUserControl()
        {
            this.InitializeComponent();

        }
    }
}