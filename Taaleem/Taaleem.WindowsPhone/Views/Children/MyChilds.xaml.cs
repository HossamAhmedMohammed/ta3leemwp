﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Controls.Dialogs;
using Taaleem.ViewModels.Children;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Taaleem.Views.Children
{
    public sealed partial class MyChilds : UserControl, IDismissRequested<bool>
    {
        private readonly ChildrenSelectionViewModel _childrenSelectionViewModel;
        public MyChilds()
        {
            this.InitializeComponent();
            _childrenSelectionViewModel = (ChildrenSelectionViewModel)DataContext;
        }

        public event EventHandler<bool> DismissRequested;

        private void Children_Tapped(object sender, TappedRoutedEventArgs e)
        {
            OnDismissRequested(true);
            childName.Text = _childrenSelectionViewModel.SelectedChild.FirstName;
        }
        private void OnDismissRequested(bool index)
        {
            var handler = DismissRequested;
            if (handler != null) handler(this, index);
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            _childrenSelectionViewModel.NavigateToMessages();
        }

        private void Image_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            _childrenSelectionViewModel.NavigateToNotifications();
        }

        private void Image_Tapped_2(object sender, TappedRoutedEventArgs e)
        {
            _childrenSelectionViewModel.NavigateToReminders();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (ViewModels.ViewModelLocator.Locator.AppSettings.AmIParent)
            {
                ChildrenList.ItemsSource = _childrenSelectionViewModel.Children;
            }
            else
            {
                ChildrenList.ItemsSource = _childrenSelectionViewModel.ChildrenStudent;
            }
        }
    }
}
