﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Controls.Dialogs;
using Taaleem.ViewModels.Children;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Taaleem.Views.Children
{
    public sealed partial class ChildrenSelectionUserControl : UserControl, IDismissRequested<bool>
    {
        private readonly ChildrenSelectionViewModel _childrenSelectionViewModel;
        public ChildrenSelectionUserControl()
        {
            this.InitializeComponent();
            _childrenSelectionViewModel = (ChildrenSelectionViewModel)DataContext;
          
        }


        private void Children_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            OnDismissRequested(true);
        }
        public event EventHandler<bool> DismissRequested;

        private void OnDismissRequested(bool index)
        {
            var handler = DismissRequested;
            if (handler != null) handler(this, index);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (ViewModels.ViewModelLocator.Locator.AppSettings.AmIParent)
            {
                ChildrenList.ItemsSource = _childrenSelectionViewModel.Children;
            }
            else
            {
                ChildrenList.ItemsSource = _childrenSelectionViewModel.ChildrenStudent;
            }
        }

        //private void UserControl_Loaded(object sender, RoutedEventArgs e)
        //{
        //    _childrenSelectionViewModel.callLoadChildren();
        //}
    }
}
