﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Taaleem.Common;
using Taaleem.Controls.Lists;
using Taaleem.Models.Children;
using Taaleem.Models.Children.Response;
using Taaleem.ViewModels.Children;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Children
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Notifications
    {
        private const string ResultsStateName = "SearchResults";
        private const string CategoriesStateName = "SearchCategories";
        private NotificationsViewModel _notificationsViewModel;
        public Notifications()
        {
            InitializeComponent();
            _notificationsViewModel = (NotificationsViewModel)DataContext;
            LoadState(this, null);
        }


        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await ShowStatusBarAsync();
            base.OnNavigatedTo(e);
            mainMenu.IsOpened = false;
            appBar.Visibility = Visibility.Visible;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }

        protected override async void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);

            if (sender == this)
            {
                _notificationsViewModel = ViewModels.ViewModelLocator.Locator.NotificationsVM;
                DataContext = _notificationsViewModel;
                //means child changed, clear previous selected items and move privot to first section
                //NotificationsPivot.SelectedIndex = 0;
                //if (SearchCategoriesListView.SelectedItems != null)
                //    SearchCategoriesListView.SelectedItems.Clear();
            }
            await _notificationsViewModel.LoadFavouritesAsync();
            await _notificationsViewModel.LoadInitialFeedsAsync();
        }


        private async void CustomListView_OnLoadMoreRequested(object sender, CustomListView.LoadMoreEventArgs e)
        {
            await _notificationsViewModel.LoadMoreFeedsAsync();
            e.IsLoadingMore = false;
        }

        private void Border_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ICommand command = _notificationsViewModel.ToggleFavouriteCommand;
            var frameworkElement = sender as FrameworkElement;
            if (frameworkElement != null)
            {
                var feed = (frameworkElement.DataContext as Feed);
                if (feed != null)
                    command.Execute(feed);
            }
        }

        //private async void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (NotificationsPivot.SelectedIndex == 0) AppBar.Visibility = Visibility.Visible;
        //    else { AppBar.Visibility = Visibility.Collapsed; }

        //    if (NotificationsPivot.SelectedIndex == 2)
        //    {
        //        await _notificationsViewModel.LoadCategoriesAsync();
        //        VisualStateManager.GoToState(this, CategoriesStateName, false);
        //    }
        //}

        private async void MatchedFeedsList_OnLoadMoreRequested(object sender, CustomListView.LoadMoreEventArgs e)
        {
            await _notificationsViewModel.SearchMoreFeedsAsync();
            e.IsLoadingMore = false;
        }

        private void CustomListView_SelectionChanged(object sender, Windows.UI.Xaml.Controls.SelectionChangedEventArgs e)
        {
            foreach (var removedItem in e.RemovedItems)
            {
                _notificationsViewModel.SelectedFeedCategories.Remove((FeedCategoryWrapper)removedItem);
            }

            foreach (var addedItem in e.AddedItems)
            {
                _notificationsViewModel.SelectedFeedCategories.Add((FeedCategoryWrapper)addedItem);
            }
        }
        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;
            appBar.Visibility = Visibility.Visible;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;
            appBar.Visibility = Visibility.Collapsed;
            MenuBlackGrid.Visibility = Visibility.Visible;
        }
    }
}
