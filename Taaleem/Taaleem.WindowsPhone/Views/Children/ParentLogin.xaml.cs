﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.ViewModels.Children;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Taaleem.Views.Children
{
    public sealed partial class ParentLogin : UserControl
    {
        public ParentLogin()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ViewModels.ViewModelLocator.Locator.ParentLoginVM.ErrorMessage = null; // disable showing the message after close 
            ViewModels.ViewModelLocator.Locator.DialogManager.CloseDialog();
            ViewModels.ViewModelLocator.Locator.MainMenuVM.SelectedMenuItem = ViewModels.ViewModelLocator.Locator.MainMenuVM.CurrentMenuItem;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            usertxt.Text = "";
            passtxt.Password = "";
        }
    }
}
