﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Children
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RememberMe : Page
    {
        DispatcherTimer timer;
        public RememberMe()
        {
            this.InitializeComponent();
            timer = new DispatcherTimer();
            timer.Tick += Timer_Tick;
        }

        private void Timer_Tick(object sender, object e)
        {
            ViewModels.ViewModelLocator.Locator.ToastService.ShowToastThreeLines(NotificationName.Text);
            timer.Stop();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void DatePicker_DateChanged(object sender, DatePickerValueChangedEventArgs e)
        {
            if(timer.IsEnabled == true)
                timer.Stop();
            timer.Interval =TimeSpan.FromTicks(e.NewDate.Date.Ticks)+ TimeSpan.FromTicks(TimePicker.Time.Ticks);
           
           
        }

        private void Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(NotificationName.Text) && timer.Interval.TotalHours > 0.0000000000)
            {
                timer.Start();
                Frame.GoBack();
            }
            else
                ViewModels.ViewModelLocator.Locator.DialogManager.ShowMessage("Required", "Please insert notification title and set time for it");
        }

        private void Button_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
          if(timer.IsEnabled==true)
            {
                timer.Stop();
            }
        }

        private void TimePicker_TimeChanged(object sender, TimePickerValueChangedEventArgs e)
        {
            if (timer.IsEnabled == true)
                timer.Stop();
            timer.Interval =new TimeSpan(e.NewTime.Hours-DateTime.Now.Hour, e.NewTime.Minutes-DateTime.Now.Minute, e.NewTime.Seconds- DateTime.Now.Second);
        }

        private void NotificationName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void hourButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (timer.IsEnabled == true)
                timer.Stop();
            if (btn.Name == "hourButton")
            { 
                timer.Interval = new TimeSpan(1, 0, 0);
            }
            else if (btn.Name == "TwohoursButton")
            {
                timer.Interval = new TimeSpan(2, 0, 0);
            }
            else if (btn.Name == "ThreehoursButton")
            {
                timer.Interval = new TimeSpan(3, 0, 0);
            }
        }
    }
}
