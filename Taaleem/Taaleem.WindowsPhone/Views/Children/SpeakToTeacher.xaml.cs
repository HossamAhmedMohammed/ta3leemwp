﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Taaleem.Models.Children.Response;
using Taaleem.ViewModels.Children;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Children
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SpeakToTeacher 
    {
        private readonly SpeakToTeacherViewModel _speakToTeacherViewModel;
        public SpeakToTeacher()
        {
            this.InitializeComponent();
            _speakToTeacherViewModel = (SpeakToTeacherViewModel)DataContext;
        }

        protected async override void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            if (e.NavigationParameter != null)
                _speakToTeacherViewModel.ContactTeacher = (ContactTeacher)e.NavigationParameter;
            await _speakToTeacherViewModel.LoadPrivateNotesAsync();
        }
    }
}
