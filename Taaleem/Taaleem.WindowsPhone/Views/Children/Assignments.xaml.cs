﻿using Taaleem.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Taaleem.ViewModels.Children;
using Taaleem.Controls.Lists;
using Taaleem.ViewModels;
using Taaleem.Controls;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Children
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public partial class Assignments
    {
        private AssignmentsViewModel _assignmentsViewModel;
        private ExpanderControl _lastTappedControl;
        public Assignments()
        {
            this.InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Enabled;
            this.LoadState(this, null);
        }

        #region Loading & Navigation Section
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {

      
            await ShowStatusBarAsync();
            base.OnNavigatedTo(e);
            mainMenu.IsOpened = false;
            appBar.Visibility = Visibility.Visible;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        
        protected override async void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            if (NeedsRefresh || sender == this/*Child Changed*/)
            {
                _assignmentsViewModel = ViewModelLocator.Locator.AssignmentsViewModel;
                DataContext = _assignmentsViewModel;
                _lastTappedControl = null;
                await _assignmentsViewModel.LoadAssignmentsAsync();
               
            }
        }



        #endregion

       
        private void ExpanderControl_OnExpandingChanged(object sender, bool e)
        {
            var newControl = (ExpanderControl)sender;
            if (_lastTappedControl != null && newControl != _lastTappedControl)
            {
                _lastTappedControl.IsExpanded = false;
            }
            _lastTappedControl = newControl;
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
          
            mainMenu.IsOpened = true;
            appBar.Visibility = Visibility.Collapsed;
            MenuBlackGrid.Visibility = Visibility.Visible;

        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;
            appBar.Visibility = Visibility.Visible;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
    }
}

