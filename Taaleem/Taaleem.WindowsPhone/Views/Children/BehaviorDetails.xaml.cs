﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Taaleem.ViewModels.Children;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Children
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BehaviorDetails 
    {
        private readonly BehaviorDetailsViewModel _behaviorDetailsViewModel;
        public BehaviorDetails()
        {
            this.InitializeComponent();
            _behaviorDetailsViewModel = (BehaviorDetailsViewModel)DataContext;
        }

        protected override void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            if (NeedsRefresh && e.NavigationParameter != null)
            {
                _behaviorDetailsViewModel.Behavior = (Taaleem.Models.Children.Response.Behavior)e.NavigationParameter;
            }
        }
    }
}
