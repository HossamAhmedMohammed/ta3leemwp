﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Email;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Taaleem.Views.Children
{
    public sealed partial class TalkToTeacherUserControl : UserControl
    {
        public TalkToTeacherUserControl()
        {
            this.InitializeComponent();
        }

        private async void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            string uri = "whatsapp://send?text=Hello%20World!";
            await Launcher.LaunchUriAsync(new Uri(uri));
        }

        private async void Image_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            // Define Recipient
            EmailRecipient sendTo = new EmailRecipient()
            {
                Name = "Teacher Name",
                Address = "shaymaaa.montaser@gmail.com"
            };

            // Create email object
            EmailMessage mail = new EmailMessage();
            mail.Subject = "this is the Subject";
            mail.Body = "this is the Body";

            // Add recipients to the mail object
            mail.To.Add(sendTo);
            //mail.Bcc.Add(sendTo);
            //mail.CC.Add(sendTo);

            // Open the share contract with Mail only:
            await EmailManager.ShowComposeNewEmailAsync(mail);
        }

        private void Image_Tapped_2(object sender, TappedRoutedEventArgs e)
        {
            ViewModels.ViewModelLocator.Locator.NavigationService.NavigateByPage<SpeakToTeacher>();
        }

        private void StackPanel_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ViewModels.ViewModelLocator.Locator.NavigationService.NavigateByPage<RememberMe>();
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }
    }
}
