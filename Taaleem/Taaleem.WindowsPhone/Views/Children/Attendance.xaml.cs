﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Taaleem.ViewModels.Children;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Children
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Attendance 
    {
        private AttendancesViewModel _attendancesViewModel;

        public object Viewmodels { get; private set; }

        public Attendance()
        {
            this.InitializeComponent();
            _attendancesViewModel = (AttendancesViewModel)DataContext;
            NavigationCacheMode = NavigationCacheMode.Enabled;
            this.LoadState(this, null);
        }

        #region Loading & Navigation Section
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await ShowStatusBarAsync();
            base.OnNavigatedTo(e);
            mainMenu.IsOpened = false;
            appBar.Visibility = Visibility.Visible;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            if (await _attendancesViewModel.ShowThisWeekButtonVisibility() == true)
            { ShowThisWeekButton.Visibility = Visibility.Visible; } else { ShowThisWeekButton.Visibility = Visibility.Collapsed; }
           
        }

        protected override async void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            if (NeedsRefresh)
            {
                _attendancesViewModel =ViewModels.ViewModelLocator.Locator.AttendancesVM;
                DataContext = _attendancesViewModel;
              //  _attendancesViewModel.InitializeWeekDaysList();
                await _attendancesViewModel.LoadAttendancesAsync();
            }
        }
        #endregion

        private void StatusItems_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //Split widths in Status List equally
            //VariableSizedWrapGrid will wrap the remaining items to the next line 
            var itemsPanel = sender as VariableSizedWrapGrid;
            if (itemsPanel != null) itemsPanel.ItemWidth = e.NewSize.Width / itemsPanel.MaximumRowsOrColumns;
        }

        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            // Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;
            appBar.Visibility = Visibility.Collapsed;
            MenuBlackGrid.Visibility = Visibility.Visible;
        }
        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;
            appBar.Visibility = Visibility.Visible;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }

        private async void  button_Click(object sender, RoutedEventArgs e)
        {
            if (await _attendancesViewModel.ShowThisWeekButtonVisibility() == true)
            { ShowThisWeekButton.Visibility = Visibility.Visible; }
            else { ShowThisWeekButton.Visibility = Visibility.Collapsed; }
        }

        private void ShowThisWeekButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            _attendancesViewModel.AssignCurrentWeekDates();
            ShowThisWeekButton.Visibility = Visibility.Collapsed;
        }
    }
}
