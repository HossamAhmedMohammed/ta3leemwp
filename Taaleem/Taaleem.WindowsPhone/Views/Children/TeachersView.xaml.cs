﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Taaleem.Common;
using Taaleem.Models.Children.Response;
using Taaleem.ViewModels.Children;
using Windows.ApplicationModel.Email;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Children
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TeachersView
    { 
        private TeachersViewModel _teachersViewModel;

    public TeachersView()
    {
        InitializeComponent();
        NavigationCacheMode = NavigationCacheMode.Enabled;
    }

    #region Loading & Navigation Section

    protected override async void OnNavigatedTo(NavigationEventArgs e)
    {
        await ShowStatusBarAsync();
        base.OnNavigatedTo(e);
        mainMenu.IsOpened = false;
        appBar.Visibility = Visibility.Visible;
        MenuBlackGrid.Visibility = Visibility.Collapsed;
    }

    protected override async void LoadState(object sender, LoadStateEventArgs e)
    {
        base.LoadState(sender, e);
        if (NeedsRefresh || sender == this/*Child Changed*/)
        {
            _teachersViewModel = ViewModels.ViewModelLocator.Locator.TeachersViewModel;
            DataContext = _teachersViewModel;
            await _teachersViewModel.LoadTeachersAsync();
        }
    }
    private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
    {
        mainMenu.IsOpened = false;
        appBar.Visibility = Visibility.Visible;
        MenuBlackGrid.Visibility = Visibility.Collapsed;
    }
    private void Image_Tapped(object sender, TappedRoutedEventArgs e)
    {
        //Frame.Navigate(typeof(ParentMenuPage));
        mainMenu.IsOpened = true;
        appBar.Visibility = Visibility.Collapsed;
        MenuBlackGrid.Visibility = Visibility.Visible;
    }
        #endregion

        private void Image_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            ContactTeacher item = (sender as Image).DataContext as ContactTeacher;
         
            ICommand command = _teachersViewModel.ItemClickedCommand;
            command.Execute(item);
        }

        private async void Image_Tapped_2(object sender, TappedRoutedEventArgs e)
        {
            ContactTeacher item = (sender as Image).DataContext as ContactTeacher;
            // Define Recipient
            EmailRecipient sendTo = new EmailRecipient ()
            {
                Name = item.Teacher.DisplayName,
                Address = item.Teacher.Email
            };

            // Create email object
            EmailMessage mail = new EmailMessage();

            mail.To.Add(sendTo);
            // Open the share contract with Mail only:
            await EmailManager.ShowComposeNewEmailAsync(mail);
        }
    }
}
