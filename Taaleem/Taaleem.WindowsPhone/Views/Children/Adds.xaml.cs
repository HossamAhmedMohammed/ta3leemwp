﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Taaleem.ViewModels;
using Taaleem.ViewModels.Children;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem.Views.Children
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Adds
    {
        private AnnouncementsViewModel _announcementsViewModel;
        public Adds()
        {
            this.InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Enabled;
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await ShowStatusBarAsync();
            base.OnNavigatedTo(e);
            mainMenu.IsOpened = false;
            appBar.Visibility = Visibility.Visible;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }

        protected override async void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            if (NeedsRefresh)
            {
                _announcementsViewModel = ViewModelLocator.Locator.AnnouncementsViewModel;
                DataContext = _announcementsViewModel;
                await _announcementsViewModel.LoadAnnouncementsAsync();
            }
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;
            appBar.Visibility = Visibility.Visible;
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;
            appBar.Visibility = Visibility.Collapsed;
            MenuBlackGrid.Visibility = Visibility.Visible;
        }

    }
}

