﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Taaleem.Views
{
    public sealed partial class MainMenu : UserControl
    {
        public MainMenu()
        {
            this.InitializeComponent();
            DrawerLayout.InitializeDrawerLayout();
            DrawerLayout.OpenDrawer();
           
        }

       

           
private void ListMenuItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
          
            //switch (obj.Name)
            //{
            //    case "الرئيسيه":
            //        // _navigationService.NavigateByPage<>();
            //        break;
            //    case "خدمات الاباء":
            //        F<ParentMenuPage>();
            //        break;
            //    case "خدمات الموظف":
            //        // _navigationService.NavigateByPage<>();
            //        break;
            //    case "خدمات المعلمين":
            //        // _navigationService.NavigateByPage<>();
            //        break;
            //    case "خدمات عامه":
            //        // _navigationService.NavigateByPage<>();
            //        break;
            //    case "الاخبار":
            //        // _navigationService.NavigateByPage<>();
            //        break;
            //    case "عن المجلس":
            //        // _navigationService.NavigateByPage<>();
            //        break;
            //}
    }


    public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty) ; }
            set { SetValueDp(IsOpenedProperty , value);
                if (value == true)
                {
                    this.Visibility = Visibility.Visible;
                    DrawerLayout.OpenDrawer();
                   
                }
                else
                {
                    DrawerLayout.CloseDrawer();
                    this.Visibility = Visibility.Collapsed;
                }
            }
        }

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(MainMenu), null);

        //reuse
        public event PropertyChangedEventHandler PropertyChanged;

        void SetValueDp(DependencyProperty property, object value, [System.Runtime.CompilerServices.CallerMemberName] String p = null)
        {
            SetValue(property, value);
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(p));
        }

        private void ListFragment_Tapped(object sender, TappedRoutedEventArgs e)
        {
           

        }

        private void StackPanel_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //ICommand command = ViewModels.ViewModelLocator.Locator.MainMenuVM.ItemClickedCommand;
          //  command.Execute(null);
        }
    }
}
