﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Common;
using Taaleem.Views;
using Taaleem.Views.Children;
using Taaleem.Views.PublicServices;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Taaleem
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Enabled;

        }

       
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await ShowStatusBarAsync();
            base.OnNavigatedTo(e);
            mainMenu.IsOpened = false;
          
            MenuBlackGrid.Visibility = Visibility.Collapsed;
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            Windows.Phone.UI.Input.HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }
        protected override async void LoadState(object sender, LoadStateEventArgs e)
        {
            base.LoadState(sender, e);
            if (NeedsRefresh || sender == this /*Child Changed*/)
            {
                ////_behaviorsViewModel = ViewModels.ViewModelLocator.Locator.BehaviorVM;
                ////DataContext = _behaviorsViewModel;
                ////await _behaviorsViewModel.LoadBehaviorsAsync();
            }
        }

        private void LayoutRoot_Tapped(object sender, TappedRoutedEventArgs e)
        {
            mainMenu.IsOpened = false;
           
            MenuBlackGrid.Visibility = Visibility.Collapsed;
        }
        private void Image_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //Frame.Navigate(typeof(ParentMenuPage));
            mainMenu.IsOpened = true;
         
            MenuBlackGrid.Visibility = Visibility.Visible;
        }

      
      


      
           
        void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            e.Handled = true;
            Frame.Navigate(typeof(Splash));
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await ViewModels.ViewModelLocator.Locator.DialogManager.ShowParentLoginDialog();
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            await ViewModels.ViewModelLocator.Locator.DialogManager.ShowEmployeeLoginDialog();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ViewModels.ViewModelLocator.Locator.NavigationService.NavigateByPage<PublicServicesMenuPage>();
        }
    }
}
