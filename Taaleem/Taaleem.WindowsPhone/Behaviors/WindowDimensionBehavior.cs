﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Xaml.Interactivity;

namespace Taaleem.Behaviors
{
    //snippet from https://blogs.windows.com/buildingapps/2014/07/22/tips-and-tricks-for-using-xaml-controls-in-your-universal-windows-apps/
    [TypeConstraint(typeof(FrameworkElement))]
    class WindowDimensionBehavior : DependencyObject, IBehavior
    {
        public WindowDimensionBehavior ()
        {
            this.WidthMultiple = 1;
            this.HeightMultiple = 1;
            this.HeightPercentage = double.NaN;
            this.WidthPercentage = double.NaN;
        }
        public DependencyObject AssociatedObject { get; set; }

        public FrameworkElement TypedObject { get; set; }
        public int WidthMultiple { get; set; }

        public double WidthPercentage { get; set; }

        public int HeightMultiple { get; set; }

        public double HeightPercentage { get; set; }

        public void Attach ( DependencyObject associatedObject )
        {
            this.AssociatedObject = this.TypedObject = associatedObject as FrameworkElement;

            var frameworkElement = this.TypedObject;
            if(frameworkElement != null) frameworkElement.Loaded += TypedObject_Loaded;
            Window.Current.SizeChanged += Current_SizeChanged;

        }

        void TypedObject_Loaded ( object sender, RoutedEventArgs e )
        {
            Handle();
        }

        void Current_SizeChanged ( object sender, Windows.UI.Core.WindowSizeChangedEventArgs e )
        {
            Handle();
        }

        private void Handle ()
        {
            var frameWorkElement = (this.TypedObject as FrameworkElement);

            //I base all the percentage calculations on shortest dimension, you can modify this depending on your layouts requirements.
            double shortestDimension = (Window.Current.Bounds.Width <= Window.Current.Bounds.Height) ?
                            Window.Current.Bounds.Width : Window.Current.Bounds.Height;

            if(frameWorkElement != null)
            {
                if(!double.IsNaN(this.WidthPercentage))
                    frameWorkElement.Width = shortestDimension * this.WidthPercentage * this.WidthMultiple;
                if(!double.IsNaN(this.HeightPercentage))
                    frameWorkElement.Height = shortestDimension * this.HeightPercentage * this.HeightMultiple;
            }
        }

        public void Detach ()
        {
            Window.Current.SizeChanged -= Current_SizeChanged;
            var control = this.AssociatedObject as Control;
            if(control != null)
                control.Loaded -= TypedObject_Loaded;
        }
    }
}

