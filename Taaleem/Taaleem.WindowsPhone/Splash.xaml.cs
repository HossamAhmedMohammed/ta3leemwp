﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Taaleem.Views.Children;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.ApplicationModel.Activation;
using Windows.UI.Core;
using Taaleem.Common;
using System.Threading.Tasks;
using Windows.Phone.UI.Input;
using Taaleem.ViewModels;
using Taaleem.Helpers;
using Taaleem.Views.PublicServices;
using Windows.ApplicationModel.Resources;
using Windows.Globalization;
using Windows.ApplicationModel.Resources.Core;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Taaleem
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Splash : Page
    {
        internal Rect splashImageRect; // Rect to store splash screen image coordinates.
        private SplashScreen splash; // Variable to hold the splash screen object.
        internal bool dismissed = false; // Variable to track splash screen dismissal status.
        internal Frame rootFrame;

        public Splash()//SplashScreen splashscreen, bool loadState)
        {

            InitializeComponent();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().QualifierValues.MapChanged += QualifierValues_MapChanged;
        }

        private void QualifierValues_MapChanged(IObservableMap<string, string> sender, IMapChangedEventArgs<string> @event)
        {
            try
            {
                Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
            }
            catch
            {
               // Debug.WriteLine("Crach on start here");
            }
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Application.Current.Exit();
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;

        }
        

        // Include code to be executed when the system has transitioned from the splash screen to the extended splash screen (application's first view).
        void DismissedEventHandler(SplashScreen sender, object e)
        {
            dismissed = true;

            // Complete app setup operations here...
        }
       
        void DismissExtendedSplash()
        {
            // Navigate to Home
           // rootFrame.Navigate(typeof(Home));
            // Place the frame in the current Window
            Window.Current.Content = rootFrame;
        }
        void ExtendedSplash_OnResize(Object sender, WindowSizeChangedEventArgs e)
        {
            // Safely update the extended splash screen image coordinates. This function will be executed when a user resizes the window.
            if (splash != null)
            {
               // Update the coordinates of the splash screen image.
                  splashImageRect = splash.ImageLocation;
               // PositionImage();

              //  If applicable, include a method for positioning a progress control.
               // PositionRing();
            }
        }

        async void RestoreStateAsync(bool loadState)
        {
            if (loadState)
                await SuspensionManager.RestoreAsync();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            //  await Task.Delay(5000);
            try {
                await ViewModelLocator.Locator.DatabaseManager.CreateOrMigrateDatabaseAsync(DatabaseManager.DbPath);
            }
            catch (Exception ex)
            {

            }
        }


       
        private  void Button_Click(object sender, RoutedEventArgs e)
        {

            Frame rootFrame = Window.Current.Content as Frame;
            ((App)Application.Current).SetLanguage(rootFrame,"ar");
            //  SetLanguage(rootFrame, "ar");
            //// produce exception 
            //try {
            //    Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
            //    var culture = new CultureInfo("ar");
            //    Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = culture.Name;
            //    // await Task.Delay(100);
            //    ////CultureInfo.DefaultThreadCurrentCulture = culture;
            //    ////CultureInfo.DefaultThreadCurrentUICulture = culture;
            //    //// Windows.ApplicationModel.Resources.Core.ResourceContext.GetForViewIndependentUse().Reset();

            //    Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
            //    //// Windows.ApplicationModel.Resources.Core.ResourceContext.GetForViewIndependentUse().Reset();
            //    ////  Windows.ApplicationModel.Resources.Core.ResourceManager.Current.DefaultContext.Reset();
            //    (Window.Current.Content as Frame).FlowDirection = Windows.UI.Xaml.FlowDirection.RightToLeft;
            ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId = 2;
                ViewModels.ViewModelLocator.Locator.MainMenuVM.loadItems();
            //  //  await Task.Delay(10);
            //    //await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            //    //{
            //    //    if (!Frame.Navigate(typeof(Home)))
            //    //    {
            //    //        throw new Exception("NavigationFailedExceptionMessage");
            //    //    }
            //    //});
            //    //   await Task.Delay(100);
            //    //To refresh the UI without restart the phone

              Frame.Navigate(typeof(Home));
            //}
            //catch (System.ArgumentException)
            //{

            //}
            //catch (Exception)
            //{

            //}

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            ((App)Application.Current).SetLanguage(rootFrame,"en-US");
         
           // SetLanguage(rootFrame, "en");
            //try
            //{
            //    Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
            //    var culture = new CultureInfo("en");
            //    Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = culture.Name;
            //    // await Task.Delay(100);
            //    ////CultureInfo.DefaultThreadCurrentCulture = culture;
            //    ////CultureInfo.DefaultThreadCurrentUICulture = culture;
            //    //// Windows.ApplicationModel.Resources.Core.ResourceContext.GetForViewIndependentUse().Reset();

            //    Windows.ApplicationModel.Resources.Core.ResourceContext.GetForCurrentView().Reset();
            //    //// Windows.ApplicationModel.Resources.Core.ResourceContext.GetForViewIndependentUse().Reset();
            //    ////  Windows.ApplicationModel.Resources.Core.ResourceManager.Current.DefaultContext.Reset();
            //    (Window.Current.Content as Frame).FlowDirection = Windows.UI.Xaml.FlowDirection.LeftToRight;
                ViewModels.ViewModelLocator.Locator.AppSettings.LanguageId = 1;
                ViewModels.ViewModelLocator.Locator.MainMenuVM.loadItems();
            //    //  await Task.Delay(10);
            //    //await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            //    //{
            //    //    if (!Frame.Navigate(typeof(Home)))
            //    //    {
            //    //        throw new Exception("NavigationFailedExceptionMessage");
            //    //    }
            //    //});
            //    //   await Task.Delay(100);
            //    //To refresh the UI without restart the phone

              Frame.Navigate(typeof(Home));
            //}
            //catch (System.ArgumentException)
            //{

            //}
            //catch (Exception)
            //{

            //}
        }

             private  void SetLanguage(Frame rootFrame, string language)
        {
            //Set Frame Language
            rootFrame.Language = language;


            //Be Aware that this value is persisted between sessions
            ApplicationLanguages.PrimaryLanguageOverride = language;

            //On Windows 8.1 
            try {

               
               // ResourceContext.ResetGlobalQualifierValues();// need to be call to reset the language.
                                                             //Check this link for more info https://timheuer.com/blog/archive/2013/03/26/howto-refresh-languages-winrt-xaml-windows-store.aspx
            }
            catch(Exception e)
            {
                
            }


            ////reset the context of the resource manager.
            //var resourceContext = ResourceContext.GetForCurrentView();
            //resourceContext.Reset();

            //ResourceContext.ResetGlobalQualifierValues();// need to be call to reset the language.

            //Set Flow Direction
            FlowDirection flow = FlowDirection.LeftToRight;
            try
            {
                var loader = ResourceLoader.GetForCurrentView();
                var direction = loader.GetString("FlowDirection");
                Enum.TryParse<FlowDirection>(direction, out flow);
            }
            catch (Exception)
            {
                // ignored
            }
            rootFrame.FlowDirection = flow;
        }



    
        
    }
}
